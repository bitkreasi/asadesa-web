'use client'

import { BASE_URL, VERSION } from '@/services'
import axios from 'axios'
import { useEffect, useState } from 'react'

type roleI = {
  name: string
  id: string
}

export const useRoles = () => {
  const [data, setData] = useState<roleI[]>([])
  const [loading, setLoading] = useState<boolean>(true)
  const [error, setError] = useState(null)

  useEffect(() => {
    axios
      .get(BASE_URL + VERSION + '/roles')
      .then((response) => {
        setData(response.data.data)
        setLoading(false)
      })
      .catch((error) => {
        setError(error)
        setLoading(false)
      })
  }, [])

  return { data, loading, error }
}
