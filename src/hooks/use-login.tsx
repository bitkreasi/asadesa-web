import { authService } from '@/services'
import { LoginResponse } from '@/services/auth'
import { cookies } from 'next/headers'

type Payload = {
  email: string
  password: string
}

export const useLogin = () => {
  const login = async (payload: Payload) => {
    const res = await authService.login(payload.email, payload.password)
    return res
  }

  return { login }
}
