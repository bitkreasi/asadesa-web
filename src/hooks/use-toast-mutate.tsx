import { toast } from "@/app/ui/components/toast/toast";
import React from "react";

interface ToastOptions {
  success?: string;
  error?: string;
}

const useToastMutate = (options?: ToastOptions) => {
  const [isLoading, setIsLoading] = React.useState<boolean>(false);

  const mutate = (mutateFn: Promise<{ error: string } | undefined>) => {
    setIsLoading(true);
    mutateFn
      .then((res) => {
        if (res?.error) {
          toast.error(options?.error ? options.error : res?.error);
        } else {
          toast.success(options?.success ? options.success : "Berhasil");
        }
      })
      .catch(() => {
        toast.error("Terjadi kesalahan");
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  return { isLoading, mutate };
};

export { useToastMutate, type ToastOptions };
