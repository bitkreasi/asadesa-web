import { BASE_URL, VERSION } from '@/services'
import axios from 'axios'
import { useEffect, useState } from 'react'

type Province = {
  id: 'string'
  code: 'string'
  name: 'string'
}

type Regency = {
  id: 'string'
  province_code: 'string'
  code: 'string'
  name: 'string'
}

type District = {
  id: 'string'
  regency_code: 'string'
  code: 'string'
  name: 'string'
}

type Village = {
  id: 'string'
  regency_code: 'string'
  code: 'string'
  name: 'string'
}

const URL = BASE_URL + VERSION + '/region'

export const useRegion = () => {
  const [provinces, setProvinces] = useState<Province[]>([])
  const [regencies, setRegencies] = useState<Regency[]>([])
  const [districts, setDistricts] = useState<District[]>([])
  const [villages, setVillages] = useState<Village[]>([])
  const [idProvince, setIdProvince] = useState<string>('')
  const [idRegency, setIdRegency] = useState<string>('')
  const [idDistrict, setIdDistrict] = useState<string>('')
  const [provinceLoading, setProvinceLoading] = useState<boolean>(true)
  const [regencyLoading, setRegencyLoading] = useState<boolean>(true)
  const [districtLoading, setDistrictLoading] = useState<boolean>(true)
  const [villageLoading, setVillageLoading] = useState<boolean>(true)

  useEffect(() => {
    axios
      .get(URL + '/provinces')
      .then((response) => {
        setProvinces(response.data)
        setProvinceLoading(false)
      })
      .catch((error) => {
        setProvinceLoading(false)
      })
  }, [])

  useEffect(() => {
    if (idProvince == '') return
    axios
      .get(URL + '/regencies?province_code=' + idProvince)
      .then((response) => {
        setRegencies(response.data)
        setRegencyLoading(false)
      })
      .catch((error) => {
        setRegencyLoading(false)
      })
  }, [idProvince])

  useEffect(() => {
    if (idRegency == '') return
    axios
      .get(URL + '/districts?regency_code=' + idRegency)
      .then((response) => {
        setDistricts(response.data)
        setDistrictLoading(false)
      })
      .catch((error) => {
        setDistrictLoading(false)
      })
  }, [idRegency])

  useEffect(() => {
    if (idDistrict == '') return
    axios
      .get(URL + '/villages?district_code=' + idDistrict)
      .then((response) => {
        setVillages(response.data)
        setVillageLoading(false)
      })
      .catch((error) => {
        setVillageLoading(false)
      })
  }, [idDistrict])

  return {
    provinces,
    regencies,
    districts,
    villages,
    setIdProvince,
    setIdRegency,
    setIdDistrict,
    provinceLoading,
    regencyLoading,
    districtLoading,
    villageLoading,
  }
}
