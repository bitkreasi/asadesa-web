import { authService } from '@/services'
import { RegisterPayload } from '@/services/auth'

export const useRegister = () => {
  const registerWithEmail = async (payload: RegisterPayload) => {
    const res = await authService.register(payload)
    return res
  }

  return { registerWithEmail }
}
