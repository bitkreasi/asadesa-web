import { villageService } from '@/services'
import { useEffect, useState } from 'react'

export const useVillageInfo = (id: string) => {
  const [data, setData] = useState<any>({})
  const [loading, setLoading] = useState<boolean>(true)
  useEffect(() => {
    async function getData(id: string) {
      const res = await villageService.getInstitutions(id)
      setLoading(false)
      setData(res.data.data)
    }
    setLoading(true)
    getData(id)
  }, [])

  return { data, loading }
}
