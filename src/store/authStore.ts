import { create } from 'zustand'

export type User = {
  id: string
  fullname: string
  email: string
  role: string
  institution_id: string
}

interface AuthState {
  user: User | null
  isLoading: boolean
  addUser: (payload: User) => void
  removeUser: () => void
  toggleLoading: (value: boolean) => void
}

export const useAuthStore = create<AuthState>((set) => ({
  user: null,
  isLoading: true,
  addUser: (payload) => {
    set(() => ({
      user: payload,
    }))
  },
  removeUser: () => {
    set(() => ({
      user: null,
    }))
  },
  toggleLoading: (value) => {
    set(() => ({
      isLoading: value,
    }))
  },
}))
