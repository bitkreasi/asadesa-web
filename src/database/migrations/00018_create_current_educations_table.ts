import { Kysely, sql } from 'kysely';

const tableName = 'current_educations';

export async function up(db: Kysely<unknown>) {
  await db.schema
    .createTable(tableName)
    .addColumn('id', 'uuid', (col) =>
      col.primaryKey().defaultTo(sql`gen_random_uuid()`)
    )
    .addColumn('name', 'varchar', (col) => col.notNull())
    .execute();
}
