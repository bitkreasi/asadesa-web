import { Kysely, sql } from 'kysely';

const tableName = 'institutions';

export async function up(db: Kysely<unknown>): Promise<void> {
  await db.schema
    .createTable(tableName)
    .addColumn('id', 'uuid', (col) =>
      col.primaryKey().defaultTo(sql`gen_random_uuid()`)
    )
    .addColumn('province_id', 'uuid', (col) => col.references('provinces.id').notNull())
    .addColumn('regency_id', 'uuid', (col) => col.references('regencies.id').notNull())
    .addColumn('district_id', 'uuid', (col) => col.references('districts.id').notNull())
    .addColumn('village_id', 'uuid', (col) => col.references('villages.id').notNull())
    .addColumn('postal_code', 'varchar')
    .addColumn('address', 'varchar')
    .addColumn('email', 'varchar')
    .addColumn('phone', 'varchar')
    .addColumn('cell_phone', 'varchar')
    .addColumn('website', 'varchar')
    .addColumn('chief_village_name', 'varchar')
    .addColumn('chief_village_nik', 'varchar')
    .addColumn('chief_district_name', 'varchar')
    .addColumn('chief_district_nik', 'varchar')
    
    // analytical columns
    .addColumn('created_at', 'bigint', (col) =>
      col.defaultTo(sql`EXTRACT(EPOCH FROM NOW()) * 1000`).notNull()
    )
    .addColumn('updated_at', 'bigint', (col) =>
      col.defaultTo(sql`EXTRACT(EPOCH FROM NOW()) * 1000`).notNull()
    )
    .addColumn('deleted_at', 'bigint')
    .addColumn('created_by', 'uuid')
    .addColumn('updated_by', 'uuid')
    .addColumn('deleted_by', 'uuid')
    .execute();
}

export async function down(db: Kysely<unknown>): Promise<void> {
  await db.schema.dropTable(tableName).execute();
}
