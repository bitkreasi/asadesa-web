import { Kysely, sql } from 'kysely';

const tableName = 'residents';

export async function up(db: Kysely<unknown>): Promise<void> {
  await db.schema
    .createTable(tableName)
    .addColumn('id', 'uuid', (col) =>
      col.primaryKey().defaultTo(sql`gen_random_uuid()`)
    )

    .addColumn('institution_id', 'uuid', (col) =>
      col.references('institutions.id').notNull()
    )

    .addColumn('report_date', 'date')
    .addColumn('resident_type', 'varchar')
    .addColumn('entry_date', 'date')

    .addColumn('fullname', 'varchar')
    .addColumn('nik', 'varchar')
    .addColumn('family_role_id', 'uuid', (col) =>
      col.references('family_roles.id')
    )
    .addColumn('mandatory_identity', 'varchar')
    .addColumn('electronic_identity_id', 'uuid', (col) =>
      col.references('electronic_identities.id')
    )
    .addColumn('record_status_id', 'uuid', (col) =>
      col.references('record_status.id')
    )
    .addColumn('tag_card', 'varchar')
    .addColumn('prev_nomor_kk', 'varchar')
    .addColumn('gender', 'varchar')
    .addColumn('religion_id', 'uuid', (col) => col.references('religions.id'))
    .addColumn('resident_status', 'varchar')

    .addColumn('birth_certificate', 'varchar', (col) => col.unique())
    .addColumn('birth_city_id', 'uuid', (col) => col.references('regencies.id'))
    .addColumn('birth_date', 'date')
    .addColumn('birth_time', 'time')
    .addColumn('birth_place_id', 'uuid', (col) =>
      col.references('birth_places.id')
    )
    .addColumn('birth_type_id', 'uuid', (col) =>
      col.references('birth_types.id')
    )
    .addColumn('child_of', 'int4')
    .addColumn('healthcare_worker_id', 'uuid', (col) =>
      col.references('healthcare_workers.id')
    )
    .addColumn('birth_weight', 'float4')
    .addColumn('birth_length', 'float4')

    .addColumn('education_kk_id', 'uuid', (col) =>
      col.references('education_kk.id')
    )
    .addColumn('current_education_id', 'uuid', (col) =>
      col.references('current_educations.id')
    )
    .addColumn('job_id', 'uuid', (col) => col.references('jobs.id'))

    .addColumn('ethnic_id', 'uuid', (col) => col.references('ethnics.id'))
    .addColumn('citizen_status', 'varchar')
    .addColumn('passport_number', 'varchar')
    .addColumn('passport_exp_date', 'date')

    .addColumn('father_fullname', 'varchar')
    .addColumn('father_nik', 'varchar')
    .addColumn('mother_fullname', 'varchar')
    .addColumn('mother_nik', 'varchar')

    .addColumn('address_village_id', 'uuid', (col) =>
      col.references('villages.id')
    )
    .addColumn('rt', 'varchar')
    .addColumn('rw', 'varchar')
    .addColumn('phone_number', 'varchar')
    .addColumn('email', 'varchar')

    .addColumn('marriage_status', 'varchar')
    .addColumn('marriage_certificate', 'varchar')
    .addColumn('marriage_date', 'date')
    .addColumn('divorce_certificate', 'varchar')
    .addColumn('divorce_date', 'date')

    .addColumn('blood_group_id', 'uuid', (col) =>
      col.references('blood_groups.id')
    )
    .addColumn('disability_id', 'uuid', (col) =>
      col.references('disabilities.id')
    )
    .addColumn('chronic_pain_id', 'uuid', (col) =>
      col.references('chronic_pains.id')
    )
    .addColumn('kb_acceptor_id', 'uuid', (col) =>
      col.references('kb_acceptors.id')
    )
    .addColumn('insurance_id', 'uuid', (col) => col.references('insurances.id'))
    .addColumn('bpjs_naker', 'varchar', (col) => col.unique())

    .addColumn('can_read_id', 'uuid', (col) => col.references('can_reads.id'))
    .addColumn('note', 'text')
    .execute();
}
