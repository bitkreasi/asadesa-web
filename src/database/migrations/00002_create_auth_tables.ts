import { sql } from "kysely";

import { KyselyDatabase } from "../client";
import {
  AUDITOR_COLUMN,
  defaultJSONB,
  defaultTimestamp,
  DELETED_BY_COLUMN,
  PRIMARY_KEY_COLUMN,
  SOFT_DELETED_COLUMN,
  TIMESTAMPS_COLUMN,
} from "../common";

export async function up({ schema }: KyselyDatabase): Promise<void> {
  // --------------------------------------------------------------------------
  // Create `users` table
  // --------------------------------------------------------------------------
  await schema
    .createTable("users")
    .$call(PRIMARY_KEY_COLUMN)
    .addColumn("email", "varchar", (col) => col.unique().notNull())
    .addColumn("name", "varchar")
    .addColumn("metadata", "jsonb", (col) => col.defaultTo(defaultJSONB))
    .addColumn("email_confirmed_at", "bigint")
    .$call(AUDITOR_COLUMN)
    .$call(TIMESTAMPS_COLUMN)
    .$call(SOFT_DELETED_COLUMN)
    .$call(DELETED_BY_COLUMN)
    .execute();

  await schema
    .createIndex("users_email_index")
    .on("users")
    .column("email")
    .execute();

  // --------------------------------------------------------------------------
  // Create `user_keys` table
  // --------------------------------------------------------------------------
  await schema
    .createTable("user_keys")
    .addColumn("id", "varchar(255)", (col) => col.primaryKey())
    .addColumn("user_id", "uuid", (col) =>
      col
        .references("users.id")
        .onDelete("no action")
        .onUpdate("no action")
        .notNull()
    )
    .addColumn("hashed_password", "varchar")
    .$call(TIMESTAMPS_COLUMN)
    .execute();

  // --------------------------------------------------------------------------
  // Create `sessions` table
  // --------------------------------------------------------------------------
  await schema
    .createTable("sessions")
    .addColumn("id", "varchar(255)", (col) => col.primaryKey())
    .addColumn("user_id", "uuid", (col) =>
      col
        .references("users.id")
        .onDelete("no action")
        .onUpdate("no action")
        .notNull()
    )
    .addColumn("expires_at", "timestamptz", (col) => col.notNull())
    .addColumn("created_at", "bigint", (col) =>
      col.defaultTo(defaultTimestamp).notNull()
    )
    .execute();

  await schema
    .createIndex("sessions_user_id_index")
    .on("sessions")
    .column("user_id")
    .execute();

  // --------------------------------------------------------------------------
  // Create `tokens` table
  // --------------------------------------------------------------------------

  await schema
    .createTable("tokens")
    .$call(PRIMARY_KEY_COLUMN)
    .addColumn("user_id", "uuid", (col) =>
      col
        .references("users.id")
        .onDelete("no action")
        .onUpdate("no action")
        .notNull()
    )
    .addColumn("kind", "varchar(100)", (col) => col.notNull())
    .addColumn("token", "text", (col) => col.notNull().unique())
    .addColumn("expires", "bigint", (col) => col.notNull())
    .addColumn("created_at", "bigint", (col) =>
      col.defaultTo(defaultTimestamp).notNull()
    )
    .execute();
}

export async function down({ schema }: KyselyDatabase): Promise<void> {
  await schema.dropTable("tokens").ifExists().execute();
  await schema.dropTable("sessions").ifExists().execute();
  await schema.dropTable("user_keys").ifExists().execute();
  await schema.dropTable("users").ifExists().execute();
}
