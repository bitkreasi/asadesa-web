import { Kysely, sql } from 'kysely';

const tableName = 'families';

export async function up(db: Kysely<unknown>): Promise<void> {
  await db.schema
    .createTable(tableName)
    .addColumn('id', 'uuid', (col) =>
      col.primaryKey().defaultTo(sql`gen_random_uuid()`)
    )
    .addColumn('institution_id', 'uuid', (col) =>
      col.references('institutions.id').notNull()
    )
    .addColumn('head_family_id', 'uuid', (col) =>
      col.references('residents.id')
    )
    .addColumn('no_kk', 'varchar')
    .execute();
}
