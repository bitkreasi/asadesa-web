import {
  Generated,
  Insertable,
  JSONColumnType,
  Selectable,
  Updateable,
} from "kysely";

import { RoleEnum } from "@/utils/enums/role";
import {
  WithAuditorSchema,
  WithDeletedBySchema,
  WithSoftDeleteSchema,
  WithTimeStampSchema,
} from "../common";

export interface UsersTable
  extends WithTimeStampSchema,
    WithAuditorSchema,
    WithSoftDeleteSchema,
    WithDeletedBySchema {
  id: Generated<string>;
  email: string;
  name: string;
  email_confirmed_at: number | null;
  is_verified: boolean | null;
  is_banned: boolean | null;
  organization_id: string | null;
  // You can specify JSON columns using the `JSONColumnType` wrapper.
  // It is a shorthand for `ColumnType<T, string, string>`, where T
  // is the type of the JSON object/array retrieved from the database,
  // and the insert and update types are always `string` since you're
  // always stringifying insert/update values.
  metadata: JSONColumnType<{
    avatar_url?: string | null;
    title?: string | null;
    contact_number?: string | null;
    role: RoleEnum;
    avatar_filename?: string;
  }>;
}

export type User = Selectable<UsersTable>;
export type UserInsert = Insertable<UsersTable>;
export type UserUpdate = Updateable<UsersTable>;
