import { env } from "@/config";
import { seederUpsert } from "@/utils/database";

import { UserKeyInsert } from "../tables/auth.table";
import { UserInsert } from "../tables/users.table";
import { hashPassword } from "@/utils/security";
import { KyselyDatabaseMigration } from "../client";
import { DEFAULT_ORG_ID } from "../common";

export async function userSeeder(
  db: KyselyDatabaseMigration,
  option: {
    upload?: boolean;
    sync?: boolean;
    table?: string[];
  }
) {
  const newUsers: UserInsert[] = [
    {
      email: "superadmin@asadesa.id",
      name: "Super Admin",
      metadata: JSON.stringify({
        contact_number: "+6569963799",
        avatar_url: "https://api.dicebear.com/7.x/adventurer/svg?seed=Sassy",
        role: "super_admin",
      }),
      email_confirmed_at: new Date().getTime(),
    },
    {
      email: "admin@asadesa.id",
      name: "Admin",
      metadata: JSON.stringify({
        contact_number: "+6569963791",
        avatar_url: "https://api.dicebear.com/7.x/adventurer/svg?seed=Sassy",
        role: "admin",
      }),
      email_confirmed_at: new Date().getTime(),
    },
    {
      email: "owner@asadesa.id",
      name: "Owner",
      metadata: JSON.stringify({
        contact_number: "+6587279660",
        avatar_url: "https://api.dicebear.com/7.x/adventurer/svg?seed=Sassy",
        role: "owner",
      }),
      email_confirmed_at: new Date().getTime(),
    },
  ];

  await db.transaction().execute(async (trx) => {
    const hashed_password = await hashPassword("secret");

    const users = await trx
      .insertInto("users")
      .values(newUsers)
      .returning(["id", "email"])
      .execute();

    if (!users.length) {
      return;
    }

    // Create authentication key for Lucia Auth
    const userKeys: UserKeyInsert[] = users.map((item) => ({
      id: `email:${item.email}`,
      user_id: item.id,
      hashed_password,
    }));

    return await trx
      .insertInto("user_keys")
      .values(userKeys)
      .onConflict((conflict) => {
        if (option.sync) {
          return conflict.column("id").doUpdateSet(seederUpsert(userKeys));
        }
        return conflict.column("id").doNothing();
      })
      .execute();
  });
}
