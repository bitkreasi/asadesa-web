import { Kysely, PostgresDialect } from "kysely";
import { Pool } from "pg";

import { env } from "../config";
import { Database } from "./schema";

interface KyselyMigration {
  name: string;
  timestamp: string;
}
interface DatabaseMigration {
  _migration: KyselyMigration;
}

export type KyselyDatabase = Kysely<Database>;
export type KyselyDatabaseMigration = Kysely<Database & DatabaseMigration>;

export const pool = new Pool({ connectionString: env.DATABASE_URL });

export const db: KyselyDatabase | KyselyDatabaseMigration =
  new Kysely<Database>({
    dialect: new PostgresDialect({ pool }),
    log: (event) => {
      if (process.env.DATABASE_DEBUG === "true") {
        if (event.level === "query") {
          console.info();
          console.info(event.query.sql);
          console.info(event.query.parameters);
        }
      }
      if (event.level === "error") {
        console.error("Error on database query : ");
        console.info(event.query.sql);
        console.info(event.query.parameters);
      }
    },
    plugins: [],
  });

export default db as KyselyDatabase;
