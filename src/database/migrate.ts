import { FileMigrationProvider, Migrator, NO_MIGRATIONS } from "kysely";
import { table } from "node:console";
import { promises as fs } from "node:fs";
import path, { join } from "node:path";
import { KyselyDatabaseMigration, db as database } from "./client";
import { userSeeder } from "./seeders/users.seed";

// import { categorySeeder } from './seeders/categories.seed'
// import { citiesSeeder } from './seeders/cities.seed'
// import { countriesSeeder } from './seeders/countries.seed'
// import { organizationSeeder } from './seeders/organizations.seed'
// import { topicSeeder } from './seeders/topics.seed'
// import { userSeeder } from './seeders/users.seed'
// import { db as database, KyselyDatabaseMigration } from './client'

const db = database as KyselyDatabaseMigration;

const migrationFolder = join(__dirname, "migrations");

const migrator = new Migrator({
  db,
  provider: new FileMigrationProvider({ fs, path, migrationFolder }),
  migrationTableName: "_migration",
  migrationLockTableName: "_migration_lock",
  allowUnorderedMigrations: false,
});

async function runMigration(action: "migrate" | "rollback" | "refresh") {
  if (action === "rollback" || action === "refresh") {
    const dbMigrations = await db
      .selectFrom("_migration")
      .selectAll("_migration")
      .execute();

    for (const _ of dbMigrations) {
      const { error, results } = await migrator.migrateDown();
      results?.forEach((it) => {
        if (it.status === "Success") {
          console.info(
            `🍀 migration rollback "${it.migrationName}" was executed successfully`
          );
        } else if (it.status === "Error") {
          console.error(
            `🔥 failed to execute migration rollback "${it.migrationName}"`
          );
        }
      });

      if (error) {
        console.error("🔥 failed to migrate");
        console.error(error);
        process.exit(1);
      }
    }
  }

  if (action === "migrate" || action === "refresh") {
    const { error, results } = await migrator.migrateToLatest();

    results?.forEach((it) => {
      if (it.status === "Success") {
        console.info(
          `🍀 migration "${it.migrationName}" was executed successfully`
        );
      } else if (it.status === "Error") {
        console.error(`🔥 failed to execute migration "${it.migrationName}"`);
      }
    });

    if (error) {
      console.error("🔥 failed to migrate");
      console.error(error);
      process.exit(1);
    }
  }

  if (action === "refresh") {
    await runSeeder();
  } else {
    db.destroy();
  }
}

async function resetMigration() {
  await migrator
    .migrateTo(NO_MIGRATIONS)
    .then(() => {
      console.info("🍀 Database reset succeed");
      process.exit(0);
    })
    .catch((e) => {
      console.error("🔥 Failed to reset database:", e.message);
      process.exit(1);
    })
    .finally(async () => await db.destroy());
}

async function runSeeder(
  option: { upload?: boolean; sync?: boolean; table?: string[] } = {}
) {
  const seeds = async () => {
    // if (!option.table || option.table?.includes("organizations")) {
    //   console.info("🍀 Seed table organizations...");
    //   await organizationSeeder(db, option);
    // }

    // let categories: Awaited<ReturnType<typeof categorySeeder>> = [];
    // if (!option.table || option.table?.includes("categories")) {
    //   console.info("🍀 Seed table categories...");
    //   categories = await categorySeeder(db, option);
    // }

    // if (!option.table || option.table?.includes("topics")) {
    //   console.info("🍀 Seed table topics...");
    //   await topicSeeder(db, option, categories);
    // }

    if (!option.table || option.table?.includes("users")) {
      console.info("🍀 Seed table users...");
      await userSeeder(db, option);
    }

    // if (!option.table || option.table?.includes("countries")) {
    //   console.info("🍀 Seed table countries...");
    //   await countriesSeeder(db, option);
    // }

    if (!option.table || option.table?.includes("cities")) {
      console.info("🍀 Seed table cities...");
      // await citiesSeeder(db, option)
    }
  };

  seeds()
    .then(async () => {
      console.info("🍀 Database has been populated with seeders");
      process.exit(0);
    })
    .catch(async (e) => {
      console.error("🔥 Failed running database seeder:", e.message);
      process.exit(1);
    })
    .finally(async () => await db.destroy());
}

switch (process.argv[2]) {
  case "migrate":
    console.info("🍀 Running database migration...");
    runMigration("migrate");
    break;
  case "rollback":
    console.info("🍀 Rolling back migration...");
    runMigration("rollback");
    break;
  case "reset":
    console.info("🍀 Reset database migration...");
    resetMigration();
    break;
  case "refresh":
    console.info("🍀 Refresh database migration...");
    runMigration("refresh");
    break;
  case "seed": {
    console.info("🍀 Populating database with seeders...");
    const table = process.argv.find(
      (arg) => arg.substring(0, 8) === "--table="
    );
    runSeeder({
      upload: process.argv.some((arg) => arg === "--upload"),
      sync: process.argv.some((arg) => arg === "--sync"),
      table: table ? table.replace("--table=", "").split(",") : undefined,
    });
    break;
  }
  default:
    console.warn("🔥 Invalid argument provided!");
    break;
}
