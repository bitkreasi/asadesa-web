"use client";
import { FormEvent, useState } from "react";
import styles from "./signin.module.scss";
import { useRouter } from "next/navigation";
import Link from "next/link";
import IconAsadesa from "../ui/icons/icon-asadesa";
import Button from "../ui/components/button/button";
import Input from "../ui/components/input/input";
import { useLogin } from "@/hooks/use-login";
import { cookieStorage } from "@/utils/cookie";
import { User, useAuthStore } from "@/store/authStore";

export default function Signin() {
  const { addUser } = useAuthStore();
  const { login } = useLogin();
  const router = useRouter();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [errorRes, setErrorRes] = useState("");
  const [isLoading, setLoading] = useState(false);

  const onSubmit = async (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    setLoading(true);

    const res = await login({ email, password });

    if (res.status == 200) {
      console.log(res.data.data.token);
      handleLoginSuccess(res.data.data.token);
    }
    setLoading(false);
  };

  function handleLoginSuccess(token: string) {
    cookieStorage.set("AUTH_TOKEN", token);
    router.push("/dashboard");
  }

  return (
    <>
      <section>
        <div className={styles["logo"]}>
          <IconAsadesa />
        </div>
        <div className={styles["container"]}>
          <div className={styles["container__text"]}>
            <span className={styles["container__text--title"]}>
              Selamat Datang di Asadesa
              <span role="img" aria-label="waving hand">
                👋
              </span>
            </span>
            <span className={styles["container__text--sub-title"]}>
              Silakan masukkan detail akun anda
            </span>
          </div>
          <form onSubmit={onSubmit}>
            <div className={styles["container__input"]}>
              <Input
                label="Email"
                value={email}
                setValue={setEmail}
                size="md"
                helperText=""
                placeholder="Masukkan Email anda"
                textFieldType="email"
              />
              <div className={styles["container__input--password"]}>
                <Input
                  label="Kata Sandi"
                  variant="password"
                  value={password}
                  setValue={setPassword}
                  size="md"
                  helperText={
                    errorRes !== "" && errorRes.includes("Password")
                      ? errorRes
                      : ""
                  }
                  isError={errorRes !== "" && errorRes.includes("Password")}
                  placeholder="Masukkan Kata Sandi"
                />
                <div className={styles["container__input--password__button"]}>
                  <div
                    className={
                      styles["container__input--password__button--item"]
                    }
                  >
                    <Link href="/forgot-password">
                      <Button
                        text="Lupa Kata Sandi?"
                        color="primary"
                        variant="text-link"
                        size="lg"
                        disable={false}
                      />
                    </Link>
                  </div>
                </div>
              </div>
            </div>
            <div className={styles["container__button"]}>
              <Button
                text="Masuk"
                color="primary"
                variant="solid"
                type="submit"
                disable={
                  email.length === 0 || password.length === 0 || isLoading
                    ? true
                    : false
                }
                isLoading={isLoading}
              />
              <div className={styles["container__button--link"]}>
                <span>Belum Punya akun?</span>
                <Link href={"/signup"}>
                  <Button
                    text="Daftar"
                    color="primary"
                    variant="text-link"
                    disable={false}
                    size="lg"
                  />
                </Link>
              </div>
            </div>
          </form>
        </div>
      </section>
      <div className={styles.background} />
    </>
  );
}
