'use client'

import Link from 'next/link'
import styles from './page.module.scss'
import Button from '../ui/components/button/button'

export default function Page() {
  return (
    <section>
      <div className={styles['container']}>
        <div className={styles['container__text']}>
          <h1 className={styles['container__text-heading']}>Page not found</h1>
        </div>
        <div className={styles['container__button']}>
          <Link href='/'>
            <Button color='primary' text='kembali' />
          </Link>
        </div>
      </div>
    </section>
  )
}
