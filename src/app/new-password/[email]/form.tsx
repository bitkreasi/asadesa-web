'use client'

import * as yup from 'yup'
import { FormEvent, useState } from 'react'
import { extractErrors } from '@/utils/extract-error'
import { useRouter } from 'next/navigation'
import { Shape } from '@/app/shared'
import Input from '@/app/ui/components/input/input'
import Button from '@/app/ui/components/button/button'

import IconSuccess from '@/app/ui/icons/icon-success'
import IconInfo from '@/app/ui/icons/icon-info'

import { validatePassword } from '@/utils/validates'
import styles from './page.module.scss'
import Alert from '@/app/ui/components/alert/alert'

interface FormSchema {
  password: string
  confirmPassword: string
}

type ErrorType = {
  case: boolean
  length: boolean
  symbol: boolean
  number: boolean
  confirmPassword?: string | boolean
  password?: string
}

const initialState: FormSchema = {
  password: '',
  confirmPassword: '',
}

const initErrors: ErrorType = {
  case: false,
  length: false,
  symbol: false,
  number: false,
  confirmPassword: false,
}

const requirement = [
  { key: 'length', message: 'Minimum 8 karakater' },
  { key: 'case', message: 'Memiliki huruf besar dan kecil' },
  { key: 'number', message: 'Memiliki angka' },
  { key: 'symbol', message: 'Memiliki simbol' },
]

const SchemaValidation = yup.object<Shape<FormSchema>>({
  password: yup
    .string()
    .required('Password is required')
    .matches(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=!])(?=\S+$).{8,}$/),
  confirmPassword: yup
    .string()
    .oneOf([yup.ref('password'), ''], 'Passwords tidak sesuai')
    .required('Konfirmasi kata sandi dibutuhkan'),
})

export default function FormPassword() {
  const router = useRouter()
  const [data, setData] = useState<FormSchema>(initialState)
  const [errors, setErrors] = useState<ErrorType>(initErrors)

  function handleChange(value: string, name: string) {
    setData((prev) => ({ ...prev, [name]: value }))
    if (name === 'password') validatePassword(value, setErrors)
  }

  async function handleSubmit(e: FormEvent<HTMLFormElement>) {
    e.preventDefault()
    try {
      await SchemaValidation.validate(data, { abortEarly: false })
      setErrors({
        case: true,
        length: true,
        symbol: true,
        number: true,
      })

      router.push('/signin')
    } catch (error: any) {
      extractErrors(error, setErrors)
    }
  }

  return (
    <form onSubmit={handleSubmit}>
      <div className={styles['container__input']}>
        <Input
          label='Kata sandi baru'
          variant='password'
          value={data.password}
          setValue={(val) => handleChange(val, 'password')}
          size='md'
          helperText=''
          placeholder='Masukkan Kata Sandi'
          isError={typeof errors?.password == 'string' ? true : false}
        />
        <div className={styles['error']}>
          <Conditions items={requirement} errors={errors} />
        </div>

        <Input
          label='Konfirmasi kata Sandi'
          variant='password'
          value={data.confirmPassword}
          setValue={(val) => handleChange(val, 'confirmPassword')}
          size='md'
          helperText={
            data.password !== '' && typeof errors.confirmPassword == 'string'
              ? errors.confirmPassword
              : ''
          }
          placeholder='Masukkan Kata Sandi'
          isError={
            typeof errors.confirmPassword == 'string' &&
            errors.confirmPassword !== ''
              ? true
              : false
          }
        />
      </div>

      <div className={styles['container__button']}>
        <Alert
          message='Setelah kata sandi diubah, silahkan masuk kembali dengan kata sandi baru.'
          title='Informasi'
          variant='INFO'
        />
        <Button
          text='Ubah Sandi'
          color='primary'
          variant='solid'
          type='submit'
        />
      </div>
    </form>
  )
}

type PropsConditions = {
  items: {
    key: string
    message: string
  }[]
  errors: ErrorType
}

function Conditions(props: PropsConditions) {
  return props.items.map((item, index) => (
    <div key={index} className={styles['error__wrapper']}>
      {props.errors[item.key as keyof ErrorType] ? (
        <IconSuccess backgroundColor='#2CAE70' color='#2CAE70' />
      ) : (
        <IconInfo backgroundColor='#243576' color='#243576' />
      )}

      <span className={styles['error__text']}>{item.message}</span>
    </div>
  ))
}
