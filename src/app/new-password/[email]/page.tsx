import { Metadata } from 'next'
import styles from './page.module.scss'
import FormPassword from './form'

export const metadata: Metadata = {
  title: 'set new password',
}

type Props = {
  params: {
    email: string
  }
}

export default function Page(props: Props) {
  return (
    <>
      <div className={styles['container__text']}>
        <h1 className={styles['container__text--title']}>Kata sandi baru</h1>
        <span className={styles['container__text--sub-title']}>
          Buat kata sandi yang kuat untuk akun dengan e-mail{' '}
          {props.params.email.replace('%40', '@')}
        </span>
      </div>
      <div>
        <FormPassword />
      </div>
    </>
  )
}
