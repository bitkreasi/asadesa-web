import IconAsadesa from '../ui/icons/icon-asadesa'
import styles from './layout.module.scss'

export default function AuthLayout({ children }: React.PropsWithChildren) {
  return (
    <>
      <section>
        <div className={styles['logo']}>
          <IconAsadesa />
        </div>
        <div className={styles['container']}>{children}</div>
      </section>
      <div className={styles.background} />
    </>
  )
}
