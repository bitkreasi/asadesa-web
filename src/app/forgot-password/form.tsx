'use client'

import styles from './page.module.scss'
import * as yup from 'yup'
import { Shape } from '../shared'
import { FormEvent, useState } from 'react'
import { extractErrors } from '@/utils/extract-error'
import { useRouter } from 'next/navigation'
import Input from '../ui/components/input/input'
import Button from '../ui/components/button/button'

interface Schema {
  email: string
}

const SchemaValidation = yup.object<Shape<Schema>>({
  email: yup
    .string()
    .email('Format email yang anda masukkan salah.')
    .required(),
})

export default function Form() {
  const router = useRouter()
  const [email, setEmail] = useState<string>('')
  const [errors, setErrors] = useState<any>({})

  async function handleSubmit(e: FormEvent) {
    e.preventDefault()
    try {
      await SchemaValidation.validate({ email }, { abortEarly: false })
      setErrors({})
      router.push('/forgot-password/' + email)
    } catch (error) {
      extractErrors(error, setErrors)
    }
  }

  console.log(errors)

  return (
    <form onSubmit={handleSubmit}>
      <div className={styles['container__input']}>
        <Input
          label='Email'
          value={email}
          setValue={setEmail}
          size='md'
          placeholder='Masukkan email anda'
          textFieldType='text'
          isError={errors?.email}
          helperText={errors?.email ?? ''}
        />
      </div>
      <div className={styles['container__button']}>
        <Button
          text='Atur ulang'
          color='primary'
          variant='solid'
          type='submit'
          disable={email == ''}
        />
      </div>
    </form>
  )
}
