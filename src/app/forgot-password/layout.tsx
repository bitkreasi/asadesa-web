import IconAsadesa from '../ui/icons/icon-asadesa'
import styles from './page.module.scss'

export default function Layout({ children }: React.PropsWithChildren) {
  return (
    <>
      <section>
        <div className={styles['logo']}>
          <IconAsadesa />
        </div>
        <div className={styles['container']}>{children}</div>
      </section>
      <div className={styles.background} />
    </>
  )
}
