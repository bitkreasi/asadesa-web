import IconAsadesa from "@/app/ui/icons/icon-asadesa";
import styles from "./forgot-password-success.module.scss";
import emailIcon from "./email-1.svg";
import Image from "next/image";
import Button from "@/app/ui/components/button/button";

export default function ForgetPasswordSuccess({
  searchParams,
}: {
  searchParams: { [key: string]: string | string[] | undefined };
}) {
  return (
    <section>
      <div className={styles["logo"]}>
        <IconAsadesa />
      </div>
      <div className={styles["container"]}>
        <Image
          src={emailIcon}
          className={styles["container__image"]}
          alt="Email reset password berhasil dikirim"
        />
        <h1 className={styles["container__text--title"]}>
          Link pemulihan kata sandi telah dikirim!
        </h1>
        <p className={styles["container__text--sub-title"]}>
          Kami telah mengirimkan link pemulihan kata sandi ke email{" "}
          <strong>{searchParams.email}</strong>, silahkan klik link tersebut
          untuk mengatur ulang kata sandi.
        </p>
        <div className={styles["container__action"]}>
          <Button
            text="Kembali"
            color="primary"
            type="button"
            variant="outline"
          />
          <Button text="Buka gmail" color="primary" type="button" />
        </div>
      </div>
    </section>
  );
}
