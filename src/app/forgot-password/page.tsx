import React, { FormEvent } from "react";
import IconAsadesa from "../ui/icons/icon-asadesa";
import styles from "./forgot-password.module.scss";
import ForgetPasswordForm from "./_components/forgot-password-form";
import IconArrowCircleLeft from "../ui/icons/icon-arrow-circle-left";
import Link from "next/link";

export default async function ForgetPassword() {
  return (
    <section>
      <div className={styles["logo"]}>
        <IconAsadesa />
      </div>
      <div className={styles["container"]}>
        <Link href="/signin" aria-label="Kembali ke laman login">
          <IconArrowCircleLeft />
        </Link>
        <div className={styles["container__text"]}>
          <span className={styles["container__text--title"]}>
            Atur ulang kata sandi
          </span>
          <span className={styles["container__text--sub-title"]}>
            Masukkan email yang terdaftar. Kami akan mengirimkan kode verifikasi
            untuk mengatur ulang kata sandi.
          </span>
        </div>
        <ForgetPasswordForm />
      </div>
    </section>
  );
}
