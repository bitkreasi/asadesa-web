"use client";

import Button from "@/app/ui/components/button/button";
import Input from "@/app/ui/components/input/input";
import React, { FormEvent } from "react";
import styles from "../forgot-password.module.scss";
import { useRouter } from "next/navigation";

const ForgetPasswordForm = () => {
  const [email, setEmail] = React.useState("");
  const [isLoading, setIsLoading] = React.useState(false);
  const router = useRouter();
  const onSubmit = async (event: FormEvent<HTMLFormElement>) => {
    setIsLoading(true);
    event.preventDefault();
    setIsLoading(false);
    router.push("/forgot-password/success?email=" + email);
  };
  return (
    <form onSubmit={onSubmit}>
      <div className={styles["container__input"]}>
        <Input
          label="Email"
          value={email}
          setValue={setEmail}
          size="md"
          helperText=""
          placeholder="Masukkan Email anda"
          textFieldType="email"
        />
      </div>
      <div className={styles["container__button"]}>
        <Button
          text="Atur Ulang"
          color="primary"
          variant="solid"
          type="submit"
          disable={email.length === 0 || isLoading ? true : false}
          isLoading={isLoading}
        />
      </div>
    </form>
  );
};

export default ForgetPasswordForm;
