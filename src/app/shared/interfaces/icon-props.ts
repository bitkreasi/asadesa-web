export interface IconProps{
  color?: string;
  backgroundColor?: string;
  height?: number,
  opacity?: number;
  width?: number,
}