export interface ResidentProps {
  id: string;
  nama: string;
  imageUrl: string;
  nik: string;
  dusun: string;
  rt: string;
  rw: string;
  umur: number;
  pekerjaan: string;
  status: string;
  tanggalLapor: string;
  jenisPenduduk: string;
  peranDalamKeluarga: string;
  tanggalMasuk?: string;
  wajibIdentitas: string;
  identitasElektronik: string;
  statusRekam: string;
  tagIdCard: string;
  nomorKKSebelumnya: string;
  JenisKelamin: string;
  agama: string;
  statusPenduduk: string;
  nomorAktaKelahiran: string;
  KotaKelahiran: string;
  tanggalKelahiran: string;
  waktuKelahiran: string;
  tempatDilahirkan: string;
  jenisKelahiran: string;
  kelahiranKe: string;
  tenagaKesehatan: string;
  beratLahir: string;
  panjangLahir: string;
  pendidikanDalamKK: string;
  pendidikanDitempuh: string;
  sukuAtauEtnis: string;
  kewarganegaraan: string;
  nomorPaspor: string;
  tanggalBerakhirPaspor: string;
  namaAyah: string;
  nikAyah: string;
  namaIbu: string;
  nikIbu: string;
  nomorTelepon: string;
  email: string;
  statusNikah: string;
  nomorAktaNikah?: string;
  tanggalPernikahan?: string;
  aktaCerai?: string;
  tanggalCerai?: string;
  golonganDarah: string;
  cacat: string;
  sakitMenahun: string;
  akseptorKB: string;
  asuransi: string;
  nomorBPJS: string;
  dapatMembacaHuruf: string;
  catatan?: string;
  jumlahAnggota: string;
}

export const dummyResident: ResidentProps[] = [
  {
    id: "atrhsd",
    nama: "Asep Uyee",
    imageUrl: "https://source.unsplash.com/ycZjz2Uw6VY/1600x900",
    nik: "3302042001040001",
    dusun: "Mangsit",
    rt: "01",
    rw: "05",
    umur: 20,
    pekerjaan: "Programmer",
    peranDalamKeluarga: "SUami",
    status: "Menikah",
    tanggalLapor: "12/19/2023",
    jenisPenduduk: "Penduduk Lahir",
    wajibIdentitas: "Belum Wajib",
    identitasElektronik: "KTP-EL",
    statusRekam: "Belum Rekam",
    tagIdCard: "LK12345",
    nomorKKSebelumnya: "3302042001040001",
    JenisKelamin: "Laki-laki",
    agama: "Islam",
    statusPenduduk: "Tetap",
    nomorAktaKelahiran: "1345789012342790",
    KotaKelahiran: "Jakarta",
    tanggalKelahiran: "12/19/2023",
    waktuKelahiran: "20:00",
    tempatDilahirkan: "Rumah Sakit",
    jenisKelahiran: "Tunggal",
    kelahiranKe: "1",
    tenagaKesehatan: "Dokter",
    beratLahir: "3.5",
    panjangLahir: "50",
    pendidikanDalamKK: "Strata II",
    pendidikanDitempuh: "Sedang S-3/Sederajat",
    sukuAtauEtnis: "Jawa",
    kewarganegaraan: "WNI",
    nomorPaspor: "1234567890",
    tanggalBerakhirPaspor: "12/19/2023",
    namaAyah: "Budi",
    nikAyah: "3302042001040001",
    namaIbu: "Siti",
    nikIbu: "3302042001040001",
    nomorTelepon: "081234567890",
    email: "asep@gmail.com",
    statusNikah: "Kawin",
    nomorAktaNikah: "1234567890",
    tanggalPernikahan: "12/19/2023",
    golonganDarah: "AB",
    cacat: "Tidak Cacat",
    sakitMenahun: "Tidak Ada/Tidak Sakit",
    akseptorKB: "Pil",
    asuransi: "Tidak/Belum Punya",
    nomorBPJS: "1234567890",
    dapatMembacaHuruf: "Arab, Latin dan Daerah",
    jumlahAnggota: "3",
  },
  {
    id: "dasdfd",
    nama: "Joko Anwar",
    imageUrl: "https://source.unsplash.com/c4a_0kycTUE/1600x900",
    nik: "3302042001040001",
    dusun: "Karang Pule",
    rt: "11",
    rw: "10",
    umur: 32,
    pekerjaan: "Guru",
    status: "Menikah",
    tanggalLapor: "12/19/2023",
    jenisPenduduk: "Penduduk Masuk",
    tanggalMasuk: "12/19/2023",
    peranDalamKeluarga: "Anak",
    wajibIdentitas: "Belum Wajib",
    identitasElektronik: "KTP-EL",
    statusRekam: "Belum Rekam",
    tagIdCard: "LK12345",
    nomorKKSebelumnya: "3302042001040001",
    JenisKelamin: "Laki-laki",
    agama: "Islam",
    statusPenduduk: "Tetap",
    nomorAktaKelahiran: "1345789012342790",
    KotaKelahiran: "Jakarta",
    tanggalKelahiran: "12/19/2023",
    waktuKelahiran: "20:00",
    tempatDilahirkan: "Rumah Sakit",
    jenisKelahiran: "Tunggal",
    kelahiranKe: "1",
    tenagaKesehatan: "Dokter",
    beratLahir: "3.5",
    panjangLahir: "50",
    pendidikanDalamKK: "Strata II",
    pendidikanDitempuh: "Sedang S-3/Sederajat",
    sukuAtauEtnis: "Jawa",
    kewarganegaraan: "WNI",
    nomorPaspor: "1234567890",
    tanggalBerakhirPaspor: "12/19/2023",
    namaAyah: "Budi",
    nikAyah: "3302042001040001",
    namaIbu: "Siti",
    nikIbu: "3302042001040001",
    nomorTelepon: "081234567890",
    email: "asep@gmail.com",
    statusNikah: "Kawin",
    nomorAktaNikah: "1234567890",
    tanggalPernikahan: "12/19/2023",
    golonganDarah: "AB",
    cacat: "Tidak Cacat",
    sakitMenahun: "Tidak Ada/Tidak Sakit",
    akseptorKB: "Pil",
    asuransi: "Tidak/Belum Punya",
    nomorBPJS: "1234567890",
    dapatMembacaHuruf: "Arab, Latin dan Daerah",
    jumlahAnggota: "2",
  },
  {
    id: "uiojkl",
    nama: "Desi Kurniawati",
    imageUrl: "https://source.unsplash.com/qTIIfQv0Sw0/1600x900",
    nik: "3302042001040001",
    dusun: "Karang Pule",
    rt: "11",
    rw: "10",
    umur: 32,
    pekerjaan: "Guru",
    status: "Menikah",
    tanggalLapor: "12/19/2023",
    jenisPenduduk: "Penduduk Lahir",
    wajibIdentitas: "Belum Wajib",
    identitasElektronik: "KTP-EL",
    statusRekam: "Belum Rekam",
    peranDalamKeluarga: "Istri",
    tagIdCard: "LK12345",
    nomorKKSebelumnya: "3302042001040001",
    JenisKelamin: "Laki-laki",
    agama: "Islam",
    statusPenduduk: "Tetap",
    nomorAktaKelahiran: "1345789012342790",
    KotaKelahiran: "Jakarta",
    tanggalKelahiran: "12/19/2023",
    waktuKelahiran: "20:00",
    tempatDilahirkan: "Rumah Sakit",
    jenisKelahiran: "Tunggal",
    kelahiranKe: "1",
    tenagaKesehatan: "Dokter",
    beratLahir: "3.5",
    panjangLahir: "50",
    pendidikanDalamKK: "Strata II",
    pendidikanDitempuh: "Sedang S-3/Sederajat",
    sukuAtauEtnis: "Jawa",
    kewarganegaraan: "WNI",
    nomorPaspor: "1234567890",
    tanggalBerakhirPaspor: "12/19/2023",
    namaAyah: "Budi",
    nikAyah: "3302042001040001",
    namaIbu: "Siti",
    nikIbu: "3302042001040001",
    nomorTelepon: "081234567890",
    email: "asep@gmail.com",
    statusNikah: "Kawin",
    nomorAktaNikah: "1234567890",
    tanggalPernikahan: "12/19/2023",
    golonganDarah: "AB",
    cacat: "Tidak Cacat",
    sakitMenahun: "Tidak Ada/Tidak Sakit",
    akseptorKB: "Pil",
    asuransi: "Tidak/Belum Punya",
    nomorBPJS: "1234567890",
    dapatMembacaHuruf: "Arab, Latin dan Daerah",
    jumlahAnggota: "3",
  },
  {
    id: "cvbnm,",
    nama: "Upik Haswati",
    imageUrl: "https://source.unsplash.com/qrdwJfAAVEI/1600x900",
    nik: "3302042001040001",
    dusun: "Karang Pule",
    rt: "11",
    rw: "10",
    umur: 32,
    pekerjaan: "Guru",
    status: "Menikah",
    tanggalLapor: "12/19/2023",
    peranDalamKeluarga: "Menantu",
    jenisPenduduk: "Penduduk Lahir",
    wajibIdentitas: "Belum Wajib",
    identitasElektronik: "KTP-EL",
    statusRekam: "Belum Rekam",
    tagIdCard: "LK12345",
    nomorKKSebelumnya: "3302042001040001",
    JenisKelamin: "Laki-laki",
    agama: "Islam",
    statusPenduduk: "Tetap",
    nomorAktaKelahiran: "1345789012342790",
    KotaKelahiran: "Jakarta",
    tanggalKelahiran: "12/19/2023",
    waktuKelahiran: "20:00",
    tempatDilahirkan: "Rumah Sakit",
    jenisKelahiran: "Tunggal",
    kelahiranKe: "1",
    tenagaKesehatan: "Dokter",
    beratLahir: "3.5",
    panjangLahir: "50",
    pendidikanDalamKK: "Strata II",
    pendidikanDitempuh: "Sedang S-3/Sederajat",
    sukuAtauEtnis: "Jawa",
    kewarganegaraan: "WNI",
    nomorPaspor: "1234567890",
    tanggalBerakhirPaspor: "12/19/2023",
    namaAyah: "Budi",
    nikAyah: "3302042001040001",
    namaIbu: "Siti",
    nikIbu: "3302042001040001",
    nomorTelepon: "081234567890",
    email: "asep@gmail.com",
    statusNikah: "Kawin",
    nomorAktaNikah: "1234567890",
    tanggalPernikahan: "12/19/2023",
    golonganDarah: "AB",
    cacat: "Tidak Cacat",
    sakitMenahun: "Tidak Ada/Tidak Sakit",
    akseptorKB: "Pil",
    asuransi: "Tidak/Belum Punya",
    nomorBPJS: "1234567890",
    dapatMembacaHuruf: "Arab, Latin dan Daerah",
    jumlahAnggota: "4",
  },
  {
    id: "zxcvbn",
    nama: "Koko Supriyadi",
    imageUrl: "https://source.unsplash.com/U1I0IoQx3Yo/1600x900",
    nik: "3302042001040001",
    dusun: "Karang Pule",
    rt: "11",
    rw: "10",
    umur: 32,
    peranDalamKeluarga: "Cucu",
    pekerjaan: "Guru",
    status: "Menikah",
    tanggalLapor: "12/19/2023",
    jenisPenduduk: "Penduduk Lahir",
    wajibIdentitas: "Belum Wajib",
    identitasElektronik: "KTP-EL",
    statusRekam: "Belum Rekam",
    tagIdCard: "LK12345",
    nomorKKSebelumnya: "3302042001040001",
    JenisKelamin: "Laki-laki",
    agama: "Islam",
    statusPenduduk: "Tetap",
    nomorAktaKelahiran: "1345789012342790",
    KotaKelahiran: "Jakarta",
    tanggalKelahiran: "12/19/2023",
    waktuKelahiran: "20:00",
    tempatDilahirkan: "Rumah Sakit",
    jenisKelahiran: "Tunggal",
    kelahiranKe: "1",
    tenagaKesehatan: "Dokter",
    beratLahir: "3.5",
    panjangLahir: "50",
    pendidikanDalamKK: "Strata II",
    pendidikanDitempuh: "Sedang S-3/Sederajat",
    sukuAtauEtnis: "Jawa",
    kewarganegaraan: "WNI",
    nomorPaspor: "1234567890",
    tanggalBerakhirPaspor: "12/19/2023",
    namaAyah: "Budi",
    nikAyah: "3302042001040001",
    namaIbu: "Siti",
    nikIbu: "3302042001040001",
    nomorTelepon: "081234567890",
    email: "asep@gmail.com",
    statusNikah: "Kawin",
    nomorAktaNikah: "1234567890",
    tanggalPernikahan: "12/19/2023",
    golonganDarah: "AB",
    cacat: "Tidak Cacat",
    sakitMenahun: "Tidak Ada/Tidak Sakit",
    akseptorKB: "Pil",
    asuransi: "Tidak/Belum Punya",
    nomorBPJS: "1234567890",
    dapatMembacaHuruf: "Arab, Latin dan Daerah",
    jumlahAnggota: "4",
  },
];
