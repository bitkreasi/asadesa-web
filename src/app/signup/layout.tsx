import styles from './layout.module.scss'

export default function AuthLayout({ children }: React.PropsWithChildren) {
  return (
    <>
      {children}
      <div className={styles.background} />
    </>
  )
}
