'use client'

import * as yup from 'yup'
import { Shape } from '../shared'
import Input from '../ui/components/input/input'
import { FormEvent, useEffect, useState } from 'react'
import styles from './signup.module.scss'
import Button from '../ui/components/button/button'
import Link from 'next/link'
import axios from 'axios'
import { BASE_URL, VERSION } from '@/services'
import { useRoles } from '@/hooks/use-roles'
import { extractErrors } from '@/utils/extract-error'

const KEY_SESSION_STORAGE: string = 'SESSION_SIGNUP'

interface FirstFormSchema {
  fullname: string
  email: string
  phone_number: string
  role: string
}

const initialState: FirstFormSchema = {
  email: '',
  fullname: '',
  phone_number: '',
  role: '',
}

const SchemaValidation = yup.object<Shape<FirstFormSchema>>({
  fullname: yup.string().required(),
  email: yup.string().email('Format email yang anda masukan salah').required(),
  phone_number: yup
    .string()
    .matches(/^\d{10,12}$/, 'Phone number is not valid'),
  role: yup.string().required(),
})

type Props = {
  next: () => void
}

export default function FormProfile(props: Props) {
  const [profile, setProfile] = useState<FirstFormSchema>(initialState)
  const [errors, setErrors] = useState<any>({})
  const { data: roles, loading: roles_loading } = useRoles()

  function handleChange(value: string, name: string) {
    setProfile((prev) => ({ ...prev, [name]: value }))
  }

  async function handleSubmit(e: FormEvent<HTMLFormElement>) {
    e.preventDefault()
    try {
      await SchemaValidation.validate(profile, { abortEarly: false })
      setErrors({})

      const selected_role = roles.find((role) => role.name == profile.role)

      const local = sessionStorage.getItem(KEY_SESSION_STORAGE)
      if (local) {
        const oldData = JSON.parse(local)
        const newData = { ...oldData, ...profile, role_id: selected_role?.id, role_name: selected_role?.name }
        sessionStorage.setItem(KEY_SESSION_STORAGE, JSON.stringify(newData))
      } else {
        const newData = { ...profile, role_id: selected_role?.id, role_name: selected_role?.name }
        sessionStorage.setItem(KEY_SESSION_STORAGE, JSON.stringify(newData))
      }

      props.next()
    } catch (error: any) {
      extractErrors(error, setErrors)
    }
  }

  useEffect(() => {
    const local = sessionStorage.getItem(KEY_SESSION_STORAGE)
    if (local) {
      const oldData = JSON.parse(local)
      setProfile({ ...oldData, role: oldData.role_name})
    }
  }, [])

  return (
    <form onSubmit={handleSubmit}>
      <div className={styles['container__input']}>
        <Input
          label='Nama'
          value={profile.fullname}
          setValue={(val) => handleChange(val, 'fullname')}
          size='md'
          placeholder='Masukkan nama lengkap anda'
          textFieldType='text'
          isError={errors?.fullname}
          helperText={errors?.fullname ?? ''}
        />
        <Input
          label='Email'
          value={profile.email}
          setValue={(val) => handleChange(val, 'email')}
          size='md'
          placeholder='Masukkan Email anda'
          textFieldType='text'
          isError={errors?.email}
          helperText={errors?.email ?? ''}
        />
        <Input
          label='Nomor telepon dan Whatsapp'
          value={profile.phone_number}
          setValue={(val) => handleChange(val, 'phone_number')}
          size='md'
          placeholder='Masukkan nomor telepon'
          textFieldType='text'
          isError={errors?.phone_number}
          helperText={errors?.phone_number ?? ''}
        />
        <Input
          label='Jabatan'
          value={profile.role}
          variant='dropdown-select'
          setValue={(val) => handleChange(val, 'role')}
          size='md'
          items={roles.map(role => role.name) ?? []}
          placeholder='Contoh: Kepala desa, Perangkat desa'
          textFieldType='text'
          isError={errors?.role}
          helperText={errors?.role ?? ''}
          isDisable={roles_loading}
        />
      </div>

      <div className={styles['container__button']}>
        <Button
          text='Lanjutkan'
          color='primary'
          variant='solid'
          type='submit'
        />
        <div className={styles['container__button--link']}>
          <span>Sudah Punya akun?</span>
          <Link href={'/'}>
            <Button
              text='Masuk'
              color='primary'
              variant='text-link'
              disable={false}
              size='lg'
            />
          </Link>
        </div>
      </div>
    </form>
  )
}
