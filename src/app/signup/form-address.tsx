'use client'

import Input from '../ui/components/input/input'
import { FormEvent, useEffect, useState } from 'react'
import styles from './signup.module.scss'
import Button from '../ui/components/button/button'
import Link from 'next/link'
import IconArrowCircleLeft from '../ui/icons/icon-arrow-circle-left'
import * as yup from 'yup'
import { Shape } from '../shared'
import { extractErrors } from '@/utils/extract-error'
import { useRegion } from '@/hooks/use-region'
import { KEY_SESSION_STORAGE } from '../shared/constants/session'

interface FormSchema {
  province: string
  regency: string
  district: string
  village: string
}

const initialState: FormSchema = {
  province: '',
  regency: '',
  district: '',
  village: '',
}

const SchemaValidation = yup.object<Shape<FormSchema>>({
  province: yup.string().required(),
  regency: yup.string().required(),
  district: yup.string().required(),
  village: yup.string().required(),
})

type Props = {
  next: () => void
  back: () => void
}

export default function FormAddress(props: Props) {
  const [address, setAddress] = useState<FormSchema>(initialState)
  const [oldAddress, setOldAddress] = useState(initialState)
  const [errors, setErrors] = useState<any>({})
  const regions = useRegion()

  function handleChange(value: string, name: string) {
    setAddress((prev) => ({ ...prev, [name]: value }))
  }

  async function handleSubmit(e: FormEvent<HTMLFormElement>) {
    e.preventDefault()
    try {
      await SchemaValidation.validate(address, { abortEarly: false })
      setErrors({})

      const selectedProvince = regions.provinces.find((data: any) => data.name === address.province )
      const selectedRegency = regions.regencies.find((data: any) => data.name === address.regency )
      const selectedDistrict = regions.districts.find((data: any) => data.name === address.district )
      const selectedVillage = regions.villages.find((data: any) => data.name === address.village )

      const address_ids = {
        province_id: selectedProvince?.id,
        regency_id: selectedRegency?.id,
        district_id: selectedDistrict?.id,
        village_id: selectedVillage?.id,
      }

      const local = sessionStorage.getItem(KEY_SESSION_STORAGE)
      if (local) {
        const oldData = JSON.parse(local)
        const newData = { ...oldData, ...address_ids }
        sessionStorage.setItem(KEY_SESSION_STORAGE, JSON.stringify(newData))
      } else {
        const newData = { ...address_ids }
        sessionStorage.setItem(KEY_SESSION_STORAGE, JSON.stringify(newData))
      }

      props.next()
    } catch (error: any) {
      extractErrors(error, setErrors)
    }
  }

  // handle when user choose addres to fetch regency -> district -> village
  useEffect(() => {
    if(address.province !== '') {
      const selectedProvince = regions.provinces.find((data) => data.name === address.province)
      regions.setIdProvince(selectedProvince?.code ?? "")
    }
    // to retrieve district data
    if(address.regency !== '') {
      const selectedRegency = regions.regencies.find((data) => data.name === address.regency)
      regions.setIdRegency(selectedRegency?.code ?? "")
    }
    // to retrieve village data
    if(address.district !== '') {
      const selectedDistrict = regions.districts.find((data) => data.name === address.district)
      regions.setIdDistrict(selectedDistrict?.code ?? "")
    }

  },[address])

  // handle if user select another address
  useEffect(() => {
    setOldAddress(address) 

    if(address.province !== oldAddress.province) {
      handleChange('', 'regency')
    }
    if(address.regency !== oldAddress.regency) {
      handleChange('', 'district')
    }
    if(address.district !== oldAddress.district) {
      handleChange('', 'village')
    }
  },[address]) 

  return (
    <form onSubmit={handleSubmit}>
      <div className={styles['container__input']}>
        <Input
          label='Provinsi'
          value={address.province}
          setValue={(val) => handleChange(val, 'province')}
          size='md'
          placeholder='Pilih provinsi'
          variant='dropdown-select'
          items={regions.provinces?.map((data) => data.name) ?? []}
          isDisable={regions.provinceLoading}
          isError={errors?.province}
          helperText={errors.province ?? ''}
        />
        <Input
          label='Kabupaten'
          value={address.regency}
          setValue={(val) => handleChange(val, 'regency')}
          size='md'
          placeholder='Pilih kabupaten'
          variant='dropdown-select'
          items={regions.regencies?.map((data) => data.name) ?? []}
          isDisable={address.province == '' || regions.regencyLoading}
          isError={errors?.regency}
          helperText={errors?.regency ?? ''}
        />
        <Input
          label='Kecamatan'
          value={address.district}
          setValue={(val) => handleChange(val, 'district')}
          size='md'
          placeholder='Pilih kecamatan'
          variant='dropdown-select'
          items={regions.districts?.map((data) => data.name) ?? []}
          isDisable={address.regency == '' || regions.districtLoading}
          isError={errors?.district}
          helperText={errors?.district ?? ''}
        />
        <Input
          label='Desa'
          value={address.village}
          setValue={(val) => handleChange(val, 'village')}
          size='md'
          placeholder='Pilih desa'
          variant='dropdown-select'
          items={regions.villages?.map((data) => data.name) ?? []}
          isDisable={address.district == '' || regions.villageLoading}
          isError={errors?.village}
          helperText={errors?.village ?? ''}
        />
      </div>

      <div className={styles['container__button']}>
        <div className={styles['container__button--top']}>
          <Button
            suffix='Kembali'
            variant='outline'
            color='neutral'
            type='button'
            onHandleClick={props.back}
            icon={<IconArrowCircleLeft />}
          />
          <Button
            text='Lanjutkan'
            color='primary'
            variant='solid'
            type='submit'
          />
        </div>
        <div className={styles['container__button--link']}>
          <span>Sudah Punya akun?</span>
          <Link href={'/'}>
            <Button
              text='Masuk'
              color='primary'
              variant='text-link'
              disable={false}
              size='lg'
            />
          </Link>
        </div>
      </div>
    </form>
  )
}
