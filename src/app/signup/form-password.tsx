'use client'

import * as yup from 'yup'
import { Shape } from '../shared'
import Input from '../ui/components/input/input'
import { FormEvent, useState } from 'react'
import styles from './signup.module.scss'
import Button from '../ui/components/button/button'
import Link from 'next/link'
import IconArrowCircleLeft from '../ui/icons/icon-arrow-circle-left'
import { useRouter } from 'next/navigation'
import IconInfo from '../ui/icons/icon-info'
import IconSuccess from '../ui/icons/icon-success'
import { extractErrors } from '@/utils/extract-error'
import { KEY_SESSION_STORAGE } from '../shared/constants/session'
import { RegisterPayload } from '@/services/auth'
import { useRegister } from '@/hooks/use-register'

interface FormSchema {
  password: string
  confirmPassword: string
}

type ErrorType = {
  case: boolean
  length: boolean
  symbol: boolean
  number: boolean
  confirmPassword?: string | boolean
  password?: string
}

const initialState: FormSchema = {
  password: '',
  confirmPassword: '',
}

const initErrors: ErrorType = {
  case: false,
  length: false,
  symbol: false,
  number: false,
  confirmPassword: false,
}

const requirement = [
  { key: 'length', message: 'Minimum 8 karakater' },
  { key: 'case', message: 'Memiliki huruf besar dan kecil' },
  { key: 'number', message: 'Memiliki angka' },
  { key: 'symbol', message: 'Memiliki simbol' },
]

const SchemaValidation = yup.object<Shape<FormSchema>>({
  password: yup
    .string()
    .required('Password is required')
    .matches(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=!])(?=\S+$).{8,}$/),
  confirmPassword: yup
    .string()
    .oneOf([yup.ref('password'), ''], 'Passwords tidak sesuai')
    .required('Konfirmasi kata sandi dibutuhkan'),
})

type Props = {
  back: () => void
}

export default function FormPassword(props: Props) {
  const { registerWithEmail } = useRegister()
  const router = useRouter()
  const [data, setData] = useState<FormSchema>(initialState)
  const [errors, setErrors] = useState<ErrorType>(initErrors)

  function handleChange(value: string, name: string) {
    setData((prev) => ({ ...prev, [name]: value }))
    if (name === 'password') validatePassword(value)
  }

  async function handleSubmit(e: FormEvent<HTMLFormElement>) {
    e.preventDefault()
    try {
      await SchemaValidation.validate(data, { abortEarly: false })
      setErrors({
        case: true,
        length: true,
        symbol: true,
        number: true,
      })

      const local = sessionStorage.getItem(KEY_SESSION_STORAGE)
      if (local) {
        const oldData = JSON.parse(local)
        const payload: RegisterPayload = {
          full_name: oldData.fullname,
          email: oldData.email,
          phone_number: oldData.phone_number,
          role_id: oldData.role_id,
          province_id: oldData.province_id,
          regency_id: oldData.regency_id,
          district_id: oldData.district_id,
          village_id: oldData.village_id,
          password: data.password,
          password_confirmation: data.confirmPassword,
        }
        const response = await registerWithEmail(payload)
        if (response.status == 200) {
          sessionStorage.removeItem(KEY_SESSION_STORAGE)
          router.push('/signup/success/' + oldData.email)
        }
      }
    } catch (error: any) {
      extractErrors(error, setErrors)
    }
  }

  function validatePassword(value: string) {
    const errors: any = {}

    if (!/(?=.*\d)/.test(value)) {
      errors.number = false
    } else {
      errors.number = true
    }

    if (!/(?=.*[a-z])(?=.*[A-Z])/.test(value)) {
      errors.case = false
    } else {
      errors.case = true
    }

    if (!/(?=.*[@#$%^&+=!])/.test(value)) {
      errors.symbol = false
    } else {
      errors.symbol = true
    }

    if (value.length < 8) {
      errors.length = false
    } else {
      errors.length = true
    }

    setErrors(errors)
  }

  return (
    <form onSubmit={handleSubmit}>
      <div className={styles['container__input']}>
        <Input
          label='Kata sandi baru'
          variant='password'
          value={data.password}
          setValue={(val) => handleChange(val, 'password')}
          size='md'
          helperText=''
          placeholder='Masukkan Kata Sandi'
          isError={typeof errors?.password == 'string' ? true : false}
        />
        <div className={styles['error']}>
          <Conditions items={requirement} errors={errors} />
        </div>

        <Input
          label='Konfirmasi kata Sandi'
          variant='password'
          value={data.confirmPassword}
          setValue={(val) => handleChange(val, 'confirmPassword')}
          size='md'
          helperText={
            data.password !== '' && typeof errors.confirmPassword == 'string'
              ? errors.confirmPassword
              : ''
          }
          placeholder='Masukkan Kata Sandi'
          isError={
            typeof errors.confirmPassword == 'string' &&
            errors.confirmPassword !== ''
              ? true
              : false
          }
        />
      </div>

      <div className={styles['container__button']}>
        <div className={styles['container__button--top']}>
          <Button
            suffix='Kembali'
            variant='outline'
            color='neutral'
            type='button'
            icon={<IconArrowCircleLeft />}
            onHandleClick={props.back}
          />
          <Button text='Daftar' color='primary' variant='solid' type='submit' />
        </div>
        <div className={styles['container__button--link']}>
          <span>Sudah Punya akun?</span>
          <Link href={'/'}>
            <Button
              text='Masuk'
              color='primary'
              variant='text-link'
              disable={false}
              size='lg'
            />
          </Link>
        </div>
      </div>
    </form>
  )
}

type PropsConditions = {
  items: {
    key: string
    message: string
  }[]
  errors: ErrorType
}

function Conditions(props: PropsConditions) {
  return props.items.map((item, index) => (
    <div key={index} className={styles['error__wrapper']}>
      {props.errors[item.key as keyof ErrorType] ? (
        <IconSuccess backgroundColor='#2CAE70' color='#2CAE70' />
      ) : (
        <IconInfo backgroundColor='#243576' color='#243576' />
      )}

      <span className={styles['error__text']}>{item.message}</span>
    </div>
  ))
}
