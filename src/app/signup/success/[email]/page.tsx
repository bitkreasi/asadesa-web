import styles from './page.module.scss'
import IconAsadesa from '@/app/ui/icons/icon-asadesa'
import Illustration from '../../success-signup.png'
import Image from 'next/image'
import Button from '@/app/ui/components/button/button'
import Link from 'next/link'

type Props = {
  params: {
    email: string
  }
}

export default function Page({ params }: Props) {
  return (
    <section>
      <div className={styles['logo']}>
        <IconAsadesa />
      </div>
      <div className={styles['container']}>
        <Image
          src={Illustration}
          alt='signup success'
          className={styles['container__image']}
        />
        <div className={styles['container__text']}>
          <h1 className={styles['container__text-heading']}>
            Pendaftaran akun anda berhasil!
          </h1>
          <p className={styles['container__text-body']}>
            Kami telah mengirimkan link verifikasi pendaftaran ke email{' '}
            <span
              data-testid='email'
              className={styles['container__text-email']}
            >
              {params.email.replace('%40', '@')}
            </span>
            , silahkan klik link tersebut untuk masuk ke akun anda.
          </p>
        </div>
        <div className={styles['container__button']}>
          <Link href='https://gmail.com' target='_blank'>
            <Button color='primary' text='Buka gmail' />
          </Link>
        </div>
      </div>
    </section>
  )
}
