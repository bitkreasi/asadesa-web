'use client'

import { useState } from 'react'
import styles from './signup.module.scss'
import IconAsadesa from '../ui/icons/icon-asadesa'
import Stepper from '../ui/components/stepper/stepper'
import FormProfile from './form-profile'
import FormAddress from './form-address'
import FormPassword from './form-password'

const textStepper: {
  [key: number]: string
} = {
  0: 'Lengkapi data admin',
  1: 'Lengkapi data desa',
  2: 'Buat kata sandi akun',
}

export default function Page() {
  const [step, setStep] = useState<number>(0)
  const nextStep = () => setStep((prev) => prev + 1)
  const backStep = () => step > 0 && setStep((prev) => prev - 1)

  return (
    <section>
      <div className={styles['logo']}>
        <IconAsadesa />
      </div>
      <div className={styles['container']}>
        <div className={styles['container__text']}>
          <h1 className={styles['container__text--title']}>
            Bergabunglah dengan asadesa!
          </h1>
          <span className={styles['container__text--sub-title']}>
            Daftarkan akun anda dengan mengisi formulir dibawah ini.
          </span>
        </div>
        <div>
          <Stepper activeStep={step} steps={3} text={textStepper[step]} />
          <FormSignup step={step} back={backStep} next={nextStep} />
        </div>
      </div>
    </section>
  )
}

type PropsFormSignup = {
  step: number
  next: () => void
  back: () => void
}

function FormSignup(props: PropsFormSignup) {
  switch (props.step) {
    case 1:
      return <FormAddress back={props.back} next={props.next} />
    case 2:
      return <FormPassword back={props.back} />
    case 0:
    default:
      return <FormProfile next={props.next} />
  }
}
