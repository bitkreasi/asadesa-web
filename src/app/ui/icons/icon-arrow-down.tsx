import { IconProps } from "@/app/shared";

export default function IconArrowDown({
  color = "#272E38",
  backgroundColor = "#272E38",
  opacity = 0.4,
  width = 24,
  height = 24,
}: IconProps) {
  return (
    <svg
      width={width}
      height={height}
      viewBox="0 0 20 20"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        opacity="0.4"
        d="M12.9003 11.025L9.74194 6.81665H5.06695C4.26695 6.81665 3.86695 7.78332 4.43361 8.34998L8.75028 12.6667C9.44195 13.3583 10.5669 13.3583 11.2586 12.6667L12.9003 11.025Z"
        fill={backgroundColor}
      />
      <path
        d="M14.9339 6.81665H9.74219L12.9005 11.025L15.5755 8.34998C16.1339 7.78332 15.7339 6.81665 14.9339 6.81665Z"
        fill={color}
      />
    </svg>
  );
}
