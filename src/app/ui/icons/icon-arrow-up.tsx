import { IconProps } from "@/app/shared";

export default function IconArrowUp({
  color = "#272E38",
  backgroundColor = "#272E38",
  opacity = 0.4,
  width = 24,
  height = 24,
}: IconProps) {
  return (
    <svg
      width={width}
      height={height}
      viewBox="0 0 24 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        opacity="0.4"
        d="M15.48 10.77L11.69 15.82H6.07999C5.11999 15.82 4.63999 14.66 5.31999 13.98L10.5 8.79999C11.33 7.96999 12.68 7.96999 13.51 8.79999L15.48 10.77Z"
        fill={backgroundColor}
      />
      <path
        d="M17.92 15.82H11.69L15.48 10.77L18.69 13.98C19.36 14.66 18.88 15.82 17.92 15.82Z"
        fill={color}
      />
    </svg>
  );
}
