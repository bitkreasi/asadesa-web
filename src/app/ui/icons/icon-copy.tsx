import { IconProps } from "@/app/shared";

export default function IconCopy({
  color = "#272E38",
  backgroundColor = "#272E38",
  opacity = 0.4,
  width = 24,
  height = 24,
}: IconProps) {
  return (
    <svg
      width={width}
      height={height}
      viewBox="0 0 22 22"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M14.6673 11.8252V15.6752C14.6673 18.8835 13.384 20.1668 10.1757 20.1668H6.32565C3.11732 20.1668 1.83398 18.8835 1.83398 15.6752V11.8252C1.83398 8.61683 3.11732 7.3335 6.32565 7.3335H10.1757C13.384 7.3335 14.6673 8.61683 14.6673 11.8252Z"
        fill={backgroundColor}
      />
      <path
        opacity="0.4"
        d="M15.6743 1.8335H11.8243C8.6618 1.8335 7.37846 3.08933 7.3418 6.18766H10.1743C14.0243 6.18766 15.8118 7.97516 15.8118 11.8252V14.6577C18.9101 14.621 20.166 13.3377 20.166 10.1752V6.32516C20.166 3.11683 18.8826 1.8335 15.6743 1.8335Z"
        fill={color}
      />
    </svg>
  );
}
