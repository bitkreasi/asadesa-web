import { IconProps } from "@/app/shared";

const IconArrowRight = ({
  color = "#272E38",
  backgroundColor = "#272E38",
  opacity = 0.4,
  width = 24,
  height = 24,
}: IconProps) => {
  return (
    <svg
      width={width}
      height={height}
      viewBox="0 0 24 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        opacity="0.4"
        d="M13.2297 8.51999L8.17969 12.31V17.92C8.17969 18.88 9.33969 19.36 10.0197 18.68L15.1997 13.5C16.0297 12.67 16.0297 11.32 15.1997 10.49L13.2297 8.51999Z"
        fill={backgroundColor}
      />
      <path
        d="M8.17969 6.07999V12.31L13.2297 8.51999L10.0197 5.30999C9.33969 4.63999 8.17969 5.11999 8.17969 6.07999Z"
        fill={color}
      />
    </svg>
  );
};

export default IconArrowRight;
