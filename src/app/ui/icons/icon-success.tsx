import { IconProps } from "@/app/shared";

export default function IconSuccess({
  color = "#272E38",
  backgroundColor = "#272E38",
  opacity = 0.4,
  width = 24,
  height = 24,
}: IconProps) {
  return (
    <svg
      width={width}
      height={height}
      viewBox="0 0 24 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        opacity={opacity}
        d="M12 22C17.5228 22 22 17.5228 22 12C22 6.47715 17.5228 2 12 2C6.47715 2 2 6.47715 2 12C2 17.5228 6.47715 22 12 22Z"
        fill={backgroundColor}
      />
      <path
        d="M10.5795 15.58C10.3795 15.58 10.1895 15.5 10.0495 15.36L7.21945 12.53C6.92945 12.24 6.92945 11.76 7.21945 11.47C7.50945 11.18 7.98945 11.18 8.27945 11.47L10.5795 13.77L15.7195 8.63001C16.0095 8.34001 16.4895 8.34001 16.7795 8.63001C17.0695 8.92001 17.0695 9.40001 16.7795 9.69001L11.1095 15.36C10.9695 15.5 10.7795 15.58 10.5795 15.58Z"
        fill={color}
      />
    </svg>
  );
}
