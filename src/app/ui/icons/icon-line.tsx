import { IconProps } from "@/app/shared";

export default function IconLine({
  color = "#272E38",
  backgroundColor = "#272E38",
  opacity = 0.4,
  width = 24,
  height = 24,
}: IconProps) {
  return (
    <svg
      width="1"
      height="40"
      viewBox="0 0 1 40"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <line x1="0.5" y1="40" x2="0.5" stroke="#E8E8E8" />
    </svg>
  );
}
