import { IconProps } from "@/app/shared";

export default function IconLocation({
  color = "#272E38",
  backgroundColor = "#272E38",
  opacity = 0.4,
  width = 24,
  height = 24,
}: IconProps) {
  return (
<svg width={width} height={height} viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
<path opacity={opacity} d="M18.9017 7.74518C17.9392 3.51018 14.245 1.60352 11 1.60352C11 1.60352 11 1.60352 10.9908 1.60352C7.755 1.60352 4.05166 3.50102 3.08916 7.73602C2.01666 12.466 4.91333 16.4718 7.535 18.9927C8.50666 19.9277 9.75333 20.3952 11 20.3952C12.2467 20.3952 13.4933 19.9277 14.4558 18.9927C17.0775 16.4718 19.9742 12.4752 18.9017 7.74518Z" fill={backgroundColor}/>
<path d="M11 12.3375C12.5947 12.3375 13.8875 11.0447 13.8875 9.45C13.8875 7.85528 12.5947 6.5625 11 6.5625C9.40527 6.5625 8.11249 7.85528 8.11249 9.45C8.11249 11.0447 9.40527 12.3375 11 12.3375Z" fill={color}/>
</svg>
  );
}

