import styles from "./spinner.module.scss";

/* eslint-disable-next-line */
export interface SpinnerProps {}

export default function Spinner(props: SpinnerProps) {
  return <span className={styles["loader"]}></span>;
}
