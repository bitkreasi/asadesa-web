import React from "react";
import styles from "./card.module.scss";
import { Text } from "../typography/typography";

type CardProps = React.ComponentPropsWithoutRef<"div">;

const Card = ({ className, children, ...props }: CardProps) => {
  return (
    <div className={`${styles["card__wrapper"]} ${className}`} {...props}>
      {children}
    </div>
  );
};

const CardHeader = ({ className, children, ...props }: CardProps) => {
  return (
    <div className={`${styles["card__header"]} ${className}`} {...props}>
      {children}
    </div>
  );
};

type CardTitleProps = React.ComponentPropsWithoutRef<"p">;
const CardTitle = ({ className, children, ...props }: CardTitleProps) => {
  return (
    <Text
      variant="display"
      size="xs"
      fontStyle="medium"
      className={`${styles["card__title"]} ${className}`}
      {...props}
    >
      {children}
    </Text>
  );
};

type CardDescriptionProps = React.ComponentPropsWithoutRef<"p">;
const CardDescription = ({
  className,
  children,
  ...props
}: CardDescriptionProps) => {
  return (
    <Text
      variant="text"
      size="md"
      fontStyle="regular"
      muted
      className={`${styles["card__description"]} ${className}`}
      {...props}
    >
      {children}
    </Text>
  );
};

const CardFooter = ({ className, children, ...props }: CardProps) => {
  return (
    <div className={`${styles["card__footer"]} ${className}`} {...props}>
      {children}
    </div>
  );
};

const CardContent = ({ className, children, ...props }: CardProps) => {
  return (
    <div className={`${styles["card__content"]} ${className}`} {...props}>
      {children}
    </div>
  );
};

export {
  Card,
  CardHeader,
  CardTitle,
  CardDescription,
  CardContent,
  CardFooter,
};
