import React from "react";
import styles from "./modal.module.scss";
import { Text } from "../../typography/typography";
import IconCloseCircle from "../../../icons/icon-close-circle";

function useModal() {
  const [openModal, setOpenModal] = React.useState(false);
  return { openModal, setOpenModal };
}

type ModalContext = {
  openModal: boolean;
  setOpenModal: (value: boolean) => void;
};

interface ModalProps extends ModalContext {
  children: React.ReactNode;
}
const Modal = (props: ModalProps) => {
  return (
    props.openModal && (
      <div className={styles["wrapper"]}>
        <div className={styles["container"]} {...props}>
          <span
            onClick={() => {
              props.setOpenModal(false);
            }}
            className={styles["close__icon"]}
          >
            <IconCloseCircle />
          </span>
          <div>{props.children}</div>
        </div>
      </div>
    )
  );
};

type ModalHeaderProps = React.HTMLAttributes<HTMLDivElement>;
const ModalHeader = React.forwardRef<HTMLDivElement, ModalHeaderProps>(
  ({ className, children, ...props }, ref) => (
    <div className={`${styles["header"]} ${className}`} ref={ref} {...props}>
      {children}
    </div>
  ),
);
ModalHeader.displayName = "ModalHeader";

type ModalTitleProps = React.ComponentPropsWithRef<"p">;
const ModalTitle = React.forwardRef<HTMLParagraphElement, ModalTitleProps>(
  ({ className, children, ...props }, ref) => (
    <Text
      ref={ref}
      variant="text"
      size="lg"
      fontStyle="medium"
      className={`${styles["title"]} ${className}`}
      {...props}
    >
      {children}
    </Text>
  ),
);
ModalTitle.displayName = "ModalTitle";

type ModalDescriptionProps = React.ComponentPropsWithRef<"p">;
const ModalDescription = React.forwardRef<
  HTMLParagraphElement,
  ModalDescriptionProps
>(({ className, children, ...props }, ref) => (
  <Text
    ref={ref}
    variant="text"
    size="md"
    fontStyle="regular"
    className={`${styles["description"]} ${className}`}
    {...props}
  >
    {children}
  </Text>
));
ModalDescription.displayName = "ModalDescription";

type ModalContentProps = React.HTMLAttributes<HTMLDivElement>;
const ModalContent = React.forwardRef<HTMLDivElement, ModalContentProps>(
  ({ className, children, ...props }, ref) => (
    <div ref={ref} className={`${styles["content"]} ${className}`} {...props}>
      {children}
    </div>
  ),
);
ModalContent.displayName = "ModalContent";

type DialogFooterProps = React.HTMLAttributes<HTMLDivElement>;
const ModalFooter = React.forwardRef<HTMLDivElement, DialogFooterProps>(
  ({ className, children, ...props }, ref) => (
    <div ref={ref} className={`${styles["footer"]} ${className}`} {...props}>
      {children}
    </div>
  ),
);
ModalFooter.displayName = "ModalFooter";

export {
  useModal,
  Modal,
  ModalHeader,
  ModalTitle,
  ModalDescription,
  ModalContent,
  ModalFooter,
};
