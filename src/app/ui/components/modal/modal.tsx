import IconCloseCircle from "../../icons/icon-close-circle";
import Button from "../button/button";
import styles from "./style.module.scss";
type Props = {
  children: React.ReactNode;
  isOpen: boolean;
  toggle: () => any;
};
export default function Modal(props: Props) {
  if (!!props.isOpen)
    return (
      <div className={styles.wrapper}>
        <div className={styles.layer} onClick={props.toggle} />
        <div className={styles.modal}>
          <div className={styles["modal__top"]}>
            <button onClick={props.toggle}>
              <IconCloseCircle />
            </button>
          </div>
          <div className={styles["modal__body"]}>{props.children}</div>
        </div>
      </div>
    );
}
