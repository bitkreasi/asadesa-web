import styles from "./detail-data.module.scss";

/* eslint-disable-next-line */
export interface DetailDataProps {
  size?: "sm" | "md";
  title?: string;
  subTitle?: string;
}

export function DetailData({
  size = "sm",
  title = "Lorem ipsum",
  subTitle = "Lorem ipsum",
}: DetailDataProps) {
  return (
    <div className={`${styles["container"]} ${styles[`container-${size}`]}`}>
      <span className={`${styles[`container__title-${size}`]}`}>{title}</span>
      <span className={`${styles[`container__subTitle-${size}`]}`}>
        {subTitle}
      </span>
    </div>
  );
}

export default DetailData;
