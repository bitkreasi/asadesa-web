import IconDanger from '../../icons/icon-danger'
import IconInfo from '../../icons/icon-info'
import IconTickCircle from '../../icons/icon-tick-circle'
import styles from './alert.module.scss'

type Variant = 'SUCCESS' | 'WARNING' | 'DESCTRUCTIVE' | 'INFO'

type Props = {
  variant: Variant
  title: string
  message: string
}

export default function Alert(props: Props) {
  return (
    <div className={styles[`alert--${props.variant}`]}>
      <div>
        <div className={styles[`alert__icon-wrapper--${props.variant}`]}>
          <AlertIcon variant={props.variant} />
        </div>
      </div>
      <div>
        <p className={styles['alert__title']}>{props.title}</p>
        <p className={styles['alert__message']}>{props.message}</p>
      </div>
    </div>
  )
}

type AlertProps = {
  variant: Variant
}

const COLORS: {
  [key in Variant]: string
} = {
  INFO: '#E4FAFF',
  SUCCESS: '#edfbf6',
  DESCTRUCTIVE: '#ffe8e8',
  WARNING: '#fffaf2',
}

function AlertIcon(props: AlertProps) {
  switch (props.variant) {
    case 'DESCTRUCTIVE':
    case 'WARNING':
      return <IconDanger backgroundColor={COLORS[props.variant]} color='#fff' />
    case 'INFO':
      return <IconInfo backgroundColor={COLORS[props.variant]} color='#fff' />
    case 'SUCCESS':
    default:
      return (
        <IconTickCircle backgroundColor={COLORS[props.variant]} color='#fff' />
      )
  }
}
