"use client";

import React from "react";
import styles from "./tabs.module.scss";

type ComponentProps = React.ComponentPropsWithoutRef<"div">;

const TabsContext = React.createContext<{
  activeTab: string;
  setActiveTab: React.Dispatch<React.SetStateAction<string>>;
} | null>(null);

export type TabsProps = {
  defaultValue: string;
} & ComponentProps;

export const Tabs = ({
  defaultValue,
  children,
  className,
  ...props
}: TabsProps) => {
  const [activeTab, setActiveTab] = React.useState<string>(defaultValue);
  return (
    <TabsContext.Provider value={{ activeTab, setActiveTab }}>
      <div className={`${styles["tabs"]} ${className}`} {...props}>
        {children}
      </div>
    </TabsContext.Provider>
  );
};

export type TabsListProps = ComponentProps;

export const TabsList = ({ className, children, ...props }: TabsListProps) => {
  return (
    <div className={`${styles["list"]} ${className}`} {...props}>
      {children}
    </div>
  );
};

export type TabsTriggerProps = { value: string } & ComponentProps;

export const TabsTrigger = ({
  value,
  className,
  children,
  ...props
}: TabsTriggerProps) => {
  const tabsContext = React.useContext(TabsContext);
  return (
    <div className={styles["dialog__container"]}>
      <div
        className={`
        ${styles["trigger"]} 
        ${className} 
        ${tabsContext?.activeTab === value ? styles["active"] : styles["disabled"]}
      `}
        onClick={() => tabsContext?.setActiveTab(value)}
        {...props}
      >
        {children}
      </div>
    </div>
  );
};

export type TabsContentProps = { value: string } & ComponentProps;

export const TabsContent = ({
  value,
  className,
  children,
  ...props
}: TabsContentProps) => {
  const tabsContext = React.useContext(TabsContext);
  return tabsContext?.activeTab === value ? (
    <div className={`${styles["content"]} ${className}`} {...props}>
      {children}
    </div>
  ) : null;
};
