import React from "react";
import styles from "./label.module.scss";

const Label = React.forwardRef<
  HTMLLabelElement,
  React.ComponentPropsWithRef<"label">
>(({ className, children, htmlFor, ...props }, ref) => {
  return (
    <label className={`${styles["label"]} ${className}`} {...props} ref={ref}>
      {children}
    </label>
  );
});
Label.displayName = "Label";

export { Label };
