import IconDocument from "../../icons/icon-document";
import Button from "../button/button";
import styles from "./empty-state.module.scss";

/* eslint-disable-next-line */
export interface EmptyStatesProps {
  title: string;
  subTitle: string;
  primaryText?: string;
  secondaryText?: string;
  prefixPrimary?: React.ReactNode;
  prefixSecondary?: React.ReactNode;
  suffixPrimary?: React.ReactNode;
  suffixSecondary?: React.ReactNode;
}

export function EmptyStates({
  title = "Tidak ada yang bisa dilihat disini",
  subTitle = "Tidak ada hasil atau data untuk ditampilkan di sini. Silakan pilih tindakan di bawah untuk melanjutkan",
  primaryText = "primary",
  secondaryText = "secondary",
  prefixPrimary,
  prefixSecondary,
  suffixPrimary,
  suffixSecondary,
}: EmptyStatesProps) {
  return (
    <div className={`${styles.container}`}>
      <div className={`${styles.iconContainer}`}>
        <IconDocument width={32} height={32} />
      </div>
      <div className={`${styles.title}`}>{title}</div>
      <div className={`${styles.subTitle}`}>{subTitle}</div>
      <div className={`${styles.buttonContainer}`}>
        <div style={{ border: "none" }}>
          <Button
            text={secondaryText}
            variant="outline"
            color="primary"
            suffix={suffixSecondary}
            prefix={prefixSecondary}
          />
        </div>
        <div>
          <Button
            text={primaryText}
            variant="solid"
            color="primary"
            suffix={suffixPrimary}
            prefix={prefixPrimary}
          />
        </div>
      </div>
    </div>
  );
}

export default EmptyStates;
