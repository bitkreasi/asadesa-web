"use client";
import React, { useContext } from "react";
import styles from "./accordion.module.scss";
import IconArrowDown from "../../icons/icon-arrow-down";
import IconArrowUp from "../../icons/icon-arrow-up";

export type AccordionProps = React.ComponentPropsWithRef<"div">;

export const Accordion = React.forwardRef<HTMLDivElement, AccordionProps>(
  ({ children, className, ...props }, ref) => {
    return (
      <div
        className={`${styles["accordion"]} ${className}}`}
        ref={ref}
        {...props}
      >
        {children}
      </div>
    );
  },
);

Accordion.displayName = "Accordion";

const AccordionContext = React.createContext<{
  isOpen: boolean;
  setIsOpen: React.Dispatch<React.SetStateAction<boolean>>;
} | null>(null);

export type AccordionItemProps = React.ComponentPropsWithoutRef<"div">;
export const AccordionItem = ({
  children,
  className,
  ...props
}: AccordionItemProps) => {
  const [isOpen, setIsOpen] = React.useState<boolean>(false);
  return (
    <AccordionContext.Provider value={{ isOpen, setIsOpen }}>
      <div className={`${styles["accord_item"]} ${className}`} {...props}>
        {children}
      </div>
    </AccordionContext.Provider>
  );
};

export type AccordionTriggerProps = React.ComponentPropsWithoutRef<"div">;

export const AccordionTrigger = ({
  className,
  children,
  ...props
}: AccordionTriggerProps) => {
  const context = useContext(AccordionContext);
  return (
    context && (
      <div
        onClick={() => {
          context.setIsOpen(!context.isOpen);
        }}
        className={`${styles["trigger"]} ${className}`}
        {...props}
      >
        <div>
          <div>{children}</div>
        </div>
        {context.isOpen ? <IconArrowUp /> : <IconArrowDown />}
      </div>
    )
  );
};

export type AccordionContentProps = React.ComponentPropsWithoutRef<"div">;

export const AccordionContent = ({
  children,
  className,
  ...props
}: AccordionContentProps) => {
  const context = useContext(AccordionContext);
  return (
    context?.isOpen && (
      <div className={`${styles["content"]} ${className}`} {...props}>
        {children}
      </div>
    )
  );
};
