import React from "react";
import styles from "./button.module.scss";
import Spinner from "../../animations/spinner/spinner";

export interface ButtonProps {
  text?: string;
  icon?: React.ReactNode;
  type?: "button" | "submit" | "reset";
  color?: "primary" | "secondary" | "neutral" | "error";
  variant?: "solid" | "outline" | "text" | "text-link";
  size?: "xs" | "sm" | "md" | "lg";
  width?: "fit" | "max";
  disable?: boolean;
  prefix?: React.ReactNode;
  suffix?: React.ReactNode;
  onHandleClick?: () => void;
  isLoading?: boolean;
}

export default function Button({
  text,
  icon,
  type = "button",
  color = "primary",
  variant = "solid",
  size = "md",
  disable = false,
  width = "fit",
  prefix,
  suffix,
  onHandleClick,
  isLoading,
}: ButtonProps) {
  return (
    <button
      type={type}
      className={`
    ${styles.container}
    ${styles[size]}
    ${styles[`text-link-${size}`]}
    ${styles[`${color}_${variant}`]}
    ${styles[width]}
    ${disable && styles[`${color}_${variant}-disable`]}
    ${text === '' ? styles['container--without-text']: ''}
    `}
      disabled={disable}
      onClick={onHandleClick}
    >
      {!isLoading ? (
        <>
          {prefix}
          <span className={styles.text}>{text || icon}</span>
          {suffix}
        </>
      ) : (
        <Spinner />
      )}
    </button>
  );
}
