"use client";

import { useRouter } from "next/navigation";
import styles from "./header.module.scss";
import IconArrowLeft from "../../icons/icon-arrow-left";
import Breadcrumbs, { BreadcrumbItem } from "../breadcrumb/breadcrumb";

/* eslint-disable-next-line */
export interface HeaderProps {
  size?: "sm" | "md" | "lg" | "xl";
  title?: string;
  subTitle?: string;
  items?: BreadcrumbItem[];
  prefix?: React.ReactNode;
  onPrefixClick?: () => void;
  isBack?: boolean;
}

export function Header({
  size = "xl",
  title = "Lorem Ipsum",
  subTitle = "Lorem Ipsum",
  items = [],
  isBack = false,
  prefix,
  onPrefixClick,
}: HeaderProps) {
  const router = useRouter();

  return (
    <div className={styles["container"]}>
      <div className={size === "xl" ? styles["container__content-xl"] : ""}>
        {size === "xl" && <Breadcrumbs items={items} />}

        <div
          className={`${isBack ? styles["container__content-xl--back"] : ""} ${
            size === "lg" && styles["container__content--lg"]
          }`}
        >
          {isBack && (
            <button
              className={styles["container__content-xl--back__prev-button"]}
              onClick={() => router.back()}
            >
              <IconArrowLeft width={22} height={22} />
              {/* <IconArrowLeft name="left-arrow-2" width={22} height={22} /> */}
            </button>
          )}
          {prefix && (
            // * you can change the button with Link from next/link
            <button
              className={styles["container__content--prefix-button"]}
              onClick={onPrefixClick && onPrefixClick}
            >
              {prefix}
            </button>
          )}
          <div className={styles["container__content--text"]}>
            <p
              className={`${styles["container__content--text__title"]} ${
                styles[`container__content--text__title-${size}`]
              }`}
            >
              {title}
            </p>
            {subTitle && (
              <p
                className={`${styles["container__content--text__sub-title"]} ${
                  styles[`container__content--text__sub-title-${size}`]
                }`}
              >
                {subTitle}
              </p>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}

export default Header;
