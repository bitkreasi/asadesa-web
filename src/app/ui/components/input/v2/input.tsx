import React from "react";
import styles from "./input.module.scss";

export interface InputProps
  extends React.InputHTMLAttributes<HTMLInputElement> {
  prefixIcon?: React.ReactNode;
}

const Input = React.forwardRef<HTMLInputElement, InputProps>(
  ({ className, prefixIcon, type, ...props }, ref) => {
    return prefixIcon ? (
      <div className={styles["container"]}>
        <i className={styles["icon"]}>{prefixIcon}</i>
        <input
          type={type}
          style={{ paddingLeft: 40 }}
          className={`${styles["input"]} ${className}`}
          ref={ref}
          {...props}
        />
      </div>
    ) : (
      <input
        type={type}
        className={`${styles["input"]}  ${className}`}
        ref={ref}
        {...props}
      />
    );
  },
);
Input.displayName = "Input";

export { Input };
