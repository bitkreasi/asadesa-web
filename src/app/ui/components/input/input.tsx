import { useEffect, useRef, useState } from "react";
// import Icon from '../icon/icon';
import styles from "./input.module.scss";
import IconEye from "../../icons/icon-eye";
import IconEyeSlash from "../../icons/icon-eye-slash";
import IconArrowUp from "../../icons/icon-arrow-up";
import IconArrowDown from "../../icons/icon-arrow-down";
import Dropdown from "../dropdown/dropdown";
// import { DayPicker } from 'react-day-picker';
// import "react-day-picker/dist/style.css";
// import { format } from 'date-fns';
// import Dropdown from '../dropdown/dropdown';

/* eslint-disable-next-line */
export interface InputProps {
  variant?:
    | "text-field"
    | "text-area"
    | "password"
    | "dropdown"
    | "datetime"
    | "dropdown-select"
    | "time"
    | "dropdown-menu";
  size?: "sm" | "md";
  label: string;
  placeholder?: string;
  helperText?: string;
  // helpertext for side by side
  helperTextCol?: string;
  value?: string;
  setValue?: (e: string) => void;
  isDisable?: boolean;
  isError?: boolean;
  // max value for the 'text-area' variant
  maxLength?: number;
  minLength?: number;
  // items for 'dropdown' variant
  items?: string[];
  suffix?: React.ReactNode;
  prefix?: React.ReactNode;
  onHandleClick?: () => void;
  // text for 'dropdown-menu' variant
  text?: string;
  // date type for 'datepicker' variant
  date?: Date;
  setDate?: (e: Date) => void;
  // search functionality for 'dropdown-select' variant
  withSearch?: boolean;
  // custom input type for the input tag
  textFieldType?: React.HTMLInputTypeAttribute;
  // filter the dropdown items based on the input value for 'dropdown-select' variant
  isDropdownFiltered?: boolean;
  // auto focus for the 'text-field' variant
  isFocus?: boolean;
}

export default function Input({
  variant = "text-field",
  size = "md",
  label = "Label",
  placeholder = "Lorem Ipsum",
  helperText = "Helper Text",
  value = "",
  setValue,
  isDisable = false,
  isError = false,
  maxLength = 100,
  minLength = 0,
  items = [
    "Lorem Ipsum",
    "Lorem Ipsum",
    "Dolor Sit Amet",
    "Consectetur Adipiscing Elit",
    "Sed Do Eiusmod Tempor Incididunt",
  ],
  suffix,
  text,
  prefix,
  onHandleClick,
  date,
  setDate,
  withSearch = true,
  textFieldType = "text",
  isDropdownFiltered = false,
  helperTextCol = "",
  isFocus = false,
}: InputProps) {
  const [showPassword, setShowPassword] = useState(false);
  const [hideItem, setHideItem] = useState(true);
  const [showDatepicker, setShowDatepicker] = useState(false);
  const [isOpen, setIsOpen] = useState(false);
  const [isOpenDropWithSelect, setIsOpenDropWithSelect] =
    useState<boolean>(false);

  // type for the 'time' variant
  const [type, setType] = useState("text");

  // selected value for dropdown select
  const [actualValue, setActualValue] = useState("");
  const [localValue, setLocalValue] = useState("")

  const dropdownRef = useRef<HTMLDivElement>(null);
  const dropdownWithSelectRef = useRef<HTMLDivElement>(null);
  const dayPickerRef = useRef<HTMLDivElement>(null);

  const handleClickOutside = (e: MouseEvent) => {
    if (
      (dropdownRef.current &&
        !dropdownRef.current.contains(e.target as Node)) ||
      (dayPickerRef.current && !dayPickerRef.current.contains(e.target as Node))
    ) {
      setHideItem(true);
      setShowDatepicker(false);
      setIsOpen(false);
    }
  };

  // useMemo(() => {
  //   document.addEventListener("mousedown", handleClickOutside);
  //   return () => {
  //     document.removeEventListener("mousedown", handleClickOutside);
  //   };
  // }, []);

  // const filteredItem = items?.filter((item: string) => {
  //   return item.toLowerCase().includes(value.toLowerCase());
  // });

  const handleClickItem = (item: string) => {
    setValue?.(item);
    setHideItem(true);
  };

  useEffect(() => {
    const handleClick = (event: MouseEvent) => {
      if (
        dropdownWithSelectRef.current &&
        !dropdownWithSelectRef.current.contains(event.target as Node)
      ) {
        setIsOpenDropWithSelect(false);
      }
    };

    document.addEventListener("mousedown", handleClick);

    return () => {
      document.removeEventListener("mousedown", handleClick);
    };
  }, [isOpenDropWithSelect]);

  return (
    <div className={styles["container"]}>
      {variant === "text-area" && (
        <>
          <div className={styles["container__text-area"]}>
            <label
              htmlFor={label}
              className={`${styles[`container__text-area--label-${size}`]} ${
                isDisable && styles["container__content--label-disable"]
              }`}
            >
              {label}
            </label>
            <textarea
              id={label}
              className={`${styles["container__content"]} ${
                styles[`container__text-area--content-${size}`]
              } ${isError && styles["container__content--error"]} ${
                isDisable && styles["container__content--disable"]
              }`}
              placeholder={placeholder}
              value={value}
              onChange={(e) => setValue?.(e.target.value)}
              disabled={isDisable}
              maxLength={maxLength}
            />
            <span
              className={`${
                styles[`container__text-area--text-length-${size}`]
              } ${styles["container__text-area--text-length"]}`}
            >
              {value.length}/{maxLength}
            </span>
          </div>
          <span
            className={`${styles["container__helper-text"]} ${
              styles[`container__helper-text--${size}`]
            }`}
          >
            {helperText}
          </span>
        </>
      )}

      {variant === "text-field" && (
        <>
          <div className={styles["container__text-field"]}>
            {helperTextCol ? (
              <div className={styles["container__text-field--helper-text-col"]}>
                <label
                  htmlFor={label}
                  className={`${
                    styles[`container__text-field--label-${size}`]
                  } ${
                    isDisable && styles["container__content--label-disable"]
                  }`}
                >
                  {label}
                </label>
                <span
                  className={
                    styles["container__text-field--helper-text-col__text"]
                  }
                >
                  {helperTextCol}
                </span>
              </div>
            ) : (
              <label
                htmlFor={label}
                className={`${styles[`container__text-field--label-${size}`]} ${
                  isDisable && styles["container__content--label-disable"]
                }`}
              >
                {label}
              </label>
            )}

            <div className={styles["container__text-field--icon-content"]}>
              {prefix && (
                <button
                  className={
                    styles["container__text-field--icon-content__prefix"]
                  }
                  onClick={onHandleClick}
                >
                  {prefix}
                </button>
              )}
              <input
                id={label}
                className={`${styles["container__content"]} ${
                  styles[`container__text-field--content-${size}`]
                } ${isError && styles["container__content--error"]} ${
                  isDisable && styles["container__content--disable"]
                } ${prefix && styles[`container__content--prefix-${size}`]}`}
                placeholder={placeholder}
                value={value}
                onChange={(e) => setValue?.(e.target.value)}
                disabled={isDisable}
                type={textFieldType}
                autoFocus={isFocus}
                min={minLength}
              />
              {suffix && (
                <span
                  className={`${
                    styles["container__text-field--icon-content__suffix"]
                  } ${
                    styles[
                      `container__text-field--icon-content__suffix-${size}`
                    ]
                  }`}
                >
                  {suffix}
                </span>
              )}
            </div>
          </div>
          <span
            className={`${styles["container__helper-text"]} ${
              styles[`container__helper-text--${size}`]
            }`}
          >
            {helperText}
          </span>
        </>
      )}

      {variant === "password" && (
        <>
          <div className={styles["container__password"]}>
            <label
              htmlFor={label}
              className={`${styles[`container__password--label-${size}`]} ${
                isDisable && styles["container__content--label-disable"]
              }`}
            >
              {label}
            </label>
            <div className={styles["container__password--base"]}>
              <input
                id={label}
                className={`${styles["container__content"]} ${
                  styles[`container__password--content-${size}`]
                } ${isError && styles["container__content--error"]} ${
                  isDisable && styles["container__content--disable"]
                } ${prefix && styles[`container__content--prefix-${size}`]} ${
                  !showPassword && styles["container__password--input"]
                }`}
                placeholder={placeholder}
                value={value}
                onChange={(e) => setValue?.(e.target.value)}
                disabled={isDisable}
                type={showPassword ? "text" : "password"}
              />
              <span
                className={styles["container__password--icon"]}
                onClick={() => setShowPassword((prev) => !prev)}
              >
                {showPassword ? <IconEye /> : <IconEyeSlash />}
              </span>
            </div>
          </div>
          <span
            className={`${styles["container__helper-text"]} ${
              styles[`container__helper-text--${size}`]
            }`}
          >
            {helperText}
          </span>
        </>
      )}

      {/* dropdown-autocomplete */}
      {variant === "dropdown" && (
        <>
          <div className={styles["container__dropdown"]} ref={dropdownRef}>
            <label
              htmlFor={label}
              className={`${styles[`container__dropdown--label-${size}`]} ${
                isDisable && styles["container__content--label-disable"]
              }`}
            >
              {label}
            </label>
            <div className={styles["container__dropdown--icon-content"]}>
              {prefix && (
                <button
                  className={
                    styles["container__dropdown--icon-content__prefix"]
                  }
                  onClick={onHandleClick}
                >
                  {prefix}
                </button>
              )}
              <input
                id={label}
                className={`${styles["container__content"]} ${
                  styles[`container__password--content-${size}`]
                } ${isError && styles["container__content--error"]} ${
                  isDisable && styles["container__content--disable"]
                } ${prefix && styles[`container__content--prefix-${size}`]} `}
                placeholder={placeholder}
                value={value}
                onChange={(e) => {
                  setValue?.(e.target.value);
                  setHideItem(false);
                }}
                disabled={isDisable}
                type="text"
              />
              <button
                className={styles["container__dropdown--icon"]}
                onClick={() => setHideItem((prev) => !prev)}
              >
                {/* <Icon name={!hideItem && value ? 'up-arrow' : 'bottom-arrow'} /> */}
              </button>
            </div>

            <div
              className={`${styles[`container__dropdown--items`]} ${
                styles[`container__dropdown--items-${size}`]
              } ${hideItem && styles["container__dropdown--items-hide"]}`}
            >
              {/* {filteredItem?.length > 0 && value ? (
                filteredItem?.map((item: string) => (
                  <span
                    className={`${styles['container__dropdown--item']} ${
                      styles[`container__dropdown--item-${size}`]
                    }`}
                    onClick={() => handleClickItem(item)}
                  >
                    {item}
                  </span>
                ))
              ) : (
                <span>No Item Found</span>
              )} */}
            </div>
          </div>
          <span
            className={`${styles["container__helper-text"]} ${
              styles[`container__helper-text--${size}`]
            }`}
          >
            {helperText}
          </span>
        </>
      )}

      {/* dropdown-with-items */}
      {variant === "dropdown-select" && (
        <div
          className={styles["container__dropdown-select"]}
          ref={dropdownWithSelectRef}
        >
          <label
            className={`${
              styles[`container__dropdown-select--label-${size}`]
            } ${isDisable && styles["container__content--label-disable"]}`}
          >
            {label}
          </label>
          <div className={styles["container__dropdown-select--base"]}>
            <div
              onClick={() => setIsOpenDropWithSelect((prev) => !prev)}
              className={`${styles["container__content"]} ${
                styles[`container__dropdown-select--input-${size}`]
              } ${isError && styles["container__content--error"]} ${
                isDisable && styles["container__content--disable"]
              } ${!value && styles[`container__dropdown-select--placeholder`]}`}
            >
              {value ? value : localValue || placeholder}
            </div>
            <div className={styles["container__dropdown-select--prefix"]}>
              {isOpenDropWithSelect ? <IconArrowUp /> : <IconArrowDown />}
            </div>
          </div>
          <span
            className={[
              styles["container__helper-text"],
              styles[`container__helper-text--${size}`],
            ].join("")}
          >
            {helperText}
          </span>
          {!!isOpenDropWithSelect && (
            <Dropdown
              items={items}
              isHelperText={helperText !== ""}
              withSearch
              size="small"
              setDropdownSelectValue={setValue ? setValue : setLocalValue}
              setIsOpen={setIsOpenDropWithSelect}
            />
          )}
        </div>
      )}

      {/* only dropdown button */}
      {variant === "dropdown-menu" && (
        <button
          className={styles["container__dropdown-menu"]}
          onClick={onHandleClick}
        >
          {prefix}
          {text}
          {suffix}
        </button>
      )}

      {variant === "datetime" && (
        <div className={styles["container__datetime"]}>
          <label
            htmlFor={label}
            className={`${styles[`container__datetime--label`]} ${
              isDisable && styles["container__content--label-disable"]
            }`}
          >
            {label}
          </label>
          <div className={styles["container__datetime--base"]}>
            <input
              id="input"
              className={`${styles["container__content"]} ${
                styles["container__datetime--input-date"]
              }   ${isError && styles["container__content--error"]} ${
                isDisable && styles["container__content--disable"]
              }`}
              placeholder={placeholder}
              disabled={isDisable}
              type="text"
              // ! ERROR: the date props is a object type
              // value={date ? format(date, 'dd/MM/yyyy') : ''}
              // value={
              //   typeof date == 'object' ? format(date, 'dd/MM/yyyy') : date
              // }
              onClick={() => setShowDatepicker(true)}
              autoComplete="off"
            />
            <button className={styles["container__datetime--button"]}>
              {/* <Icon name="calendar" width={20} height={20} /> */}
            </button>
          </div>

          {showDatepicker && (
            <div
              ref={dayPickerRef}
              className={styles["container__datetime--datepicker"]}
            >
              {/* <DayPicker
                mode="single"
                selected={date}
                onDayClick={(day) => (setDate ? setDate(day) : null)}
                className={styles['container__datetime--datepicker__base']}
              /> */}
            </div>
          )}

          <span
            className={`${styles["container__helper-text"]} ${
              styles[`container__helper-text--${size}`]
            }`}
          >
            {helperText}
          </span>
        </div>
      )}

      {variant === "time" && (
        <div className={styles["container__time"]}>
          <label
            htmlFor={label}
            className={`${styles[`container__time--label-${size}`]} ${
              isDisable && styles["container__content--label-disable"]
            }`}
          >
            {label}
          </label>
          <div className={styles["container__time--base"]}>
            <input
              id="input"
              className={`${styles["container__content"]} ${
                styles["container__time--input-date"]
              }   ${isError && styles["container__content--error"]} ${
                isDisable && styles["container__content--disable"]
              }`}
              placeholder={placeholder}
              disabled={isDisable}
              type={type}
              onFocus={() => setType("time")}
              onBlur={() => setType("text")}
              value={value}
              onChange={(e) => setValue?.(e.target.value)}
            />
          </div>
        </div>
      )}
    </div>
  );
}
