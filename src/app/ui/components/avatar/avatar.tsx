// import Icon from '../icon/icon';
import IconProfile from "../../icons/icon-profile";
import styles from "./avatar.module.scss";

/* eslint-disable-next-line */
export interface AvatarProps {
  size?: "xs" | "sm" | "md" | "lg" | "xl" | "xxl" | "xxxl";
  text?: string;
  variant?: "photo" | "icon" | "initial";
  source?: string;
  iconWidth?: number;
  iconHeight?: number;
}

export default function Avatar({
  size = "sm",
  text = "Alan Alin",
  variant = "initial",
  source,
  iconWidth = 22,
  iconHeight = 22,
}: AvatarProps) {
  return (
    <div className={`${styles["container"]} ${styles[size]}`}>
      {variant === "initial" && (
        <span className={styles[`text-${size}`]}>
          {text
            .split(" ")
            .map((x) => x[0])
            .slice(0, 2)
            .join("")}
        </span>
      )}
      {variant === "photo" && (
        // TODO: change it with Image from next/image
        <img className={styles[`photo`]} src={source} alt="avatar" />
      )}
      {variant === "icon" && (
        <div className={styles["icon"]}>
          <IconProfile width={iconWidth} height={iconHeight} />
        </div>
      )}
    </div>
  );
}
