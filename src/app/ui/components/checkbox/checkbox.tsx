import styles from "./checkbox.module.scss";

/* eslint-disable-next-line */
export interface CheckboxProps {
  size: "sm" | "md" | "lg";
  label?: string;
  helperText?: string;
  disable?: boolean;
  error?: boolean;
  // when checkbox is in a group, item is the value of the checkbox
  item?: string;
  onCheckboxChange?: (checked: boolean, item: string) => void;
}

export function Checkbox({
  size = "md",
  disable = false,
  error = false,
  helperText = "Supporting Text",
  label = "Checkbox Label",
  onCheckboxChange,
  item = "",
}: CheckboxProps) {
  return (
    <div className={`${styles.container}`}>
      <input
        id={label ? label : "checkbox"}
        type="checkbox"
        className={`${styles.input} ${styles[`input-${size}`]} ${
          disable && styles.disable
        } ${error && styles.error}`}
        disabled={disable}
        onChange={(e) =>
          onCheckboxChange && onCheckboxChange(e.target.checked, item)
        }
      />
      <div className={`${styles.container_text}`}>
        <label
          htmlFor={label ? label : "checkbox"}
          className={`${styles[`label-${size}`]} ${
            disable && styles["label-disable"]
          } ${error && styles["label-error"]}`}
        >
          {label}
        </label>
        <span
          className={`${styles[`helper_text-${size}`]} ${
            disable && styles["helper_text-disable"]
          } ${error && styles["helper_text-error"]}`}
        >
          {helperText}
        </span>
      </div>
    </div>
  );
}

export default Checkbox;
