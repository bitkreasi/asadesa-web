import React from "react";
import styles from "./checkbox.module.scss";

function IndeterminateCheckbox({
  indeterminate,
  className = "",
  ...rest
}: { indeterminate?: boolean } & React.HTMLProps<HTMLInputElement>) {
  const ref = React.useRef<HTMLInputElement>(null!);

  React.useEffect(() => {
    if (typeof indeterminate === "boolean") {
      ref.current.indeterminate = !rest.checked && indeterminate;
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [ref, indeterminate]);

  return (
    <input
      type="checkbox"
      ref={ref}
      className={className + " cursor-pointer"}
      {...rest}
    />
  );
}

const Checkbox = React.forwardRef<
  HTMLInputElement,
  React.ComponentPropsWithRef<"input">
>(({ className, checked, ...props }, ref) => (
  <input
    className={`${styles["checkbox"]} ${className}`}
    type="checkbox"
    ref={ref}
    checked={checked}
    {...props}
  />
));
Checkbox.displayName = "Checkbox";

export { Checkbox, IndeterminateCheckbox };
