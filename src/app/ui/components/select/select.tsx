import React from "react";
import styles from "./select.module.scss";

const Select = React.forwardRef<
  HTMLSelectElement,
  React.ComponentPropsWithRef<"select">
>(({ className, children, value, onChange, ...props }, ref) => {
  return (
    <select
      ref={ref}
      className={`${styles["select"]} ${className}`}
      value={value}
      onChange={onChange}
      {...props}
    >
      <option hidden value="placehold"></option>
      {children}
    </select>
  );
});
Select.displayName = "Select";

const SelectItem = React.forwardRef<
  HTMLOptionElement,
  React.ComponentPropsWithRef<"option">
>(({ className, value, children, ...props }, ref) => {
  return (
    <option
      value={value}
      ref={ref}
      className={`${styles["select__item"]} ${className}`}
      {...props}
    >
      {children}
    </option>
  );
});
SelectItem.displayName = "SelectItem";

export { Select, SelectItem };
