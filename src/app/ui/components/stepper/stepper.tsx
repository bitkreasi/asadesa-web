'use client'

import { useMemo } from 'react'
import styles from './stepper.module.scss'

interface StepperProps {
  steps: number
  activeStep: number
  text: string
}

const StepText: {
  [key: number]: string
} = {
  0: 'langkah pertama',
  1: 'langkah kedua',
  2: 'langkah ketiga',
}

export default function Stepper(props: StepperProps) {
  const stepsCount = useMemo(() => {
    let tmp = []
    for (let i = 0; i < props.steps; i++) {
      tmp.push(i)
    }
    return tmp
  }, [props.steps])

  return (
    <div className={styles.stepper}>
      <div className={styles['stepper__text']}>
        <p className={styles['stepper__text--left']}>
          {StepText[props.activeStep]}:
        </p>
        <p
          className={styles['stepper__text--right']}
          data-testid='stepper-text'
        >
          {props.text}
        </p>
      </div>
      <div className={styles['stepper__blocks']}>
        {stepsCount.map((__, index) => (
          <div
            key={index}
            className={
              index > props.activeStep
                ? styles['stepper__block--inactive']
                : styles['stepper__block--active']
            }
          ></div>
        ))}
      </div>
    </div>
  )
}
