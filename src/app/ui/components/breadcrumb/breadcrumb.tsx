import Link from "next/link";
import styles from "./breadcrumb.module.scss";
import IconArrowRight from "../../icons/icon-arrow-right";

/* eslint-disable-next-line */
export interface BreadcrumbItem {
  id?: number | string;
  name: string;
  link: string;
  onHandleClick?: () => void;
}
export interface BreadcrumbsProps {
  items: BreadcrumbItem[];
}

const exampleBreadcrumbs: BreadcrumbItem[] = [
  {
    id: 1,
    name: "Beranda",
    link: "/",
  },
  {
    id: 2,
    name: "Lorem Ipsum",
    link: "/lorem",
  },
];

export function Breadcrumbs({ items = exampleBreadcrumbs }: BreadcrumbsProps) {
  return (
    <div className={styles["container"]}>
      {items.map((item, i) => (
        <div
          key={item.id || i}
          className={`${styles["item"]} ${
            i === items.length - 1 && styles["active"]
          }`}
        >
          {/* ERROR : "process is not defined" */}
          {/* If you run it inside Storybook, you'll get an error like that */}
          {/* but, when call it in another component it shoud be okay */}
          {/* you can change the 'Link' tag to 'span' tag */}
          <Link href={item.link || ""} onClick={item.onHandleClick}>
            {item.name}
          </Link>
          {i !== items.length - 1 && <IconArrowRight width={20} height={20} />}
        </div>
      ))}
    </div>
  );
}

export default Breadcrumbs;
