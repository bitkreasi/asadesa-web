import React from "react";
import { toast as sonner } from "sonner";
import styles from "./toast.module.scss";
import { Text } from "../typography/typography";
import IconCloseCircle from "../../icons/icon-close-circle";
import IconTickCircle from "../../icons/icon-tick-circle";
import IconDanger from "../../icons/icon-danger";
import IconInfo from "../../icons/icon-info";

const Toast = ({
  variant,
  children,
}: {
  variant: "success" | "info" | "error" | "warning";
  children: React.ReactNode;
}) => {
  return (
    <div data-variant={variant} className={styles["toast"]}>
      {children}
    </div>
  );
};

const ToastClose = ({ t }: { t: string | number }) => {
  return (
    <button
      className={styles["toast__close"]}
      onClick={() => sonner.dismiss(t)}
    >
      <IconCloseCircle width={20} height={20} />
    </button>
  );
};

const ToastContent = ({ children }: { children: React.ReactNode }) => {
  return <div className={styles["toast__content"]}>{children}</div>;
};

const toast = (message: string | React.ReactNode) => {
  return sonner.custom((t) => (
    <div>
      {message}
      <button onClick={() => sonner.dismiss(t)}>dismiss</button>
    </div>
  ));
};

toast.success = (message: string, desc?: string) => {
  return sonner.custom((t) => (
    <Toast variant="success">
      <ToastContent>
        <i className={styles["toast__icon"]}>
          <IconTickCircle
            width={20}
            height={20}
            color="#fff"
            backgroundColor="#fff"
          />
        </i>
        <div>
          <Text variant="text" size="md" fontStyle="medium">
            {message}
          </Text>
          <Text variant="text" size="sm" fontStyle="regular">
            {desc}
          </Text>
        </div>
      </ToastContent>
      <ToastClose t={t} />
    </Toast>
  ));
};

toast.error = (message: string, desc?: string) => {
  return sonner.custom((t) => (
    <Toast variant="error">
      <ToastContent>
        <i className={styles["toast__icon"]}>
          <IconDanger
            width={20}
            height={20}
            color="#fff"
            backgroundColor="#fff"
          />
        </i>
        <div>
          <Text variant="text" size="md" fontStyle="medium">
            {message}
          </Text>
          <Text variant="text" size="sm" fontStyle="regular">
            {desc}
          </Text>
        </div>
      </ToastContent>
      <ToastClose t={t} />
    </Toast>
  ));
};

toast.info = (message: string, desc?: string) => {
  return sonner.custom((t) => (
    <Toast variant="info">
      <ToastContent>
        <i className={styles["toast__icon"]}>
          <IconInfo
            width={20}
            height={20}
            color="#fff"
            backgroundColor="#fff"
          />
        </i>
        <div>
          <Text variant="text" size="md" fontStyle="medium">
            {message}
          </Text>
          <Text variant="text" size="sm" fontStyle="regular">
            {desc}
          </Text>
        </div>
      </ToastContent>
      <ToastClose t={t} />
    </Toast>
  ));
};

toast.warning = (message: string, desc?: string) => {
  return sonner.custom((t) => (
    <Toast variant="warning">
      <ToastContent>
        <i className={styles["toast__icon"]}>
          <IconInfo
            width={20}
            height={20}
            color="#fff"
            backgroundColor="#fff"
          />
        </i>
        <div>
          <Text variant="text" size="md" fontStyle="medium">
            {message}
          </Text>
          <Text variant="text" size="sm" fontStyle="regular">
            {desc}
          </Text>
        </div>
      </ToastContent>
      <ToastClose t={t} />
    </Toast>
  ));
};

export { toast };
