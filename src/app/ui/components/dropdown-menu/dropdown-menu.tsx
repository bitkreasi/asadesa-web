"use client";

import React from "react";
import styles from "./dropdown-menu.module.scss";
import IconArrowDown from "../../icons/icon-arrow-down";
import IconArrowUp from "../../icons/icon-arrow-up";

interface DropdowmMenuContext {
  dropdownMenu: boolean;
  setDropdownMenu: React.Dispatch<React.SetStateAction<boolean>>;
}

const DropdownMenuContext = React.createContext<
  DropdowmMenuContext | undefined
>(undefined);

const DropdownMenu = ({ children }: { children: React.ReactNode }) => {
  const [dropdownMenu, setDropdownMenu] = React.useState(false);

  return (
    <DropdownMenuContext.Provider value={{ dropdownMenu, setDropdownMenu }}>
      <div style={{ position: "relative" }}>{children}</div>
    </DropdownMenuContext.Provider>
  );
};

type DropdownMenuTriggerProps = {
  withArrow?: boolean;
} & React.ComponentPropsWithRef<"div">;
const DropdownMenuTrigger = React.forwardRef<
  HTMLDivElement,
  DropdownMenuTriggerProps
>(({ className, children, withArrow = false, ...props }, ref) => {
  const context = React.useContext(DropdownMenuContext);
  return (
    <div
      onClick={() => context?.setDropdownMenu(!context?.dropdownMenu)}
      className={`${styles["dropdown__menu__trigger"]} ${className}`}
      {...props}
      ref={ref}
    >
      {children}
      {withArrow ? (
        context?.dropdownMenu ? (
          <IconArrowUp width={20} height={20} />
        ) : (
          <IconArrowDown width={20} height={20} />
        )
      ) : null}
    </div>
  );
});
DropdownMenuTrigger.displayName = "DropdownMenuTrigger";

const DropdownMenuClose = React.forwardRef<
  HTMLDivElement,
  React.ComponentPropsWithRef<"div">
>(({ className, children, ...props }, ref) => {
  const context = React.useContext(DropdownMenuContext);

  return (
    <div
      onClick={() => context?.setDropdownMenu(false)}
      className={`${styles["dropdown__menu__close"]} ${className}`}
      ref={ref}
      {...props}
    >
      {children}
    </div>
  );
});
DropdownMenuClose.displayName = "DropdownMenuClose";

interface DropdownMenuContentProps extends React.ComponentPropsWithRef<"div"> {}
const DropdownMenuContent = React.forwardRef<
  HTMLDivElement,
  DropdownMenuContentProps
>(({ className, children, ...props }, ref) => {
  const context = React.useContext(DropdownMenuContext);
  return context?.dropdownMenu ? (
    <div
      className={`${styles["dropdown__menu__content"]} ${className}`}
      {...props}
      ref={ref}
    >
      {children}
    </div>
  ) : null;
});
DropdownMenuContent.displayName = "DropdownMenuContent";

export {
  DropdownMenu,
  DropdownMenuTrigger,
  DropdownMenuContent,
  DropdownMenuClose,
};
