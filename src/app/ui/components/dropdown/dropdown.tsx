import { useState } from "react";
import styles from "./dropdown.module.scss";
import IconSearch from "../../icons/icon-search";

const defaultItems: string[] = ["Hello world", "Hi", "Good morning"];

type DropdownProps = {
  items: string[];
  setDropdownSelectValue?: (e: string) => void;
  size?: "small" | "medium";
  withSearch?: boolean;
  setIsOpen: (val: boolean) => void;
  isHelperText?: boolean;
};

export default function Dropdown({
  size = "small",
  items = defaultItems,
  withSearch = false,
  ...props
}: DropdownProps) {
  const [search, setSearch] = useState<string>("");

  function handleChange(value: string) {
    props.setDropdownSelectValue?.(value);
    props.setIsOpen(false);
  }

  return (
    <div
      className={[
        props.isHelperText
          ? styles["dropdown--with-helper-text"]
          : styles["dropdown"],
      ].join(" ")}
    >
      <div className={styles["dropdown__inner--" + size]}>
        {!!withSearch && (
          <div className={styles["dropdown__search-wrapper"]}>
            <input
              value={search}
              onChange={(e) => setSearch(e.target.value)}
              alt="search"
              placeholder="Search"
              className={styles["dropdown__search-input"]}
              autoFocus
            />
            <div className={styles["dropdown__search-icon"]}>
              <IconSearch />
            </div>
          </div>
        )}
        {search !== ""
          ? items
              .filter((item) =>
                item.toLowerCase().includes(search.toLocaleLowerCase()),
              )
              .map((item, index) => (
                <button
                  key={index}
                  className={styles["dropdown__item"]}
                  onClick={() => handleChange(item)}
                  type="button"
                >
                  {item}
                </button>
              ))
          : items.map((item, index) => (
              <button
                key={index}
                className={styles["dropdown__item"]}
                onClick={() => handleChange(item)}
                type="button"
              >
                {item}
              </button>
            ))}
      </div>
    </div>
  );
}
