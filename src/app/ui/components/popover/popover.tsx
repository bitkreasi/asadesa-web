import { IconProps } from "@/app/shared";
import styles from "./popover.module.scss";
import { ReactNode } from "react";

export interface PopoverItemProps {
  name: string;
  icon?: ReactNode;
  color?: string;
  onClick?: () => void;
}

export interface PopoverProps {
  children?: React.ReactNode;
}

export function PopoverItem(props: PopoverItemProps) {
  return (
    <div className={styles["content__more--item"]}>
      <button onClick={props.onClick}>
        {props.icon}
        <span style={{ color: props.color }}>{props.name}</span>
      </button>
    </div>
  );
}

export function Popover(props: PopoverProps) {
  return (
    <div className={styles["content__more--container"]}>{props.children}</div>
  );
}

export default Popover;
