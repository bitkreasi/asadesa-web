"use client";

import React from "react";
import styles from "./dialog.module.scss";
import IconCloseCircle from "../../icons/icon-close-circle";
import { Text } from "../typography/typography";

const DialogContext = React.createContext<{
  isDialogOpen: boolean;
  setIsDialogOpen: React.Dispatch<React.SetStateAction<boolean>>;
  onClose?: () => void;
} | null>(null);

export function useDialog() {
  const context = React.useContext(DialogContext);
  if (!context) {
    throw new Error("useDialog must be call inside dialog");
  }
  return context;
}

export const Dialog = ({
  children,
  onClose,
}: {
  children: React.ReactNode;
  onClose?: () => void;
}) => {
  const [isDialogOpen, setIsDialogOpen] = React.useState(false);
  return (
    <DialogContext.Provider
      value={{
        isDialogOpen,
        setIsDialogOpen,
        onClose,
      }}
    >
      {children}
    </DialogContext.Provider>
  );
};
export type DialogTriggerProps = React.ComponentPropsWithoutRef<"div">;

export const DialogTrigger = ({
  children,
  className,
  ...props
}: DialogTriggerProps) => {
  const dialogContext = useDialog();
  return (
    <div
      className={`${styles["dialog__trigger"]} ${className}`}
      {...props}
      onClick={() => {
        dialogContext.setIsDialogOpen(true);
      }}
    >
      {children}
    </div>
  );
};

export type DialogContentProps = React.ComponentPropsWithoutRef<"div">;

export const DialogContent = ({
  children,
  className,
  ...props
}: DialogContentProps) => {
  const dialogContext = useDialog();
  return (
    dialogContext.isDialogOpen && (
      <div className={styles["dialog__wrapper"]}>
        <div className={`${styles["content"]} ${className}`} {...props}>
          <span
            onClick={() => {
              dialogContext.setIsDialogOpen(false);
              dialogContext.onClose && dialogContext.onClose();
            }}
            className={styles["close__icon"]}
          >
            <IconCloseCircle />
          </span>
          <div>{children}</div>
        </div>
      </div>
    )
  );
};

export type DialogHeaderProps = React.ComponentPropsWithoutRef<"div">;

export const DialogHeader = ({
  children,
  className,
  ...props
}: DialogHeaderProps) => {
  return (
    <div className={`${styles["header"]} ${className}`} {...props}>
      {children}
    </div>
  );
};

export type DialogTitleProps = React.ComponentPropsWithoutRef<"div">;

export const DialogTitle = ({
  children,
  className,
  ...props
}: DialogTitleProps) => {
  return (
    <Text
      variant="text"
      size="lg"
      fontStyle="medium"
      className={`${styles["title"]} ${className}`}
      {...props}
    >
      {children}
    </Text>
  );
};

export type DialogDescriptionProps = React.ComponentPropsWithoutRef<"div">;

export const DialogDescription = ({
  children,
  className,
  ...props
}: DialogDescriptionProps) => {
  return (
    <Text
      variant="text"
      size="md"
      fontStyle="regular"
      className={`${styles["description"]} ${className}`}
      {...props}
    >
      {children}
    </Text>
  );
};

export type DialogFooterProps = React.ComponentPropsWithoutRef<"div">;

export const DialogFooter = ({
  children,
  className,
  ...props
}: DialogFooterProps) => {
  return (
    <div className={`${styles["footer"]} ${className}`} {...props}>
      {children}
    </div>
  );
};

export type DialogCloseProps = React.ComponentPropsWithoutRef<"div">;

export const DialogClose = ({
  children,
  className,
  ...props
}: DialogCloseProps) => {
  const dialogContext = useDialog();

  return (
    <div
      className={`${styles["close"]} ${className}`}
      {...props}
      onClick={() => {
        dialogContext.setIsDialogOpen(false);
        dialogContext.onClose && dialogContext.onClose();
      }}
    >
      {children}
    </div>
  );
};
