"use client";

import { useEffect, useRef, useState } from "react";
import styles from "./pagination.module.scss";
import IconArrowUp from "../../icons/icon-arrow-up";
import IconBottomArrow from "../../icons/icon-bottom-arrow";
import IconSuccess from "../../icons/icon-success";
import IconArrowLeft from "../../icons/icon-arrow-left";
import IconLine from "../../icons/icon-line";
import IconArrowRight from "../../icons/icon-arrow-right";
/* eslint-disable-next-line */
export interface PaginationProps {
  variant?: "item-per-page" | "page-number";
  // value of 'item per page' variant
  numberOfView?: number[];
  // value of 'page number' variant
  totalPage?: number;
}
export function Pagination({
  variant = "item-per-page",
  numberOfView = [10, 50, 100, 200],
  totalPage = 5,
}: PaginationProps) {
  const [isItemPageOpen, setIsItemPageOpen] = useState(false);
  // get first value of numberOfView
  const [currentItemPage, setCurrentItemPage] = useState(numberOfView[0]);
  // get current number of page
  const [currentPage, setCurrentPage] = useState(1);
  const itemPageRef = useRef<HTMLDivElement>(null);
  const handleClickOutside = (e: MouseEvent) => {
    if (
      itemPageRef.current &&
      !itemPageRef.current.contains(e.target as Node)
    ) {
      setIsItemPageOpen(false);
    }
  };
  useEffect(() => {
    document.addEventListener("mousedown", handleClickOutside);
    return () => document.removeEventListener("mousedown", handleClickOutside);
  }, []);
  return (
    <div className={styles["container"]}>
      {variant === "item-per-page" && (
        <div className={styles["container__item-page"]}>
          <span className={styles["container__item-page--text"]}>
            Tampilkan
          </span>
          <div
            className={styles["container__item-page--content"]}
            onClick={() => setIsItemPageOpen((prev) => !prev)}
          >
            <span className={styles["container__item-page--content__number"]}>
              {currentItemPage}
            </span>
            {isItemPageOpen ? (
              <div
                className={styles["container__item-page--content__icon"]}
                onClick={() => setIsItemPageOpen(true)}
              >
                <IconArrowUp />
              </div>
            ) : (
              <div
                className={styles["container__item-page--content__icon"]}
                onClick={() => setIsItemPageOpen(false)}
              >
                <IconBottomArrow />
              </div>
            )}
            {isItemPageOpen && (
              <div
                ref={itemPageRef}
                className={styles["container__item-page--content__list"]}
              >
                {numberOfView.map((item, i) => (
                  <div
                    key={i}
                    className={
                      styles["container__item-page--content__list--item"]
                    }
                    onClick={() => setCurrentItemPage(item)}
                  >
                    <span
                      className={`${
                        item === currentItemPage
                          ? styles[
                              "container__item-page--content__list--item__selected"
                            ]
                          : ""
                      } ${
                        styles[
                          "container__item-page--content__list--item__text"
                        ]
                      }`}
                    >
                      {item}
                      {item === currentItemPage && (
                        <div
                          className={
                            styles[
                              "container__item-page--content__list--item__icon"
                            ]
                          }
                        >
                          <IconSuccess
                            backgroundColor="#2CAE70"
                            color="#2CAE70"
                            width={20}
                            height={20}
                          />
                        </div>
                      )}
                    </span>
                  </div>
                ))}
              </div>
            )}
          </div>
          <span className={styles["container__item-page--text"]}>data</span>
        </div>
      )}
      {variant === "page-number" && (
        <div className={styles["container__page-number"]}>
          <div
            className={`${styles["container__page-number--icon"]} ${
              currentPage > 1
                ? styles["container__page-number--icon__active"]
                : styles["container__page-number--icon__inactive"]
            }`}
            onClick={() => {
              if (currentPage > 1) {
                setCurrentPage((prev) => prev - 1);
              }
            }}
          >
            {currentPage > 1 ? (
              <IconArrowLeft backgroundColor="#272E38" color="#272E38" />
            ) : (
              <IconArrowLeft backgroundColor="#8F97A6" color="#8F97A6" />
            )}
          </div>
          <div className={styles["container__page-number--icon__pipe"]}>
            <IconLine />
          </div>
          <div className={styles["container__page-number--base"]}>
            <span className={styles["container__page-number--base__text"]}>
              Halaman
            </span>
            <span className={styles["container__page-number--base__item"]}>
              {currentPage}
            </span>
            <span className={styles["container__page-number--base__text"]}>
              dari {totalPage}
            </span>
          </div>
          <div className={styles["container__page-number--icon__pipe"]}>
            <IconLine />
          </div>
          <div
            className={`${styles["container__page-number--icon"]} ${
              currentPage < totalPage
                ? styles["container__page-number--icon__active"]
                : styles["container__page-number--icon__inactive"]
            }`}
            onClick={() => {
              if (currentPage < totalPage) {
                setCurrentPage((prev) => prev + 1);
              }
            }}
          >
            {currentPage < totalPage ? (
              <IconArrowRight backgroundColor="#272E38" color="#272E38" />
            ) : (
              <IconArrowRight backgroundColor="#8F97A6" color="#8F97A6" />
            )}
          </div>
        </div>
      )}
    </div>
  );
}
export default Pagination;
