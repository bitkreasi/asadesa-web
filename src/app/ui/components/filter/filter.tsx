import React, { Children } from 'react'
import useOutsideClick from '@/hooks/use-outside-click'
import { useState } from 'react'
import IconArrowDown from '../../icons/icon-arrow-down'
import styles from './style.module.scss'
import IconCloseCircle from '../../icons/icon-close-circle'

type Props = {
  children: React.ReactNode,
}

export default function Filter(props: Props) {
  const [isOpen, setIsOpen] = useState<boolean>(false)
  const toggleOff = () => setIsOpen(false)

  const filterRef = useOutsideClick({
    callback: toggleOff,
    isOpen: isOpen,
  })

  return (
    <div ref={filterRef} className={styles['filter__container']}>
      <button
        className={styles['filter__button']}
        onClick={() => setIsOpen(true)}
      >
        <span>Filter</span>
        <IconArrowDown />
      </button>
      {!!isOpen && <div className={styles['filter__body']}>
        <div className={styles['filter__body-title']}>
          <span>Filter</span>
          <button onClick={toggleOff}>
            <IconCloseCircle/>
          </button>
        </div>
        <div>{props.children}</div></div>}
    </div>
  )
}