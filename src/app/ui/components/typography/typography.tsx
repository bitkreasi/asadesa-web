import clsx from "clsx";
import React from "react";
import styles from "./typography.module.scss";

type HeadingProps = {
  variant: "display" | "text";
  size: "3xl" | "2xl" | "xl" | "lg" | "md" | "sm" | "xs";
  fontStyle: "regular" | "medium" | "semibold" | "bold";
  muted?: boolean;
} & React.HTMLAttributes<HTMLHeadingElement>;

export const Heading1 = React.forwardRef<HTMLHeadingElement, HeadingProps>(
  (
    {
      children,
      variant = "display",
      muted = false,
      size = "3xl",
      fontStyle = "bold",
      className,
      ...props
    },
    ref,
  ) => {
    return (
      <h1
        className={clsx(
          styles[variant + "__" + size],
          styles[fontStyle],
          muted && styles["muted"],
          className,
        )}
        ref={ref}
        {...props}
      >
        {children}
      </h1>
    );
  },
);
Heading1.displayName = "Heading1";

export const Heading2 = React.forwardRef<HTMLHeadingElement, HeadingProps>(
  (
    {
      children,
      variant = "display",
      muted = false,
      size = "2xl",
      fontStyle = "semibold",
      className,
      ...props
    },
    ref,
  ) => {
    return (
      <h2
        className={clsx(
          styles[variant + "__" + size],
          styles[fontStyle],
          muted && styles["muted"],
          className,
        )}
        ref={ref}
        {...props}
      >
        {children}
      </h2>
    );
  },
);
Heading2.displayName = "Heading2";

export const Heading3 = React.forwardRef<HTMLHeadingElement, HeadingProps>(
  (
    {
      children,
      variant = "display",
      muted = false,
      size = "xl",
      fontStyle = "semibold",
      className,
      ...props
    },
    ref,
  ) => {
    return (
      <h3
        className={clsx(
          styles[variant + "__" + size],
          styles[fontStyle],
          muted && styles["muted"],
          className,
        )}
        ref={ref}
        {...props}
      >
        {children}
      </h3>
    );
  },
);
Heading3.displayName = "Heading3";

export const Heading4 = React.forwardRef<HTMLHeadingElement, HeadingProps>(
  (
    {
      children,
      variant = "display",
      muted = false,
      size = "lg",
      fontStyle = "medium",
      className,
      ...props
    },
    ref,
  ) => {
    return (
      <h4
        className={clsx(
          styles[variant + "__" + size],
          styles[fontStyle],
          muted && styles["muted"],
          className,
        )}
        ref={ref}
        {...props}
      >
        {children}
      </h4>
    );
  },
);
Heading4.displayName = "Heading4";

type TextProps = {
  variant: "display" | "text";
  size: "3xl" | "2xl" | "xl" | "lg" | "md" | "sm" | "xs";
  fontStyle: "regular" | "medium" | "semibold" | "bold";
  muted?: boolean;
} & React.HTMLAttributes<HTMLParagraphElement>;

export const Text = React.forwardRef<HTMLParagraphElement, TextProps>(
  (
    {
      className,
      variant = "text",
      muted = false,
      size = "md",
      fontStyle = "regular",
      children,
      ...props
    },
    ref,
  ) => {
    return (
      <p
        className={clsx(
          styles[variant + "__" + size],
          styles[fontStyle],
          muted && styles["muted"],
          className,
        )}
        ref={ref}
        {...props}
      >
        {children}
      </p>
    );
  },
);
Text.displayName = "Text";
