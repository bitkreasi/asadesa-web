import IconAddCircle from "../icons/icon-add-circle";
import IconFolder from "../icons/icon-folder";
import IconHome2 from "../icons/icon-home-2";
import IconPeople from "../icons/icon-people";
import { MenuItem } from "./sidebar/sidebar";

export const menuItems: MenuItem[] = [
  {
    id: "dashboard",
    name: "Beranda",
    link: "/dashboard",
    icon: <IconHome2 />,
  },
  {
    id: "village-info",
    name: "Info Desa",
    link: "",
    icon: <IconFolder />,
    subMenu: [
      {
        id: "village",
        name: "Identitas Desa",
        link: "/dashboard/village/:id",
      },
      {
        id: "administrative",
        name: "Wilayah Administratif",
        link: "/dashboard/administrative/:id",
      },
      {
        id: "officials",
        name: "Pemerintahan Desa",
        link: "/dashboard/officials/:id",
      },
      {
        id: "institutions",
        name: "Lembaga Desa",
        link: "/dashboard/institutions/:id",
      },
    ],
  },
  {
    id: "population",
    name: "Kependudukan",
    link: "",
    icon: <IconPeople />,
    subMenu: [
      {
        id: "resident",
        name: "Penduduk",
        link: "/dashboard/resident",
      },
      {
        id: "family",
        name: "Keluarga",
        link: "/dashboard/family",
      },
      {
        id: "household",
        name: "Rumah Tangga",
        link: "/dashboard/household",
      },
    ],
  },
];
