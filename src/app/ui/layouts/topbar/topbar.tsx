import { useState } from 'react'
// import Avatar from '../avatar/avatar';
// import Icon from '../icon/icon';
// import Input from '../input/input';
import styles from './topbar.module.scss'
// import Popover, { PopoverItem } from '../popover/popover';
// import { deleteCookie } from 'cookies-next';
// import { CookieKey } from '@asadesa/shared/utils';
import { useRouter } from 'next/navigation'
import Button from '../../components/button/button'
import Input from '../../components/input/input'
import IconSearchStatus from '../../icons/icon-search-status'
import Avatar from '../../components/avatar/avatar'
import Popover, { PopoverItem } from '../../components/popover/popover'
import Iconsetting2 from '../../icons/icon-setting-2'
import IconLogout from '../../icons/icon-logout'
import { cookieStorage } from '@/utils/cookie'
// import Modal from '../modal/modal';
// import Button from '../button/button';

/* eslint-disable-next-line */
export interface TopBarProps {
  value: string
  setValue: (value: string) => void
  imageSrc?: string
  username?: string
  isBorder?: boolean
  handleClick?: () => void
}

export default function Topbar({
  value,
  setValue,
  imageSrc = 'https://source.unsplash.com/vGAm0obF0K4/1600x900',
  username = 'Lorem ipsum',
  isBorder = true,
  handleClick,
}: TopBarProps) {
  const [isShowMoreOpen, setIsShowMoreOpen] = useState(false)
  const [isShowAlertExitOpen, setisShowAlertExitOpen] = useState(false)

  const router = useRouter()
  const handleLogout = () => {
    cookieStorage.delete('AUTH_TOKEN')
    router.push('/signin')
  }

  return (
    <div
      className={`${styles['container']} ${
        isBorder && styles['container--border']
      }`}
    >
      <div className={styles['container__input']}>
        <Input
          variant='text-field'
          placeholder='Cari'
          value={value}
          setValue={setValue}
          prefix={<IconSearchStatus width={20} height={20} />}
          onHandleClick={handleClick}
          label=''
          helperText=''
        />
      </div>
      <div
        className={styles['user__content']}
        onClick={() => {
          setIsShowMoreOpen(!isShowMoreOpen)
        }}
      >
        <Avatar
          variant={imageSrc ? 'photo' : 'icon'}
          source={imageSrc}
          size='sm'
        />
        <span className={styles['username']}>{username}</span>
      </div>
      {isShowMoreOpen && (
        <Popover>
          <PopoverItem
            name='Pengaturan'
            icon={<Iconsetting2 width={20} height={20} />}
            onClick={() => {
              router.push('/dashboard/settings')
              setIsShowMoreOpen(false)
            }}
          />
          <PopoverItem
            name='Keluar'
            icon={<IconLogout width={20} height={20} color='#F42E2E' />}
            color='#F42E2E'
            onClick={() => {
              setisShowAlertExitOpen(true)
              setIsShowMoreOpen(false)
              handleLogout()
            }}
          />
        </Popover>
      )}
      {isShowAlertExitOpen && (
        <div className={styles['container__modal']}>
          {/* <Modal
            variant="question-mark"
            title="Anda yakin ingin keluar dari akun ini?"
            prefixButton={
              <Button
                color="neutral"
                variant="outline"
                text="Batal"
                onHandleClick={() => setisShowAlertExitOpen(false)}
              />
            }
            suffixButton={
              <Button
                color="error"
                variant="solid"
                text="Keluar"
                // onHandleClick={handleLogout}
              />
            }
          /> */}
        </div>
      )}
    </div>
  )
}
