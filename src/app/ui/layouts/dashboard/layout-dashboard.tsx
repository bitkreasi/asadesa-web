'use client'
import { useMemo } from 'react'
import { menuItems } from '../menuItems'
import Sidebar from '../sidebar/sidebar'
import Topbar from '../topbar/topbar'
import styles from './layout-dashboard.module.scss'
import { useAuthStore } from '@/store/authStore'

export interface LayoutProps {
  children: React.ReactNode
}

export default function LayoutDashboard(props: LayoutProps) {
  const { user } = useAuthStore()

  const newMenuItems = useMemo(() => {
    return menuItems.map((menu) => {
      if (menu.id === 'village-info') {
        const newSubMenu = menu.subMenu?.map((submenu) => {
          if (submenu.link?.slice(-3) === ':id') {
            if (user?.institution_id)
              return {
                ...submenu,
                link: submenu.link.replace(':id', user.institution_id),
              }
          }
          return submenu
        })
        return { ...menu, subMenu: newSubMenu }
      }
      return menu
    })
  }, [menuItems, user?.institution_id])

  return (
    <div className={styles['container']}>
      <Sidebar menuItems={newMenuItems} />
      <div className={styles['content']}>
        <Topbar
          value={''}
          setValue={function (value: string): void {
            throw new Error('Function not implemented.')
          }}
        />
        {props.children}
      </div>
    </div>
  )
}
