"use client";

// import { Icon, IconProps } from '@asadesa/shared/ui';
import styles from "./sidebar.module.scss";
import { ReactElement, ReactNode, useEffect, useMemo, useState } from "react";
import Link from "next/link";
import IconAsadesa from "../../icons/icon-asadesa";
import IconSidebarRight from "../../icons/icon-sidebar-right";
import IconSidebarLeft from "../../icons/icon-sidebar-left";
import IconArrowUp from "../../icons/icon-arrow-up";
import IconArrowDown from "../../icons/icon-arrow-down";
import { usePathname } from "next/navigation";
import ActiveMenu from "./active-menu";

/* eslint-disable-next-line */
export interface MenuItem {
  id: string;
  name: string;
  link?: string;
  icon?: ReactNode;
  subMenu?: MenuItem[];
}

interface SidebarProps {
  menuItems: MenuItem[];
  // isCollapse?: boolean;
  // setIsCollapse?: React.Dispatch<React.SetStateAction<boolean>>;
}

export function Sidebar({
  menuItems,
}: // isCollapse,
// setIsCollapse,
SidebarProps) {
  const [activeItem, setActiveItem] = useState<string | null>(null);
  const [activeSubItem, setActiveSubItem] = useState<string | null>(null);
  const [dropdownActive, setDropdownActive] = useState<string | null>(null);
  const [isCollapse, setIsCollapse] = useState(false);
  const pathname = usePathname();

  useEffect(() => {
    menuItems.forEach((item) => {
      if (item.subMenu) {
        item.subMenu.forEach((subItem) => {
          if (subItem.link === pathname) {
            setActiveItem(item.id);
            setActiveSubItem(subItem.id);
          }
        });
      } else {
        if (item.link === pathname) {
          setActiveItem(item.id);
          setActiveSubItem(null);
        }
      }
    });
  }, [pathname]);

  useMemo(() => {
    menuItems.forEach((item) => {
      if (item.subMenu) {
        item.subMenu.forEach((subItem) => {
          if (subItem.link === pathname) {
            setDropdownActive(item.id);
          }
        });
      } else {
        if (item.link === pathname) {
          setDropdownActive(null);
        }
      }
    });
  }, [pathname, menuItems]);

  const handleToggleSidebar = () => {
    setIsCollapse?.((prev) => !prev);
  };

  const handleItemClick = (id: string) => {
    const item = menuItems.find((item) => item.id === id);
    if (item && item.subMenu) {
      setDropdownActive((prev) => (prev === id ? null : id));
    }
    setActiveItem(id);
    setActiveSubItem(null);
  };

  const handleToggleSubMenu = (id: string) => {
    const parentItem = menuItems.find((item) =>
      item.subMenu?.some((sub) => sub.id === id),
    );

    if (parentItem && parentItem.id !== activeItem) {
      setActiveItem(parentItem.id);
    }
    setActiveSubItem((prev) => (prev === id ? id : id));
  };

  return (
    <div
      className={`${
        isCollapse ? styles["container__collapse"] : styles["container"]
      }`}
    >
      {/* header */}
      <div className={styles["header"]}>
        <h3
          className={`${
            isCollapse
              ? styles["header__logo--collapse"]
              : styles["header__logo"]
          }`}
        >
          <IconAsadesa />
        </h3>
        <button
          onClick={handleToggleSidebar}
          className={`${
            isCollapse
              ? styles["header__icon--collapse"]
              : styles["header__icon"]
          }`}
        >
          {isCollapse ? <IconSidebarRight /> : <IconSidebarLeft />}
        </button>
      </div>

      {/* menu */}
      {menuItems.map((item) => (
        <div key={item.id}>
          {item.subMenu ? (
            <div
              className={`${styles["item__content"]} ${
                activeItem === item.id && styles["item__content--active"]
              }`}
              onClick={() => handleItemClick(item.id)}
            >
              <div className={styles["item"]}>
                <div
                  className={`${
                    isCollapse
                      ? styles["item__icon--collapse"]
                      : styles["item__icon"]
                  } ${activeItem === item.id && styles["item__icon--active"]}`}
                >
                  {item.icon &&
                    activeItem === item.id ? <ActiveMenu icon={item.icon as ReactElement}/> : item.icon}
                </div>
                <span
                  className={`${
                    isCollapse
                      ? styles["item__name--collapse"]
                      : styles["item__name"]
                  } ${activeItem === item.id && styles["item__name--active"]}`}
                >
                  {item.name}
                </span>
              </div>
              <div
                className={
                  isCollapse ? styles["sub__item--icon--collapse"] : ""
                }
              >
                {item.subMenu && (
                  <div>
                    {dropdownActive === item.id ? (
                      <ActiveMenu icon={
                        <IconArrowUp />
                      }/>
                    ) : (
                      <IconArrowDown />
                    )}
                  </div>
                )}
              </div>
            </div>
          ) : (
            <Link
              href={item.link || ""}
              className={`${styles["item__content"]} ${
                activeItem === item.id && styles["item__content--active"]
              }`}
              onClick={() => handleItemClick(item.id)}
            >
              <div className={styles["item"]}>
                <div
                  className={`${
                    isCollapse
                      ? styles["item__icon--collapse"]
                      : styles["item__icon"]
                  } ${activeItem === item.id && styles["item__icon--active"]}`}
                >
                  {item.icon &&
                    activeItem === item.id ? <ActiveMenu icon={item.icon as ReactElement}/> : item.icon}
                </div>
                <span
                  className={`${
                    isCollapse
                      ? styles["item__name--collapse"]
                      : styles["item__name"]
                  } ${activeItem === item.id && styles["item__name--active"]}`}
                >
                  {item.name}
                </span>
              </div>
              <div
                className={
                  isCollapse ? styles["sub__item--icon--collapse"] : ""
                }
              >
                {item.subMenu && (
                  <div>
                    {dropdownActive === item.id ? (
                      <ActiveMenu icon={
                        <IconArrowUp />
                      }/>
                    ) : (
                        <IconArrowDown />
                    )}
                  </div>
                )}
              </div>
            </Link>
          )}

          {item.subMenu?.map((subItem) => (
            <div key={subItem.id} className={styles["sub__item"]}>
              {dropdownActive === item.id && (
                <Link
                  href={subItem.link || ""}
                  key={subItem.id}
                  className={styles["sub__item--content"]}
                >
                  <span
                    className={`${
                      isCollapse
                        ? styles["sub__item--name--collapse"]
                        : styles["sub__item--name"]
                    } ${
                      activeSubItem === subItem.id
                        ? styles["sub__item--name--active"]
                        : ""
                    }`}
                    onClick={() => handleToggleSubMenu(subItem.id)}
                  >
                    {subItem.name}
                  </span>
                </Link>
              )}
            </div>
          ))}
        </div>
      ))}
    </div>
  );
}

export default Sidebar;
