import React from 'react'

type Props = {
  icon: React.ReactElement
}
export default function ActiveMenu(props: Props) {
  return React.cloneElement(props.icon, { color: '#ffff', backgroundColor: '#fff' })
}
