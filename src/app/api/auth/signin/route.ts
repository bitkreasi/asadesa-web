import { cookies } from "next/headers";
import { NextRequest } from "next/server";
import { sql } from "kysely";

import * as schema from "@/app/api/auth/auth-schema";
import db from "@/database/client";
import { User } from "@/database/tables/users.table";
import * as auth from "@/utils/auth";
import { responseWithData, throwResponse } from "@/utils/response";
import { verifyPassword } from "@/utils/security";
import { handleApiError } from "@/utils/api-error-handler";
import { TokenKind } from "@/utils/token-kind";

export async function POST(req: NextRequest) {
  try {
    const body = await req.json();
    const validatedFields = schema.signInSchema.safeParse(body);

    if (!validatedFields.success) {
      throw throwResponse(
        400,
        "Invalid Request",
        validatedFields.error.flatten().fieldErrors
      );
    }
    console.log("body", body);

    const existingUser = await db
      .selectFrom("users as u")
      .leftJoin("user_keys as uk", "u.id", "uk.user_id")
      .select([
        sql<User>`row_to_json(u.*)`.as("user"),
        sql<string>`uk.hashed_password`.as("hashed_password"),
      ])
      .where(
        sql<string>`${sql.raw("LOWER(email)")}`,
        "=",
        body.email.toLowerCase()
      )
      .executeTakeFirst();

    console.log("existingUser:::::::", !existingUser);

    if (!existingUser) {
      throw throwResponse(
        401,
        "The email or password you entered did not match our records. Please double-check and try again."
      );
    }

    // if (!existingUser.user.is_verified) {
    //   throw throwResponse(
    //     401,
    //     "Your account has to be activated before you can login. Please check your email for activation link."
    //   );
    // }

    if (existingUser.user.is_banned) {
      throw throwResponse(
        401,
        'Your account has been suspended. Please contact <a href="mailto:secretariat@capacity-building.org" class="text-primary-50">secretariat@capacity-building.org</a> for more information'
      );
    }

    const passwordMatches = await verifyPassword(
      body.password,
      existingUser.hashed_password
    );
    if (!passwordMatches) {
      throw throwResponse(
        401,
        "The email or password you entered did not match our records. Please double-check and try again."
      );
    }

    const session = await auth.lucia.createSession(existingUser.user.id, {});
    const sessionCookie = auth.lucia.createSessionCookie(session.id);
    cookies().set(
      sessionCookie.name,
      sessionCookie.value,
      sessionCookie.attributes
    );
    await db
      .insertInto("tokens")
      .values({
        user_id: existingUser.user.id,
        token: session.id,
        kind: TokenKind.Enum.access_token,
        expires: session.expiresAt.getTime(),
      })
      .execute();

    return responseWithData(200, "You have successfully signed in.", {
      token: session.id,
    });
  } catch (error: any) {
    return handleApiError(error);
  }
}
