import { cookies } from 'next/headers'
import { NextRequest } from 'next/server'

import db from '@/database/client'
import * as auth from '@/utils/auth'
import { responseWithData, throwResponse } from '@/utils/response'

export async function POST(_request: NextRequest) {
  try {
    const { session, user } = await auth.validateRequest()
    if (!session) {
      throw throwResponse(401, 'Unauthorized')
    }

    await auth.lucia.invalidateSession(session.id)
    const sessionCookie = auth.lucia.createBlankSessionCookie()

    await db
      .deleteFrom('tokens')
      .where('token', '=', session.id)
      .where('user_id', '=', user!.id)
      .executeTakeFirst()

    cookies().set(sessionCookie.name, sessionCookie.value, sessionCookie.attributes)

    return responseWithData(200, 'You have successfully signed out.')
  } catch (error: any) {
    return error instanceof Response
      ? throwResponse(error.status, error.statusText)
      : throwResponse(500, error.message)
  }
}
