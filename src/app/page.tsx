"use client";

import Signin from "./signin/page";

import { useEffect } from "react";
import { cookieStorage } from "@/utils/cookie";
import { useAuthStore } from "@/store/authStore";
import { useRouter } from "next/navigation";

export default function Home() {
  const { addUser } = useAuthStore();
  const router = useRouter();

  useEffect(() => {
    const token = cookieStorage.get("AUTH_TOKEN");
    console.log(token);
    if (token) {
      router.push("/dashboard");
    }
  }, [router]);

  return <Signin />;
}
