"use client";

import payment_success_icon from "../payment_success_icon.png";
import Button from "@/app/ui/components/button/button";
import {
  Dialog,
  DialogClose,
  DialogContent,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/app/ui/components/dialog/dialog";
import { Text } from "@/app/ui/components/typography/typography";
import IconExport from "@/app/ui/icons/icon-export";
import React from "react";
import styles from "./upload-payment-modal.module.scss";
import IconTrash from "@/app/ui/icons/icon-trash";
import IconDocument from "@/app/ui/icons/icon-document";
import { useRouter } from "next/navigation";
import Image from "next/image";

const FileDrop = () => {
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        gap: 8,
        padding: "15px 5px",
        alignItems: "center",
      }}
    >
      <div
        style={{
          padding: 8,
          borderRadius: 9999,
          backgroundColor: "pink",
          width: 40,
          height: 40,
        }}
      >
        <IconExport />
      </div>
      <Text variant="text" size="md" fontStyle="semibold">
        Drag & drop file atau cari file
      </Text>
      <Text variant="text" size="sm" fontStyle="regular" muted>
        Pastikan file data menggunakan format gambar yaitu .jpg/ .jpeg / .png
        dan berukuran maksimal 5MB
      </Text>
    </div>
  );
};

const UploadedFile = () => {
  const router = useRouter();
  return (
    <section className={styles["upload__wrapper"]}>
      <div className={styles["uploaded__file"]}>
        <div className={styles["file"]}>
          <div className={styles["info"]}>
            <span className={styles["file__icon"]}>
              <IconDocument />
            </span>
            <div className={styles["text"]}>
              <Text variant="text" size="sm" fontStyle="semibold">
                SS9182991.jpg
              </Text>
              <Text variant="text" size="xs" fontStyle="regular" muted>
                159kb
              </Text>
            </div>
          </div>
          <div className={styles["delete__icon"]}>
            <IconTrash />
          </div>
        </div>
        <div className={styles["progress"]}>
          <div className={styles["bar"]}></div>
          <div className={styles["status"]}>
            <Text variant="text" size="xs" fontStyle="regular" muted>
              0% uploading
            </Text>
          </div>
        </div>
      </div>
      <Dialog>
        <DialogTrigger>
          <Button text="Upload" color="primary" variant="solid" width="max" />
        </DialogTrigger>
        <DialogContent>
          <div className={styles["success__dialog"]}>
            <Image
              style={{ margin: "0px auto" }}
              src={payment_success_icon}
              width={172}
              height={172}
              loading="eager"
              alt="Pembayaran berhasil"
            />
            <div className={styles["success__text"]}>
              <Text variant="text" size="lg" fontStyle="semibold">
                Upload Screenshot Pembayaran Sukses
              </Text>
              <Text variant="text" size="sm" fontStyle="regular" muted>
                Silakan tunggu konfirmasi dari Admin melalui Email
              </Text>
            </div>
            <DialogClose>
              <Button
                onHandleClick={() => router.push("/dashboard")}
                text="Ke Beranda"
                width="max"
                color="primary"
                variant="solid"
              />
            </DialogClose>
          </div>
        </DialogContent>
      </Dialog>
    </section>
  );
};

const UploadPaymentModal = () => {
  return (
    <Dialog>
      <DialogTrigger>
        <Button width="max" text="Upload Bukti Pembayaran" variant="solid" />
      </DialogTrigger>
      <DialogContent>
        <DialogHeader>
          <DialogTitle>Upload Screenshot Pembayaran</DialogTitle>
        </DialogHeader>
        {/* <FileDrop /> */}
        <UploadedFile />
      </DialogContent>
    </Dialog>
  );
};

export default UploadPaymentModal;
