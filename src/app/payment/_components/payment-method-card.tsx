"use client";
import React from "react";
import styles from "./payment-method-card.module.scss";
import { Heading2, Text } from "@/app/ui/components/typography/typography";
import IconCopy from "@/app/ui/icons/icon-copy";
import BCALogo from "../logo-bca.png";
import {
  Accordion,
  AccordionContent,
  AccordionItem,
  AccordionTrigger,
} from "@/app/ui/components/accordion/accordion";
import {
  Tabs,
  TabsContent,
  TabsList,
  TabsTrigger,
} from "@/app/ui/components/tabs/tabs";
import Image from "next/image";
import UploadPaymentModal from "./upload-payment-modal";

type PaymentMethodCardProps = {
  accountNumber?: string;
} & React.ComponentPropsWithoutRef<"div">;

const PaymentMethodCard = (props: PaymentMethodCardProps) => {
  function copyAccountNumber() {
    navigator.clipboard.writeText(props.accountNumber ?? "");
  }
  return (
    <div className={styles["wrapper"]}>
      <Heading2 variant="display" size="sm" fontStyle="bold">
        Metode Pembayaran
      </Heading2>

      <Tabs defaultValue="bca">
        <TabsList>
          <TabsTrigger value="bca">Bank BCA</TabsTrigger>
          <TabsTrigger value="dana">Dana</TabsTrigger>
          <TabsTrigger value="gopay">GOPAY</TabsTrigger>
          <TabsTrigger value="ovo">OVO</TabsTrigger>
        </TabsList>
        <TabsContent value="bca">
          <div className={styles["account_info"]}>
            <section className={styles["account_info"]}>
              <div className={styles["title"]}>
                <Image
                  src={BCALogo}
                  alt="Logo Bank BCA"
                  width={36}
                  height={11}
                />
                <Text variant="text" size="md" fontStyle="semibold">
                  BCA Virtual Account
                </Text>
              </div>

              <div className={styles["account_number"]}>
                <Text variant="display" size="sm" fontStyle="semibold">
                  1234567890123
                </Text>
                <div onClick={copyAccountNumber} style={{ cursor: "pointer" }}>
                  <IconCopy />
                </div>
              </div>

              <Text variant="text" size="sm" fontStyle="regular" muted>
                a.n Wawan Hendrawan
              </Text>
            </section>

            <section className={styles["instructions"]}>
              <Text variant="text" size="lg" fontStyle="semibold">
                Petunjuk Pembayaran
              </Text>

              <Accordion>
                <AccordionItem>
                  <AccordionTrigger>
                    <Text variant="text" size="md" fontStyle="regular">
                      ATM BCA
                    </Text>
                  </AccordionTrigger>
                  <AccordionContent>
                    <>
                      <strong>Melalui ATM</strong>
                      <ol
                        style={{
                          display: "flex",
                          flexDirection: "column",
                          gap: "10px",
                          paddingTop: "10px",
                          paddingLeft: "20px",
                        }}
                      >
                        <li>Masukkan kartu ATM dan PIN BCA kamu.</li>
                        <li>
                          Pada menu utama, pilih menu “
                          <strong>Transaksi lainnya</strong>”.
                        </li>
                        <li>
                          Pilih menu “<strong>Transfer</strong>” dan kemudian
                          pilih “<strong>BCA AVirtual Account</strong>”.
                        </li>
                        <li>
                          Masukkan no. BCA Virtual Account & klik “
                          <strong>Lanjutkan</strong>”.
                        </li>
                        <li>
                          Periksa kembali rincian pembayaran kamu, lalu pilih
                          Ya.
                        </li>
                      </ol>
                    </>
                  </AccordionContent>
                </AccordionItem>
                <AccordionItem>
                  <AccordionTrigger>
                    <Text variant="text" size="md" fontStyle="regular">
                      M-BCA
                    </Text>
                  </AccordionTrigger>
                  <AccordionContent>
                    <>
                      <strong>Melalui M-BCA</strong>
                      <ol
                        style={{
                          display: "flex",
                          flexDirection: "column",
                          gap: "10px",
                          paddingTop: "10px",
                          paddingLeft: "20px",
                        }}
                      >
                        <li>Lakukan log in pada aplikai BCA mobile.</li>
                        <li>
                          Pilih “<strong>m-BCA</strong>” masukkan kode akses
                          m-BCA.
                        </li>
                        <li>
                          Pilih “<strong>m-Transfer</strong>”.
                        </li>
                        <li>
                          Pilih “<strong>BCA Virtual Account</strong>”.
                        </li>
                        <li>
                          Masukkan nomor BCA Virtual Account dan klik “
                          <strong>OK</strong>”.
                        </li>
                        <li>
                          Konfirmasi no virtual account dan rekening pendebetan.
                        </li>
                        <li>
                          Periksa kembalian rincian pembayaran kamu, lalu klik “
                          <strong>Ya</strong>”.
                        </li>
                      </ol>
                    </>
                  </AccordionContent>
                </AccordionItem>
                <AccordionItem>
                  <AccordionTrigger>
                    <Text variant="text" size="md" fontStyle="regular">
                      Klik BCA
                    </Text>
                  </AccordionTrigger>
                  <AccordionContent>
                    <>
                      <strong>Melalui Klik BCA</strong>
                      <ol
                        style={{
                          display: "flex",
                          flexDirection: "column",
                          gap: "10px",
                          paddingTop: "10px",
                          paddingLeft: "20px",
                        }}
                      >
                        <li>
                          Login pada aplikasi KlikBCA, masukkan user ID & PIN.
                        </li>
                        <li>
                          Pilih “<strong>Transfer Dana</strong>”, kemudian pilih
                          “<strong>Transfer ke BCA Virtual Account</strong>”.
                        </li>
                        <li>
                          Masukkan nomor BCA Virtual Account & klik “
                          <strong>Lanjutkan</strong>”.
                        </li>
                        <li>
                          Pastikan data yang dimasukkan sudah benar, dan input “
                          <strong>Respon KeyBCA</strong>”, lalu klik “
                          <strong>Kirim</strong>”.
                        </li>
                      </ol>
                    </>
                  </AccordionContent>
                </AccordionItem>
              </Accordion>
            </section>
          </div>
        </TabsContent>
        <TabsContent value="dana">
          <div className={styles["account_info"]}>DANA</div>
        </TabsContent>
        <TabsContent value="gopay">
          <div className={styles["account_info"]}>GOPAY</div>
        </TabsContent>
        <TabsContent value="ovo">
          <div className={styles["account_info"]}>OVO</div>
        </TabsContent>
      </Tabs>

      <UploadPaymentModal />
    </div>
  );
};

export default PaymentMethodCard;
