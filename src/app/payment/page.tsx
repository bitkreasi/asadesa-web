import styles from "./payment.module.scss";
import PaymentDetailCard from "./_components/payment-detail-card";
import PaymentMethodCard from "./_components/payment-method-card";
import { Metadata } from "next";

export const metadata: Metadata = {
  title: "Pembayaran - Asadesa",
  description: "Halaman Pembayaran",
};

export default function PaymentPage() {
  return (
    <div className={styles["payment__wrapper"]}>
      <div className={styles["payment__info__container"]}>
        <PaymentDetailCard type="premium" />
      </div>
      <div className={styles["payment__method__container"]}>
        <PaymentMethodCard accountNumber="1234567890123" />
      </div>
    </div>
  );
}
