type DataI = {
  name_village: string
  name_chief_village: string
  imageUrl: string,
  nik: string
  rt: number
  rw: number
  woman: number
  man: number
}

export const dummyData: DataI[] = [
  {
    name_village: 'Village A',
    name_chief_village: 'John Doe',
    nik: '1234567890',
    imageUrl: "https://source.unsplash.com/ycZjz2Uw6VY/1600x900",
    rt: 1,
    rw: 2,
    woman: 50,
    man: 60,
  },
  {
    name_village: 'Village B',
    name_chief_village: 'Jane Smith',
    imageUrl: "https://source.unsplash.com/ycZjz2Uw6VY/1600x900",
    nik: '0987654321',
    rt: 3,
    rw: 4,
    woman: 70,
    man: 80,
  },
  {
    name_village: 'Village C',
    name_chief_village: 'Michael Johnson',
    imageUrl: "https://source.unsplash.com/ycZjz2Uw6VY/1600x900",
    nik: '5432167890',
    rt: 5,
    rw: 6,
    woman: 90,
    man: 100,
  },
]
