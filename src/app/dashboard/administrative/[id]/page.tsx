'use client'
import Header from '@/app/ui/components/header/header'
import styles from './page.module.scss'
import Button from '@/app/ui/components/button/button'
import IconMoreCircle from '@/app/ui/icons/icon-more-circle'
import IconAddCircle from '@/app/ui/icons/icon-add-circle'
import Input from '@/app/ui/components/input/input'
import IconBottomArrow from '@/app/ui/icons/icon-bottom-arrow'
import IconSearchStatus from '@/app/ui/icons/icon-search-status'
import Checkbox from '@/app/ui/components/checkbox/checkbox'
import { dummyData } from './dummy-data'
import Avatar from '@/app/ui/components/avatar/avatar'
import Pagination from '@/app/ui/components/pagination/pagination'
import IconTrash from '@/app/ui/icons/icon-trash'
import { useState } from 'react'
import useOutsideClick from '@/hooks/use-outside-click'
import IconPrinter from '@/app/ui/icons/icon-printer'
import IconEdit from '@/app/ui/icons/icon-edit'
import IconDownload from '@/app/ui/icons/icon-download'
import Filter from '@/app/ui/components/filter/filter'
import { useModal } from '@/hooks/use-modal'
import Modal from '@/app/ui/components/modal/modal'

type Props = {
  params: {
    id: string
  }
}

export default function Page(props: Props) {
  const [isMoreOpen, setIsMoreOpen] = useState<boolean>(false)
  const { show: showAddModal, toggle: toggleOpenModal } = useModal()
  const { show: showDeleteModal, toggle: toggleDeleteModal } = useModal()

  function handleDeleteSingle(id: string) {
    toggleDeleteModal()
  }

  const moreRef = useOutsideClick({
    callback: () => setIsMoreOpen(!isMoreOpen),
    isOpen: isMoreOpen,
  })

  return (
    <>
      <div className={styles['main__content']}>
        {/* header */}
        <div className={styles['content__header']}>
          <Header
            items={[
              {
                id: 'dashboard',
                name: 'Beranda',
                link: '/dashboard',
              },
              {
                id: 'administrative',
                name: 'wilayah administrative',
                link: `/dashboard/administrative/${props.params.id}`,
              },
            ]}
            size='xl'
            title='Wilayah Administratif Dusun'
            subTitle=''
          />
          <div className={styles['header__action']}>
            <Button
              prefix={
                <IconAddCircle color='#4163E7' backgroundColor='#4163E7' />
              }
              variant='outline'
              color='primary'
              text='Tambah Dusun'
              onHandleClick={toggleOpenModal}
            />
            <Button
              prefix={
                <IconMoreCircle color='#4163E7' backgroundColor='#4163E7' />
              }
              onHandleClick={() => setIsMoreOpen(!isMoreOpen)}
              variant='outline'
              color='primary'
              text=''
            />

            {!!isMoreOpen && (
              <div className={styles['more__menu']} ref={moreRef}>
                <button className={styles['more__item']}>
                  <IconPrinter />
                  <span>Cetak Massal</span>
                </button>
                <button className={styles['more__item']}>
                  <IconEdit />
                  <span>Edit Data Dusun</span>
                </button>
                <button className={styles['more__item']}>
                  <IconDownload />
                  <span>Download</span>
                </button>
                <button className={styles['more__item']}>
                  <IconTrash />
                  <span>Hapus Semua Data</span>
                </button>
              </div>
            )}
          </div>
        </div>

        {/* search and filter table */}
        <div className={styles['content__search']}>
          <div className={styles['content__search--input']}>
            <Input
              placeholder='Cari data dusun'
              variant='text-field'
              value={''}
              label=''
              helperText=''
              prefix={<IconSearchStatus />}
            />
          </div>
          <div className={styles['conter__search--filter__button']}>
            <Filter>
              <div className={styles['content__filter--data']}>
                {/* resident status */}
                <div className={styles['content__filter--data--main']}>
                  <span>Status Penduduk</span>
                  <div className={styles['content__filter--data--item']}>
                    <div>
                      <Checkbox size='md' helperText='' label='Tetap' />
                    </div>
                    <div>
                      <Checkbox size='md' helperText='' label='Tidak Tetap' />
                    </div>
                  </div>
                </div>
                {/* existence status */}
                <div className={styles['content__filter--data--main']}>
                  <span>Status Keberadaan</span>
                  <div className={styles['content__filter--data--item']}>
                    <div>
                      <Checkbox size='md' helperText='' label='Hidup' />
                    </div>
                    <div>
                      <Checkbox size='md' helperText='' label='Meninggal' />
                    </div>
                    <div>
                      <Checkbox size='md' helperText='' label='Pindah' />
                    </div>
                    <div>
                      <Checkbox size='md' helperText='' label='Hilang' />
                    </div>
                    <div>
                      <Checkbox size='md' helperText='' label='Pergi' />
                    </div>
                    <div>
                      <Checkbox
                        size='md'
                        helperText=''
                        label='Tidak Diketahui'
                      />
                    </div>
                  </div>
                </div>
                {/* gender */}
                <div className={styles['content__filter--data--main']}>
                  <span>Jenis Kelamin</span>
                  <div className={styles['content__filter--data--item']}>
                    <div>
                      <Checkbox size='md' helperText='' label='Laki - Laki' />
                    </div>
                    <div>
                      <Checkbox size='md' helperText='' label='Perempuan' />
                    </div>
                  </div>
                </div>
                {/* village */}
                <div className={styles['content__filter--data--main']}>
                  <span>Dusun</span>
                  <div className={styles['content__filter--data--item']}>
                    {['Dusun 1', 'Dusun 2', 'Dusun 3'].map((dusun, i) => (
                      <div key={i}>
                        <Checkbox size='md' helperText='' label={dusun} />
                      </div>
                    ))}
                  </div>
                </div>
              </div>

              {/* the filter button */}
              <div className={styles['content__filter--button']}>
                <Button
                  variant='solid'
                  text='Terapkan Filter'
                  color='primary'
                  size='md'
                />
                <Button
                  variant='outline'
                  text='Reset Filter'
                  color='primary'
                  size='md'
                />
              </div>
            </Filter>
          </div>
        </div>

        {/* table */}
        <div className={styles['content__resident']}>
          <div className={styles['resident__table--container']}>
            <table className={styles['resident__table']}>
              <thead>
                <tr>
                  <th>
                    <Checkbox size='lg' helperText='' label='' />
                  </th>
                  <th>No</th>
                  <th>Nama Dusun</th>
                  <th>Kepala Dusun</th>
                  <th>NIK Kepala Dusun</th>
                  <th>RT</th>
                  <th>RW</th>
                  <th>Jumlah KK</th>
                  <th>L+P</th>
                  <th>Laki-Laki(L)</th>
                  <th>Perempuan(P)</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                {dummyData?.map((data, index) => (
                  <tr key={index}>
                    <td>
                      <Checkbox size='lg' helperText='' label='' />
                    </td>
                    <td>{index + 1}</td>
                    <td>
                      <span className={styles['resident__table--user__name']}>
                        {data.name_village}
                      </span>
                    </td>
                    <td>
                      <div className={styles['resident__table--user__profile']}>
                        <Avatar
                          variant='photo'
                          source={data.imageUrl}
                          size='sm'
                        />
                        <span className={styles['resident__table--user__name']}>
                          {data.name_chief_village}
                        </span>
                      </div>
                    </td>
                    <td>
                      <span>{data.nik}</span>
                    </td>
                    <td>
                      <span>{data.rt}</span>
                    </td>
                    <td>
                      <span>{data.rw}</span>
                    </td>
                    <td>
                      <span>{data.man}</span>
                    </td>
                    <td>
                      <span>{data.man + data.woman}</span>
                    </td>
                    <td>
                      <span>{data.man}</span>
                    </td>
                    <td>
                      <span>{data.woman}</span>
                    </td>
                    <td>
                      <button
                        className={styles['button__remove']}
                        onClick={() => handleDeleteSingle(data.nik)}
                      >
                        <IconTrash backgroundColor='#F49C56' color='#F49C56' />
                      </button>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>

          {/* pagination */}
          <div className={styles['pagination__content']}>
            <Pagination
              variant='item-per-page'
              numberOfView={[10, 20, 100, 1000]}
            />
            <Pagination variant='page-number' totalPage={5} />
          </div>
        </div>

        <Modal isOpen={showDeleteModal} toggle={toggleDeleteModal}>
          <div className={styles['modal__body modal__body--center']}>
            <p className={styles['modal__title']}>
              Anda yakin ingin menghapus data dusun ini?
            </p>
            <p className={styles['modal__body']}>
              Data yang dusun yang dihapus akan terhapus secara permanen.
            </p>
            <div className={styles['modal__action']}>
              <Button text='Batal' variant='outline' color='neutral' />
              <Button text='Hapus' variant='solid' color='error' />
            </div>
          </div>
        </Modal>

        <Modal isOpen={showAddModal} toggle={toggleOpenModal}>
          <div className={styles['modal__body']}>
            <p className={styles['modal__title']}>
              Tambah Data Wilayah Administratif Dusun
            </p>
            <div className={styles['modal__body--form']}>
              <div>
                <Input
                  label='Nama Dusun'
                  size='md'
                  placeholder='Nama Dusun'
                  textFieldType='text'
                  helperText=''
                />
              </div>
              <div>
                <Input
                  label='NIK Dusun'
                  size='md'
                  placeholder=''
                  textFieldType='text'
                  helperText='Silahkan cari nama / NIK dari data penduduk yang sudah terinput. Penduduk yang dipilih otomatis berstatus sebagai Kepala Dusun baru tersebut.'
                />
              </div>
            </div>
            <div className={styles['modal__action']}>
              <Button text='Cek Ulang' variant='outline' color='neutral' />
              <Button text='Simpan' variant='solid' color='primary' />
            </div>
          </div>
        </Modal>
      </div>
    </>
  )
}
