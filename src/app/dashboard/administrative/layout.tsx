import LayoutDashboard from '@/app/ui/layouts/dashboard/layout-dashboard'

export default function LayoutAdministrative({ children }: React.PropsWithChildren) {
  return (
    <LayoutDashboard>
      <div style={{ padding: '24px' }}>{children}</div>
    </LayoutDashboard>
  )
}
