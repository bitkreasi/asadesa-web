'use client'

import styles from '../style.module.scss'
import Header from '@/app/ui/components/header/header'
import { useModal } from '@/hooks/use-modal'
import Button from '@/app/ui/components/button/button'
import IconAddCircle from '@/app/ui/icons/icon-add-circle'
import Modal from '@/app/ui/components/modal/modal'
import { DataTable } from '../../_component/category-table/datatable'
import { columns } from '../../_component/category-table/column'
import { DataInstitutionsCategory } from './category-data'
import Input from '@/app/ui/components/input/input'
import { useState } from 'react'

export type Breadcrumb = {
  id: string
  name: string
  link: string
}

function getBreadcrumb(id: string): Breadcrumb[] {
  return [
    {
      id: 'dashboard',
      name: 'beranda',
      link: '/dashboard',
    },
    {
      id: 'institutions',
      name: 'Lembaga Desa',
      link: `/dashboard/institutions/${id}`,
    },
    {
      id: 'institutionsAdd',
      name: 'Tambah Kategori Lembaga',
      link: `/dashboard/institutions/${id}/category`,
    },
  ]
}

type Props = {
  params: {
    id: string
  }
}

export default function Page(props: Props) {
  const { show: showAddCategory, toggle: toggleAddCategory } = useModal()
  const [isConfirmSave, setIsConfirmSave] = useState<boolean>(false)

  return (
    <section>
      <div className={styles['header__wrapper']}>
        <Header
          items={getBreadcrumb(props.params.id)}
          size='xl'
          title='Tambah Kategori Lembaga'
          subTitle=''
        />
        <div className={styles['header__action']}>
          <Button
            prefix={<IconAddCircle color='#4163E7' backgroundColor='#4163E7' />}
            variant='outline'
            color='primary'
            text='Tambah Kategori Lembaga'
            onHandleClick={toggleAddCategory}
          />
        </div>
      </div>
      <DataTable columns={columns} data={DataInstitutionsCategory} />
      <Modal isOpen={showAddCategory} toggle={toggleAddCategory}>
        <div className={styles['modal__body']}>
          {isConfirmSave ? (
            <>
              <p className={styles['modal__title']}>
                Anda Yakin ingin menyimpan data ini
              </p>
              <p className={styles['modal__subtitle']}>
                Setelah anda menyimpan data ini, anda dapat kembali mengedit
                data ini dengan pergi ke detail dan edit data.
              </p>
            </>
          ) : (
            <>
              <p className={styles['modal__title']}>Tambah Kategori Lembaga</p>
              <div className={styles['modal__body--form']}>
                <div>
                  <Input
                    label='Klasifikasi/Kategori Lembaga'
                    size='sm'
                    placeholder='kategori lembaga'
                    textFieldType='text'
                    helperText=''
                  />
                </div>
                <div>
                  <Input
                    label='Deskripsi Lembaga'
                    size='sm'
                    placeholder=''
                    variant='text-area'
                    helperText=''
                  />
                </div>
              </div>
            </>
          )}
          <div className={styles['modal__action']}>
            <Button
              text={!!isConfirmSave ? 'Cek Ulang' : 'Batal'}
              variant='outline'
              color='neutral'
              onHandleClick={() => {
                if (!!isConfirmSave) {
                  setIsConfirmSave(false)
                } else toggleAddCategory()
              }}
            />
            <Button
              text='Simpan'
              variant='solid'
              color='primary'
              onHandleClick={() => {
                if (!!isConfirmSave) {
                  toggleAddCategory()
                } else setIsConfirmSave(true)
              }}
            />
          </div>
        </div>
      </Modal>
    </section>
  )
}
