'use client'

import styles from './style.module.scss'
import Header from '@/app/ui/components/header/header'
import { useModal } from '@/hooks/use-modal'
import Button from '@/app/ui/components/button/button'
import IconAddCircle from '@/app/ui/icons/icon-add-circle'
import { useRouter } from 'next/navigation'
import IconMoreCircle from '@/app/ui/icons/icon-more-circle'
import { useState } from 'react'
import useOutsideClick from '@/hooks/use-outside-click'
import IconPrinter from '@/app/ui/icons/icon-printer'
import IconEdit from '@/app/ui/icons/icon-edit'
import IconDownload from '@/app/ui/icons/icon-download'
import IconTrash from '@/app/ui/icons/icon-trash'
import Modal from '@/app/ui/components/modal/modal'
import Input from '@/app/ui/components/input/input'
import { InstitutionsData } from './institutions-data'
import { DataTable } from '../_component/institutions-table/datatable'
import { columns } from '../_component/institutions-table/column'
import Link from 'next/link'

export type Breadcrumb = {
  id: string
  name: string
  link: string
}

function getBreadcrumb(id: string): Breadcrumb[] {
  return [
    {
      id: 'dashboard',
      name: 'beranda',
      link: '/dashboard',
    },
    {
      id: 'institutions',
      name: 'Lembaga Desa',
      link: `/dashboard/institutions/${id}`,
    },
  ]
}

type Props = {
  params: {
    id: string
  }
}

export default function Page(props: Props) {
  const [isMoreOpen, setIsMoreOpen] = useState<boolean>(false)
  const [isConfirmSave, setIsConfirmSave] = useState<boolean>(false)

  const { show: showDownload, toggle: toggleDownload } = useModal()
  const { show: showDelete, toggle: toggleDelete } = useModal()
  const { show: showAdd, toggle: toggleAdd } = useModal()

  const moreRef = useOutsideClick({
    callback: () => setIsMoreOpen(!isMoreOpen),
    isOpen: isMoreOpen,
  })

  return (
    <section>
      <div className={styles['header__wrapper']}>
        <Header
          items={getBreadcrumb(props.params.id)}
          size='xl'
          title='Lembaga Desa'
          subTitle=''
        />
        <div className={styles['header__action']}>
          <Button
            prefix={<IconAddCircle color='#fff' backgroundColor='#fff' />}
            color='primary'
            text='Tambah Lembaga'
            onHandleClick={toggleAdd}
          />
          <Link href={`/dashboard/institutions/${props.params.id}/category`}>
            <Button
              prefix={
                <IconAddCircle color='#4163E7' backgroundColor='#4163E7' />
              }
              variant='outline'
              color='primary'
              text='Tambah Kategori Lembaga'
            />
          </Link>

          <Button
            prefix={
              <IconMoreCircle color='#4163E7' backgroundColor='#4163E7' />
            }
            onHandleClick={() => setIsMoreOpen(!isMoreOpen)}
            variant='outline'
            color='primary'
            text=''
          />

          {!!isMoreOpen && (
            <div className={styles['header__more-container']} ref={moreRef}>
              <button className={styles['header__more-item']}>
                <IconPrinter />
                <span>Cetak Massal</span>
              </button>
              <button
                className={styles['header__more-item']}
                onClick={toggleDownload}
              >
                <IconDownload />
                <span>Download</span>
              </button>
            </div>
          )}
        </div>
      </div>
      <DataTable columns={columns} data={InstitutionsData} params={props.params}/>
      <Modal isOpen={showDownload} toggle={toggleDownload}>
        <Input
          label='Tipe file'
          variant='dropdown-select'
          helperText=''
          size='md'
          items={['xlsx', 'csv']}
        />
        <div className={styles['modal__below']}>
          <Button
            text='Batal'
            color='neutral'
            variant='outline'
            onHandleClick={toggleDownload}
            size='lg'
          />
          <Button color='primary' text='Download' size='lg' />
        </div>
      </Modal>
      <Modal isOpen={showDelete} toggle={toggleDelete}>
        <div className={styles['modal__top']}>
          <p className={styles['modal__title']}>
            Anda yakin ingin menghapus data ini?
          </p>
          <p className={styles['modal__body']}>
            Data desa yang dihapus akan terhapus secara permanen.
          </p>
        </div>
        <div className={styles['modal__below']}>
          <Button
            text='Batal'
            color='neutral'
            variant='outline'
            onHandleClick={toggleDelete}
            size='lg'
          />
          <Button text='Hapus' color='error' size='lg' />
        </div>
      </Modal>
      <Modal isOpen={showAdd} toggle={toggleAdd}>
        <div className={styles['modal__body']}>
          {isConfirmSave ? (
            <>
              <p className={styles['modal__title']}>
                Anda Yakin ingin menyimpan data ini
              </p>
              <p className={styles['modal__subtitle']}>
                Setelah anda menyimpan data ini, anda dapat kembali mengedit
                data ini dengan pergi ke detail dan edit data.
              </p>
            </>
          ) : (
            <>
              <p className={styles['modal__title']}>Tambah Kategori Lembaga</p>
              <div className={styles['modal__body--form']}>
                <div>
                  <Input
                    label='Nama Lembaga'
                    size='sm'
                    placeholder='Nama Lembaga'
                    textFieldType='text'
                    helperText=''
                  />
                </div>
                <div>
                  <Input
                    label='Kode Lembaga'
                    size='sm'
                    placeholder='Kode Lembaga'
                    textFieldType='text'
                    helperText='Pastikan Kode belum pernah dipakai di data lembaga/di data kelompok'
                  />
                </div>
                <div>
                  <Input
                    label='Kategori Lembaga'
                    size='sm'
                    placeholder='Pilih Kategori Lembaga'
                    textFieldType='text'
                    helperText=''
                    variant='dropdown-select'
                    items={['Posyandu', 'Karang taruna', 'Pemuda']}
                  />
                </div>
                <div>
                  <Input
                    label='Ketua Lembaga'
                    size='sm'
                    placeholder='Pilih Ketua Lembaga'
                    textFieldType='text'
                    helperText=''
                    variant='dropdown-select'
                    items={[
                      '200001245 - Bintang',
                      '2000111234 - Lintang',
                      '2235655321 - Novi',
                    ]}
                  />
                </div>
                <div>
                  <Input
                    label='Deskripsi Lembaga'
                    size='sm'
                    placeholder='Deskripsi Lembaga'
                    textFieldType='text'
                    helperText=''
                    variant='text-area'
                  />
                </div>
              </div>
            </>
          )}
          <div className={styles['modal__action']}>
            <Button
              text={!!isConfirmSave ? 'Cek Ulang' : 'Batal'}
              variant='outline'
              color='neutral'
              onHandleClick={() => {
                if (!!isConfirmSave) {
                  setIsConfirmSave(false)
                } else toggleAdd()
              }}
            />
            <Button
              text='Simpan'
              variant='solid'
              color='primary'
              onHandleClick={() => {
                if (!!isConfirmSave) {
                  toggleAdd()
                } else setIsConfirmSave(true)
              }}
            />
          </div>
        </div>
      </Modal>
    </section>
  )
}
