import { Institutions } from '../_component/institutions-table/column'

export const InstitutionsData: Institutions[] = [
  {
    category: 'Posyandu',
    chief_institutions: 'Bintang',
    code_institutions: 'D1',
    name: 'Desa',
    number_member: 15,
  },
  {
    category: 'Posyandu Remaja',
    chief_institutions: 'Bintang',
    code_institutions: 'D1',
    name: 'Desa',
    number_member: 15,
  },
  {
    category: 'Pemuda',
    chief_institutions: 'Bintang',
    code_institutions: 'D2',
    name: 'Desa',
    number_member: 15,
  },
]
