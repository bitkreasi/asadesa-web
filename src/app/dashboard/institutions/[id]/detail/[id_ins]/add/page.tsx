'use client'

import { Card, CardContent, CardHeader } from '@/app/ui/components/card/card'
import styles from '../../../style.module.scss'
import Header from '@/app/ui/components/header/header'
import IconPeople from '@/app/ui/icons/icon-people'
import Input from '@/app/ui/components/input/input'
import DefaultImg from '@/app/ui/images/foto-profil-default.png'
import Button from '@/app/ui/components/button/button'
import IconImport from '@/app/ui/icons/icon-import'
import { useRef, useState } from 'react'
import { useModal } from '@/hooks/use-modal'
import Modal from '@/app/ui/components/modal/modal'
import { useRouter } from 'next/navigation'

export type Breadcrumb = {
  id: string
  name: string
  link: string
}

function getBreadcrumb(id: string, id_ins: string): Breadcrumb[] {
  return [
    {
      id: 'dashboard',
      name: 'beranda',
      link: '/dashboard',
    },
    {
      id: 'institutions',
      name: 'Lembaga Desa',
      link: `/dashboard/institutions/${id}`,
    },
    {
      id: 'detail',
      name: 'Detail Lembaga',
      link: `/dashboard/institutions/${id}/detail/${id_ins}`,
    },
    {
      id: 'detailAdd',
      name: 'Tambah Anggota',
      link: `/dashboard/institutions/${id}/detail/${id_ins}/add`,
    },
  ]
}

type Props = {
  params: {
    id: string
    id_ins: string
  }
}

export default function Page(props: Props) {
  const inputRef = useRef<HTMLInputElement>(null)
  const [preview, setPreview] = useState<string>(DefaultImg.src)
  const { show, toggle } = useModal()
  const router = useRouter()

  async function uploadImageDisplay() {
    const uploadedFile = inputRef.current?.files?.[0]
    if (uploadedFile) {
      const cachedURL = URL.createObjectURL(uploadedFile)
      setPreview(cachedURL)
    } else {
      console.error('No file selected')
    }
  }

  return (
    <section className={styles['section--detail']}>
      <div className={styles['header__wrapper']}>
        <Header
          items={getBreadcrumb(props.params.id, props.params.id_ins)}
          size='xl'
          title='Tambah Anggota Lembaga'
          subTitle=''
          isBack
        />
      </div>
      <form encType='multipart/form-data'>
        <Card>
          <CardHeader>
            <IconPeople />
            <p className={styles['form__header']}>Data Anggota Lembaga</p>
          </CardHeader>
          <CardContent>
            <Card>
              <div className={styles['form__photo']}>
                <div className={styles['form__photo--left']}>
                  <img src={preview} alt='preview' />
                  <div>
                    <p>Foto Profil</p>
                    <p>PNG, JPEG dibawah 2MB</p>
                  </div>
                  <input
                    type='file'
                    hidden
                    ref={inputRef}
                    onChange={uploadImageDisplay}
                  />
                </div>
                <Button
                  prefix={<IconImport backgroundColor='#fff' color='#fff' />}
                  text='Unggah Photo'
                  color='primary'
                  onHandleClick={() => inputRef.current?.click()}
                />
              </div>
            </Card>
            <div className={styles['form__add-member']}>
              <div className={styles['form__add-member__wrapper']}>
                <div>
                  <Input
                    label='Nama Anggota'
                    size='md'
                    variant='dropdown-select'
                    items={['200001', '2000002', '200004']}
                    helperText=''
                  />
                </div>
                <div>
                  <Input
                    label='Jabatan'
                    size='md'
                    variant='dropdown-select'
                    items={['ketua', 'anggota']}
                    helperText=''
                  />
                </div>
                <div>
                  <Input
                    label='Nomor SK Pengangkatan'
                    size='md'
                    helperText=''
                  />
                </div>
                <div>
                  <Input
                    label='Nomor SK Pemberhentian'
                    size='md'
                    helperText=''
                  />
                </div>
                <div>
                  <Input
                    label='Masa Jabatan (usia/Periode)'
                    size='md'
                    helperText=''
                  />
                </div>
              </div>
              <div className={styles['form__add-member__wrapper']}>
                <div>
                  <Input label='Nomor Anggota' size='md' helperText='' />
                </div>
                <div>
                  <Input label='Nomor SK Jabatan' size='md' helperText='' />
                </div>
                <div>
                  <Input
                    label='Tanggal SK Pengangkatan'
                    size='md'
                    textFieldType='date'
                    helperText=''
                  />
                </div>
                <div>
                  <Input
                    label='Tanggal SK Pemberhentian'
                    size='md'
                    textFieldType='date'
                    helperText=''
                  />
                </div>
                <div>
                  <Input
                    label='Keterangan'
                    size='md'
                    variant='text-area'
                    helperText=''
                  />
                </div>
              </div>
            </div>
          </CardContent>
        </Card>
        <div className={styles['form__submit']}>
          <Button
            type='button'
            text='Batal'
            color='neutral'
            variant='outline'
          />
          <Button
            type='button'
            text='Simpan'
            color='primary'
            onHandleClick={toggle}
          />
        </div>
      </form>
      <Modal isOpen={show} toggle={toggle}>
        <div className={styles['modal__body']}>
          <p className={styles['modal__title']}>
            Anda Yakin ingin menyimpan data ini
          </p>
          <p className={styles['modal__subtitle']}>
            Setelah anda menyimpan data ini, anda dapat kembali mengedit data
            ini dengan pergi ke detail dan edit data.
          </p>

          <div className={styles['modal__action']}>
            <Button text='Cek Ulang' variant='outline' color='neutral' />
            <Button
              text='Simpan'
              variant='solid'
              color='primary'
              onHandleClick={() =>
                router.push(
                  `/dashboard/institutions/${props.params.id}/detail/${props.params.id_ins}`
                )
              }
            />
          </div>
        </div>
      </Modal>
    </section>
  )
}
