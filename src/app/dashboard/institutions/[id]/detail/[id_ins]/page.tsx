'use client'

import styles from '../../style.module.scss'
import Header from '@/app/ui/components/header/header'
import Button from '@/app/ui/components/button/button'
import IconAddCircle from '@/app/ui/icons/icon-add-circle'
import IconDocument from '@/app/ui/icons/icon-document'
import { DataTable } from '../../../_component/detail-table/datatable'
import { columns } from '../../../_component/detail-table/column'
import { DetailData } from './detail-data'
import { Card, CardContent, CardTitle } from '@/app/ui/components/card/card'
import Link from 'next/link'

export type Breadcrumb = {
  id: string
  name: string
  link: string
}

function getBreadcrumb(id: string, id_ins: string): Breadcrumb[] {
  return [
    {
      id: 'dashboard',
      name: 'beranda',
      link: '/dashboard',
    },
    {
      id: 'institutions',
      name: 'Lembaga Desa',
      link: `/dashboard/institutions/${id}`,
    },
    {
      id: 'detail',
      name: 'Lembaga Desa',
      link: `/dashboard/institutions/${id}/detail/${id_ins}`,
    },
  ]
}

type Props = {
  params: {
    id: string
    id_ins: string
  }
}

export default function Page(props: Props) {
  return (
    <section className={styles['section--detail']}>
      <div className={styles['header__wrapper']}>
        <Header
          items={getBreadcrumb(props.params.id, props.params.id_ins)}
          size='xl'
          title='Detail Lembaga'
          subTitle=''
          isBack
        />
        <div className={styles['header__action']}>
          <Link
            href={`/dashboard/institutions/${props.params.id}/detail/${props.params.id_ins}/add`}
          >
            <Button
              prefix={
                <IconAddCircle color='#4163E7' backgroundColor='#4163E7' />
              }
              text='Tambah Anggota Lembaga'
              variant='outline'
              color='primary'
            />
          </Link>
          <Button
            prefix={<IconDocument color='#4163E7' backgroundColor='#4163E7' />}
            variant='outline'
            color='primary'
            text='Unduh'
          />
        </div>
      </div>
      <Card>
        <CardTitle>Rincian Lembaga</CardTitle>
        <CardContent>
          <div className={styles['detail--top']}>
            <div>
              <p className={styles['detail__label']}>Kode Lembaga</p>
              <p className={styles['detail__value']}>124</p>
            </div>
            <div>
              <p className={styles['detail__label']}>Nama Lembaga</p>
              <p className={styles['detail__value']}>Posyandu</p>
            </div>
          </div>
          <div className={styles['detail--below']}>
            <div>
              <p className={styles['detail__label']}>Ketua Lembaga</p>
              <p className={styles['detail__value']}>Bintang</p>
            </div>
            <div>
              <p className={styles['detail__label']}>Kategori Lembaga</p>
              <p className={styles['detail__value']}>Posyandu</p>
            </div>
            <div>
              <p className={styles['detail__label']}>Keterangan</p>
              <p className={styles['detail__value']}>lorem ipsum dolor</p>
            </div>
          </div>
        </CardContent>
      </Card>
      <DataTable
        columns={columns({ id: props.params.id, id_ins: props.params.id_ins })}
        data={DetailData}
      />
    </section>
  )
}
