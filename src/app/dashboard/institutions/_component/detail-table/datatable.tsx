'use client'

import {
  ColumnDef,
  flexRender,
  getCoreRowModel,
  getFilteredRowModel,
  getPaginationRowModel,
  useReactTable,
} from '@tanstack/react-table'
import React from 'react'
import styles from './datatable.module.scss'
import { Input } from '@/app/ui/components/input/v2/input'
import IconSearch from '@/app/ui/icons/icon-search'
import {
  DropdownMenu,
  DropdownMenuClose,
  DropdownMenuContent,
  DropdownMenuTrigger,
} from '@/app/ui/components/dropdown-menu/dropdown-menu'
import { Text } from '@/app/ui/components/typography/typography'
import IconCloseCircle from '@/app/ui/icons/icon-close-circle'
import { Checkbox } from '@/app/ui/components/checkbox/v2/checkbox'
import {
  DataTableProps,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeader,
  TableRow,
  fuzzyFilter,
} from '@/app/ui/components/table/table'
import IconArrowLeft from '@/app/ui/icons/icon-arrow-left'
import IconArrowRight from '@/app/ui/icons/icon-arrow-right'
import { useRouter } from 'next/navigation'
import { useAuthStore } from '@/store/authStore'
import Button from '@/app/ui/components/button/button'

export function DataTable<TData, TValue>({
  columns,
  data,
}: DataTableProps<TData, TValue>) {
  const [globalFilter, setGlobalFilter] = React.useState('')
  const table = useReactTable({
    data,
    columns,
    filterFns: {
      fuzzy: fuzzyFilter,
    },
    state: {
      globalFilter,
    },
    onGlobalFilterChange: setGlobalFilter,
    globalFilterFn: fuzzyFilter,
    getCoreRowModel: getCoreRowModel(),
    getFilteredRowModel: getFilteredRowModel(),
    getPaginationRowModel: getPaginationRowModel(),
  })
  const router = useRouter()

  const { user } = useAuthStore()

  return (
    <div className={styles['datatable__wrapper']}>
      <p className={styles['datatable__title']}>Daftar Anggota Lembaga</p>

      {/* start -- table */}
      <div>
        <Table>
          <TableHeader>
            {table.getHeaderGroups().map((headerGroup) => (
              <TableRow key={headerGroup.id}>
                {headerGroup.headers.map((header) => {
                  return (
                    <TableHead key={header.id}>
                      {header.isPlaceholder
                        ? null
                        : flexRender(
                            header.column.columnDef.header,
                            header.getContext()
                          )}
                    </TableHead>
                  )
                })}
              </TableRow>
            ))}
          </TableHeader>

          <TableBody>
            {table.getRowModel().rows?.length ? (
              table.getRowModel().rows.map((row) => (
                <TableRow className={styles['table__row']} key={row.id}>
                  {row.getVisibleCells().map((cell) => (
                    <TableCell key={cell.id}>
                      {flexRender(
                        cell.column.columnDef.cell,
                        cell.getContext()
                      )}
                    </TableCell>
                  ))}
                </TableRow>
              ))
            ) : (
              <TableRow>
                <TableCell colSpan={columns.length} className=''>
                  No results.
                </TableCell>
              </TableRow>
            )}
          </TableBody>
        </Table>
      </div>
      {/* end -- table */}

      {/* start -- pagination */}
      <div className={styles['datatable__footer']}>
        <div className={styles['pagination__page']}>
          <Text variant='text' size='sm' fontStyle='regular'>
            Tampilkan
          </Text>
          <select
            name='paginationPage'
            id='paginationPage'
            value={table.getState().pagination.pageSize}
            onChange={(e) => {
              table.setPageSize(Number(e.target.value))
            }}
            className={styles['page__select']}
          >
            {[10, 20, 30, 40, 50].map((pageSize) => (
              <option key={pageSize} value={pageSize}>
                {pageSize}
              </option>
            ))}
          </select>
          <Text variant='text' size='sm' fontStyle='regular'>
            data
          </Text>
        </div>
        <div className={styles['datatable__pagination']}>
          <button
            className={styles['btn__navigation']}
            onClick={() => table.previousPage()}
            disabled={!table.getCanPreviousPage()}
          >
            <IconArrowLeft />
          </button>
          <div className={styles['pagination__number']}>
            <Text variant='text' size='sm' fontStyle='regular'>
              Halaman
            </Text>
            <input
              value={table.getState().pagination.pageIndex + 1}
              onChange={(e) => table.setPageIndex(Number(e.target.value) - 1)}
              className={styles['pagination__input']}
            />
            <Text variant='text' size='sm' fontStyle='regular'>
              dari {table.getPageCount()}
            </Text>
          </div>
          <button
            className={styles['btn__navigation']}
            onClick={() => table.nextPage()}
            disabled={!table.getCanNextPage()}
          >
            <IconArrowRight />
          </button>
        </div>
      </div>
      {/* end -- pagination */}
    </div>
  )
}
