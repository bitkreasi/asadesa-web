'use client'

import { ColumnDef } from '@tanstack/react-table'
import styles from './datatable.module.scss'
import IconEdit from '@/app/ui/icons/icon-edit'
import IconPrinter from '@/app/ui/icons/icon-printer'
import Link from 'next/link'

export type InstitutionsDetail = {
  nama: string
  no_anggota: number
  nik: number
  tempat_lahir: string
  tanggal_lahir: string
  usia: number
  jns_kelamin: 'male' | 'female'
  alamat: string
  jabatan: string
  no_sk_jabatan: number
  no_sk_pengangkatan: number
  no_sk_pemberhentian: number
  tanggal_sk_pengangkatan: string
  tanggal_sk_pemberhentian: string
  masa_jabatan: number
  keterangan: string
  id: string
}

type Params = {
  id: string
  id_ins: string
}

export function columns(param: Params): ColumnDef<InstitutionsDetail>[] {
  return [
    {
      id: 'row_number',
      header: 'No',
      cell: ({ row }) => {
        return <div>{row.index + 1}</div>
      },
    },
    {
      id: 'nama',
      accessorKey: 'nama',
      header: 'Nama',
      cell: ({ row }) => {
        return (
          <div className={styles['table__value']}>{row.getValue('nama')}</div>
        )
      },
    },
    {
      id: 'no_anggota',
      accessorKey: 'no_anggota',
      header: 'No. Anggota',
      cell: ({ row }) => {
        return (
          <div className={styles['table__value']}>
            {row.getValue('no_anggota')}
          </div>
        )
      },
    },
    {
      id: 'nik',
      accessorKey: 'nik',
      header: 'NIK',
      cell: ({ row }) => {
        return (
          <div className={styles['table__value']}>{row.getValue('nik')}</div>
        )
      },
    },
    {
      id: 'nik',
      accessorKey: 'nik',
      header: 'NIK',
      cell: ({ row }) => {
        return (
          <div className={styles['table__value']}>{row.getValue('nik')}</div>
        )
      },
    },
    {
      id: 'ttl',
      header: 'Tempat/Tanggal Lahir',
      cell: ({ row }) => {
        return (
          <div className={styles['table__value--date']}>
            {row.original['tempat_lahir']} / {row.original['tanggal_lahir']}
          </div>
        )
      },
    },
    {
      id: 'usia',
      accessorKey: 'usia',
      header: 'Usia',
      cell: ({ row }) => {
        return (
          <div className={styles['table__value']}>{row.getValue('usia')}</div>
        )
      },
    },
    {
      id: 'jns_kelamin',
      accessorKey: 'jns_kelamin',
      header: 'Jenis Kelamin',
      cell: ({ row }) => {
        return (
          <div className={styles['table__value']}>
            {row.getValue('jns_kelamin')}
          </div>
        )
      },
    },
    {
      id: 'alamat',
      accessorKey: 'alamat',
      header: 'Alamat',
      cell: ({ row }) => {
        return (
          <div className={styles['table__value']}>{row.getValue('alamat')}</div>
        )
      },
    },
    {
      id: 'alamat',
      accessorKey: 'alamat',
      header: 'Alamat',
      cell: ({ row }) => {
        return (
          <div className={styles['table__value']}>{row.getValue('alamat')}</div>
        )
      },
    },
    {
      id: 'jabatan',
      accessorKey: 'jabatan',
      header: 'Jabatan',
      cell: ({ row }) => {
        return (
          <div className={styles['table__value']}>
            {row.getValue('jabatan')}
          </div>
        )
      },
    },
    {
      id: 'no_sk_jabatan',
      accessorKey: 'no_sk_jabatan',
      header: 'Nomor SK Jabatan',
      cell: ({ row }) => {
        return (
          <div className={styles['table__value']}>
            {row.getValue('no_sk_jabatan')}
          </div>
        )
      },
    },
    {
      id: 'no_sk_pengangkatan',
      accessorKey: 'no_sk_pengangkatan',
      header: 'Nomor SK Pengangkatan',
      cell: ({ row }) => {
        return (
          <div className={styles['table__value']}>
            {row.getValue('no_sk_pengangkatan')}
          </div>
        )
      },
    },
    {
      id: 'no_sk_pemberhentian',
      accessorKey: 'no_sk_pemberhentian',
      header: 'Nomor SK Pemberhentian',
      cell: ({ row }) => {
        return (
          <div className={styles['table__value']}>
            {row.getValue('no_sk_pemberhentian')}
          </div>
        )
      },
    },
    {
      id: 'tanggal_sk_pengangkatan',
      accessorKey: 'tanggal_sk_pengangkatan',
      header: 'Tanggal SK Pengangkatan',
      cell: ({ row }) => {
        return (
          <div className={styles['table__value']}>
            {row.getValue('tanggal_sk_pengangkatan')}
          </div>
        )
      },
    },
    {
      id: 'tanggal_sk_pemberhentian',
      accessorKey: 'tanggal_sk_pemberhentian',
      header: 'Tanggal SK Pemberhentian',
      cell: ({ row }) => {
        return (
          <div className={styles['table__value']}>
            {row.getValue('tanggal_sk_pemberhentian')}
          </div>
        )
      },
    },
    {
      id: 'masa_jabatan',
      accessorKey: 'masa_jabatan',
      header: 'Masa Jabatan',
      cell: ({ row }) => {
        return (
          <div className={styles['table__value']}>
            {row.getValue('masa_jabatan')}
          </div>
        )
      },
    },
    {
      id: 'deskripsi',
      accessorKey: 'deskripsi',
      header: 'Deskripsi',
      cell: ({ row }) => {
        return (
          <div className={styles['table__value']}>
            {row.getValue('deskripsi')}
          </div>
        )
      },
    },
    {
      id: 'action',
      cell: ({ row }) => {
        return (
          <div className={styles['table__action']}>
            <Link
              href={`/dashboard/institutions/${param.id}/detail/${param.id_ins}/edit/${row.original.id}`}
            >
              <IconEdit backgroundColor='#F49C56' color='#F49C56' />
            </Link>
            <button>
              <IconPrinter backgroundColor='#F49C56' color='#F49C56' />
            </button>
          </div>
        )
      },
    },
  ]
}
