'use client'

import { ColumnDef } from '@tanstack/react-table'
import React from 'react'
import styles from './datatable.module.scss'
import Button from '@/app/ui/components/button/button'
import IconEdit from '@/app/ui/icons/icon-edit'
import IconPrinter from '@/app/ui/icons/icon-printer'
import Input from '@/app/ui/components/input/input'
import Checkbox from '@/app/ui/components/checkbox/checkbox'

export type Institutions = {
  code_institutions: string
  name: string
  chief_institutions: string
  category: string
  number_member: number
}

export const columns: ColumnDef<Institutions>[] = [
  {
    id: 'row_number',
    header: 'No',
    cell: ({ row }) => {
      return <div>{row.index + 1}</div>
    },
  },
  {
    id: 'code_institutions',
    accessorKey: 'code_institutions',
    header: 'Kode Lembaga',
    cell: ({ row }) => {
      return <div className={styles['table__value']}>{row.getValue('code_institutions')}</div>
    },
  },
  {
    id: 'name',
    accessorKey: 'name',
    header: 'Nama Lembaga',
    cell: ({ row }) => {
      return <div className={styles['table__value']}>{row.getValue('name')}</div>
    },
  },
  {
    id: 'chief_institutions',
    accessorKey: 'chief_institutions',
    header: 'Ketua Lembaga',
    cell: ({ row }) => {
      return <div className={styles['table__value']}>{row.getValue('chief_institutions')}</div>
    },
  },
  {
    id: 'category',
    accessorKey: 'category',
    header: 'Kategori Lembaga',
    cell: ({ row }) => {
      return <div className={styles['table__value']}>{row.getValue('category')}</div>
    },
  },
  {
    id: 'number_member',
    accessorKey: 'number_member',
    header: 'Jumlah Anggota Lembaga',
    cell: ({ row }) => {
      return <div className={styles['table__value']}>{row.getValue('number_member')}</div>
    },
  },
  {
    id: 'action',
    cell: () => {
      return (
        <div className={styles['table__action']}>
          <button>
            <IconEdit backgroundColor='#F49C56' color='#F49C56' />
          </button>
          <button>
            <IconPrinter backgroundColor='#F49C56' color='#F49C56' />
          </button>
        </div>
      )
    },
  },
]
