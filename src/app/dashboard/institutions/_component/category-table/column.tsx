'use client'

import { ColumnDef } from '@tanstack/react-table'
import React from 'react'
import styles from './datatable.module.scss'
import IconEdit from '@/app/ui/icons/icon-edit'
import IconPrinter from '@/app/ui/icons/icon-printer'

export type InstitutionsCategory = {
  category: string
  description: string
  number_institutions: number
}

export const columns: ColumnDef<InstitutionsCategory>[] = [
  {
    id: 'row_number',
    header: 'No',
    cell: ({ row }) => {
      return <div>{row.index + 1}</div>
    },
  },
  {
    id: 'category',
    accessorKey: 'category',
    header: 'Kategori Lembaga',
    cell: ({ row }) => {
      return (
        <div className={styles['table__value']}>{row.getValue('category')}</div>
      )
    },
  },
  {
    id: 'description',
    accessorKey: 'description',
    header: 'Deskripsi Lembaga',
    cell: ({ row }) => {
      return (
        <div className={styles['table__value']}>
          {row.getValue('description')}
        </div>
      )
    },
  },
  {
    id: 'number_institutions',
    accessorKey: 'number_institutions',
    header: 'Jumlah Lembaga',
    cell: ({ row }) => {
      return (
        <div className={styles['table__value']}>
          {row.getValue('number_institutions')}
        </div>
      )
    },
  },
  {
    id: 'action',
    cell: () => {
      return (
        <div className={styles['table__action']}>
          <button>
            <IconEdit backgroundColor='#F49C56' color='#F49C56' />
          </button>
          <button>
            <IconPrinter backgroundColor='#F49C56' color='#F49C56' />
          </button>
        </div>
      )
    },
  },
]
