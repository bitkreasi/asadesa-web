'use client'

import { ColumnDef } from '@tanstack/react-table'
import React from 'react'
import Image from 'next/image'
import styles from './datatable.module.scss'

export type Officials = {
  staff_name: string
  nip: number
  nik: number
  status: 'aktif' | 'tidak aktif'
  tag_id_card: string
  job: string
  group: string
  serviceLength: string
}

export const columns: ColumnDef<Officials>[] = [
  {
    id: 'row_number',
    header: 'No',
    cell: ({ row }) => {
      const idx = row.index
      return <div>{idx + 1}</div>
    },
  },
  {
    id: 'staff_name',
    filterFn: 'fuzzy',
    accessorKey: 'staff_name',
    header: 'Nama Staff Desa',
    cell: ({ row }) => {
      return (
        <div style={{ display: 'flex', gap: 8, alignItems: 'center' }}>
          <Image
            style={{ borderRadius: '9999px' }}
            loading='eager'
            width={32}
            height={32}
            alt={row.getValue('staff_name')}
            src={'https://source.unsplash.com/random'}
          />
          <p>{row.getValue('staff_name')}</p>
        </div>
      )
    },
  },
  {
    id: 'nip',
    accessorKey: 'nip',
    header: 'NIP',
  },
  {
    id: 'nik',
    accessorKey: 'nik',
    header: 'NIK',
  },
  {
    id: 'status',
    accessorKey: 'status',
    header: 'Status',
    cell: ({ row }) => {
      return (
        <div
          className={
            row.getValue('status') == 'aktif'
              ? styles['status--active']
              : styles['status--unactive']
          }
        >
          {row.getValue('status')}
        </div>
      )
    },
  },
  {
    id: 'tag_id_card',
    accessorKey: 'tag_id_card',
    header: 'Tag ID Card',
  },
  {
    id: 'job',
    accessorKey: 'job',
    header: 'Jabatan',
  },
  {
    id: 'group',
    accessorKey: 'group',
    header: 'Golongan',
  },
  {
    id: 'serviceLength',
    accessorKey: 'serviceLength',
    header: 'Masa Jabatan',
  },
]
