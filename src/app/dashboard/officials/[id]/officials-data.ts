import { Officials } from '../_components/officials-table/columns'

export const officialData: Officials[] = [
  {
    staff_name: 'Bambang',
    nip: 11222222,
    nik: 22222222,
    job: 'Kaur Keuangan',
    group: 'I/A',
    serviceLength: '4 tahun',
    status: 'aktif',
    tag_id_card: '',
  },
  {
    staff_name: 'Qodari',
    nip: 11222222,
    nik: 22222222,
    job: 'Kaur Keuangan',
    group: 'I/A',
    serviceLength: '4 tahun',
    status: 'aktif',
    tag_id_card: '',
  },
  {
    staff_name: 'Rocky',
    nip: 11222222,
    nik: 22222222,
    job: 'Kaur Keuangan',
    group: 'I/A',
    serviceLength: '4 tahun',
    status: 'tidak aktif',
    tag_id_card: '',
  },
]
