'use client'

import { columns } from '../_components/officials-table/columns'
import styles from './style.module.scss'
import { DataTable } from '../_components/officials-table/datatable'
import { officialData } from './officials-data'
import Header from '@/app/ui/components/header/header'
import { useModal } from '@/hooks/use-modal'
import Button from '@/app/ui/components/button/button'
import IconAddCircle from '@/app/ui/icons/icon-add-circle'
import { useRouter } from 'next/navigation'
import IconMoreCircle from '@/app/ui/icons/icon-more-circle'
import { useState } from 'react'
import useOutsideClick from '@/hooks/use-outside-click'
import IconPrinter from '@/app/ui/icons/icon-printer'
import IconEdit from '@/app/ui/icons/icon-edit'
import IconDownload from '@/app/ui/icons/icon-download'
import IconTrash from '@/app/ui/icons/icon-trash'
import Modal from '@/app/ui/components/modal/modal'
import Input from '@/app/ui/components/input/input'

export type Breadcrumb = {
  id: string
  name: string
  link: string
}

function getBreadcrumb(id: string): Breadcrumb[] {
  return [
    {
      id: 'dashboard',
      name: 'beranda',
      link: '/dashboard',
    },
    {
      id: 'info',
      name: 'Info Desa',
      link: `/dashboard/village/${id}`,
    },
    {
      id: 'official',
      name: 'Pemerintah Desa',
      link: `/dashboard/officials/${id}`,
    },
  ]
}

type Props = {
  params: {
    id: string
  }
}

export default function Page(props: Props) {
  const data = officialData
  const [isMoreOpen, setIsMoreOpen] = useState<boolean>(false)

  const router = useRouter()
  const { show: showDownload, toggle: toggleDownload } = useModal()
  const { show: showDelete, toggle: toggleDelete } = useModal()

  const moreRef = useOutsideClick({
    callback: () => setIsMoreOpen(!isMoreOpen),
    isOpen: isMoreOpen,
  })

  return (
    <section>
      <div className={styles['header__wrapper']}>
        <Header
          items={getBreadcrumb(props.params.id)}
          size='xl'
          title='Pemerintah Desa'
          subTitle=''
        />
        <div className={styles['header__action']}>
          <Button
            prefix={<IconAddCircle color='#4163E7' backgroundColor='#4163E7' />}
            variant='outline'
            color='primary'
            text='Tambah Staff Pemerintahan Desa'
            onHandleClick={() =>
              router.push(`/dashboard/officials/${props.params.id}/add`)
            }
          />
          <Button
            prefix={
              <IconMoreCircle color='#4163E7' backgroundColor='#4163E7' />
            }
            onHandleClick={() => setIsMoreOpen(!isMoreOpen)}
            variant='outline'
            color='primary'
            text=''
          />

          {!!isMoreOpen && (
            <div className={styles['header__more-container']} ref={moreRef}>
              <button className={styles['header__more-item']}>
                <IconPrinter />
                <span>Cetak Massal</span>
              </button>
              <button className={styles['header__more-item']}>
                <IconEdit />
                <span>Edit Data Dusun</span>
              </button>
              <button
                className={styles['header__more-item']}
                onClick={toggleDownload}
              >
                <IconDownload />
                <span>Download</span>
              </button>
              <button
                className={styles['header__more-item']}
                onClick={toggleDelete}
              >
                <IconTrash />
                <span>Hapus Semua Data</span>
              </button>
            </div>
          )}
        </div>
      </div>
      <DataTable columns={columns} data={data} />
      <Modal isOpen={showDownload} toggle={toggleDownload}>
        <Input
          label='Tipe file'
          variant='dropdown-select'
          helperText=''
          size='md'
          items={['xlsx', 'csv']}
        />
        <div className={styles['modal__below']}>
          <Button
            text='Batal'
            color='neutral'
            variant='outline'
            onHandleClick={toggleDownload}
            size='lg'
          />
          <Button color='primary' text='Download' size='lg' />
        </div>
      </Modal>
      <Modal isOpen={showDelete} toggle={toggleDelete}>
        <div className={styles['modal__top']}>
          <p className={styles['modal__title']}>
            Anda yakin ingin menghapus data ini?
          </p>
          <p className={styles['modal__body']}>
            Data desa yang dihapus akan terhapus secara permanen.
          </p>
        </div>
        <div className={styles['modal__below']}>
          <Button
            text='Batal'
            color='neutral'
            variant='outline'
            onHandleClick={toggleDelete}
            size='lg'
          />
          <Button text='Hapus' color='error' size='lg' />
        </div>
      </Modal>
    </section>
  )
}
