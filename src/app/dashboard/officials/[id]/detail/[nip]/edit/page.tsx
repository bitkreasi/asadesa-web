'use client'

import Header from '@/app/ui/components/header/header'
import styles from '../../../style.module.scss'
import { Breadcrumb } from '../../../page'
import Button from '@/app/ui/components/button/button'
import Input from '@/app/ui/components/input/input'
import { Card, CardContent, CardTitle } from '@/app/ui/components/card/card'
import { useRef, useState } from 'react'
import PreviewDefault from '../../../../_assets/avatar.png'
import { useModal } from '@/hooks/use-modal'
import Modal from '@/app/ui/components/modal/modal'

type Props = {
  params: { id: string; nip: string }
}

function getBreadcrumb(id: string, nip: string): Breadcrumb[] {
  return [
    {
      id: 'dashboard',
      name: 'beranda',
      link: '/dashboard',
    },
    {
      id: 'info',
      name: 'Info Desa',
      link: `/dashboard/village/${id}`,
    },
    {
      id: 'official',
      name: 'Pemerintah Desa',
      link: `/dashboard/officials/${id}`,
    },
    {
      id: 'detail',
      name: 'Detail Staff',
      link: `/dashboard/officials/${id}/detail/${nip}`,
    },
    {
      id: 'edit',
      name: 'Edit Staff',
      link: `/dashboard/officials/${id}/detail/${nip}/edit`,
    },
  ]
}

export default function Page(props: Props) {
  const [imagePreview, setImagePreview] = useState<any>()
  const inputPhotoRef = useRef<HTMLInputElement>(null)
  const { show, toggle } = useModal()

  return (
    <section>
      <div className={styles['header__wrapper']}>
        <Header
          items={getBreadcrumb(props.params.id, props.params.nip)}
          size='xl'
          title='Edit Staff Pemerintah Desa'
          subTitle=''
          isBack
        />
      </div>
      <form>
        <div className={styles['form__wrapper']}>
          <Card>
            <CardTitle>Data Diri</CardTitle>
            <CardContent>
              <div className={styles['form__photo__wrapper']}>
                <div className={styles['form__photo__left']}>
                  <div className={styles['form__photo__img']}>
                    <img
                      alt='preview'
                      src={imagePreview ?? PreviewDefault.src}
                    />
                  </div>
                  <div className={styles['form__photo__img-description']}>
                    <p>Foto profil</p>
                    <p>PNG, JPEG dibawah 2MB</p>
                  </div>
                  <input type='file' hidden ref={inputPhotoRef} />
                </div>
                <label>
                  <Button
                    text='Unggah Foto'
                    color='primary'
                    size='md'
                    onHandleClick={() => inputPhotoRef.current?.click()}
                  />
                </label>
              </div>
            </CardContent>
          </Card>
          <Card>
            <CardTitle>Data Diri</CardTitle>
            <CardContent>
              <div className={styles['form__profile']}>
                <div className={styles['form__profile-3-col']}>
                  <Input
                    label='Nama Lengkap'
                    helperText=''
                    variant='text-field'
                    placeholder='Masukan nama lengkap'
                    size='sm'
                  />
                  <Input
                    label='Gelar Pendidikan'
                    helperText=''
                    variant='text-field'
                    placeholder='Masukan gelar pendidikan'
                    size='sm'
                  />
                  <Input
                    label='NIK'
                    helperText=''
                    variant='text-field'
                    placeholder='0'
                    size='sm'
                    textFieldType='number'
                    minLength={1}
                  />
                </div>
                <div className={styles['form__profile-3-col']}>
                  <Input
                    label='Nomor NIP'
                    helperText=''
                    variant='text-field'
                    placeholder='0'
                    size='sm'
                    textFieldType='number'
                    minLength={1}
                  />
                  <Input
                    label='Nomor NIPD'
                    helperText=''
                    variant='text-field'
                    placeholder='0'
                    size='sm'
                    textFieldType='number'
                    minLength={1}
                  />
                  <Input
                    label='Tag ID Card'
                    helperText=''
                    variant='text-field'
                    placeholder='0'
                    size='sm'
                  />
                </div>
                <div className={styles['form__profile-3-col']}>
                  <Input
                    label='Jenis Kelamin'
                    helperText=''
                    variant='dropdown-select'
                    placeholder='Pilih jenis kelamin'
                    size='sm'
                    items={['Pria', 'Perempuan']}
                  />
                  <Input
                    label='Agama'
                    helperText=''
                    variant='dropdown-select'
                    placeholder='Pilih Agama'
                    size='sm'
                    items={['Islam', 'Katolik', 'Protestan', 'Hindu', 'Budha']}
                  />
                  <Input
                    label='Pendidikan'
                    helperText=''
                    variant='dropdown-select'
                    placeholder='Pilih Pendidikan'
                    size='sm'
                    items={['SMA', 'S1', 'S2', 'S3']}
                  />
                </div>
                <div className={styles['form__profile-2-col']}>
                  <Input
                    label='Tanggal Lahir'
                    helperText=''
                    variant='text-field'
                    placeholder='Pilih jenis kelamin'
                    size='sm'
                    textFieldType='date'
                  />
                  <Input
                    label='Kota kelahiran'
                    helperText=''
                    variant='dropdown-select'
                    placeholder='Pilih Agama'
                    size='sm'
                    items={['Yogyakarta', 'Solo', 'Balapan']}
                  />
                </div>
              </div>
            </CardContent>
          </Card>
          <Card>
            <CardTitle>Data Jabatan</CardTitle>
            <CardContent>
              <div className={styles['form__role__wrapper']}>
                <div className={styles['form__role-4-col']}>
                  <Input
                    label='Pangkat atau Golongan'
                    helperText=''
                    variant='dropdown-select'
                    placeholder='Pilih pangkat atau golongan'
                    size='sm'
                  />
                  <Input
                    label='Nomor keputusan pengangkatan'
                    helperText=''
                    variant='text-field'
                    placeholder='0'
                    size='sm'
                    textFieldType='number'
                    minLength={1}
                  />
                  <Input
                    label='Tanggal keputusan pengangkatan'
                    helperText=''
                    variant='text-field'
                    textFieldType='date'
                    size='sm'
                    placeholder='Pilih tanggal'
                  />
                  <Input
                    label='Jabatan'
                    placeholder='Pilih jabatan'
                    size='sm'
                    variant='dropdown-select'
                    helperText=''
                  />
                </div>
                <div className={styles['form__role-2-col']}>
                  <Input
                    label='Nomor keputusan pemberhentian'
                    size='sm'
                    placeholder='0'
                    textFieldType='number'
                    minLength={0}
                    helperText=''
                    variant='text-field'
                  />
                  <Input
                    label='Catatan'
                    size='sm'
                    placeholder='catatan'
                    variant='text-area'
                    helperText=''
                  />
                </div>
              </div>
            </CardContent>
          </Card>
        </div>
        <div className={styles['form__action']}>
          <Button
            type='button'
            text='Batal'
            color='neutral'
            variant='outline'
            size='md'
          />
          <Button
            type='button'
            text='Tambahkan'
            color='primary'
            size='md'
            onHandleClick={toggle}
          />
        </div>
      </form>
      <Modal isOpen={show} toggle={toggle}>
        <div className={styles['modal__top']}>
          <p className={styles['modal__title']}>
            Anda yakin ingin menyimpan data ini?
          </p>
          <p className={styles['modal__body']}>
            Setelah anda menyimpan data ini, anda dapat kembali mengedit data
            ini dengan pergi ke detail dan edit data.
          </p>
        </div>
        <div className={styles['modal__below']}>
          <Button
            text='Cek Ulang'
            color='neutral'
            variant='outline'
            onHandleClick={toggle}
          />
          <Button text='Simpan' color='primary' onHandleClick={toggle} />
        </div>
      </Modal>
    </section>
  )
}
