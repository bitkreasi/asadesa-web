'use client'

import Header from '@/app/ui/components/header/header'
import styles from '../../style.module.scss'
import { Breadcrumb } from '../../page'
import Button from '@/app/ui/components/button/button'
import { Card, CardContent, CardTitle } from '@/app/ui/components/card/card'
import PreviewDefault from '../../../_assets/avatar.png'
import IconEdit from '@/app/ui/icons/icon-edit'
import IconTrash from '@/app/ui/icons/icon-trash'
import { useRouter } from 'next/navigation'
import { useModal } from '@/hooks/use-modal'
import Modal from '@/app/ui/components/modal/modal'

type Props = {
  params: { id: string; nip: string }
}

function getBreadcrumb(id: string, nip: string): Breadcrumb[] {
  return [
    {
      id: 'dashboard',
      name: 'beranda',
      link: '/dashboard',
    },
    {
      id: 'info',
      name: 'Info Desa',
      link: `/dashboard/village/${id}`,
    },
    {
      id: 'official',
      name: 'Pemerintah Desa',
      link: `/dashboard/officials/${id}`,
    },
    {
      id: 'detail',
      name: 'Detail Staff',
      link: `/dashboard/officials/${id}/detail/${nip}`,
    },
  ]
}

export default function Page(props: Props) {
  const router = useRouter()
  const { show, toggle } = useModal()

  return (
    <section>
      <div className={styles['header__wrapper']}>
        <Header
          items={getBreadcrumb(props.params.id, props.params.nip)}
          size='xl'
          title='Detail Staff Pemerintah Desa'
          subTitle=''
          isBack
        />
        <div className={styles['header__action']}>
          <Button
            text='Edit'
            prefix={<IconEdit backgroundColor='#4163E7' color='#4163E7' />}
            color='primary'
            variant='outline'
            onHandleClick={() =>
              router.push(
                `/dashboard/officials/${props.params.id}/detail/${props.params.nip}/edit`
              )
            }
          />
          <Button
            text='Hapus'
            color='error'
            variant='outline'
            prefix={<IconTrash backgroundColor='#F42E2E' color='#F42E2E' />}
            onHandleClick={toggle}
          />
        </div>
      </div>
      <div className={styles['form__wrapper']}>
        <Card>
          <CardTitle>Data Diri</CardTitle>
          <CardContent>
            <div className={styles['form__photo__wrapper']}>
              <div className={styles['form__photo__left']}>
                <div className={styles['form__photo__img']}>
                  <img alt='preview' src={PreviewDefault.src} />
                </div>
                <div className={styles['form__photo__img-description']}>
                  <p>Foto profil</p>
                </div>
              </div>
            </div>
          </CardContent>
        </Card>
        <Card>
          <CardTitle>Jenis Penduduk</CardTitle>
          <CardContent>
            <div className={styles['form__profile']}>
              <div className={styles['form__profile-3-col']}>
                <div>
                  <p className={styles['form__label']}>Tanggal lengkap</p>
                  <p className={styles['form__paragraph']}>20-01-2022</p>
                </div>
                <div>
                  <p className={styles['form__label']}>Jenis penduduk</p>
                  <p className={styles['form__paragraph']}>Penduduk Lahir</p>
                </div>
                <div>
                  <p className={styles['form__label']}>Tanggal masuk</p>
                  <p className={styles['form__paragraph']}>-</p>
                </div>
              </div>
            </div>
          </CardContent>
        </Card>
        <Card>
          <CardTitle>Data diri</CardTitle>
          <CardContent>
            <div className={styles['form__profile']}>
              <div className={styles['form__profile-3-col']}>
                <div>
                  <p className={styles['form__label']}>Nama Lengkap</p>
                  <p className={styles['form__paragraph']}>Damar Hendra</p>
                </div>
                <div>
                  <p className={styles['form__label']}>Gelar Pendidikan</p>
                  <p className={styles['form__paragraph']}>S1</p>
                </div>
                <div>
                  <p className={styles['form__label']}>Nomor NIK</p>
                  <p className={styles['form__paragraph']}>33010420010401</p>
                </div>
              </div>
              <div className={styles['form__profile-3-col']}>
                <div>
                  <p className={styles['form__label']}>Nomor NIP</p>
                  <p className={styles['form__paragraph']}>0</p>
                </div>
                <div>
                  <p className={styles['form__label']}>Nomor NIPD</p>
                  <p className={styles['form__paragraph']}>0</p>
                </div>
                <div>
                  <p className={styles['form__label']}>Tag ID card</p>
                  <p className={styles['form__paragraph']}>LK1020301</p>
                </div>
              </div>
              <div className={styles['form__profile-3-col']}>
                <div>
                  <p className={styles['form__label']}>Jenis Kelamin</p>
                  <p className={styles['form__paragraph']}>Laki-laki</p>
                </div>
                <div>
                  <p className={styles['form__label']}>Agama</p>
                  <p className={styles['form__paragraph']}>Islam</p>
                </div>
                <div>
                  <p className={styles['form__label']}>Pendidikan</p>
                  <p className={styles['form__paragraph']}>
                    Diploma IV/Strata 1
                  </p>
                </div>
              </div>
              <div className={styles['form__profile-2-col']}>
                <div>
                  <p className={styles['form__label']}>Tanggal lahir</p>
                  <p className={styles['form__paragraph']}>20-01-1975</p>
                </div>
                <div>
                  <p className={styles['form__label']}>Kota Kelahiran</p>
                  <p className={styles['form__paragraph']}>Kebumen</p>
                </div>
              </div>
            </div>
          </CardContent>
        </Card>
        <Card>
          <CardTitle>Jenis Penduduk</CardTitle>
          <CardContent>
            <div className={styles['form__profile']}>
              <div className={styles['form__profile-4-col']}>
                <div>
                  <p className={styles['form__label']}>Pangkat atau golongan</p>
                  <p className={styles['form__paragraph']}>IV/A</p>
                </div>
                <div>
                  <p className={styles['form__label']}>
                    Nomor keputusan pengangkatan
                  </p>
                  <p className={styles['form__paragraph']}>001/KEP-DS/2024</p>
                </div>
                <div>
                  <p className={styles['form__label']}>
                    Tanggal keputusan pengangkatan
                  </p>
                  <p className={styles['form__paragraph']}>20-02-2024</p>
                </div>
                <div>
                  <p className={styles['form__label']}>Masa Jabatan</p>
                  <p className={styles['form__paragraph']}>4 tahun</p>
                </div>
              </div>
              <div className={styles['form__profile-3-col']}>
                <div>
                  <p className={styles['form__label']}>
                    Nomor Keputusan Pemberhentian
                  </p>
                  <p className={styles['form__paragraph']}>-</p>
                </div>
                <div>
                  <p className={styles['form__label']}>
                    Tanggal keputusan pemberhentian
                  </p>
                  <p className={styles['form__paragraph']}>-</p>
                </div>
                <div>
                  <p className={styles['form__label']}>Jabatan</p>
                  <p className={styles['form__paragraph']}>Kepala Desa</p>
                </div>
              </div>
            </div>
          </CardContent>
        </Card>
        <Card>
          <CardTitle>Lainnya</CardTitle>
          <CardContent>
            <div className={styles['form__profile']}>
              <div className={styles['form__profile-2-col']}>
                <div>
                  <p className={styles['form__label']}>Status pegawai desa</p>
                  <p className={styles['form__paragraph']}>Aktif</p>
                </div>
                <div>
                  <p className={styles['form__label']}>Catatan</p>
                  <p className={styles['form__paragraph']}>Periode baru 2024</p>
                </div>
              </div>
            </div>
          </CardContent>
        </Card>
      </div>
      <Modal isOpen={show} toggle={toggle}>
        <div className={styles['modal__top']}>
          <p className={styles['modal__title']}>
            Anda yakin ingin menghapus data ini?
          </p>
          <p className={styles['modal__body']}>
            Data ini yang dihapus akan terhapus secara permanen.
          </p>
        </div>
        <div className={styles['modal__below']}>
          <Button
            text='Batal'
            color='neutral'
            variant='outline'
            onHandleClick={toggle}
            size='lg'
          />
          <Button text='Hapus' color='error' size='lg' />
        </div>
      </Modal>
    </section>
  )
}
