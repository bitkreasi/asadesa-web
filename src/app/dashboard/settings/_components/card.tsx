import React from "react";
import styles from "./card.module.scss";

export type CardProps = React.HTMLAttributes<HTMLDivElement>;
export const SettingsCard = React.forwardRef<HTMLDivElement, CardProps>(
  ({ children, className, ...props }, ref) => {
    return (
      <div
        className={`${styles["settings__card"]} ${className}`}
        ref={ref}
        {...props}
      >
        {children}
      </div>
    );
  }
);

SettingsCard.displayName = "SettingsCard";
