"use client";
import Input from "@/app/ui/components/input/input";
import styles from "../settings.module.scss";

export const DetailForms = () => {
  return (
    <div className={styles["settings__main__detail"]}>
      <Input label="Nama Lengkap" placeholder="Nama Lengkap" helperText="" />
      <Input
        label="Email"
        textFieldType="email"
        placeholder="Email"
        helperText=""
      />
      <Input label="No Telepon" placeholder="No Telepon" helperText="" />
      <Input label="Jabatan" placeholder="Jabatan" helperText="" />
    </div>
  );
};
