import Link from "next/link";
import { Text } from "@/app/ui/components/typography/typography";
import styles from "./settings-nav.module.scss";
import { SettingsCard } from "../(settings)/profile/_components/card";
import clsx from "clsx";

type SettingsNavProps = {
  activeLink: "profile" | "villages" | "password" | "membership";
};

const SettingsNav = (props: SettingsNavProps) => {
  return (
    <SettingsCard
      style={{
        width: "300px",
        display: "flex",
        flexDirection: "column",
        gap: "20px",
      }}
    >
      <Text
        variant="text"
        size="sm"
        fontStyle="medium"
        className={clsx(
          styles["link"],
          props.activeLink === "profile"
            ? styles["active"]
            : styles["disabled"],
        )}
      >
        <Link href="profile">Profil Saya</Link>
      </Text>

      <Text
        variant="text"
        size="sm"
        fontStyle="medium"
        className={clsx(
          styles["link"],
          props.activeLink === "villages"
            ? styles["active"]
            : styles["disabled"],
        )}
      >
        <Link href="villages">Profil Desa</Link>
      </Text>

      <Text
        variant="text"
        size="sm"
        fontStyle="medium"
        className={clsx(
          styles["link"],
          props.activeLink === "password"
            ? styles["active"]
            : styles["disabled"],
        )}
      >
        <Link href="password">Kata Sandi</Link>
      </Text>

      <Text
        variant="text"
        size="sm"
        fontStyle="medium"
        className={clsx(
          styles["link"],
          props.activeLink === "membership"
            ? styles["active"]
            : styles["disabled"],
        )}
      >
        <Link href="membership">Kelola Paket</Link>
      </Text>
    </SettingsCard>
  );
};

export default SettingsNav;
