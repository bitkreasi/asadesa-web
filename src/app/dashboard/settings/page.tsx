import { permanentRedirect } from "next/navigation";

export default function settingspage() {
  permanentRedirect("settings/profile");
}
