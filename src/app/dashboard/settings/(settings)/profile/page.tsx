import Image from "next/image";
import styles from "./profile.module.scss";
import avatar from "./Ellipse 22.png";
import Button from "@/app/ui/components/button/button";
import { SettingsCard } from "./_components/card";
import { Heading3, Text } from "@/app/ui/components/typography/typography";
import IconImport from "@/app/ui/icons/icon-import";
import IconTrash from "@/app/ui/icons/icon-trash";
import { DetailForms } from "./_components/details-form";
import SettingsNav from "../../_components/settings-nav";

export default function SettingsPage() {
  return (
    <>
      <SettingsNav activeLink="profile" />
      <div className={styles["settings__main"]}>
        <SettingsCard>
          <div className={styles["profile__card"]}>
            <div className={styles["profile__card__avatar"]}>
              <Image alt="profil avatar" src={avatar} />
              <div>
                <Text variant="text" fontStyle="medium" size="md">
                  Foto profil
                </Text>
                <Text
                  variant="text"
                  fontStyle="regular"
                  size="sm"
                  style={{ opacity: "50%" }}
                >
                  PNG, JPEG dibawah 2MB
                </Text>
              </div>
            </div>
            <div className={styles["profile__card__button"]}>
              <Button
                variant="solid"
                text="Unggah foto"
                color="primary"
                prefix={<IconImport />}
              />
              <Button
                variant="outline"
                text="Hapus foto"
                color="error"
                prefix={<IconTrash />}
              />
            </div>
          </div>
        </SettingsCard>

        <SettingsCard>
          <Heading3 variant="display" size="xs" fontStyle="medium">
            Detail Pribadi
          </Heading3>
          <DetailForms />
        </SettingsCard>

        <div style={{ display: "flex", justifyContent: "end" }}>
          <Button text="Simpan" />
        </div>
      </div>
    </>
  );
}
