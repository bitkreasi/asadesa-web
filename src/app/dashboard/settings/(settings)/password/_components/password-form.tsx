"use client";
import { Text } from "@/app/ui/components/typography/typography";
import styles from "./password-form.module.scss";
import Input from "@/app/ui/components/input/input";
import IconInfo from "@/app/ui/icons/icon-info";

const PasswordForm = () => {
  return (
    <div className={styles["password_input_wrapper"]}>
      <section
        style={{
          display: "flex",
          flexDirection: "column",
          gap: "10px",
        }}
      >
        <Input
          label="Kata sandi lama"
          placeholder="Masukkan kata sandi"
          helperText=""
        />
        <Input
          label="Kata sandi baru"
          placeholder="Masukkan kata sandi"
          helperText=""
        />
      </section>
      <section
        style={{
          marginTop: "5px",
        }}
      >
        <Text
          variant="text"
          size="md"
          fontStyle="regular"
          className={styles["input"]}
        >
          <IconInfo /> Minimum 8 karakter
        </Text>
        <Text
          variant="text"
          size="md"
          fontStyle="regular"
          className={styles["input"]}
        >
          <IconInfo /> Memiliki huruf besar dan kecil
        </Text>
        <Text
          variant="text"
          size="md"
          fontStyle="regular"
          className={styles["input"]}
        >
          <IconInfo /> Memiliki angka
        </Text>
        <Text
          variant="text"
          size="md"
          fontStyle="regular"
          className={styles["input"]}
        >
          <IconInfo /> Memiliki simbol: !@#$%^&*()
        </Text>
      </section>
    </div>
  );
};

export default PasswordForm;
