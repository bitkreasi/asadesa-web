import { Heading1 } from "@/app/ui/components/typography/typography";
import styles from "./password.module.scss";
import SettingsNav from "../../_components/settings-nav";
import { SettingsCard } from "../profile/_components/card";
import PasswordForm from "./_components/password-form";
import Button from "@/app/ui/components/button/button";

export default function SettingsPasswordPage() {
  return (
    <>
      <SettingsNav activeLink="password" />
      <div className={styles["settings__main"]}>
        <SettingsCard>
          <Heading1 variant="display" size="xs" fontStyle="medium">
            Kata Sandi
          </Heading1>
          <PasswordForm />
        </SettingsCard>
        <div style={{ display: "flex", justifyContent: "end" }}>
          <Button text="Simpan" variant="solid" />
        </div>
      </div>
    </>
  );
}
