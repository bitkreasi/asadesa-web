import {
  Heading1,
  Heading2,
  Text,
} from "@/app/ui/components/typography/typography";
import SettingsNav from "../../_components/settings-nav";
import { SettingsCard } from "../profile/_components/card";
import styles from "./membership.module.scss";
import PacketActive, {
  PacketActiveProps,
  PacketVariant,
} from "./_components/packet-active";

const BillDescription = (props: PacketActiveProps) => {
  const description: Record<PacketVariant, string> = {
    free: "Uji coba gratis akan berakhir 5 hari lagi.",
    none: "Anda tidak memilki tagihan.",
    basic: "Paket Basic akan berakhir 340 hari lagi.",
    premium: "Paket Premium akan berakhir 340 hari lagi.",
  };
  return (
    <Text
      variant="text"
      size="md"
      className={styles["sub__text"]}
      fontStyle="medium"
    >
      {description[props.variant]}
    </Text>
  );
};
export default function SettingsMembershipPage() {
  return (
    <>
      <SettingsNav activeLink="membership" />
      <div className={styles["settings__main"]} style={{ width: "100%" }}>
        <SettingsCard className={styles["member__card"]}>
          <Heading1 variant="display" size="sm" fontStyle="semibold">
            Kelola Paket
          </Heading1>
          <Heading2
            className={styles["sub_heading"]}
            variant="display"
            size="xs"
            fontStyle="medium"
          >
            Paket saat ini
          </Heading2>
          <PacketActive variant="premium" />
        </SettingsCard>
        <SettingsCard>
          <Heading2 variant="display" size="xs" fontStyle="medium">
            Tagihan
          </Heading2>
          <BillDescription variant="premium" />
        </SettingsCard>
      </div>
    </>
  );
}
