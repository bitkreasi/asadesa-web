import Button from "@/app/ui/components/button/button";
import styles from "./packet-active.module.scss";
import { Text } from "@/app/ui/components/typography/typography";

export type PacketVariant = "free" | "none" | "basic" | "premium";

export type PacketActiveProps = {
  variant: PacketVariant;
};

const PacketActive = (props: PacketActiveProps) => {
  const data: Record<
    typeof props.variant,
    { title: string; description: string; action: string }
  > = {
    free: {
      title: "Paket Gratis",
      description: "Paket berakhir pada 2 November 2023",
      action: "Langganan Sekaran",
    },
    none: {
      title: "",
      description: "Anda belum berlangganan.",
      action: "Langganan Sekarang",
    },
    basic: {
      title: "Paket Basic",
      description: "Rp5,000,000 per tahun",
      action: "Tingkatkan Paket Langganan",
    },
    premium: {
      title: "Paket Premium",
      description: "Rp5,000,000 per tahun",
      action: "Ubah Paket Langganan",
    },
  };

  return (
    <div className={styles["card__member"]}>
      <section style={{ display: "flex" }}>
        <div style={{ width: "50%" }}>
          <Text variant="text" size="md" fontStyle="medium">
            {data[props.variant].title}
          </Text>
          <Text
            variant="text"
            size="sm"
            className={styles["sub__text"]}
            fontStyle="regular"
          >
            {data[props.variant].description}
          </Text>
        </div>
        {(props.variant === "premium" || props.variant === "basic") && (
          <div>
            <Text variant="text" size="md" fontStyle="medium">
              Paket termasuk
            </Text>
            <ul style={{ marginLeft: 20 }}>
              <li>Lorem Ipsum</li>
              <li>Lorem Ipsum</li>
              <li>Lorem Ipsum</li>
              <li>Lorem Ipsum</li>
            </ul>
          </div>
        )}
      </section>
      <div style={{ display: "flex", gap: 5 }}>
        {props.variant === "none" && (
          <Button
            variant="outline"
            text="Uji Coba Gratis 14 hari"
            color="primary"
          />
        )}
        <Button
          variant="solid"
          color="primary"
          text={data[props.variant].action}
        />
      </div>
    </div>
  );
};

export default PacketActive;
