import { Heading1, Text } from "@/app/ui/components/typography/typography";
import SettingsNav from "../../_components/settings-nav";
import { SettingsCard } from "../profile/_components/card";
import styles from "./villages.module.scss";

export default function VillagesPage() {
  return (
    <>
      <SettingsNav activeLink="villages" />
      <div className={styles["settings__main"]}>
        <SettingsCard>
          <Heading1 variant="display" size="xs" fontStyle="medium">
            Detail Desa
          </Heading1>
          <div className={styles["villages_detail_wrapper"]}>
            <div>
              <Text
                variant="text"
                size="sm"
                fontStyle="medium"
                className={styles["muted_text"]}
              >
                Provinsi
              </Text>
              <Text variant="text" size="sm" fontStyle="regular">
                Jawa Tengah
              </Text>
            </div>
            <div>
              <Text
                variant="text"
                size="sm"
                fontStyle="medium"
                className={styles["muted_text"]}
              >
                Kecamatan
              </Text>
              <Text variant="text" size="sm" fontStyle="regular">
                Mrebet
              </Text>
            </div>
            <div>
              <Text
                variant="text"
                size="sm"
                fontStyle="medium"
                className={styles["muted_text"]}
              >
                Kabupaten
              </Text>
              <Text variant="text" size="sm" fontStyle="regular">
                Purbalingga
              </Text>
            </div>
            <div>
              <Text
                variant="text"
                size="sm"
                fontStyle="medium"
                className={styles["muted_text"]}
              >
                Desa
              </Text>
              <Text variant="text" size="sm" fontStyle="regular">
                Serayu Larangan
              </Text>
            </div>
          </div>
        </SettingsCard>
      </div>
    </>
  );
}
