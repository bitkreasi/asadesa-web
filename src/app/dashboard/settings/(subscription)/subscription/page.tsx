import styles from "./subscription.module.scss";
import PricingCard from "./_components/pricing-card";
import { Heading1 } from "@/app/ui/components/typography/typography";

export default function SubscriptionPage() {
  return (
    <div className={styles["settings__main"]} style={{ width: "100%" }}>
      <Heading1 variant="display" size="xs" fontStyle="medium">
        Langganan
      </Heading1>
      <div className={styles["pricing__wrapper"]}>
        <PricingCard
          title="Basic"
          description="Cocok untuk Pebisnis, Startup, dan Instansi Pemerintahan."
          price_per_year="3 Juta"
          benefits={["Lorem ipsum", "Lorem Ipsum", "Lorem"]}
        />
        <PricingCard
          title="Premium"
          description="Cocok untuk Pebisnis, Startup, dan Instansi Pemerintahan."
          price_per_year="5 Juta"
          benefits={[
            "Lorem ipsum",
            "Semua fitur yang ada",
            "Lorem Ipsum",
            "Lorem",
          ]}
          recommended
        />
      </div>
    </div>
  );
}
