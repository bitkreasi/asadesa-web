import React from "react";
import styles from "./pricing-card.module.scss";
import { Text } from "@/app/ui/components/typography/typography";
import Button from "@/app/ui/components/button/button";
import { title } from "process";
import IconSuccess from "@/app/ui/icons/icon-success";

export type PricingCardProps = {
  title: string;
  description: string;
  price_per_year: string;
  benefits: string[];
  recommended?: boolean;
} & React.ComponentPropsWithoutRef<"div">;

const PricingCard = (props: PricingCardProps) => {
  return (
    <div className={styles["card__wrapper"]}>
      {props.recommended && (
        <div className={styles["recommended__badge"]} style={{}}>
          <Text
            variant="text"
            size="md"
            fontStyle="semibold"
            style={{ textAlign: "center", color: "#fff" }}
          >
            Recommended
          </Text>
        </div>
      )}
      <div className={styles["content__wrapper"]}>
        <Text variant="text" fontStyle="semibold" size="md">
          {props.title}
        </Text>
        <Text variant="text" fontStyle="regular" size="sm">
          {props.description}
        </Text>
        <Text variant="display" size="2xl" fontStyle="bold">
          {props.price_per_year}{" "}
          <span className={styles["price__unit"]}>/per tahun</span>
        </Text>

        <div className={styles["benefit"]}>
          {props.benefits.map((benefit, index) => (
            <div key={index} className={styles["benefit__item"]}>
              <IconSuccess /> {benefit}
            </div>
          ))}
        </div>
        <Button
          width="max"
          text={"Pilih Paket " + props.title}
          variant={props.recommended ? "solid" : "outline"}
        />
      </div>
    </div>
  );
};

export default PricingCard;
