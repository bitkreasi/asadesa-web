import React from "react";
import styles from "./payment-method-card.module.scss";
import { Heading2, Text } from "@/app/ui/components/typography/typography";
import IconBottomArrow from "@/app/ui/icons/icon-bottom-arrow";
import IconClock from "@/app/ui/icons/icon-clock";
import IconArrowDown from "@/app/ui/icons/icon-arrow-down";
import Button from "@/app/ui/components/button/button";
import IconCopy from "@/app/ui/icons/icon-copy";

type PaymentMethodCardProps = React.ComponentPropsWithoutRef<"div">;

const methods = ["Bank BCA", "DANA", "GOPAY", "OVO"];
const PaymentMethodCard = (props: PaymentMethodCardProps) => {
  return (
    <div className={styles["wrapper"]}>
      <Heading2 variant="display" size="sm" fontStyle="bold">
        Metode Pembayaran
      </Heading2>

      <section className={styles["methods"]}>
        {methods.map((method, idx) => (
          <Button key={idx} text={method} variant="solid" color="primary" />
        ))}
      </section>

      <section className={styles["account_info"]}>
        <div className={styles["title"]}>
          <IconBottomArrow />
          <Text variant="text" size="md" fontStyle="semibold">
            BCA Virtual Account
          </Text>
        </div>

        <div className={styles["account_number"]}>
          <Text variant="display" size="sm" fontStyle="semibold">
            1234567890123
          </Text>
          <IconCopy />
        </div>

        <Text variant="text" size="sm" fontStyle="regular" muted>
          a.n Wawan Hendrawan
        </Text>
      </section>

      <section className={styles["instructions"]}>
        <Text variant="text" size="lg" fontStyle="semibold">
          Petunjuk Pembayaran
        </Text>

        <div className={styles["accordion"]}>
          <div className={styles["accord_item"]}>
            <Text variant="text" size="md" fontStyle="regular">
              ATM BCA
            </Text>
            <IconArrowDown />
          </div>
          <div className={styles["accord_item"]}>
            <Text variant="text" size="md" fontStyle="regular">
              M-BCA
            </Text>
            <IconArrowDown />
          </div>
          <div className={styles["accord_item"]}>
            <Text variant="text" size="md" fontStyle="regular">
              Klik BCA
            </Text>
            <IconArrowDown />
          </div>
        </div>
      </section>

      <Button width="max" text="Upload Bukti Pembayaran" variant="solid" />
    </div>
  );
};

export default PaymentMethodCard;
