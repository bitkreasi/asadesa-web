import React from "react";
import { PacketVariant } from "@/app/dashboard/settings/(settings)/membership/_components/packet-active";
import { Text } from "@/app/ui/components/typography/typography";
import IconArrowCircleLeft from "@/app/ui/icons/icon-arrow-circle-left";
import styles from "./payment-detail-card.module.scss";
import Link from "next/link";

export type PaymentDetailCardProps = {
  type: Exclude<PacketVariant, "none">;
} & React.ComponentPropsWithRef<"div">;

const PaymentDetailCard = (props: PaymentDetailCardProps) => {
  const prices: Record<
    typeof props.type,
    { price: string; tax: string; total: string }
  > = {
    premium: {
      price: "Rp4,000,000",
      tax: "Rp400,000",
      total: "Rp4,400.000",
    },
    basic: {
      price: "Rp2,000,000",
      tax: "Rp200,000",
      total: "Rp2,200.000",
    },
    free: {
      price: "Rp0",
      tax: "Rp0",
      total: "Rp0",
    },
  };
  return (
    <div className={styles["card__wrapper"]}>
      <div className={styles["header"]}>
        <Link href="/dashboard" aria-label="Kembali ke dashboard">
          <IconArrowCircleLeft />
        </Link>
        <Text
          fontStyle="semibold"
          className={styles["text"]}
          size="md"
          variant="display"
        >
          Asadesa
        </Text>
      </div>

      <div className={styles["main"]}>
        <Text muted variant="text" size="lg" fontStyle="regular">
          Berlangganan ke {props.type.toUpperCase()}
        </Text>
        <Text variant="display" fontStyle="semibold" size="xl">
          {prices[props.type].price}{" "}
          <span style={{ fontSize: 14, opacity: "50%", fontWeight: 500 }}>
            Per Tahun
          </span>
        </Text>
      </div>

      <div className={styles["container"]}>
        <div className={styles["item"]}>
          <div>
            <Text variant="display" size="xs" fontStyle="medium">
              Subtotal
            </Text>
            <Text
              muted
              style={{ paddingTop: 12 }}
              variant="text"
              size="sm"
              fontStyle="regular"
            >
              Untuk organisasi dengan kebutuhan khusus
            </Text>
          </div>

          <Text variant="display" size="xs" fontStyle="medium">
            {prices[props.type].price}
          </Text>
        </div>

        <div className={styles["item"]}>
          <Text variant="display" size="xs" fontStyle="medium">
            Subtotal
          </Text>
          <Text variant="display" size="xs" fontStyle="medium">
            {prices[props.type].price}
          </Text>
        </div>

        <div className={styles["item"]}>
          <Text variant="display" size="xs" fontStyle="medium">
            Pajak
          </Text>
          <Text variant="display" size="xs" fontStyle="medium">
            {prices[props.type].tax}
          </Text>
        </div>
      </div>

      <div className={styles["total"]}>
        <Text variant="display" size="xs" fontStyle="bold">
          TOTAL
        </Text>
        <Text variant="display" size="xs" fontStyle="bold">
          {prices[props.type].total}
        </Text>
      </div>
    </div>
  );
};

export default PaymentDetailCard;
