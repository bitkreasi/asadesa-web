import { Heading1 } from "@/app/ui/components/typography/typography";
import styles from "./payment.module.scss";
import PaymentDetailCard from "./_components/payment-detail-card";
import PaymentMethodCard from "./_components/payment-method-card";

export default function PaymentPage() {
  return (
    <div className={styles["settings__main"]} style={{ width: "100%" }}>
      <Heading1 variant="display" size="xs" fontStyle="medium">
        Langganan
      </Heading1>
      <div className={styles["payment__wrapper"]}>
        <PaymentDetailCard type="premium" />
        <PaymentMethodCard />
      </div>
    </div>
  );
}
