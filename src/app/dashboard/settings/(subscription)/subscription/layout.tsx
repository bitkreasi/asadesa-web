import LayoutDashboard from "@/app/ui/layouts/dashboard/layout-dashboard";

import Header from "@/app/ui/components/header/header";
import styles from "./layout.module.scss";

export default function SettingsLayout(props: { children: React.ReactNode }) {
  return (
    <LayoutDashboard>
      <section>
        <div className={styles["container"]}>
          <div className={styles["main__content"]}>
            <div>
              <div className={styles["content__header"]}>
                <Header
                  items={[
                    {
                      id: "beranda",
                      name: "Beranda",
                      link: "/dashboard",
                    },
                    {
                      id: "pengaturan",
                      name: "Pengaturan",
                      link: "/settings",
                    },
                    {
                      id: "keanggotaan",
                      name: "Keanggotaan",
                      link: "/subscription",
                    },
                  ]}
                  title="Keanggotaan"
                  subTitle=""
                />
              </div>
              <div className={styles["settings"]}>{props.children}</div>
            </div>
          </div>
        </div>
      </section>
    </LayoutDashboard>
  );
}
