"use client";
import styles from "../dashboard.module.scss";
import Image from "next/image";
import ChartPieDefault from "../chart-pie.png";
import ChartBarDefault from "../chart-bar.png";
import { useState } from "react";
import Button from "@/app/ui/components/button/button";
import PiechartLineLeft from "@/app/ui/shapes/piechart-line-left";
import PiechartLineRight from "@/app/ui/shapes/piechart-line-right";
import IconPeople from "@/app/ui/icons/icon-people";
import IconMap from "@/app/ui/icons/icon-map";
import IconHomeTrendUp from "@/app/ui/icons/icon-home-trend-up";
import IconProfile2user from "@/app/ui/icons/icon-profile-2user";
import IconPrinter from "@/app/ui/icons/icon-printer";

export default function Subscribed() {
  const [search, setSearch] = useState("");

  const man = "43,720";
  const woman = "66,280";
  const manPercentage = "62.5%";
  const womanPercentage = "37.5%";
  const totalDistrict = "4";
  const districtLatestUpdated = "sejak 2 tahun lalu";
  const totalHousehold = "15,783";
  const householdLatestUpdated = "sejak 2 tahun lalu";
  const totalFamily = "16,560";
  const familyLatestUpdated = "sejak 10 bulan lalu";
  const totalLetter = "240";
  const letterLatestUpdated = "sejak 4 hari lalu";
  return (
    <div className={styles["container"]}>
      <div className={styles["container__header"]}>
        <span className={styles["container__header--text"]}>Beranda</span>
      </div>
      {/* first-data */}
      <div className={styles["container__first-data"]}>
        {/* resident */}
        <div
          className={`${styles["container__first-data--resident"]} ${styles["container__content-base"]}`}
        >
          {/* box-header */}
          <div className={styles["container__content-base--header"]}>
            <div className={styles["container__content-base--header__label"]}>
              <IconPeople />
              <span className={styles["container__content-base--header__text"]}>
                Penduduk
              </span>
            </div>
            <div>
              {/* TODO: add link */}
              <Button text="Detail" color="primary" variant="text-link" />
            </div>
          </div>
          {/* content */}
          <div className={styles["container__first-data--resident__content"]}>
            {/* chart--left */}
            <div
              className={
                styles["container__first-data--resident__content--chart"]
              }
            >
              <div
                className={
                  styles[
                    "container__first-data--resident__content--chart__content"
                  ]
                }
              >
                <Image
                  src={ChartPieDefault}
                  width={240}
                  height={240}
                  alt="default-chart-image"
                />
              </div>
              <div
                className={
                  styles[
                    "container__first-data--resident__content--chart__detail"
                  ]
                }
              >
                <div
                  className={
                    styles[
                      "container__first-data--resident__content--chart__detail--left"
                    ]
                  }
                >
                  <PiechartLineLeft />
                  <span
                    className={
                      styles[
                        "container__first-data--resident__content--chart__detail--info"
                      ]
                    }
                  >
                    {manPercentage}
                  </span>
                </div>
                <div
                  className={
                    styles[
                      "container__first-data--resident__content--chart__detail--right"
                    ]
                  }
                >
                  <PiechartLineRight />
                  <span
                    className={
                      styles[
                        "container__first-data--resident__content--chart__detail--info"
                      ]
                    }
                  >
                    {womanPercentage}
                  </span>
                </div>
              </div>
            </div>
            {/* detail--right */}
            <div
              className={
                styles["container__first-data--resident__content--description"]
              }
            >
              <div
                className={
                  styles[
                    "container__first-data--resident__content--description__base"
                  ]
                }
              >
                <div
                  className={
                    styles[
                      "container__first-data--resident__content--description__base--label"
                    ]
                  }
                >
                  {/* <Dot size="lg" variant="info" /> */}
                  <span
                    className={
                      styles[
                        "container__first-data--resident__content--description__base--label__text"
                      ]
                    }
                  >
                    Laki-laki
                  </span>
                </div>
                <div
                  className={
                    styles[
                      "container__first-data--resident__content--description__base--value"
                    ]
                  }
                >
                  <span
                    className={
                      styles[
                        "container__first-data--resident__content--description__base--value__text"
                      ]
                    }
                  >
                    {man}
                  </span>
                  <span
                    className={
                      styles[
                        "container__first-data--resident__content--description__base--value__unit"
                      ]
                    }
                  >
                    orang
                  </span>
                </div>
              </div>
              <div
                className={
                  styles[
                    "container__first-data--resident__content--description__base"
                  ]
                }
              >
                <div
                  className={
                    styles[
                      "container__first-data--resident__content--description__base--label"
                    ]
                  }
                >
                  {/* <Dot size="lg" variant="secondary" /> */}
                  <span
                    className={
                      styles[
                        "container__first-data--resident__content--description__base--label__text"
                      ]
                    }
                  >
                    Perempuan
                  </span>
                </div>
                <div
                  className={
                    styles[
                      "container__first-data--resident__content--description__base--value"
                    ]
                  }
                >
                  <span
                    className={
                      styles[
                        "container__first-data--resident__content--description__base--value__text"
                      ]
                    }
                  >
                    {woman}
                  </span>
                  <span
                    className={
                      styles[
                        "container__first-data--resident__content--description__base--value__unit"
                      ]
                    }
                  >
                    orang
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>

        {/* district */}
        <div
          className={`${styles["container__first-data--district"]} ${styles["container__content-base"]}`}
        >
          {/* box-header */}
          <div className={styles["container__content-base--header"]}>
            <div className={styles["container__content-base--header__label"]}>
              <IconMap />
              <span className={styles["container__content-base--header__text"]}>
                Jumlah Dusun
              </span>
            </div>
            <div>
              {/* TODO: add link */}
              <Button text="Detail" color="primary" variant="text-link" />
            </div>
          </div>
          {/* content */}
          <div className={styles["container__first-data--base-content"]}>
            <div
              className={styles["container__first-data--base-content__data"]}
            >
              <span
                className={
                  styles["container__first-data--base-content__data--value"]
                }
              >
                {totalDistrict}
              </span>
              <span
                className={
                  styles["container__first-data--base-content__data--unit"]
                }
              >
                dusun
              </span>
            </div>
            <span
              className={
                styles["container__first-data--base-content__description"]
              }
            >
              {districtLatestUpdated}
            </span>
          </div>
        </div>

        {/* household */}
        <div
          className={`${styles["container__first-data--household"]}  ${styles["container__content-base"]}`}
        >
          {/* box-header */}
          <div className={styles["container__content-base--header"]}>
            <div className={styles["container__content-base--header__label"]}>
              <IconHomeTrendUp />
              <span className={styles["container__content-base--header__text"]}>
                Jumlah Rumah Tangga
              </span>
            </div>
            <div>
              {/* TODO: add link */}
              <Button text="Detail" color="primary" variant="text-link" />
            </div>
          </div>
          {/* content */}
          <div className={styles["container__first-data--base-content"]}>
            <div
              className={styles["container__first-data--base-content__data"]}
            >
              <span
                className={
                  styles["container__first-data--base-content__data--value"]
                }
              >
                {totalHousehold}
              </span>
              <span
                className={
                  styles["container__first-data--base-content__data--unit"]
                }
              >
                rumah tangga
              </span>
            </div>
            <span
              className={
                styles["container__first-data--base-content__description"]
              }
            >
              {householdLatestUpdated}
            </span>
          </div>
        </div>

        {/* family */}
        <div
          className={`${styles["container__first-data--family"]} ${styles["container__content-base"]}`}
        >
          {/* box-header */}
          <div className={styles["container__content-base--header"]}>
            <div className={styles["container__content-base--header__label"]}>
              <IconProfile2user />
              <span className={styles["container__content-base--header__text"]}>
                Jumlah Keluarga
              </span>
            </div>
            <div>
              {/* TODO: add link */}
              <Button text="Detail" color="primary" variant="text-link" />
            </div>
          </div>
          {/* content */}
          <div className={styles["container__first-data--base-content"]}>
            <div
              className={styles["container__first-data--base-content__data"]}
            >
              <span
                className={
                  styles["container__first-data--base-content__data--value"]
                }
              >
                {totalFamily}
              </span>
              <span
                className={
                  styles["container__first-data--base-content__data--unit"]
                }
              >
                keluarga
              </span>
            </div>
            <span
              className={
                styles["container__first-data--base-content__description"]
              }
            >
              {familyLatestUpdated}
            </span>
          </div>
        </div>

        {/* letter */}
        <div
          className={`${styles["container__first-data--letter"]}  ${styles["container__content-base"]}`}
        >
          {/* box-header */}
          <div className={styles["container__content-base--header"]}>
            <div className={styles["container__content-base--header__label"]}>
              <IconPrinter />
              <span className={styles["container__content-base--header__text"]}>
                Surat Tercetak
              </span>
            </div>
            <div>
              {/* TODO: add link */}
              <Button text="Detail" color="primary" variant="text-link" />
            </div>
          </div>
          {/* content */}
          <div className={styles["container__first-data--base-content"]}>
            <div
              className={styles["container__first-data--base-content__data"]}
            >
              <span
                className={
                  styles["container__first-data--base-content__data--value"]
                }
              >
                {totalLetter}
              </span>
              <span
                className={
                  styles["container__first-data--base-content__data--unit"]
                }
              >
                surat
              </span>
            </div>
            <span
              className={
                styles["container__first-data--base-content__description"]
              }
            >
              {letterLatestUpdated}
            </span>
          </div>
        </div>
      </div>

      {/* second-data */}
      <div className={styles["container__second-data"]}>
        <div
          className={`${styles["container__content-base"]} ${styles["container__second-data--content"]}`}
        >
          {/* box-header */}
          <div className={styles["container__content-base--header"]}>
            <div className={styles["container__content-base--header__label"]}>
              {/* <Icon name="profile-age" width={22} height={22} /> */}
              <span className={styles["container__content-base--header__text"]}>
                Distrubusi Usia Penduduk
              </span>
            </div>
            <div className={styles["container__second-data--detail"]}>
              <div className={styles["container__second-data--detail__info"]}>
                {/* <Dot size="lg" variant="info" /> */}
                <span
                  className={
                    styles["container__second-data--detail__info--text"]
                  }
                >
                  Laki-laki
                </span>
              </div>
              <div className={styles["container__second-data--detail__info"]}>
                {/* <Dot size="lg" variant="secondary" /> */}
                <span
                  className={
                    styles["container__second-data--detail__info--text"]
                  }
                >
                  Perempuan
                </span>
              </div>
            </div>
          </div>

          {/* chart-content */}
          <div className={styles["container__second-data--content__chart"]}>
            {/* TODO: add chart bar here */}
            <Image
              src={ChartBarDefault}
              width={1000}
              height={260}
              alt="chart-bar-default"
              className={
                styles["container__second-data--content__chart--image"]
              }
            />
          </div>
        </div>
      </div>
    </div>
  );
}
