import React from "react";
import styles from "./empty-state.module.scss";
import { Text } from "@/app/ui/components/typography/typography";
import IconDocument from "@/app/ui/icons/icon-document";

const EmptyState = ({
  className,
  children,
  ...props
}: React.ComponentPropsWithoutRef<"div">) => {
  return (
    <div
      className={`${styles["empty__state__wrapper"]} ${className}`}
      {...props}
    >
      <div className={styles["empty__state__icon"]}>
        <IconDocument width={32} height={32} />
      </div>
      {children}
    </div>
  );
};

const EmptyStateTitle = ({
  className,
  children,
  ...props
}: React.ComponentPropsWithoutRef<"p">) => {
  return (
    <Text
      variant="text"
      size="md"
      fontStyle="semibold"
      className={`${className} ${styles["empty__state__title"]}`}
      {...props}
    >
      {children}
    </Text>
  );
};

const EmptyStateDescription = ({
  className,
  children,
  ...props
}: React.ComponentPropsWithoutRef<"p">) => {
  return (
    <Text
      variant="text"
      size="sm"
      fontStyle="regular"
      muted
      className={`${className} ${styles["empty__state__description"]}`}
      {...props}
    >
      {children}
    </Text>
  );
};

const EmptyStateAction = ({
  className,
  children,
  ...props
}: React.ComponentPropsWithoutRef<"div">) => {
  return (
    <div
      className={`${styles["empty__state__action"]} ${className}`}
      {...props}
    >
      {children}
    </div>
  );
};
export { EmptyState, EmptyStateTitle, EmptyStateDescription, EmptyStateAction };
