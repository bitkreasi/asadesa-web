import React from "react";
import styles from "./profile-section.module.scss";
import Image from "next/image";
import { Text } from "@/app/ui/components/typography/typography";
import { Card, CardContent } from "@/app/ui/components/card/card";

interface ProfileSectionProps
  extends React.ComponentPropsWithoutRef<"section"> {
  data: {
    imageUrl: string;
  };
}

const ProfileSection = (props: ProfileSectionProps) => {
  return (
    <Card>
      <CardContent>
        <div className={styles["profile__avatar"]}>
          <Image
            alt="Profile image"
            src={props.data.imageUrl}
            width={64}
            height={64}
            className={styles["profile__avatar--img"]}
          />
          <Text
            variant="text"
            size="md"
            fontStyle="medium"
            className={styles["profile__avatar--text"]}
          >
            Foto profil
          </Text>
        </div>
      </CardContent>
    </Card>
  );
};

export default ProfileSection;
