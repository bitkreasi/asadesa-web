'use client'

import Header from '@/app/ui/components/header/header'
import styles from './page.module.scss'
import Button from '@/app/ui/components/button/button'
import IconLocation from '@/app/ui/icons/icon-location'
import VillageImg from '../../../ui/images/info-desa.png'
import LogoImg from '../../../ui/images/info-logo.png'
import IconMap2 from '@/app/ui/icons/icon-map-2'
import { useRouter } from 'next/navigation'
import { useVillageInfo } from '@/hooks/use-village-info'

type Props = {
  params: {
    id: string
  }
}

export default function Page(props: Props) {
  const { data, loading } = useVillageInfo(props.params.id)
  const router = useRouter()

  return (
    <section className={styles['main__content']}>
      {/* header */}
      <div className={styles['content__header']}>
        <Header
          items={[
            {
              id: 'dashboard',
              name: 'Beranda',
              link: '/dashboard',
            },
            {
              id: 'resident',
              name: 'Identitas Desa',
              link: `/dashboard/village/${props.params.id}`,
            },
          ]}
          size='xl'
          title='Identitas Desa'
          subTitle=''
        />
        <Button
          variant='outline'
          text='Ubah Data Desa'
          color='primary'
          size='md'
          prefix={<IconLocation color='#4163E7' backgroundColor='#4163E7' />}
          onHandleClick={() => router.push(`/dashboard/village/${props.params.id}/edit`)}
        />
      </div>

      {/* info */}
      <div className={styles['content__info']}>
        <img
          src={VillageImg.src}
          alt='village'
          className={styles['content__info__img']}
        />
        <div className={styles['content__profile']}>
          <img src={LogoImg.src} className={styles['content__profile__img']} />
          <div className={styles['content__profile__data']}>
            {loading ? (
              <>
                <div className={styles['content__loading--title']} />
                <div className={styles['content__loading--subtitle']} />
                <div className={styles['content__loading-header']}>
                  <div className={styles['content__loading-header--button']} />
                  <div className={styles['content__loading-header--button']} />
                </div>
              </>
            ) : (
              <>
                <h1 className={styles['content__profile__name']}>
                  Desa {data?.village_name.toLowerCase()}
                </h1>
                <p className={styles['content__profile__address']}>
                  Kecamatan {data?.districts_name.toLowerCase()},{' '}
                  {data?.regency_name.toLowerCase()}, Provinsi{' '}
                  {data?.province_name.toLowerCase()}
                </p>
                <div className={styles['content__profile__button-wrapper']}>
                  <Button
                    color='primary'
                    prefix={
                      <IconLocation backgroundColor='#8DA1F1' color='#fff' />
                    }
                    text='Lokasi Kantor Desa'
                    onHandleClick={() =>
                      router.push(`/dashboard/village/${props.params.id}/location`)
                    }
                  />
                  <Button
                    color='primary'
                    variant='outline'
                    prefix={<IconMap2 backgroundColor='#8DA1F1' />}
                    text='Peta Wilayah Desa'
                    onHandleClick={() =>
                      router.push(`/dashboard/village/${props.params.id}/location`)
                    }
                  />
                </div>
              </>
            )}
          </div>
        </div>
      </div>

      {/* detail */}
      <div className={styles['content__detail']}>
        <p className={styles['content__detail__title']}>Desa</p>
        <div className={styles['content__detail--village']}>
          <div>
            <p className={styles['content__detail__subtitle']}>Nama Desa</p>
            {loading ? (
              <div className={styles['content__loading--items']} />
            ) : (
              <p className={styles['content__detail__body']}>
                {data?.village_name.toLowerCase()}
              </p>
            )}
          </div>
          <div>
            <p className={styles['content__detail__subtitle']}>Kode Desa</p>
            {loading ? (
              <div className={styles['content__loading--items']} />
            ) : (
              <p className={styles['content__detail__body']}>
                {data?.village_code?.replace(/(\d{2})(\d{2})(\d{2})(\d{4})/, '$1.$2.$3.$4') ?? '-'}
              </p>
            )}
          </div>
          <div>
            <p className={styles['content__detail__subtitle']}>Kode Pos Desa</p>
            {loading ? (
              <div className={styles['content__loading--items']} />
            ) : (
              <p className={styles['content__detail__body']}>
                {data?.postal_code ?? '-'}
              </p>
            )}
          </div>
          <div>
            <p className={styles['content__detail__subtitle']}>
              Nama Kepala Desa
            </p>
            {loading ? (
              <div className={styles['content__loading--items']} />
            ) : (
              <p className={styles['content__detail__body']}>
                {data?.chief_village_name ?? '-'}
              </p>
            )}
          </div>
          <div>
            <p className={styles['content__detail__subtitle']}>
              NIP Kepala Desa
            </p>
            {loading ? (
              <div className={styles['content__loading--items']} />
            ) : (
              <p className={styles['content__detail__body']}>
                {data?.chief_village_nik ?? '-'}
              </p>
            )}
          </div>
        </div>
        <div className={styles['content__detail--village']}>
          <div>
            <p className={styles['content__detail__subtitle']}>
              Alamat Kantor Desa
            </p>
            {loading ? (
              <div className={styles['content__loading--items']} />
            ) : (
              <p className={styles['content__detail__body']}>
                {data?.address ?? '-'}
              </p>
            )}
          </div>
          <div>
            <p className={styles['content__detail__subtitle']}>E-Mail Desa</p>
            {loading ? (
              <div className={styles['content__loading--items']} />
            ) : (
              <p className={styles['content__detail__body']}>
                {data?.email ?? '-'}
              </p>
            )}
          </div>
          <div>
            <p className={styles['content__detail__subtitle']}>Nomor ponsel</p>
            {loading ? (
              <div className={styles['content__loading--items']} />
            ) : (
              <p className={styles['content__detail__body']}>
                {data?.cell_phone ?? '-'}
              </p>
            )}
          </div>
          <div>
            <p className={styles['content__detail__subtitle']}>
              Nomor Telepon Desa
            </p>
            {loading ? (
              <div className={styles['content__loading--items']} />
            ) : (
              <p className={styles['content__detail__body']}>
                {data?.phone ?? '-'}
              </p>
            )}
          </div>
          <div>
            <p className={styles['content__detail__subtitle']}>Website Desa</p>
            {loading ? (
              <div className={styles['content__loading--items']} />
            ) : (
              <p className={styles['content__detail__body']}>
                {data?.website ?? '-'}
              </p>
            )}
          </div>
        </div>
      </div>

      {/* disctrict */}
      <div className={styles['content__detail']}>
        <p className={styles['content__detail__title']}>Kecamatan</p>
        <div className={styles['content__detail--regency']}>
          <div>
            <p className={styles['content__detail__subtitle']}>
              Nama Kecamatan
            </p>
            {loading ? (
              <div className={styles['content__loading--items']} />
            ) : (
              <p className={styles['content__detail__body']}>
                {data?.districts_name.toLowerCase() ?? '-'}
              </p>
            )}
          </div>
          <div>
            <p className={styles['content__detail__subtitle']}>
              Kode Kecamatan
            </p>
            {loading ? (
              <div className={styles['content__loading--items']} />
            ) : (
              <p className={styles['content__detail__body']}>
                {data?.districts_code.replace(/(\d{2})(\d{2})(\d{2})(\d{1})/, '$1.$2.$3.$4') ?? '-'}
              </p>
            )}
          </div>
          <div>
            <p className={styles['content__detail__subtitle']}>Nama Camat</p>
            {loading ? (
              <div className={styles['content__loading--items']} />
            ) : (
              <p className={styles['content__detail__body']}>
                {data?.chief_district_name ?? '-'}
              </p>
            )}
          </div>
          <div>
            <p className={styles['content__detail__subtitle']}>NIP Camat</p>
            {loading ? (
              <div className={styles['content__loading--items']} />
            ) : (
              <p className={styles['content__detail__body']}>
                {data?.chief_district_nik ?? '-'}
              </p>
            )}
          </div>
        </div>
      </div>

      {/* regency */}
      <div className={styles['content__detail']}>
        <p className={styles['content__detail__title']}>Kabupaten</p>
        <div className={styles['content__detail--regency']}>
          <div>
            <p className={styles['content__detail__subtitle']}>
              Nama Kabupaten
            </p>
            {loading ? (
              <div className={styles['content__loading--items']} />
            ) : (
              <p className={styles['content__detail__body']}>
                {data?.regency_name?.replace('KABUPATEN', '') ?? '-'}
              </p>
            )}
          </div>
          <div>
            <p className={styles['content__detail__subtitle']}>
              Kode Kabupaten
            </p>
            {loading ? (
              <div className={styles['content__loading--items']} />
            ) : (
              <p className={styles['content__detail__body']}>
                {data?.regency_code.replace(/(\d{2})(\d{2})/, '$1.$2') ?? '-'}
              </p>
            )}
          </div>
        </div>
      </div>

      {/* province */}
      <div className={styles['content__detail']}>
        <p className={styles['content__detail__title']}>Provinsi</p>
        <div className={styles['content__detail--province']}>
          <div>
            <p className={styles['content__detail__subtitle']}>Nama Provinsi</p>
            {loading ? (
              <div className={styles['content__loading--items']} />
            ) : (
              <p className={styles['content__detail__body']}>
                {data?.province_name ?? '-'}
              </p>
            )}
          </div>
          <div>
            <p className={styles['content__detail__subtitle']}>Kode Provinsi</p>
            {loading ? (
              <div className={styles['content__loading--items']} />
            ) : (
              <p className={styles['content__detail__body']}>
                {data?.province_code ?? '-'}
              </p>
            )}
          </div>
        </div>
      </div>
    </section>
  )
}
