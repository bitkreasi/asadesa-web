'use client'

import Header from '@/app/ui/components/header/header'
import styles from './page.module.scss'
import IconBuilding4 from '@/app/ui/icons/icon-building-4'
import Image from 'next/image'

import Button from '@/app/ui/components/button/button'
import IconDownload from '@/app/ui/icons/icon-download'
import logoImg from '@/app/ui/images/info-logo.png'
import BackgroundImg from '@/app/ui/images/info-desa.png'
import Input from '@/app/ui/components/input/input'
import { useEffect, useRef, useState } from 'react'
import { extractErrors } from '@/utils/extract-error'
import { useRouter } from 'next/navigation'
import { useVillageInfo } from '@/hooks/use-village-info'
import { villageService } from '@/services'
import { useModal } from '@/hooks/use-modal'
import Modal from '@/app/ui/components/modal/modal'

type Props = {
  params: {
    id: string
  }
}

export default function Page(props: Props) {
  const [data, setData] = useState({
    village_name: '',
    village_code: '0',
    postal_code: '0',
    chief_village_name: '',
    chief_village_nik: '',
    website: '',
    address: '',
    cell_phone: '',
    phone: '',
    email: '',
    districts_name: '',
    districts_code: '',
    chief_district_name: '',
    chief_district_nik: '',
    regency_name: '',
    regency_code: '',
    province_name: '',
    province_code: '',
    logo: null,
    background: null,
  })
  const { data: dataVillage, loading } = useVillageInfo(props.params.id)
  const [errors, setErrors] = useState<any>({})
  const router = useRouter()
  const inputLogoRef = useRef<HTMLInputElement>(null)
  const inputBgRef = useRef<HTMLInputElement>(null)
  const { show, toggle } = useModal()

  const [previewLogo, setPreviewLogo] = useState<string>(logoImg.src)
  const [previewBg, setPreviewBg] = useState<string>(BackgroundImg.src)

  async function handleUploadImage(type: 'logo' | 'bg') {
    if (type == 'bg') {
      const uploadedFile = inputBgRef.current?.files?.[0]
      if (uploadedFile) {
        const cachedURL = URL.createObjectURL(uploadedFile)
        setPreviewBg(cachedURL)
      } else {
        console.error('No file selected')
      }
      return
    }
    const uploadedFile = inputLogoRef.current?.files?.[0]
    if (uploadedFile) {
      const cachedURL = URL.createObjectURL(uploadedFile)
      setPreviewLogo(cachedURL)
    } else {
      console.error('No file selected')
    }
  }

  function handleChange(value: string, name: string) {
    setData((prev) => ({ ...prev, [name]: value }))
  }

  async function handleSubmit(e: React.FormEvent) {
    e.preventDefault()
    try {
      const payload = {
        postal_code: data.postal_code,
        address: data.address,
        email: data.email,
        phone: data.phone,
        cell_phone: data.cell_phone,
        website: data.website,
        chief_village_name: data.chief_village_name,
        chief_village_nik: data.chief_village_nik,
        chief_district_name: data.chief_district_name,
        chief_district_nik: data.chief_district_nik,
      }
      await villageService.updateInstitutions(props.params.id, payload)
      router.push(`/dashboard/village/${props.params.id}`)
    } catch (error) {
      extractErrors(error, setErrors)
    }
  }

  useEffect(() => {
    if (!loading) {
      const payload = {
        ...dataVillage,
      }
      setData((prev) => ({ ...prev, ...payload }))
    }
  }, [loading, dataVillage])

  return (
    <form onSubmit={handleSubmit} encType='multipart/form-data'>
      <section className={styles['main__content']}>
        {/* header */}
        <div className={styles['content__header']}>
          <Header
            items={[
              {
                id: 'dashboard',
                name: 'Beranda',
                link: '/dashboard',
              },
              {
                id: 'resident',
                name: 'Identitas Desa',
                link: `/dashboard/village/${props.params.id}`,
              },
              {
                id: 'edit',
                name: 'Ubah Data Identitas Desa',
                link: `/dashboard/village/${props.params.id}/edit`,
              },
            ]}
            size='xl'
            title='Ubah Data Identitas Desa'
            subTitle=''
          />
        </div>
        {/* input logo and background */}
        <div className={styles.content}>
          <div className={styles['content__title__wrapper']}>
            <IconBuilding4 />
            <p className={styles['content__title']}>Identitas Desa</p>
          </div>
          <div className={styles.content}>
            <div className={styles['input-img']}>
              <div className={styles['input-img__wrapper']}>
                <img
                  src={previewLogo}
                  width={64}
                  height={64}
                  alt='logo'
                  className={styles['input-img-preview']}
                />
                <input
                  type='file'
                  hidden
                  id='input-logo'
                  ref={inputLogoRef}
                  onChange={() => handleUploadImage('logo')}
                />
                <div className={styles['input-img-label-wrapper']}>
                  <label
                    htmlFor='input-logo'
                    className={styles['input-img-label']}
                  >
                    Lambang Desa
                  </label>
                  <p className={styles['input-img-alt']}>
                    PNG, JPEG dibawah 2MB
                  </p>
                </div>
              </div>
              <Button
                prefix={<IconDownload backgroundColor='#8DA1F1' color='#fff' />}
                text='Unggah Foto'
                variant='solid'
                color='primary'
                onHandleClick={() => inputLogoRef?.current?.click()}
              />
            </div>
          </div>
          <div className={styles['content--background']}>
            <div className={styles['input-img']}>
              <div className={styles['input-img__wrapper']}>
                <img
                  src={previewBg}
                  width={338}
                  height={126}
                  alt='logo'
                  className={styles['input-img--background']}
                />
                <input
                  type='file'
                  hidden
                  id='input-logo'
                  ref={inputBgRef}
                  onChange={() => handleUploadImage('bg')}
                />

                <div className={styles['input-img-label-wrapper']}>
                  <label
                    htmlFor='input-background'
                    className={styles['input-img-label']}
                  >
                    Kantor Desa
                  </label>
                  <p className={styles['input-img-alt']}>
                    PNG, JPEG dibawah 2MB
                  </p>
                </div>
              </div>
              <Button
                prefix={<IconDownload backgroundColor='#8DA1F1' color='#fff' />}
                text='Unggah Foto'
                variant='solid'
                color='primary'
                onHandleClick={() => inputBgRef?.current?.click()}
              />
            </div>
          </div>
        </div>

        {/* input detail */}
        <div className={styles.content}>
          <p className={styles['content__title']}>Desa</p>
          <div className={styles['content__input-wrapper']}>
            <div>
              <div>
                <Input
                  label='Nama Desa'
                  value={data.village_name}
                  setValue={(val) => handleChange(val, 'village_name')}
                  size='md'
                  variant='text-field'
                  isError={errors?.village_name}
                  helperText={errors.village_name ?? ''}
                />
              </div>
              <div>
                <Input
                  label='Kode Pos Desa'
                  value={data.postal_code}
                  setValue={(val) => handleChange(val, 'postal_code')}
                  size='md'
                  variant='text-field'
                  textFieldType='number'
                  minLength={0}
                  isError={errors?.postal_code}
                  helperText={errors.postal_code ?? ''}
                />
              </div>
              <div>
                <Input
                  label='NIP Kepala Desa'
                  value={data.chief_village_nik}
                  setValue={(val) => handleChange(val, 'chief_village_nik')}
                  size='md'
                  variant='text-field'
                  textFieldType='number'
                  isError={errors?.chief_village_nik}
                  helperText={errors.chief_village_nik ?? ''}
                />
              </div>
              <div>
                <Input
                  label='E-Mail Desa'
                  value={data.email}
                  setValue={(val) => handleChange(val, 'email')}
                  size='md'
                  variant='text-field'
                  isError={errors?.email}
                  helperText={errors.email ?? ''}
                />
              </div>
              <div>
                <Input
                  label='Nomor Ponsel Desa'
                  value={data.phone}
                  setValue={(val) => handleChange(val, 'phone')}
                  size='md'
                  variant='text-field'
                  isError={errors?.phone}
                  helperText={errors.phone ?? ''}
                />
              </div>
            </div>
            <div>
              <div>
                <Input
                  label='Kode Desa'
                  value={data.village_code.replace(
                    /(\d{2})(\d{2})(\d{2})(\d{4})/,
                    '$1.$2.$3.$4'
                  )}
                  setValue={(val) => handleChange(val, 'village_code')}
                  size='md'
                  variant='text-field'
                  isError={errors?.village_code}
                  helperText={errors.village_code ?? ''}
                  isDisable
                />
              </div>
              <div>
                <Input
                  label='Nama Kepala Desa'
                  value={data.chief_village_name}
                  setValue={(val) => handleChange(val, 'chief_village_name')}
                  size='md'
                  variant='text-field'
                  isError={errors?.chief_village_name}
                  helperText={errors.chief_village_name ?? ''}
                />
              </div>
              <div>
                <Input
                  label='Alamat Kantor Desa'
                  value={data.address}
                  setValue={(val) => handleChange(val, 'address')}
                  size='md'
                  variant='text-field'
                  isError={errors?.address}
                  helperText={errors.address ?? ''}
                />
              </div>
              <div>
                <Input
                  label='Nomor Telepon Desa'
                  value={data.cell_phone}
                  setValue={(val) => handleChange(val, 'cell_phone')}
                  size='md'
                  variant='text-field'
                  isError={errors?.cell_phone}
                  helperText={errors.cell_phone ?? ''}
                />
              </div>
              <div>
                <Input
                  label='Website Desa'
                  value={data.website}
                  setValue={(val) => handleChange(val, 'website')}
                  size='md'
                  variant='text-field'
                  isError={errors?.website}
                  helperText={errors.website ?? ''}
                />
              </div>
            </div>
          </div>
        </div>

        {/* input district */}
        <div className={styles.content}>
          <p className={styles['content__title']}>Kecamatan</p>
          <div className={styles['content__input-wrapper']}>
            <div>
              <div>
                <Input
                  label='Nama Kecamatan'
                  value={data.districts_name}
                  setValue={(val) => handleChange(val, 'districts_name')}
                  size='md'
                  variant='text-field'
                  isError={errors?.districts_name}
                  helperText={errors.districts_name ?? ''}
                />
              </div>
              <div>
                <Input
                  label='Nama Camat'
                  value={data.chief_district_name}
                  setValue={(val) => handleChange(val, 'chief_district_name')}
                  size='md'
                  variant='text-field'
                  isError={errors?.chief_district_name}
                  helperText={errors.chief_district_name ?? ''}
                />
              </div>
            </div>
            <div>
              <div>
                <Input
                  label='Kode Kecamatan'
                  value={data.districts_code.replace(
                    /(\d{2})(\d{2})(\d{2})(\d{1})/,
                    '$1.$2.$3.$4'
                  )}
                  setValue={(val) => handleChange(val, 'districts_code')}
                  size='md'
                  variant='text-field'
                  isError={errors?.districts_code}
                  helperText={errors.districts_code ?? ''}
                  isDisable
                />
              </div>
              <div>
                <Input
                  label='Nama Camat'
                  value={data.chief_district_nik}
                  setValue={(val) => handleChange(val, 'chief_district_nik')}
                  size='md'
                  variant='text-field'
                  isError={errors?.chief_district_nik}
                  helperText={errors.chief_district_nik ?? ''}
                />
              </div>
            </div>
          </div>
        </div>

        {/* input regency */}
        <div className={styles.content}>
          <p className={styles['content__title']}>Kabupaten</p>
          <div className={styles['content__input-wrapper']}>
            <div>
              <div>
                <Input
                  label='Nama Kabupaten'
                  value={data.regency_name}
                  setValue={(val) => handleChange(val, 'regency_name')}
                  size='md'
                  variant='text-field'
                  isError={errors?.regency_name}
                  helperText={errors.regency_name ?? ''}
                />
              </div>
            </div>
            <div>
              <div>
                <Input
                  label='Kode Kabupaten'
                  value={data.regency_code.replace(/(\d{2})(\d{2})/, '$1.$2')}
                  setValue={(val) => handleChange(val, 'regency_code')}
                  size='md'
                  variant='text-field'
                  isError={errors?.regency_code}
                  helperText={errors.regency_code ?? ''}
                  isDisable
                />
              </div>
            </div>
          </div>
        </div>

        {/* input province */}
        <div className={styles.content}>
          <p className={styles['content__title']}>Kecamatan</p>
          <div className={styles['content__input-wrapper']}>
            <div>
              <div>
                <Input
                  label='Nama Provinsi'
                  value={data.province_name}
                  setValue={(val) => handleChange(val, 'province_name')}
                  size='md'
                  variant='text-field'
                  isError={errors?.province_name}
                  helperText={errors.province_name ?? ''}
                />
              </div>
            </div>
            <div>
              <div>
                <Input
                  label='Kode Pronvinsi'
                  value={data.province_code}
                  setValue={(val) => handleChange(val, 'province_code')}
                  size='md'
                  variant='text-field'
                  isError={errors?.province_code}
                  helperText={errors.province_code ?? ''}
                  isDisable
                />
              </div>
            </div>
          </div>
        </div>

        {/* button */}
        <div className={styles['content__button-wrapper']}>
          <Button
            type='button'
            text='Batal'
            variant='outline'
            color='neutral'
            onHandleClick={router.back}
          />
          <Button
            type='button'
            text='Simpan'
            color='primary'
            onHandleClick={toggle}
          />
        </div>
      </section>
      <Modal isOpen={show} toggle={toggle}>
        <p className={styles['modal__title']}>
          Anda yakin ingin menyimpan data ini?
        </p>
        <p className={styles['modal__body']}>
          Setelah anda menyimpan data ini, anda dapat kembali mengedit data ini
          dengan pergi ke ubah data desa.
        </p>
        <div className={styles['modal__action']}>
          <Button
            color='neutral'
            variant='outline'
            text='Cek Ulang'
            onHandleClick={toggle}
          />
          <Button type='submit' text='Simpan' color='primary' />
        </div>
      </Modal>
    </form>
  )
}
