'use client'

import Header from '@/app/ui/components/header/header'
import styles from './page.module.scss'
import Input from '@/app/ui/components/input/input'
import { useState } from 'react'
import Button from '@/app/ui/components/button/button'
import IconCloseCircle from '@/app/ui/icons/icon-close-circle'
import IconExport from '@/app/ui/icons/icon-export'
import IconSearch from '@/app/ui/icons/icon-search'
import { MapContainer, Marker, TileLayer } from 'react-leaflet'
import 'leaflet/dist/leaflet.css'
import Map from './map'

type Props = {
  params: { [key: string]: string | string[] | undefined }
}

export default function Page(props: Props) {
  const [data, setData] = useState({
    longtitude: '0',
    latitude: '0',
    search: '',
  })

  function handleChange(value: string, name: string) {
    setData((prev) => ({ ...prev, [name]: value }))
  }

  return (
    <section className={styles['main__content']}>
      {/* header */}
      <div className={styles.header}>
        <Header
          items={[
            {
              id: 'dashboard',
              name: 'Beranda',
              link: '/dashboard',
            },
            {
              id: 'village',
              name: 'Identitas Desa',
              link: `/dashboard/village/${props.params.id}`,
            },
            {
              id: 'Lokasi',
              name: 'Lokasi Identitas Desa',
              link: `/dashboard/village/${props.params.id}/location`,
            },
          ]}
          size='xl'
          title='Lokasi Kantor Desa'
          subTitle=''
          isBack
        />
      </div>

      {/* upper */}
      <div>
        <div className={styles['input__wrapper--position']}>
          <Input
            label=''
            value={data.longtitude}
            setValue={(val) => handleChange(val, 'longtitude')}
            size='md'
            variant='text-field'
            helperText=''
          />
          <Input
            label=''
            value={data.latitude}
            setValue={(val) => handleChange(val, 'latitude')}
            size='md'
            variant='text-field'
            helperText=''
          />
          <Button
            prefix={<IconExport width={20} height={20} />}
            text='Export to GPX'
            color='primary'
            variant='outline'
            size='md'
          />
          <Button
            prefix={
              <IconCloseCircle
                backgroundColor='#fff'
                color='#fff'
                height={20}
                width={20}
              />
            }
            text='Reset'
            color='error'
            variant='solid'
            size='md'
          />
        </div>
        <div className={styles['input__wrapper--search']}>
          <Input
            label=''
            value={data.search}
            setValue={(val) => handleChange(val, 'search')}
            size='md'
            variant='text-field'
            placeholder='Search'
            prefix={<IconSearch backgroundColor='#4163E7' color='#4163E7' />}
            helperText=''
          />
          <Button size='md' text='Clear' variant='outline' color='primary' />
          <Button size='md' text='Search' color='primary' />
        </div>
      </div>

      {/* map */}
      <div className={styles['map_container']}>
        <Map />
        <div className={styles['map__action']}>
          <Button variant='outline' color='neutral' text='batal' />
          <Button variant='solid' color='primary' text='Simpan' />
        </div>
      </div>
    </section>
  )
}
