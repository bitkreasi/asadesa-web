'use client'

import { TileLayer, MapContainer, Marker } from 'react-leaflet'
import 'leaflet/dist/leaflet.css'
import styles from './page.module.scss'
import L from 'leaflet'

import icon from '@/app/ui/images/pin.png'

export default function Map() {
  const customIcon = new L.Icon({
    iconUrl: icon.src,
    iconSize: [32, 32],
  })

  if (typeof window !== 'undefined') {
    return (
      <MapContainer
        center={[50.1, 50.1]}
        zoom={2}
        scrollWheelZoom={false}
        className={styles['map']}
      >
        <TileLayer
          attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
          url='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
        />
        <Marker position={[50.1, 50.1]} icon={customIcon}/>
      </MapContainer>
    )
  }

  return <div className={styles['map']} />
}
