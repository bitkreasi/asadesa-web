'use client'

import { menuItems } from '@/app/ui/layouts/menuItems'
import Sidebar from '@/app/ui/layouts/sidebar/sidebar'
import Topbar from '@/app/ui/layouts/topbar/topbar'
import styles from './layout.module.scss'

export default function LayoutInfo({ children }: React.PropsWithChildren) {
  return (
    <div className={styles['container']}>
      <Sidebar menuItems={menuItems} />
      <div className={styles['content']}>
        <Topbar
          value={''}
          setValue={function (value: string): void {
            throw new Error('Function not implemented.')
          }}
        />
        <div className={styles['container__main']}>
          {children}
        </div>
      </div>
    </div>
  )
}
