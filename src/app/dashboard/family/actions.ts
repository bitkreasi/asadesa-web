"use server";

import { publicFetch } from "@/utils/fetch";
import { revalidatePath } from "next/cache";
import { redirect } from "next/navigation";

export interface AddResidentRequestBody {
  fullname: string;
  nik: string;
  family_role: string;
  gender: string;
  religion: string;
  resident_status: string;
}

export interface AddFamilyRequestBody {
  type: string;
  resident?: AddResidentRequestBody;
  head_family_id?: string;
  no_kk: string;
}

async function addFamilyAction(data: AddFamilyRequestBody) {
  try {
    const res = await publicFetch("/families", { method: "POST", body: data });
    if (res.error) {
      return { error: res?.error };
    }
  } catch (e) {
    console.error(e);
    return { error: "Terjadi kesalahan" };
  }

  revalidatePath("/dashboard/family");
  redirect("/dashboard/family");
}

export { addFamilyAction };
