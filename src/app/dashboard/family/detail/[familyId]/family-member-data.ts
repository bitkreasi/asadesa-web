export interface FamilyMember {
  id: string;
  imageProfile: string;
  fullname: string;
  nik: string;
  village: string;
  birtdate: string;
  gender: "Laki-Laki" | "Perempuan";
  relation: "Kepala Keluarga" | "Anak" | "Istri" | "Orang Tua";
}

export const familyMemberData: FamilyMember[] = [
  {
    id: crypto.randomUUID(),
    imageProfile: "https://source.unsplash.com/random/300×300",
    fullname: "Ahlul",
    nik: "5201142005716996",
    village: "MANGSIT",
    birtdate: "20 Mei 1970",
    gender: "Laki-Laki",
    relation: "Kepala Keluarga",
  },
  {
    id: crypto.randomUUID(),
    imageProfile: "https://source.unsplash.com/random/300×300",
    fullname: "Ahmad Habib",
    nik: "52011420057152286",
    village: "MANGSIT",
    birtdate: "03 Januari 1990",
    gender: "Laki-Laki",
    relation: "Anak",
  },
  {
    id: crypto.randomUUID(),
    imageProfile: "https://source.unsplash.com/random/300×300",
    fullname: "Ahlul",
    nik: "5201142005716923",
    village: "MANGSIT",
    birtdate: "07 Juni 1995",
    gender: "Laki-Laki",
    relation: "Anak",
  },
  {
    id: crypto.randomUUID(),
    imageProfile: "https://source.unsplash.com/random/300×300",
    fullname: "Ahlul",
    nik: "5201142005716976",
    village: "MANGSIT",
    birtdate: "10 Maret 1996",
    gender: "Perempuan",
    relation: "Anak",
  },
];
