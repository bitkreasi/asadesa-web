"use client";
import React from "react";
import styles from "./member-form.module.scss";
import * as yup from "yup";
import { SubmitHandler, useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import {
  Form,
  FormControl,
  FormDescription,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/app/ui/components/form/form";
import {
  Card,
  CardContent,
  CardHeader,
  CardTitle,
} from "@/app/ui/components/card/card";
import IconHome2 from "@/app/ui/icons/icon-home-2";
import Button from "@/app/ui/components/button/button";
import IconImport from "@/app/ui/icons/icon-import";
import Image from "next/image";
import { Text } from "@/app/ui/components/typography/typography";
import { Input } from "@/app/ui/components/input/v2/input";
import { Select, SelectItem } from "@/app/ui/components/select/select";
import { Textarea } from "@/app/ui/components/textarea/textarea";
import { Label } from "@/app/ui/components/label/label";

const memberSchema = yup.object({
  jenis_tambah_anggota_keluarga: yup
    .string()
    .required("Jenis tambah anggota keluarga wajib diisi"),
  tanggal_pindah_masuk: yup.string(), // only for anggota keluarga masuk
  tanggal_lapor: yup.string().required("Tanggal lapor wajib diisi"),
  nama_lengkap: yup.string().required("Nama lengkap wajib diisi"),
  nomor_nik: yup.string().required("Nomor nik wajib diisi"),
  wajib_identitas: yup.string(),
  identitas_elektronik: yup.string(),
  status_rekam: yup.string(),
  tag_id_card: yup.string(),
  nomor_kk_sebelumnya: yup.string(),
  hubungan_dalam_kelurga: yup
    .string()
    .required("Hubungan dalam keluarga wajib diisi"),
  jenis_kelamin: yup.string().required("Jenis kelamin wajib diisi"),
  agama: yup.string().required("Agama wajib diisi"),
  status_penduduk: yup.string().required("Status penduduk wajib diisi"),
  nomor_akta_kelahiran: yup
    .string()
    .required("Nomor akta kelahiran wajib diisi"),
  kota_kelahiran: yup.string().required("Kota kelahiran wajib diisi"),
  tanggal_lahir: yup.string().required("Tanggal lahir wajib diisi"),
  waktu_kelahiran: yup.string().required("Waktu kelahiran wajib diisi"),
  tempat_dilahirkan: yup.string().required("Tempat kelahiran wajib diisi"),
  jenis_kelahiran: yup.string().required("Jenis kelahiran wajib diisi"),
  anak_ke: yup.string().required("Anak ke wajib diisi"),
  tenaga_kesehatan: yup.string().required("Tenaga kesehatan wajib diisi"),
  berat_lahir: yup.string().required("Berat lahir wajib diisi"),
  panjang_lahir: yup.string().required("Panjang lahir wajib diisi"),
  pendidikan_dalam_kk: yup.string().required("Pendidikan dalam kk wajib diisi"),
  pendidikan_sedang_ditempuh: yup
    .string()
    .required("Pendidikan sedang ditempuh wajib diisi"),
  pekerjaan: yup.string().required("Pekerjaan wajib diisi"),
  suku_etnis: yup.string().required("Suku etnis wajib diisi"),
  status_warga_negara: yup.string().required("Status warga negara wajib diisi"),
  nomor_passpor: yup.string(),
  tanggal_berakhir_paspor: yup.string(),
  nama_ayah: yup.string().required("Nama ayah wajib diisi"),
  nik_ayah: yup.string().required("NIK ayah wajib diisi"),
  nama_ibu: yup.string().required("Nama ibu wajib diisi"),
  nik_ibu: yup.string().required("NIK ibu wajib diisi"),
  dusun: yup.string().required("Dusun wajib diisi"),
  rt: yup.string().required("RT wajib diisi"),
  rw: yup.string().required("RW wajib diisi"),
  nomor_telepon: yup.string(),
  email: yup.string(),
  status_nikah: yup.string().required("Status Nikah Wajib diisi"),
  no_akta_nikah: yup.string(),
  tanggal_pernikahan: yup.string(),
  akta_perceraian: yup.string(),
  tanggal_perceraian: yup.string(),
  gol_darah: yup.string().required("Golongan darah wajib diisi"),
  cacat: yup.string(),
  sakit_menahun: yup.string(),
  akseptor_kb: yup.string(),
  asuransi: yup.string().required("Asuransi wajib diisi"),
  nomor_bpjs_ketenagakerjaan: yup.string(),
  dapat_membaca_huruf: yup.string().required("Dapat membaca huruf wajib diisi"),
  catatan: yup.string(),
});

export type MemberForm = yup.InferType<typeof memberSchema>;
interface MemberFormProps {
  headOfFamilyData: {
    nama_lengkap_kepala_keluarga: string;
    nomor_kk: string;
    alamat: string;
  };
  initialMembetData?: MemberForm;
  onSubmit: SubmitHandler<MemberForm>;
}
const MemberForm = (props: MemberFormProps) => {
  const form = useForm({
    resolver: yupResolver(memberSchema),
    defaultValues: {
      jenis_tambah_anggota_keluarga: "Anggota Keluarga Lahir",
    },
  });
  const watchJenisTambahAnggota = form.watch("jenis_tambah_anggota_keluarga");
  const watchStatusNikah = form.watch("status_nikah");

  const isMarried = watchStatusNikah === "Kawin";
  const isDivorced =
    watchStatusNikah === "Cerai Mati" || watchStatusNikah === "Cerai Hidup";

  return (
    <Form {...form}>
      <form
        className={styles["member__form__container"]}
        onSubmit={form.handleSubmit(props.onSubmit)}
      >
        <Card>
          <CardHeader>
            <CardTitle className={styles["member__form__title"]}>
              <IconHome2 /> Data Kepala Keluarga
            </CardTitle>
          </CardHeader>
          <CardContent>
            <Card>
              <CardContent className={styles["member__form__profile"]}>
                <div className={styles["member__form__profile__info"]}>
                  <Image
                    className={styles["member__form__profile__info__image"]}
                    alt="foto profil"
                    src="https://source.unsplash.com/random"
                    width={56}
                    height={56}
                  />
                  <div>
                    <Text variant="text" size="md" fontStyle="medium">
                      Foto profil
                    </Text>
                    <Text variant="text" size="sm" fontStyle="regular" muted>
                      PNG, JPEG dibawah 2MB
                    </Text>
                  </div>
                </div>
                <Button
                  prefix={<IconImport color="#fff" backgroundColor="#fff" />}
                  variant="solid"
                  text="Unggah Foto"
                />
              </CardContent>
            </Card>

            <section className={styles["member__form__input__two"]}>
              <div className={styles["member__form__item"]}>
                <Label aria-disabled>Nama Lengkap Kepala Keluarga</Label>
                <Input
                  type="text"
                  disabled
                  value={props.headOfFamilyData.nama_lengkap_kepala_keluarga}
                />
              </div>
              <div className={styles["member__form__item"]}>
                <Label aria-disabled>Nomor KK</Label>
                <Input
                  type="text"
                  disabled
                  value={props.headOfFamilyData.nomor_kk}
                />
              </div>
            </section>
            <section className={styles["member__form__input__one"]}>
              <div className={styles["member__form__item"]}>
                <Label aria-disabled>Alamat</Label>
                <Input
                  type="text"
                  disabled
                  value={props.headOfFamilyData.alamat}
                />
              </div>
            </section>
            <section className={styles["member__form__input__two"]}>
              <FormField
                control={form.control}
                name="jenis_tambah_anggota_keluarga"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Jenis Tambah Anggota Keluarga</FormLabel>
                    <FormControl>
                      <Select value={field.value} onChange={field.onChange}>
                        <SelectItem value="Anggota Keluarga Lahir">
                          Anggota Keluarga Lahir
                        </SelectItem>
                        <SelectItem value="Anggota Keluarga Masuk">
                          Anggota Keluarga Masuk
                        </SelectItem>
                      </Select>
                    </FormControl>
                  </FormItem>
                )}
              />
            </section>
          </CardContent>
        </Card>

        <Card>
          <CardHeader>
            <CardTitle>Data Diri</CardTitle>
          </CardHeader>
          <CardContent>
            <section
              className={
                watchJenisTambahAnggota === "Anggota Keluarga Masuk"
                  ? styles["member__form__input__two"]
                  : styles["member__form__input__one"]
              }
            >
              {watchJenisTambahAnggota === "Anggota Keluarga Masuk" && (
                <FormField
                  control={form.control}
                  name="tanggal_pindah_masuk"
                  render={({ field }) => (
                    <FormItem>
                      <FormLabel>Tanggal Pindah Masuk</FormLabel>
                      <FormControl>
                        <Input
                          placeholder="Pilih tanggal pindah"
                          type="date"
                          {...field}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              )}
              <FormField
                control={form.control}
                name="tanggal_lapor"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Tanggal Lapor</FormLabel>
                    <FormControl>
                      <Input
                        placeholder="Pilih tanggal lapor"
                        type="date"
                        {...field}
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
            </section>
            <section className={styles["member__form__input__two"]}>
              <FormField
                control={form.control}
                name="nama_lengkap"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Nama Lengkap</FormLabel>
                    <FormControl>
                      <Input
                        type="text"
                        placeholder="Masukkan nama lengkap"
                        {...field}
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="nomor_nik"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Nomor NIK</FormLabel>
                    <FormControl>
                      <Input placeholder="0" type="text" {...field} />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
            </section>

            <Card>
              <CardHeader>
                <CardTitle className={styles["member__form__title"]}>
                  <IconHome2 width={26} height={26} /> Status Kepemilikan
                  Identitas
                </CardTitle>
              </CardHeader>
              <CardContent>
                <section className={styles["member__form__input__four"]}>
                  <FormField
                    control={form.control}
                    name="wajib_identitas"
                    render={({ field }) => (
                      <FormItem>
                        <FormLabel aria-disabled>Wajib Identitas</FormLabel>
                        <FormControl>
                          <Select
                            disabled
                            value={field.value}
                            onChange={field.onChange}
                          >
                            {["Wajib", "Belum Wajib"].map((item) => (
                              <SelectItem value={item} key={item}>
                                {item}
                              </SelectItem>
                            ))}
                          </Select>
                        </FormControl>
                        <FormDescription aria-disabled>
                          Tidak bisa diubah
                        </FormDescription>
                        <FormMessage />
                      </FormItem>
                    )}
                  />
                  <FormField
                    control={form.control}
                    name="identitas_elektronik"
                    render={({ field }) => (
                      <FormItem>
                        <FormLabel>Identitas Elektronik</FormLabel>
                        <FormControl>
                          <Select value={field.value} onChange={field.onChange}>
                            {["Belum", "KTP-EL", "KIA"].map((item) => (
                              <SelectItem value={item} key={item}>
                                {item}
                              </SelectItem>
                            ))}
                          </Select>
                        </FormControl>
                        <FormMessage />
                      </FormItem>
                    )}
                  />
                  <FormField
                    control={form.control}
                    name="status_rekam"
                    render={({ field }) => (
                      <FormItem>
                        <FormLabel>Status Rekam</FormLabel>
                        <FormControl>
                          <Select value={field.value} onChange={field.onChange}>
                            {[
                              "Belum rekam",
                              "Sudah rekam",
                              "Card Printed",
                              "Print Reary Record",
                              "Card Shipped",
                              "Sent For Card Printing",
                              "Card Issued",
                              "Belum wajib",
                            ].map((item) => (
                              <SelectItem value={item} key={item}>
                                {item}
                              </SelectItem>
                            ))}
                          </Select>
                        </FormControl>
                        <FormMessage />
                      </FormItem>
                    )}
                  />
                  <FormField
                    control={form.control}
                    name="tag_id_card"
                    render={({ field }) => (
                      <FormItem>
                        <FormLabel>Tag ID Card</FormLabel>
                        <FormControl>
                          <Input
                            type="text"
                            placeholder="Masukkan ID Card"
                            {...field}
                          />
                        </FormControl>
                        <FormMessage />
                      </FormItem>
                    )}
                  />
                </section>
              </CardContent>
            </Card>
            <section className={styles["member__form__input__three"]}>
              <FormField
                control={form.control}
                name="nomor_kk_sebelumnya"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Nomor KK Sebelumnya</FormLabel>
                    <FormControl>
                      <Input
                        placeholder="Masukkan nomor"
                        type="text"
                        {...field}
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="hubungan_dalam_kelurga"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Hubungan Dalam Keluarga</FormLabel>
                    <FormControl>
                      <Select value={field.value} onChange={field.onChange}>
                        {["Anak", "Ayah", "Ibu", "Saudara"].map((item) => (
                          <SelectItem value={item} key={item}>
                            {item}
                          </SelectItem>
                        ))}
                      </Select>
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="jenis_kelamin"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Jenis Kelamin</FormLabel>
                    <FormControl>
                      <Select value={field.value} onChange={field.onChange}>
                        {["Laki-laki", "Perempuan"].map((item) => (
                          <SelectItem value={item} key={item}>
                            {item}
                          </SelectItem>
                        ))}
                      </Select>
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
            </section>
            <section className={styles["member__form__input__two"]}>
              <FormField
                control={form.control}
                name="agama"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Agama</FormLabel>
                    <FormControl>
                      <Select value={field.value} onChange={field.onChange}>
                        {[
                          "Islam",
                          "Katholik",
                          "Kristen",
                          "Hindu",
                          "Budha",
                          "Khonghucu",
                          "Kepercayaan Terhadap Tuhan YME / Lainnya",
                        ].map((item) => (
                          <SelectItem value={item} key={item}>
                            {item}
                          </SelectItem>
                        ))}
                      </Select>
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="status_penduduk"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Status Penduduk</FormLabel>
                    <FormControl>
                      <Select value={field.value} onChange={field.onChange}>
                        {["Tetap", "Tidak Tetap"].map((item) => (
                          <SelectItem value={item} key={item}>
                            {item}
                          </SelectItem>
                        ))}
                      </Select>
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
            </section>
          </CardContent>
        </Card>

        <Card>
          <CardHeader>
            <CardTitle>Data Kelahiran</CardTitle>
          </CardHeader>
          <CardContent>
            <section className={styles["member__form__input__two"]}>
              <FormField
                control={form.control}
                name="nomor_akta_kelahiran"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Nomor akta kelahiran</FormLabel>
                    <FormControl>
                      <Input
                        placeholder="Masukkan nomor"
                        type="text"
                        {...field}
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="kota_kelahiran"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Kota kelahiran</FormLabel>
                    <FormControl>
                      <Input
                        type="text"
                        {...field}
                        placeholder="Masukkan nama kota"
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
            </section>
            <section className={styles["member__form__input__three"]}>
              <FormField
                control={form.control}
                name="tanggal_lahir"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Tanggal lahir</FormLabel>
                    <FormControl>
                      <Input type="date" {...field} />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="waktu_kelahiran"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Waktu kelahiran</FormLabel>
                    <FormControl>
                      <Input type="time" {...field} />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="tempat_dilahirkan"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Tempat dilahirkan</FormLabel>
                    <FormControl>
                      <Select value={field.value} onChange={field.onChange}>
                        {[
                          "RS/RB",
                          "Puskesmas",
                          "Polindes",
                          "Rumah",
                          "Lainnya",
                        ].map((item) => (
                          <SelectItem value={item} key={item}>
                            {item}
                          </SelectItem>
                        ))}
                      </Select>
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
            </section>
            <section className={styles["member__form__input__three"]}>
              <FormField
                control={form.control}
                name="jenis_kelahiran"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Jenis kelahiran</FormLabel>
                    <FormControl>
                      <Select value={field.value} onChange={field.onChange}>
                        {["Kembaran 2", "Kembaran 3", "Kembaran 4"].map(
                          (item) => (
                            <SelectItem value={item} key={item}>
                              {item}
                            </SelectItem>
                          ),
                        )}
                      </Select>
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="anak_ke"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Anak ke</FormLabel>
                    <FormControl>
                      <Input placeholder="Anak ke-" type="number" {...field} />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="tenaga_kesehatan"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Tenaga kesehatan</FormLabel>
                    <FormControl>
                      <Select value={field.value} onChange={field.onChange}>
                        {["Dokter", "Bidan Perawat", "Dukun", "Lainnya"].map(
                          (item) => (
                            <SelectItem value={item} key={item}>
                              {item}
                            </SelectItem>
                          ),
                        )}
                      </Select>
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
            </section>
            <section className={styles["member__form__input__two"]}>
              <FormField
                control={form.control}
                name="berat_lahir"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Berat lahir (gram)</FormLabel>
                    <FormControl>
                      <Input
                        type="number"
                        {...field}
                        placeholder="Berat lahir"
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="panjang_lahir"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Panjang lahir (cm)</FormLabel>
                    <FormControl>
                      <Input
                        type="number"
                        placeholder="Panjang lahir"
                        {...field}
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
            </section>
          </CardContent>
        </Card>
        <Card>
          <CardHeader>
            <CardTitle>Pendidikan dan Pekerjaan</CardTitle>
          </CardHeader>
          <CardContent>
            <section className={styles["member__form__input__three"]}>
              <FormField
                control={form.control}
                name="pendidikan_dalam_kk"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Pendidikan dalam KK</FormLabel>
                    <FormControl>
                      <Select value={field.value} onChange={field.onChange}>
                        {[
                          "Tidak / Belum Sekolah",
                          "Belum Tamat SD / Sederajat",
                          "Tamat SD / Sederajat",
                          "SLTP / Sederajat",
                          "SLTA / Sederajat",
                          "Diploma I / II",
                          "AKADEMI / DIPLOMA III/S. MUDA",
                          "DIPLOMA IV/STRATA I",
                          "STRATA II",
                          "STRATA III",
                        ].map((item) => (
                          <SelectItem value={item} key={item}>
                            {item}
                          </SelectItem>
                        ))}
                      </Select>
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="pendidikan_sedang_ditempuh"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Pendidikan yang sedang ditempuh</FormLabel>
                    <FormControl>
                      <Select value={field.value} onChange={field.onChange}>
                        {[
                          "Belum Masuk TK/Kelompok Bermain",
                          "Sedang TK/Kelompok Bermain",
                          "Tidak Pernah Sekolah",
                          "Sedang SD/Sederajat",
                          "Tidak Tamat SD/Sederajat",
                          "Sedang SLTP/ Sederajat",
                          "Sedang SLTA/ Sederajat",
                          "Sedang D-1/ Sederajat",
                          "Sedang D-2/ Sederajat",
                          "Sedang D-3/ Sederajat",
                          "Sedang S-1/ Sederajat",
                          "Sedang S-2/ Sederajat",
                          "Sedang SLB A/Sederajat",
                          "Sedang SLB B/Sederajat",
                          "Sedang SLB C/Sederajat",
                          "Tidak Dapat Membaca dan Menulis Huruf latin / Arab",
                          "Tidak Sedang Sekolah",
                        ].map((item) => (
                          <SelectItem value={item} key={item}>
                            {item}
                          </SelectItem>
                        ))}
                      </Select>
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="pekerjaan"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Pekerjaan</FormLabel>
                    <FormControl>
                      <Select value={field.value} onChange={field.onChange}>
                        {[
                          "Belum /Tidak Bekerja",
                          "Mengurus Rumah Tangga",
                          "Pelajar/Mahasiswa",
                          "Pegawai Negeri Sipil (PNS)",
                          "Tentara Nasional (TNI)",
                          "Kepolisian RI (POLRI)",
                          "Perdagangan",
                          "Petani / Pekebun",
                          "Pertenak",
                          "Nelayan / Perikanan ",
                          "Industri",
                          "Konstruksi",
                          "Transportasi",
                          "Karyawan Swasta",
                          "Karyawan BUMN",
                          "Karyawan BUMD",
                          "Karyawan BUMD Honorer",
                          "Buruh Harian Lepas",
                          "Buruh Tani / Perkebunan",
                          "Pembantu Rumah Tangga",
                          "Tukang Cukur",
                          "Tukang Listrik",
                          "Tukang Batu",
                          "Tukang Kayu",
                          "Tukang Sol Sepatu",
                          "Tukang Las / Pandai Besi",
                          "Tukang Jahit",
                          "Tukang Gigi",
                          "Penata Rias",
                          "Penata Busana",
                          "Penata Rambut",
                          "Mekanik",
                          "Seniman",
                          "Tabib",
                          "Pengrajin",
                          "Perancang Busana",
                          "Penerjemah",
                          "Imam Masjid",
                          "Pendeta Pastor",
                          "Wartawan",
                          "Ustadzah / Mubaligh",
                          "Juru Masak",
                          "Promotor Acara",
                          "Anggota DPR-RI",
                          "Anggota DPD",
                          "Anggota BPK",
                          "Presiden",
                          "Wakil Presiden",
                          "Anggota Mahkamah Konstitusi",
                          "Anggota Kabinet Kementerian",
                          "Duta Besar",
                          "Gubernur",
                          "Wakil Gubernur",
                          "Bupati",
                          "Wakil Bupati",
                          "Walikota",
                          "Wakil Walikota",
                          "Anggota DPRD Provinsi",
                          "Anggota DPD Kabupaten / Kota",
                          "Dosen",
                          "Guru",
                          "Pilot",
                          "Pengacara",
                          "Notaris",
                          "Arsitek",
                          "Akuntan",
                          "Konsultan",
                          "Dokter",
                          "Bidan",
                          "Perawat",
                          "Psikiater / Psikolog",
                          "Penyiar Televisi",
                          "Pelaut",
                          "Peneliti",
                          "Sopir",
                          "Pialang",
                          "Paranormal",
                          "Pedagang",
                          "Perangkat Desa",
                          "Kepala Desa",
                          "Biarawati",
                          "Wiraswasta",
                        ]
                          .sort()
                          .map((item) => (
                            <SelectItem value={item} key={item}>
                              {item}
                            </SelectItem>
                          ))}
                      </Select>
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
            </section>
          </CardContent>
        </Card>

        <Card>
          <CardHeader>
            <CardTitle>Data Kewarganegaraan</CardTitle>
          </CardHeader>
          <CardContent>
            <section className={styles["member__form__input__four"]}>
              <FormField
                control={form.control}
                name="suku_etnis"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Suku/Etnis</FormLabel>
                    <FormControl>
                      <Select value={field.value} onChange={field.onChange}>
                        {["Jawa", "Sunda", "Betawi", "Asmat"].map((item) => (
                          <SelectItem value={item} key={item}>
                            {item}
                          </SelectItem>
                        ))}
                      </Select>
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="status_warga_negara"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Status warga negara</FormLabel>
                    <FormControl>
                      <Select value={field.value} onChange={field.onChange}>
                        {["WNI", "WNA", "Dua Kewarganegaraan"].map((item) => (
                          <SelectItem value={item} key={item}>
                            {item}
                          </SelectItem>
                        ))}
                      </Select>
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="nomor_passpor"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Nomor Paspor</FormLabel>
                    <FormControl>
                      <Input
                        placeholder="Masukkan nomor"
                        type="text"
                        {...field}
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="tanggal_berakhir_paspor"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Tanggal berakhir paspor</FormLabel>
                    <FormControl>
                      <Input type="date" {...field} />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
            </section>
          </CardContent>
        </Card>

        <Card>
          <CardHeader>
            <CardTitle>Data Orang Tua</CardTitle>
          </CardHeader>
          <CardContent>
            <section className={styles["member__form__input__two"]}>
              <FormField
                control={form.control}
                name="nama_ayah"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Nama Ayah</FormLabel>
                    <FormControl>
                      <Input
                        placeholder="Masukkan nama ayah"
                        type="text"
                        {...field}
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="nik_ayah"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>NIK Ayah</FormLabel>
                    <FormControl>
                      <Input
                        placeholder="Masukkna nomor nik ayah"
                        type="text"
                        {...field}
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
            </section>
            <section className={styles["member__form__input__two"]}>
              <FormField
                control={form.control}
                name="nama_ibu"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Nama Ibu</FormLabel>
                    <FormControl>
                      <Input
                        placeholder="Masukkan nama ibu"
                        type="text"
                        {...field}
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="nik_ibu"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>NIK Ibu</FormLabel>
                    <FormControl>
                      <Input
                        placeholder="Masukkan nomor nik ibu"
                        type="text"
                        {...field}
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
            </section>
          </CardContent>
        </Card>

        <Card>
          <CardHeader>
            <CardTitle>Alamat</CardTitle>
          </CardHeader>
          <CardContent>
            <section className={styles["member__form__input__three"]}>
              <FormField
                control={form.control}
                name="dusun"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Alamat</FormLabel>
                    <FormControl>
                      <Select value={field.value} onChange={field.onChange}>
                        {["Menjangan", "Mangsit", "Welahan"].map((item) => (
                          <SelectItem value={item} key={item}>
                            {item}
                          </SelectItem>
                        ))}
                      </Select>
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="rt"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>RT</FormLabel>
                    <FormControl>
                      <Input placeholder="Masukkan RT" type="text" {...field} />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="rw"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>RW</FormLabel>
                    <FormControl>
                      <Input type="text" placeholder="Masukkan RW" {...field} />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
            </section>
            <section className={styles["member__form__input__two"]}>
              <FormField
                control={form.control}
                name="nomor_telepon"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Nomor Telepon</FormLabel>
                    <FormControl>
                      <Input
                        type="text"
                        placeholder="Masukkan nomor telepon"
                        {...field}
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="email"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Email</FormLabel>
                    <FormControl>
                      <Input
                        type="email"
                        placeholder="Masukkan email"
                        {...field}
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
            </section>
          </CardContent>
        </Card>

        <Card>
          <CardHeader>
            <CardTitle>Status Pernikahan</CardTitle>
          </CardHeader>
          <CardContent>
            <section className={styles["member__form__input__three"]}>
              <FormField
                control={form.control}
                name="status_nikah"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Status nikah</FormLabel>
                    <FormControl>
                      <Select value={field.value} onChange={field.onChange}>
                        {[
                          "Belum Kawin",
                          "Kawin",
                          "Cerai Hidup",
                          "Cerai Mati",
                        ].map((item) => (
                          <SelectItem value={item} key={item}>
                            {item}
                          </SelectItem>
                        ))}
                      </Select>
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="no_akta_nikah"
                render={({ field }) => (
                  <FormItem>
                    {!isMarried ? (
                      <FormLabel aria-disabled>
                        No Akta Nikah (Buku nikah)
                      </FormLabel>
                    ) : (
                      <FormLabel>No Akta Nikah (Buku nikah)</FormLabel>
                    )}
                    <FormControl>
                      <Input
                        disabled={!isMarried}
                        placeholder="Masukkan no akta nikah"
                        type="text"
                        {...field}
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="tanggal_pernikahan"
                render={({ field }) => (
                  <FormItem>
                    {!isMarried ? (
                      <FormLabel aria-disabled>Tanggal pernikahan</FormLabel>
                    ) : (
                      <FormLabel>Tanggal pernikahan</FormLabel>
                    )}
                    <FormControl>
                      <Input type="date" disabled={!isMarried} {...field} />
                    </FormControl>
                    <FormDescription>
                      Diisi jika status sudah menikah
                    </FormDescription>
                    <FormMessage />
                  </FormItem>
                )}
              />
            </section>
            <section className={styles["member__form__input__two"]}>
              <FormField
                control={form.control}
                name="akta_perceraian"
                render={({ field }) => (
                  <FormItem>
                    {!isDivorced ? (
                      <FormLabel aria-disabled>Akta perceraian</FormLabel>
                    ) : (
                      <FormLabel>Akta perceraian</FormLabel>
                    )}
                    <FormControl>
                      <Select
                        value={field.value}
                        disabled={!isDivorced}
                        onChange={field.onChange}
                      >
                        {["Belum cerai", "Cerai"].map((item) => (
                          <SelectItem value={item} key={item}>
                            {item}
                          </SelectItem>
                        ))}
                      </Select>
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="tanggal_perceraian"
                render={({ field }) => (
                  <FormItem>
                    {!isDivorced ? (
                      <FormLabel aria-disabled>Tanggal perceraian</FormLabel>
                    ) : (
                      <FormLabel>Tanggal perceraian</FormLabel>
                    )}
                    <FormControl>
                      <Input
                        type="date"
                        {...field}
                        disabled={!isDivorced}
                        placeholder="Pilih tanggal"
                      />
                    </FormControl>
                    <FormDescription>Diisi jika status cerai</FormDescription>
                    <FormMessage />
                  </FormItem>
                )}
              />
            </section>
          </CardContent>
        </Card>

        <Card>
          <CardHeader>
            <CardTitle>Data Kesehatan</CardTitle>
          </CardHeader>
          <CardContent>
            <section className={styles["member__form__input__three"]}>
              <FormField
                control={form.control}
                name="gol_darah"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Golongan darah</FormLabel>
                    <FormControl>
                      <Select value={field.value} onChange={field.onChange}>
                        {[
                          "A",
                          "B",
                          "AB",
                          "O",
                          "A+",
                          "A-",
                          "B+",
                          "B-",
                          "AB+",
                          "AB-",
                          "O+",
                          "O-",
                        ].map((item) => (
                          <SelectItem value={item} key={item}>
                            {item}
                          </SelectItem>
                        ))}
                      </Select>
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="cacat"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Cacat</FormLabel>
                    <FormControl>
                      <Select value={field.value} onChange={field.onChange}>
                        {[
                          "Cacat Fisik",
                          "Cacat Netra/Buta",
                          "Cacat Rungu",
                          "Cacat Mental/Jiwa",
                          "Cacat Fisik dan Mental",
                          "Cacat Lainnya",
                          "Tidak Cacat",
                        ].map((item) => (
                          <SelectItem value={item} key={item}>
                            {item}
                          </SelectItem>
                        ))}
                      </Select>
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="sakit_menahun"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Sakit menahun</FormLabel>
                    <FormControl>
                      <Select value={field.value} onChange={field.onChange}>
                        {[
                          "Jantung",
                          "Liver",
                          "Paru Paru",
                          "Kanker",
                          "Stroke",
                          "Diabetes melitus",
                          "Ginjal",
                          "Malaria",
                          "Lepra/Kusta",
                          "HIV/AIDS",
                          "Gila/Stres",
                          "TBC",
                          "Asma",
                          "Tidak Ada/Tidak Sakit",
                        ].map((item) => (
                          <SelectItem value={item} key={item}>
                            {item}
                          </SelectItem>
                        ))}
                      </Select>
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
            </section>
            <section className={styles["member__form__input__three"]}>
              <FormField
                control={form.control}
                name="akseptor_kb"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Akseptor KB</FormLabel>
                    <FormControl>
                      <Select value={field.value} onChange={field.onChange}>
                        {[
                          "Pil",
                          "IUD",
                          "Suntik",
                          "Kondom",
                          "Susuk KB",
                          "Sterilisasi Wanita",
                          "Sterilisasi Pria",
                          "Lainnya",
                          "Tidak Menggunakan",
                        ].map((item) => (
                          <SelectItem value={item} key={item}>
                            {item}
                          </SelectItem>
                        ))}
                      </Select>
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="asuransi"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Pilih asuransi</FormLabel>
                    <FormControl>
                      <Select value={field.value} onChange={field.onChange}>
                        {[
                          "Tidak/Belum Punya",
                          "BPJS Penerima Bantuan Iuran",
                          "BPJS Non Penerima Bantuan Iuran",
                          "Asuransi Lainnya",
                        ].map((item) => (
                          <SelectItem value={item} key={item}>
                            {item}
                          </SelectItem>
                        ))}
                      </Select>
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="nomor_bpjs_ketenagakerjaan"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Nomor BPJS Ketenagakerjaan</FormLabel>
                    <FormControl>
                      <Input
                        type="text"
                        placeholder="Masukkan nomor"
                        {...field}
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
            </section>
          </CardContent>
        </Card>

        <Card>
          <CardHeader>
            <CardTitle>Lainnya</CardTitle>
          </CardHeader>
          <CardContent>
            <section className={styles["member__form__input__two"]}>
              <FormField
                control={form.control}
                name="dapat_membaca_huruf"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Dapat membaca huruf</FormLabel>
                    <FormControl>
                      <Select value={field.value} onChange={field.onChange}>
                        {[
                          "Latin",
                          "Daerah",
                          "Arab",
                          "Arab dan Latin",
                          "Arab dan Daerah",
                          "Arab, Latin, dan Daerah",
                        ].map((item) => (
                          <SelectItem value={item} key={item}>
                            {item}
                          </SelectItem>
                        ))}
                      </Select>
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="catatan"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Catatan</FormLabel>
                    <FormControl>
                      <Textarea rows={5} {...field} />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
            </section>
          </CardContent>
        </Card>

        <section className={styles["member__form__action"]}>
          <Button text="Batal" variant="outline" />
          <Button text="Simpan" variant="solid" type="submit" />
        </section>
      </form>
    </Form>
  );
};

export { MemberForm };
