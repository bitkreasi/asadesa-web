import React from "react";
import styles from "./edit-member.module.scss";
import { Metadata } from "next";
import LayoutDashboard from "@/app/ui/layouts/dashboard/layout-dashboard";
import HeaderSection from "./_components/header";
import EditMember from "./_components/edit-member";

export const metadata: Metadata = {
  title: "Edit Anggota Keluarga",
  description: "Edit anggota keluarga",
};

export default function AddFamilyMemberPage({
  params,
}: {
  params: { familyId: string };
}) {
  return (
    <LayoutDashboard>
      <section>
        <div className={styles["container"]}>
          <div className={`${styles["main__content"]}`}>
            <HeaderSection familyId={params.familyId} />
            <EditMember />
          </div>
        </div>
      </section>
    </LayoutDashboard>
  );
}
