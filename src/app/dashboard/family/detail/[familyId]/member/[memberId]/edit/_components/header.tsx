import React from "react";
import styles from "./header.module.scss";
import Header from "@/app/ui/components/header/header";

interface HeaderSectionProps {
  familyId: string;
}
const HeaderSection = (props: HeaderSectionProps) => {
  return (
    <div className={styles["content__header"]}>
      <Header
        items={[
          {
            id: "dashboard",
            name: "Beranda",
            link: "/dashboard",
          },
          {
            id: "family",
            name: "Data Keluarga",
            link: "/dashboard/family",
          },
          {
            id: "detail-family",
            name: "Detail Anggota Keluarga",
            link: `/dashboard/family/detail/${props.familyId}`,
          },
          {
            id: "edit-family",
            name: "Edit",
            link: `/dashboard/family/detail/${props.familyId}/member/edit`,
          },
        ]}
        isBack={true}
        title="Edit Anggota Keluarga"
        subTitle=""
      />
    </div>
  );
};

export default HeaderSection;
