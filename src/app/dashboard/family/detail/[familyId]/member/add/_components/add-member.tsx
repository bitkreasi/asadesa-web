"use client";

import { MemberForm } from "../../_components/member-form";

export default function AddMember() {
  return (
    <MemberForm
      headOfFamilyData={{
        nama_lengkap_kepala_keluarga: "Ahlul",
        nomor_kk: "5201140104126994",
        alamat: "DUSUN MANGSIT",
      }}
      onSubmit={(data) => console.log(data)}
    />
  );
}
