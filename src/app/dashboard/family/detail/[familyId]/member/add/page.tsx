import React from "react";
import styles from "./add-member.module.scss";
import { Metadata } from "next";
import LayoutDashboard from "@/app/ui/layouts/dashboard/layout-dashboard";
import HeaderSection from "./_components/header";
import AddMember from "./_components/add-member";

export const metadata: Metadata = {
  title: "Tambah Anggota Keluarga",
  description: "Tambah anggota keluarga",
};

export default function AddFamilyMemberPage({
  params,
}: {
  params: { familyId: string };
}) {
  return (
    <LayoutDashboard>
      <section>
        <div className={styles["container"]}>
          <div className={`${styles["main__content"]}`}>
            <HeaderSection familyId={params.familyId} />
            <AddMember />
          </div>
        </div>
      </section>
    </LayoutDashboard>
  );
}
