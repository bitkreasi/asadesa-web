import React from "react";
import styles from "./detail-family.module.scss";
import { Metadata } from "next";
import LayoutDashboard from "@/app/ui/layouts/dashboard/layout-dashboard";
import HeaderSection from "./_components/header";
import ResidentSection from "./_components/resident";
import FamilyMemberSection from "./_components/family-member";
import { familyMemberData } from "./family-member-data";
import ProfileSection from "@/app/dashboard/components/profile-section";

export const metadata: Metadata = {
  title: "Detail Keluarga",
  description: "Detail data keluarga",
};

export default function DetailFamilyPage({
  params,
}: {
  params: { familyId: string };
}) {
  return (
    <LayoutDashboard>
      <section>
        <div className={styles["container"]}>
          <div className={`${styles["main__content"]}`}>
            <HeaderSection familyId={params.familyId} />
            <ProfileSection
              data={{ imageUrl: "https://source.unsplash.com/random" }}
            />
            <ResidentSection
              data={{
                fullname: "Ahlul",
                kk: "5201140104126994",
                village: "DUSUN MANGSIT",
                rt: "004",
                rw: "008",
                assistanceProgram: "-",
              }}
            />
            <FamilyMemberSection
              familyId={params.familyId}
              data={familyMemberData}
            />
          </div>
        </div>
      </section>
    </LayoutDashboard>
  );
}
