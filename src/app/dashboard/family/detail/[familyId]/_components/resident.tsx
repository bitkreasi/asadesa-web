import React from "react";
import styles from "./resident.module.scss";
import { Text } from "@/app/ui/components/typography/typography";
import {
  Card,
  CardContent,
  CardHeader,
  CardTitle,
} from "@/app/ui/components/card/card";

export interface ResidentSectionProps
  extends React.ComponentPropsWithoutRef<"section"> {
  data: {
    fullname: string;
    kk: string;
    village: string;
    rt: string;
    rw: string;
    assistanceProgram: string;
  };
}

const ResidentSection = (props: ResidentSectionProps) => {
  return (
    <Card>
      <CardHeader>
        <CardTitle>Jenis Penduduk</CardTitle>
      </CardHeader>
      <CardContent>
        <div className={styles["resident__info"]}>
          <div className={styles["two__column"]}>
            <div className={styles["resident__info--item"]}>
              <Text variant="text" size="sm" fontStyle="medium" muted>
                Nama Lengkap
              </Text>
              <Text variant="text" size="sm" fontStyle="regular">
                {props.data.fullname}
              </Text>
            </div>
            <div className={styles["resident__info--item"]}>
              <Text variant="text" size="sm" fontStyle="medium" muted>
                Nomor KK
              </Text>
              <Text variant="text" size="sm" fontStyle="regular">
                {props.data.kk}
              </Text>
            </div>
          </div>
          <div className={styles["four__column"]}>
            <div className={styles["resident__info--item"]}>
              <Text variant="text" size="sm" fontStyle="medium" muted>
                Alamat
              </Text>
              <Text variant="text" size="sm" fontStyle="regular">
                {props.data.village}
              </Text>
            </div>
            <div className={styles["resident__info--item"]}>
              <Text variant="text" size="sm" fontStyle="medium" muted>
                RT
              </Text>
              <Text variant="text" size="sm" fontStyle="regular">
                {props.data.rt}
              </Text>
            </div>
            <div className={styles["resident__info--item"]}>
              <Text variant="text" size="sm" fontStyle="medium" muted>
                RW
              </Text>
              <Text variant="text" size="sm" fontStyle="regular">
                {props.data.rw}
              </Text>
            </div>
            <div className={styles["resident__info--item"]}>
              <Text variant="text" size="sm" fontStyle="medium" muted>
                Program Bantuan
              </Text>
              <Text variant="text" size="sm" fontStyle="regular">
                {props.data.assistanceProgram}
              </Text>
            </div>
          </div>
        </div>
      </CardContent>
    </Card>
  );
};

export default ResidentSection;
