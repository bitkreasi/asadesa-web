"use client";

import React from "react";
import styles from "./family-member.module.scss";
import { FamilyMember } from "../family-member-data";
import { Text } from "@/app/ui/components/typography/typography";
import Link from "next/link";
import Avatar from "@/app/ui/components/avatar/avatar";
import IconTrash from "@/app/ui/icons/icon-trash";
import IconScissor from "@/app/ui/icons/icon-scissor";
import IconEdit2 from "@/app/ui/icons/icon-edit-2";
import {
  Dialog,
  DialogClose,
  DialogContent,
  DialogFooter,
  DialogTrigger,
} from "@/app/ui/components/dialog/dialog";
import IconCloseCircle from "@/app/ui/icons/icon-close-circle";
import Button from "@/app/ui/components/button/button";
import { toast } from "sonner";

interface FamilyMemberSectionProps
  extends React.ComponentPropsWithoutRef<"section"> {
  familyId: string;
  data: FamilyMember[];
}

const DataTable = ({
  data,
  familyId,
}: {
  data: FamilyMember[];
  familyId: string;
}) => {
  return (
    <div className={styles["content__member"]}>
      <div className={styles["member__table--container"]}>
        <table className={styles["member__table"]}>
          <thead>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>NIK</th>
              <th>Dusun</th>
              <th>Tanggal Lahir</th>
              <th>Jenis Kelamin</th>
              <th>Hubungan</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            {data.map((member, index) => (
              <tr key={member.id}>
                <td>{index + 1}</td>
                <td>
                  {member.imageProfile ? (
                    <div className={styles["member__table--user__profile"]}>
                      <Avatar
                        variant="photo"
                        source={member.imageProfile}
                        size="sm"
                      />
                      <span className={styles["member__table--user__name"]}>
                        {member.fullname}
                      </span>
                    </div>
                  ) : (
                    <div className={styles["member__table--user__profile"]}>
                      <Avatar variant="icon" size="sm" />
                      <span>{member.fullname}</span>
                    </div>
                  )}
                </td>
                <td>{member.nik}</td>
                <td>{member.village}</td>
                <td>{member.birtdate}</td>
                <td>{member.gender}</td>
                <td>{member.relation}</td>
                <td>
                  <div style={{ display: "flex", gap: 8 }}>
                    <Link
                      href={`/dashboard/family/detail/${familyId}/member/${member.id}/edit`}
                    >
                      <IconEdit2 color="#F49C56" backgroundColor="#F49C56" />
                    </Link>
                    <Dialog>
                      <DialogTrigger>
                        <IconTrash color="#F49C56" backgroundColor="#F49C56" />
                      </DialogTrigger>
                      <DialogContent>
                        <div className={styles["action__dialog__content"]}>
                          <Text variant="text" size="lg" fontStyle="semibold">
                            Anda yakin akan menghapus anggota keluarga ini?
                          </Text>
                          <Text variant="text" size="sm" fontStyle="regular">
                            Apakah Anda yakin ingin menghapus anggota keluarga
                            ini?
                          </Text>
                        </div>
                        <DialogFooter
                          className={styles["action__dialog__footer"]}
                        >
                          <DialogClose style={{ width: "100%" }}>
                            <Button
                              text="Batal"
                              variant="outline"
                              width="max"
                            />
                          </DialogClose>
                          <Button
                            text="Hapus"
                            variant="solid"
                            color="error"
                            width="max"
                          />
                        </DialogFooter>
                      </DialogContent>
                    </Dialog>
                    <Dialog>
                      <DialogTrigger>
                        <IconScissor
                          color="#F49C56"
                          backgroundColor="#F49C56"
                        />
                      </DialogTrigger>
                      <DialogContent>
                        <div className={styles["action__dialog__content"]}>
                          <div className={styles["action__dialog__icon"]}>
                            <IconCloseCircle
                              width={32}
                              height={32}
                              color="#F42E2E"
                              backgroundColor="#F42E2E"
                            />
                          </div>
                          <Text variant="text" size="lg" fontStyle="semibold">
                            Konfirmasi
                          </Text>
                          <Text variant="text" size="sm" fontStyle="regular">
                            Apakah Anda yakin ingin memecah data anggota
                            keluarga ini?
                          </Text>
                        </div>
                        <DialogFooter
                          className={styles["action__dialog__footer"]}
                        >
                          <DialogClose style={{ width: "100%" }}>
                            <Button
                              text="Batal"
                              variant="outline"
                              width="max"
                            />
                          </DialogClose>
                          <Button
                            text="Simpan"
                            onHandleClick={() =>
                              toast.success(
                                "Anda berhasil memecah data anggtoa keluarga",
                              )
                            }
                            variant="solid"
                            width="max"
                          />
                        </DialogFooter>
                      </DialogContent>
                    </Dialog>
                  </div>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
};

const FamilyMemberSection = (props: FamilyMemberSectionProps) => {
  return (
    <section className={styles["member__container"]}>
      <Text variant="display" size="xs" fontStyle="medium">
        Daftar Anggota Keluarga
      </Text>
      <DataTable data={props.data} familyId={props.familyId} />
    </section>
  );
};

export default FamilyMemberSection;
