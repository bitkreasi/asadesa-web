"use client";

import React from "react";
import styles from "./header.module.scss";
import Header from "@/app/ui/components/header/header";
import Link from "next/link";
import Button from "@/app/ui/components/button/button";
import IconAddCircle from "@/app/ui/icons/icon-add-circle";
import IconMoreCircle from "@/app/ui/icons/icon-more-circle";
import IconTrash from "@/app/ui/icons/icon-trash";
import IconBook from "@/app/ui/icons/icon-book";
import IconEdit2 from "@/app/ui/icons/icon-edit-2";
import {
  Dialog,
  DialogClose,
  DialogContent,
  DialogFooter,
  DialogTrigger,
} from "@/app/ui/components/dialog/dialog";
import { Text } from "@/app/ui/components/typography/typography";
import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuTrigger,
} from "@/app/ui/components/dropdown-menu/dropdown-menu";
import { toast } from "@/app/ui/components/toast/toast";

const HeaderSection = ({ familyId }: { familyId: string }) => {
  return (
    <div className={styles["content__header"]}>
      <Header
        items={[
          {
            id: "dashboard",
            name: "Beranda",
            link: "/dashboard",
          },
          {
            id: "family",
            name: "Data Keluarga",
            link: "/dashboard/family",
          },
          {
            id: "detail-family",
            name: "Detail Keluarga",
            link: `/dashboard/family/detail/${familyId}`,
          },
        ]}
        isBack={true}
        title="Detail Keluarga"
        subTitle=""
      />
      <div className={styles["content__header--button"]}>
        <Link href={`/dashboard/family/detail/${familyId}/member/add`}>
          <Button
            variant="outline"
            text="Tambah Anggota Keluarga"
            color="primary"
            size="md"
            prefix={<IconAddCircle color="#4163E7" backgroundColor="#4163E7" />}
          />
        </Link>
        <DropdownMenu>
          <DropdownMenuTrigger className={styles["header__dropdown__trigger"]}>
            <IconMoreCircle color="#4163E7" backgroundColor="#4163E7" />
          </DropdownMenuTrigger>
          <DropdownMenuContent className={styles["header__dropdown__content"]}>
            <Link
              href="/dashboard/family/edit"
              className={styles["header__dropdown__item"]}
            >
              <IconEdit2
                color="#243576"
                backgroundColor="#272E38"
                width={20}
                height={20}
              />
              <Text variant="text" size="sm" fontStyle="medium">
                Edit
              </Text>
            </Link>
            <Link
              href={`/dashboard/family/detail/${familyId}/kk`}
              className={styles["header__dropdown__item"]}
            >
              <IconBook
                color="#243576"
                backgroundColor="#272E38"
                width={20}
                height={20}
              />
              <Text variant="text" size="sm" fontStyle="medium">
                Kartu Keluarga
              </Text>
            </Link>
            <Dialog>
              <DialogTrigger>
                <div className={styles["header__dropdown__item"]}>
                  <IconTrash
                    color="#243576"
                    backgroundColor="#243576"
                    width={20}
                    height={20}
                  />
                  <Text variant="text" size="sm" fontStyle="medium">
                    Hapus
                  </Text>
                </div>
              </DialogTrigger>
              <DialogContent>
                <div className={styles["delete__dialog__content"]}>
                  <Text variant="text" size="lg" fontStyle="semibold">
                    Anda yakin akan menghapus Keluarga ini?
                  </Text>
                  <Text variant="text" size="md" muted fontStyle="regular">
                    Anda yakin akan menghapus Keluarga ini?
                  </Text>
                </div>
                <DialogFooter className={styles["delete__dialog__footer"]}>
                  <DialogClose className={styles["delete__dialog__close"]}>
                    <Button text="Batal" variant="outline" width="max" />
                  </DialogClose>
                  <Button
                    onHandleClick={() =>
                      toast.success("Anda berhasil menghapus data keluarga ini")
                    }
                    text="Hapus"
                    variant="solid"
                    color="error"
                    width="max"
                  />
                </DialogFooter>
              </DialogContent>
            </Dialog>
          </DropdownMenuContent>
        </DropdownMenu>
      </div>
    </div>
  );
};
export default HeaderSection;
