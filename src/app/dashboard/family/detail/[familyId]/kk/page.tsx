import React from "react";
import styles from "./kk.module.scss";
import LayoutDashboard from "@/app/ui/layouts/dashboard/layout-dashboard";
import HeaderSection from "./_components/header";
import sampleKk from "./sample-kk.png";
import Image from "next/image";
import { Metadata } from "next";

export const metadata: Metadata = {
  title: "Kartu Keluarga",
  description: "Kartu Keluarga",
};

export default function FamilyCardPage({
  params,
}: {
  params: { familyId: string };
}) {
  return (
    <LayoutDashboard>
      <section>
        <div className={styles["container"]}>
          <div className={`${styles["main__content"]}`}>
            <HeaderSection familyId={params.familyId} />
            <Image
              src={sampleKk}
              style={{ width: "100%", height: "auto" }}
              alt="Salinan Kartu Keluarga"
            />
          </div>
        </div>
      </section>
    </LayoutDashboard>
  );
}
