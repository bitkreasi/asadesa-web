import React from "react";
import styles from "./header.module.scss";
import Header from "@/app/ui/components/header/header";
import Button from "@/app/ui/components/button/button";
import IconPrinter from "@/app/ui/icons/icon-printer";
import IconDocument from "@/app/ui/icons/icon-document";

export default function HeaderSection({ familyId }: { familyId: string }) {
  return (
    <div className={styles["content__header"]}>
      <Header
        items={[
          {
            id: "dashboard",
            name: "Beranda",
            link: "/dashboard",
          },
          {
            id: "family",
            name: "Data Keluarga",
            link: "/dashboard/family",
          },
          {
            id: "family-detil",
            name: "Daftar Anggota Keluarga",
            link: `/dashboard/family/detail/${familyId}`,
          },
          {
            id: "family-kk",
            name: "Kartu Keluarga",
            link: `/dashboard/family/detail/${familyId}/kk`,
          },
        ]}
        isBack={true}
        title="Salinan Kartu Keluarga"
        subTitle=""
      />
      <div className={styles["content__header--button"]}>
        <Button
          variant="outline"
          text="Cetak"
          color="primary"
          size="md"
          prefix={<IconPrinter color="#4163E7" backgroundColor="#4163E7" />}
        />
        <Button
          variant="outline"
          text="Unduh"
          color="primary"
          size="md"
          prefix={<IconDocument color="#4163E7" backgroundColor="#4163E7" />}
        />
      </div>
    </div>
  );
}
