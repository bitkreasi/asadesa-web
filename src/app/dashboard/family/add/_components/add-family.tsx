"use client";

import Header from "@/app/ui/components/header/header";
import React from "react";
import styles from "./add-family.module.scss";
import AddFamilyForm, {
  FamilySchema,
} from "../../_components/form/family-form";
import { ResidentFormSelectOptions } from "@/app/dashboard/resident/add/page";
import { ResidentNikResponse } from "../page";
import { useToastMutate } from "@/hooks/use-toast-mutate";
import { AddResidentRequestBody, addFamilyAction } from "../../actions";

export default function AddFamily({
  selectOptions,
  niks,
}: {
  selectOptions: ResidentFormSelectOptions;
  niks: ResidentNikResponse[];
}) {
  const mutateAddFamilyToast = useToastMutate({
    success: "Anda berhasil menambahkan keluaga baru",
  });

  function submitAddFamily(data: FamilySchema) {
    const residentData: AddResidentRequestBody = {
      fullname: data.fullname!,
      nik: data.nik!,
      family_role: data.family_role!,
      gender: data.gender!,
      religion: data.religion!,
      resident_status: data.resident_status!,
    };

    mutateAddFamilyToast.mutate(
      addFamilyAction({
        no_kk: data.no_kk,
        head_family_id: data.head_family_nik,
        type: data.family_add_type,
        resident: residentData,
      }),
    );
  }
  return (
    <section>
      <div className={styles["header"]}>
        <Header
          items={[
            {
              id: "dashboard",
              name: "Beranda",
              link: "/dashboard",
            },
            {
              id: "family",
              name: "Data Keluarga",
              link: "/dashboard/resident",
            },
            {
              id: "add-family",
              name: "Tambah Keluarga",
              link: "/dashboard/family/add",
            },
          ]}
          isBack={true}
          title="Tambah Keluarga"
          subTitle=""
        />
      </div>
      <AddFamilyForm
        variant="add"
        selectOptions={selectOptions}
        niks={niks}
        onSubmit={submitAddFamily}
      />
    </section>
  );
}
