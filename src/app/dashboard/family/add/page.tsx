import React from "react";
import { Metadata } from "next";
import LayoutDashboard from "@/app/ui/layouts/dashboard/layout-dashboard";
import { commonDescription, sufixTitle } from "@/app/shared";
import { unstable_noStore } from "next/cache";
import { getAllSelectOptions } from "../../resident/add/page";
import AddFamily from "./_components/add-family";
import { publicFetch } from "@/utils/fetch";

export const metadata: Metadata = {
  title: `Tambah Keluarga ${sufixTitle}`,
  description: `${commonDescription}`,
};

export interface ResidentNikResponse {
  id: string;
  fullname: string;
  nik: string;
}

async function getAllResidentNik() {
  const res = await publicFetch<Array<ResidentNikResponse>>("/residents/nik");
  return res?.data;
}

export default async function AddFamilyPage() {
  unstable_noStore();
  const selectOptions = await getAllSelectOptions();
  const residentNiks = await getAllResidentNik();
  return (
    <LayoutDashboard>
      <AddFamily selectOptions={selectOptions} niks={residentNiks ?? []} />
    </LayoutDashboard>
  );
}
