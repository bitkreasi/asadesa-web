import React from "react";
import styles from "./edit-family.module.scss";
import { Metadata } from "next";
import LayoutDashboard from "@/app/ui/layouts/dashboard/layout-dashboard";
import HeaderSection from "./_components/header";
import EditFamilyForm, {
  EditFamilyFormData,
} from "./_components/edit-family-form";

export const metadata: Metadata = {
  title: "Edit Keluarga",
  description: "Edit data keluarga",
};

const initialData: EditFamilyFormData = {
  imageUrl: "https://source.unsplash.com/random",
  kk: "123433535375757",
  village: "Mangsit",
  rt: "004",
  rw: "002",
  kkPrintDate: new Date(),
  socialClass: "Sejahtera",
};

export default function EditFamilyPage() {
  return (
    <LayoutDashboard>
      <section>
        <div className={styles["container"]}>
          <div className={`${styles["main__content"]}`}>
            <HeaderSection />
            <EditFamilyForm initialData={initialData} />
          </div>
        </div>
      </section>
    </LayoutDashboard>
  );
}
