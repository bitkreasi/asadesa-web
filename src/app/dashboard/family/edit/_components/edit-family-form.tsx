"use client";

import React from "react";
import styles from "./edit-family-form.module.scss";
import Image from "next/image";
import { Text } from "@/app/ui/components/typography/typography";
import Button from "@/app/ui/components/button/button";
import IconImport from "@/app/ui/icons/icon-import";
import { Input as InputV2 } from "@/app/ui/components/input/v2/input";
import { Card, CardContent } from "@/app/ui/components/card/card";
import * as yup from "yup";
import { SubmitHandler, useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/app/ui/components/form/form";
import { Select, SelectItem } from "@/app/ui/components/select/select";

export interface EditFamilyFormData {
  imageUrl: string;
  kk: string;
  village: string;
  rt: string;
  rw: string;
  kkPrintDate: Date;
  socialClass: string;
}

export interface EditFamilyFormProps
  extends React.ComponentPropsWithoutRef<"form"> {
  initialData: EditFamilyFormData;
}

const schema = yup.object({
  nomor_kk: yup
    .string()
    .required("Nomor KK wajib diisi")
    .min(16, "Nomor KK wajib terdiri dari 16 angka")
    .max(16, "Nomor KK wajib terdiri dari 16 angka"),
  dusun: yup.string().required("Dusun wajib diisi"),
  rt: yup
    .string()
    .required("RT wajib diisi")
    .matches(/^[0-9]+$/, "RT harus terdiri dari angka"),
  rw: yup
    .string()
    .required("RW wajib diisi")
    .matches(/^[0-9]+$/, "RW harus terdiri dari angka"),
  tanggal_cetak_kk: yup
    .string()
    .required("Tanggal cetak kartu keluarga wajib diisi"),
  kelas_sosial: yup.string().required("Kelas sosial wajib diisi"),
});

type FormSchema = yup.InferType<typeof schema>;

const EditFamilyForm = (props: EditFamilyFormProps) => {
  const form = useForm({
    defaultValues: {
      nomor_kk: "1233333333333333",
      dusun: "Dusun 2",
      rt: "004",
      rw: "002",
      tanggal_cetak_kk: "2017-06-01",
      kelas_sosial: "Sosial 2",
    },
    resolver: yupResolver(schema),
  });
  const onSubmit: SubmitHandler<FormSchema> = (data) => console.log(data);
  return (
    <Form {...form}>
      <form onSubmit={form.handleSubmit(onSubmit)}>
        <Card>
          <CardContent className={styles["form__card__content"]}>
            <Card>
              <CardContent className={styles["profile__image__card"]}>
                <div className={styles["image__display"]}>
                  <Image
                    className={styles["img"]}
                    loading="eager"
                    src={props.initialData.imageUrl}
                    width={64}
                    height={64}
                    alt="Family profile image"
                  />
                  <div className={styles["text"]}>
                    <Text variant="text" size="md" fontStyle="medium">
                      Foto Profil
                    </Text>
                    <Text variant="text" size="sm" fontStyle="regular" muted>
                      PNG, JPEG dibawah 2MB
                    </Text>
                  </div>
                </div>

                <Button
                  text="Unggah Foto"
                  variant="solid"
                  prefix={
                    <IconImport color="#ffffff" backgroundColor="#ffffff" />
                  }
                />
              </CardContent>
            </Card>
            <div className={styles["input__wrapper"]}>
              <FormField
                control={form.control}
                name="nomor_kk"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Nomor KK</FormLabel>
                    <FormControl>
                      <InputV2 placeholder="Nomor KK" {...field} />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
            </div>
            <div className={styles["input__wrapper"]}>
              <div className={styles["three__input"]}>
                <FormField
                  control={form.control}
                  name="dusun"
                  render={({ field }) => (
                    <FormItem>
                      <FormLabel>Dusun</FormLabel>
                      <FormControl>
                        <Select value={field.value} onChange={field.onChange}>
                          <SelectItem value={"Dusun 1"}>Dusun 1</SelectItem>
                          <SelectItem value={"Dusun 2"}>Dusun 2</SelectItem>
                        </Select>
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
                <FormField
                  control={form.control}
                  name="rt"
                  render={({ field }) => (
                    <FormItem>
                      <FormLabel>RT</FormLabel>
                      <FormControl>
                        <InputV2 placeholder="RT" {...field} />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
                <FormField
                  control={form.control}
                  name="rw"
                  render={({ field }) => (
                    <FormItem>
                      <FormLabel>RW</FormLabel>
                      <FormControl>
                        <InputV2 placeholder="RW" {...field} />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
            </div>
            <div className={styles["input__wrapper"]}>
              <div className={styles["two__input"]}>
                <FormField
                  control={form.control}
                  name="tanggal_cetak_kk"
                  render={({ field }) => (
                    <FormItem>
                      <FormLabel>Tanggal Cetak Kartu Keluarga</FormLabel>
                      <FormControl>
                        <InputV2
                          placeholder="Pilih tanggal"
                          type="date"
                          {...field}
                        />
                      </FormControl>
                    </FormItem>
                  )}
                />
                <FormField
                  control={form.control}
                  name="kelas_sosial"
                  render={({ field }) => (
                    <FormItem>
                      <FormLabel>Kelas Sosial</FormLabel>
                      <Select {...field}>
                        <SelectItem value="Sosial 1">Sosial 1</SelectItem>
                        <SelectItem value="Sosial 2">Sosial 2</SelectItem>
                      </Select>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
            </div>
          </CardContent>
        </Card>
        <div className={styles["form__footer"]}>
          <div className={styles["form__action"]}>
            <Button text="Batal" variant="outline" />
            <Button text="Simpan" variant="solid" type="submit" />
          </div>
        </div>
      </form>
    </Form>
  );
};

export default EditFamilyForm;
