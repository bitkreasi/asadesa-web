import React from "react";
import styles from "./header.module.scss";
import Header from "@/app/ui/components/header/header";

const HeaderSection = () => {
  return (
    <div className={styles["content__header"]}>
      <Header
        items={[
          {
            id: "dashboard",
            name: "Beranda",
            link: "/dashboard",
          },
          {
            id: "family",
            name: "Data Keluarga",
            link: "/dashboard/family",
          },
          {
            id: "edit-family",
            name: "Edit Data Keluarga",
            link: "/dashboard/family/edit",
          },
        ]}
        isBack={true}
        title="Edit Data Keluarga"
        subTitle=""
      />
    </div>
  );
};

export default HeaderSection;
