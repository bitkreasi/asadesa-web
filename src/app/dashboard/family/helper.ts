import { FamilySearchParams } from "./page";

export function buildFamilySearchParams(searchParams: FamilySearchParams) {
  let gendersQuery = "";
  let villagesQuery = "";
  if (searchParams.genders) {
    if (typeof searchParams.genders === "string") {
      gendersQuery = `genders=${encodeURIComponent(searchParams.genders)}`;
    } else {
      gendersQuery = searchParams.genders
        .map((item) => `genders=${encodeURIComponent(item)}`)
        .join("&");
    }
  }

  if (searchParams.villages) {
    if (typeof searchParams.villages === "string") {
      villagesQuery = `villages=${encodeURIComponent(searchParams.villages)}`;
    } else {
      villagesQuery = searchParams.villages
        .map((item) => `villages=${encodeURIComponent(item)}`)
        .join("&");
    }
  }
  const finalQuery = `${gendersQuery}&${villagesQuery}`.replace(/^&+|&+$/g, "");
  return finalQuery;
}
