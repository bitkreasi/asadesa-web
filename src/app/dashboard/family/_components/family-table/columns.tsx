"use client";

import { ColumnDef } from "@tanstack/react-table";
import React from "react";
import Image from "next/image";
import Link from "next/link";
import IconPrinter from "@/app/ui/icons/icon-printer";
import { FamilyResponse } from "../../page";
import { IndeterminateCheckbox } from "@/app/ui/components/checkbox/v2/checkbox";

export const columns: ColumnDef<FamilyResponse>[] = [
  {
    id: "select",
    header: ({ table }) => {
      return (
        <IndeterminateCheckbox
          {...{
            checked: table.getIsAllRowsSelected(),
            indeterminate: table.getIsSomeRowsSelected(),
            onChange: table.getToggleAllRowsSelectedHandler(),
          }}
        />
      );
    },
    cell: ({ row }) => {
      return (
        <div>
          <IndeterminateCheckbox
            {...{
              checked: row.getIsSelected(),
              disabled: !row.getCanSelect(),
              indeterminate: row.getIsSomeSelected(),
              onChange: row.getToggleSelectedHandler(),
            }}
          />
        </div>
      );
    },
  },
  {
    header: "No",
    cell: ({ row }) => {
      const idx = row.index;
      return <div>{idx + 1}</div>;
    },
  },
  {
    id: "fullname",
    filterFn: "fuzzy",
    accessorKey: "fullname",
    header: "Kepala Keluarga",
    cell: ({ row }) => {
      return (
        <div style={{ display: "flex", gap: 8, alignItems: "center" }}>
          <Image
            style={{ borderRadius: "9999px" }}
            loading="eager"
            width={32}
            height={32}
            alt={row.original.fullname}
            src={"https://source.unsplash.com/random"}
          />
          <p>{row.original.fullname}</p>
        </div>
      );
    },
  },
  {
    accessorKey: "no_kk",
    header: "Nomor KK",
  },
  {
    accessorKey: "total_member",
    header: "Jumlah Aggota",
  },
  {
    accessorKey: "gender",
    header: "Jenis Kelamin",
  },
  {
    accessorKey: "village",
    header: "Dusun",
  },
  {
    accessorKey: "rt",
    header: "RT",
  },
  {
    accessorKey: "rw",
    header: "RW",
  },
  {
    id: "action",
    cell: () => {
      return (
        <Link
          href="/dashboard/family/print-family"
          style={{ display: "flex", gap: 8 }}
        >
          <IconPrinter color="#F49C56" backgroundColor="#F49C56" />
        </Link>
      );
    },
  },
];
