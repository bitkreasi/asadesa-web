"use client";

import { flexRender, Table as TableProps } from "@tanstack/react-table";
import React from "react";
import styles from "./datatable.module.scss";
import { Input } from "@/app/ui/components/input/v2/input";
import IconSearch from "@/app/ui/icons/icon-search";
import {
  DropdownMenu,
  DropdownMenuClose,
  DropdownMenuContent,
  DropdownMenuTrigger,
} from "@/app/ui/components/dropdown-menu/dropdown-menu";
import { Text } from "@/app/ui/components/typography/typography";
import IconCloseCircle from "@/app/ui/icons/icon-close-circle";
import { Checkbox } from "@/app/ui/components/checkbox/v2/checkbox";
import {
  DataTableProps,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeader,
  TableRow,
} from "@/app/ui/components/table/table";
import IconArrowLeft from "@/app/ui/icons/icon-arrow-left";
import IconArrowRight from "@/app/ui/icons/icon-arrow-right";
import { useRouter } from "next/navigation";
import { ResidentSearchParams } from "@/app/dashboard/resident/page";
import { useForm, SubmitHandler } from "react-hook-form";
import { buildResidentSearchParams } from "@/app/dashboard/resident/helper";
import { Input as InputV2 } from "@/app/ui/components/input/v2/input";
import Button from "@/app/ui/components/button/button";
import Link from "next/link";
import EmptyStates from "@/app/ui/components/empty-state/empty-state";
import IconAddCircle from "@/app/ui/icons/icon-add-circle";
import IconImport from "@/app/ui/icons/icon-import";
import { Select, SelectItem } from "@/app/ui/components/select/select";
import { FamilyFilter } from "../family";
import { FamilySearchParams } from "../../page";
import { buildFamilySearchParams } from "../../helper";

const FilterDropdown = ({
  setGlobalFilter,
  filter,
  defaultValues,
}: {
  setGlobalFilter: (value: string) => void;
  filter: FamilyFilter;
  defaultValues: FamilySearchParams;
}) => {
  const form = useForm<FamilySearchParams>({ defaultValues: defaultValues });
  const router = useRouter();
  const onSubmit: SubmitHandler<ResidentSearchParams> = (data) => {
    const queryParams = buildFamilySearchParams(data);
    router.push("/dashboard/family?" + queryParams);
  };
  return (
    <div className={styles["search__filter__container"]}>
      <div className={styles["search"]}>
        <InputV2
          onChange={(e) => setGlobalFilter(String(e.target.value))}
          type="text"
          placeholder="Cari data keluarga"
          prefixIcon={<IconSearch />}
        />
      </div>

      <DropdownMenu>
        <DropdownMenuTrigger
          withArrow
          className={styles["filter__dropdown__trigger"]}
        >
          <Text variant="text" size="sm" fontStyle="regular">
            Filter
          </Text>
        </DropdownMenuTrigger>

        <DropdownMenuContent
          title="Filter"
          className={styles["filter__dropdown"]}
        >
          <form onSubmit={form.handleSubmit(onSubmit)}>
            <section className={styles["filter__dropdown__head"]}>
              <Text variant="text" size="md" fontStyle="medium">
                Filter
              </Text>
              <DropdownMenuClose>
                <IconCloseCircle width={24} height={24} />
              </DropdownMenuClose>
            </section>
            <section className={styles["filter__dropdown__content"]}>
              <div className={styles["filter__category"]}>
                <Text variant="text" muted size="sm" fontStyle="medium">
                  Jenis Kelamin
                </Text>
                {filter.gender.map((item) => (
                  <div key={item} className={styles["filter__sub"]}>
                    <Checkbox {...form.register("genders")} value={item} />{" "}
                    <Text variant="text" size="sm" fontStyle="regular">
                      {item === "L" ? "Laki-Laki" : "Perempuan"}
                    </Text>
                  </div>
                ))}
              </div>
              <div className={styles["filter__category"]}>
                <Text variant="text" muted size="sm" fontStyle="medium">
                  Dusun
                </Text>
                {filter.residentVillages.map((item) => (
                  <div key={item.name} className={styles["filter__sub"]}>
                    <Checkbox
                      {...form.register("villages")}
                      value={item.name}
                    />{" "}
                    <Text variant="text" size="sm" fontStyle="regular">
                      {item.name}
                    </Text>
                  </div>
                ))}
              </div>
            </section>
            <section className={styles["filter__dropdown__footer"]}>
              <Button
                width="max"
                type="submit"
                text="Terapkan Filter"
                variant="solid"
                color="primary"
              />
              <Button
                onHandleClick={() => router.replace("/dashboard/family")}
                type="reset"
                width="max"
                text="Reset Filter"
                variant="outline"
                color="primary"
              />
            </section>
          </form>
        </DropdownMenuContent>
      </DropdownMenu>
    </div>
  );
};

interface FamilyDataTableProps<TData, TValue>
  extends DataTableProps<TData, TValue> {
  table: TableProps<any>;
  filter: { globalFilter: string; setGlobalFilter: (value: string) => void };
  filterValues: FamilyFilter;
  filterDefaults: FamilySearchParams;
}

export function DataTable<TData, TValue>({
  columns,
  filter,
  table,
  filterValues,
  filterDefaults,
}: FamilyDataTableProps<TData, TValue>) {
  return (
    <div className={styles["datatable__wrapper"]}>
      {/* start -- search and filter */}
      <FilterDropdown
        filter={filterValues}
        setGlobalFilter={filter.setGlobalFilter}
        defaultValues={filterDefaults}
      />
      {/* end -- search and filter */}

      {/* start -- table */}
      <div>
        <Table>
          <TableHeader>
            {table.getHeaderGroups().map((headerGroup) => (
              <TableRow key={headerGroup.id}>
                {headerGroup.headers.map((header) => {
                  return (
                    <TableHead key={header.id}>
                      {header.isPlaceholder
                        ? null
                        : flexRender(
                            header.column.columnDef.header,
                            header.getContext(),
                          )}
                    </TableHead>
                  );
                })}
              </TableRow>
            ))}
          </TableHeader>

          <TableBody>
            {table.getRowModel().rows?.length ? (
              table.getRowModel().rows.map((row) => (
                <TableRow key={row.id}>
                  {row.getVisibleCells().map((cell) => (
                    <TableCell key={cell.id}>
                      {cell.column.id !== "select" &&
                      cell.column.id !== "action" ? (
                        <Link
                          href={`/dashboard/family/detail/${row.original.id}`}
                        >
                          {flexRender(
                            cell.column.columnDef.cell,
                            cell.getContext(),
                          )}
                        </Link>
                      ) : (
                        flexRender(
                          cell.column.columnDef.cell,
                          cell.getContext(),
                        )
                      )}
                    </TableCell>
                  ))}
                </TableRow>
              ))
            ) : (
              <TableRow>
                <TableCell colSpan={columns.length} className="">
                  <div className={styles["content__empty"]}>
                    <EmptyStates
                      title="Tidak ada yang bisa dilihat di sini"
                      subTitle="Tidak ada hasil atau data untuk ditampilkan di sini. Silakan pilih tindakan di bawah untuk melanjutkan."
                      primaryText="Tambah Keluarga"
                      secondaryText="Import Data Keluarga"
                      prefixSecondary={
                        <IconImport color="#4163e7" backgroundColor="#4163e7" />
                      }
                      prefixPrimary={
                        <IconAddCircle color="#fff" backgroundColor="#fff" />
                      }
                    />
                  </div>
                </TableCell>
              </TableRow>
            )}
          </TableBody>
        </Table>
      </div>
      {/* end -- table */}

      {/* start -- pagination */}
      <div className={styles["datatable__footer"]}>
        <div className={styles["pagination__page"]}>
          <Text variant="text" size="sm" fontStyle="regular">
            Tampilkan
          </Text>
          <Select
            name="paginationPage"
            id="paginationPage"
            value={table.getState().pagination.pageSize}
            onChange={(e) => table.setPageSize(Number(e.target.value))}
          >
            {[10, 20, 30, 40, 50].map((pageSize) => (
              <SelectItem key={pageSize} value={pageSize}>
                {pageSize}
              </SelectItem>
            ))}
          </Select>
          <Text variant="text" size="sm" fontStyle="regular">
            data
          </Text>
        </div>
        <div className={styles["datatable__pagination"]}>
          <button
            className={styles["btn__navigation"]}
            onClick={() => table.previousPage()}
            disabled={!table.getCanPreviousPage()}
          >
            <IconArrowLeft />
          </button>
          <div className={styles["pagination__number"]}>
            <Text variant="text" size="sm" fontStyle="regular">
              Halaman
            </Text>

            <Input
              value={table.getState().pagination.pageIndex + 1}
              onChange={(e) => table.setPageIndex(Number(e.target.value) - 1)}
              className={styles["pagination__input"]}
            />
            <Text variant="text" size="sm" fontStyle="regular">
              dari {table.getPageCount()}
            </Text>
          </div>
          <button
            className={styles["btn__navigation"]}
            onClick={() => table.nextPage()}
            disabled={!table.getCanNextPage()}
          >
            <IconArrowRight />
          </button>
        </div>
      </div>
      {/* end -- pagination */}
    </div>
  );
}
