"use client";

import React from "react";
import styles from "./header.module.scss";
import Header from "@/app/ui/components/header/header";
import Link from "next/link";
import Button from "@/app/ui/components/button/button";
import IconAddCircle from "@/app/ui/icons/icon-add-circle";
import IconMoreCircle from "@/app/ui/icons/icon-more-circle";
import IconPrinter from "@/app/ui/icons/icon-printer";
import IconDownload from "@/app/ui/icons/icon-download";
import IconTrash from "@/app/ui/icons/icon-trash";
import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuTrigger,
} from "@/app/ui/components/dropdown-menu/dropdown-menu";
import { Text } from "@/app/ui/components/typography/typography";
import {
  Dialog,
  DialogClose,
  DialogContent,
  DialogFooter,
  DialogTrigger,
} from "@/app/ui/components/dialog/dialog";
import { Label } from "@/app/ui/components/label/label";
import { Select, SelectItem } from "@/app/ui/components/select/select";

const HeaderSection = ({
  selectedRowIds,
  isEmpty,
}: {
  selectedRowIds: string[];
  isEmpty: boolean;
}) => {
  const isRowSelectedExists = selectedRowIds.length !== 0;
  return (
    <div className={styles["content__header"]}>
      <Header
        items={[
          {
            id: "dashboard",
            name: "Beranda",
            link: "/dashboard",
          },
          {
            id: "family",
            name: "Data Keluarga",
            link: "/dashboard/family",
          },
        ]}
        size="xl"
        title="Data Keluarga"
        subTitle=""
      />

      {!isEmpty && (
        <div className={styles["content__header__action"]}>
          <Link href={"/dashboard/family/add"}>
            <Button
              variant="outline"
              text="Tambah Keluarga"
              color="primary"
              size="md"
              prefix={
                <IconAddCircle color="#4163E7" backgroundColor="#4163E7" />
              }
            />
          </Link>
          <DropdownMenu>
            <DropdownMenuTrigger
              className={styles["header__dropdown__trigger"]}
            >
              <IconMoreCircle
                color="#4163E7"
                width={25}
                height={25}
                backgroundColor="#4163E7"
              />
            </DropdownMenuTrigger>
            <DropdownMenuContent
              className={styles["header__dropdown__content"]}
            >
              {isRowSelectedExists ? (
                <Link
                  href="/dashboard/family/prints"
                  className={styles["header__dropdown__item"]}
                >
                  <IconPrinter color="#243576" backgroundColor="#272E38" />
                  <Text variant="text" size="sm" fontStyle="medium">
                    Cetak massal
                  </Text>
                </Link>
              ) : (
                <div className={styles["header__dropdown__item__disabled"]}>
                  <IconPrinter color="#243576" backgroundColor="#243576" />
                  <Text variant="text" size="sm" fontStyle="medium">
                    Cetak massal
                  </Text>
                </div>
              )}
              <Dialog>
                <DialogTrigger>
                  <div className={styles["header__dropdown__item"]}>
                    <IconDownload
                      color="#243576"
                      backgroundColor="#272E38"
                      width={20}
                      height={20}
                    />
                    <Text variant="text" size="sm" fontStyle="medium">
                      Download
                    </Text>
                  </div>
                </DialogTrigger>
                <DialogContent>
                  <div className={styles["download__dialog__content"]}>
                    <Label>Tipe file</Label>
                    <Select defaultValue="xlsx">
                      <SelectItem>xlsx</SelectItem>
                      <SelectItem>csv</SelectItem>
                    </Select>
                  </div>
                  <DialogFooter className={styles["download__dialog__footer"]}>
                    <DialogClose style={{ width: "100%" }}>
                      <Button text="Batal" variant="outline" width="max" />
                    </DialogClose>
                    <Button
                      text="Download"
                      variant="solid"
                      color="primary"
                      width="max"
                    />
                  </DialogFooter>
                </DialogContent>
              </Dialog>
              <Dialog>
                <DialogTrigger>
                  <div className={styles["header__dropdown__item"]}>
                    <IconTrash
                      color="#243576"
                      backgroundColor="#243576"
                      width={20}
                      height={20}
                    />
                    <Text variant="text" size="sm" fontStyle="medium">
                      Hapus
                    </Text>
                  </div>
                </DialogTrigger>
                <DialogContent>
                  <div className={styles["delete__dialog__content"]}>
                    <Text variant="text" size="lg" fontStyle="semibold">
                      Anda yakin ingin menghapus data Keluarga ini?
                    </Text>
                    <Text muted variant="text" size="sm" fontStyle="regular">
                      Data keluarga yang dihapus akan terhapus secara permanen.
                    </Text>
                  </div>
                  <DialogFooter className={styles["delete__dialog__footer"]}>
                    <DialogClose style={{ width: "100%" }}>
                      <Button text="Batal" variant="outline" width="max" />
                    </DialogClose>
                    <Button
                      text="Hapus"
                      variant="solid"
                      color="error"
                      width="max"
                    />
                  </DialogFooter>
                </DialogContent>
              </Dialog>
            </DropdownMenuContent>
          </DropdownMenu>
        </div>
      )}
    </div>
  );
};

export default HeaderSection;
