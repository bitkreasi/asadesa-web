"use client";

import React from "react";
import { FamilyResponse, FamilySearchParams } from "../page";
import {
  getCoreRowModel,
  getFilteredRowModel,
  getPaginationRowModel,
  useReactTable,
} from "@tanstack/react-table";
import { fuzzyFilter } from "@/app/ui/components/table/table";
import { columns } from "./family-table/columns";
import HeaderSection from "./header";
import { DataTable } from "./family-table/datatable";

export interface FamilyFilter {
  gender: string[];
  residentVillages: { name: string }[];
}

export default function Family({
  data,
  filter,
  filterDefaults,
}: {
  data: FamilyResponse[];
  filter: FamilyFilter;
  filterDefaults: FamilySearchParams;
}) {
  const [globalFilter, setGlobalFilter] = React.useState<string>("");
  const table = useReactTable({
    data,
    columns,
    filterFns: {
      fuzzy: fuzzyFilter,
    },
    state: {
      globalFilter,
    },
    enableRowSelection: true,
    onGlobalFilterChange: setGlobalFilter,
    globalFilterFn: fuzzyFilter,
    getCoreRowModel: getCoreRowModel(),
    getFilteredRowModel: getFilteredRowModel(),
    getPaginationRowModel: getPaginationRowModel(),
  });
  const selectedRowIds: string[] = table
    .getSelectedRowModel()
    .rows.map((row) => row.getValue("id"));
  const isDataEmpty = data.length === 0;
  return (
    <>
      <HeaderSection selectedRowIds={selectedRowIds} isEmpty={isDataEmpty} />
      <DataTable
        data={data}
        table={table}
        columns={columns}
        filter={{ globalFilter, setGlobalFilter }}
        filterValues={filter}
        filterDefaults={filterDefaults}
      />
    </>
  );
}
