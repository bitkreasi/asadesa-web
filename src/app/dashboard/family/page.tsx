import styles from "./family.module.scss";
import LayoutDashboard from "@/app/ui/layouts/dashboard/layout-dashboard";
import { publicFetch } from "@/utils/fetch";
import { buildFamilySearchParams } from "./helper";
import Family from "./_components/family";

export interface FamilyResponse {
  id: string;
  fullname: string;
  no_kk: string;
  total_member: number;
  gender: string;
  rt: string;
  rw: string;
  village: string;
}

export interface FamilySearchParams {
  genders?: string[] | string;
  villages?: string[] | string;
}

export default async function FamilyPage({
  searchParams,
}: {
  searchParams: FamilySearchParams;
}) {
  const families = await publicFetch<Array<FamilyResponse>>(
    "/families?" + buildFamilySearchParams(searchParams),
    { cache: "no-store" },
  );
  const genders = await publicFetch<string[]>("/common/gender");
  const familyVillages = await publicFetch<{ name: string }[]>(
    "/common/family-villages",
  );
  return (
    <LayoutDashboard>
      <section>
        <div className={styles["container"]}>
          <div className={`${styles["main__content"]}`}>
            <Family
              data={families?.data ?? []}
              filter={{
                gender: genders.data ?? [],
                residentVillages: familyVillages.data ?? [],
              }}
              filterDefaults={searchParams}
            />
          </div>
        </div>
      </section>
    </LayoutDashboard>
  );
}
