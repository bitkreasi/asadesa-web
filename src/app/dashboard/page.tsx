import LayoutDashboard from "@/app/ui/layouts/dashboard/layout-dashboard";
import { Metadata } from "next";
import { commonDescription, sufixTitle } from "../shared";
import Subscribed from "./components/subscribed";

export const metadata: Metadata = {
  title: `Dasbor ${sufixTitle}`,
  description: `${commonDescription}`,
};

export default function Dashboard() {
  return (
    <LayoutDashboard>
      <Subscribed />
    </LayoutDashboard>
  );
}
