import { ResidentSearchParams } from "./page";

export function buildResidentSearchParams(searchParams: ResidentSearchParams) {
  let gendersQuery = "";
  let statusQuery = "";
  let villagesQuery = "";
  if (searchParams.genders) {
    if (typeof searchParams.genders === "string") {
      gendersQuery = `genders=${encodeURIComponent(searchParams.genders)}`;
    } else {
      gendersQuery = searchParams.genders
        .map((item) => `genders=${encodeURIComponent(item)}`)
        .join("&");
    }
  }

  if (searchParams.status) {
    if (typeof searchParams.status === "string") {
      statusQuery = `status=${encodeURIComponent(searchParams.status)}`;
    } else {
      statusQuery = searchParams.status
        .map((item) => `status=${encodeURIComponent(item)}`)
        .join("&");
    }
  }

  if (searchParams.villages) {
    if (typeof searchParams.villages === "string") {
      villagesQuery = `villages=${encodeURIComponent(searchParams.villages)}`;
    } else {
      villagesQuery = searchParams.villages
        .map((item) => `villages=${encodeURIComponent(item)}`)
        .join("&");
    }
  }
  const finalQuery = `${gendersQuery}&${statusQuery}&${villagesQuery}`.replace(
    /^&+|&+$/g,
    "",
  );
  return finalQuery;
}
