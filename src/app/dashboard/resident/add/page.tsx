import { commonDescription, sufixTitle } from "@/app/shared";
import LayoutDashboard from "@/app/ui/layouts/dashboard/layout-dashboard";
import { Metadata } from "next";
import AddResident from "./_components/add-resident";
import { publicFetch } from "@/utils/fetch";
import { unstable_noStore } from "next/cache";

export const metadata: Metadata = {
  title: `Tambah Penduduk ${sufixTitle}`,
  description: `${commonDescription}`,
};

interface CommonResponse {
  id: string;
  name: string;
}

export interface ResidentFormSelectOptions {
  birthPlaces: CommonResponse[];
  birthTypes: CommonResponse[];
  bloodGroups: CommonResponse[];
  canReads: CommonResponse[];
  chronicPains: CommonResponse[];
  currentEducations: CommonResponse[];
  disabilities: CommonResponse[];
  educationKK: CommonResponse[];
  electronicIdentities: CommonResponse[];
  ethnics: CommonResponse[];
  familyRoles: CommonResponse[];
  healthcareWorkers: CommonResponse[];
  insurances: CommonResponse[];
  jobs: CommonResponse[];
  kbAcceptors: CommonResponse[];
  recordStatus: CommonResponse[];
  religions: CommonResponse[];
}

export async function getAllSelectOptions(): Promise<ResidentFormSelectOptions> {
  const birthPlaces = await publicFetch<CommonResponse[]>(
    "/common/birth-places",
  );
  const birthTypes = await publicFetch<CommonResponse[]>("/common/birth-types");
  const bloodGroups = await publicFetch<CommonResponse[]>(
    "/common/blood-groups",
  );
  const canReads = await publicFetch<CommonResponse[]>("/common/can-reads");
  const chronicPains = await publicFetch<CommonResponse[]>(
    "/common/chronic-pains",
  );
  const currentEducations = await publicFetch<CommonResponse[]>(
    "/common/current-educations",
  );
  const disabilities = await publicFetch<CommonResponse[]>(
    "/common/disabilities",
  );
  const educationKK = await publicFetch<CommonResponse[]>(
    "/common/education-kk",
  );
  const electronicIdentities = await publicFetch<CommonResponse[]>(
    "/common/electronic-identities",
  );
  const ethnics = await publicFetch<CommonResponse[]>("/common/ethnics");
  const familyRoles = await publicFetch<CommonResponse[]>(
    "/common/family-roles",
  );
  const healthcareWorkers = await publicFetch<CommonResponse[]>(
    "/common/healthcare-workers",
  );
  const insurances = await publicFetch<CommonResponse[]>("/common/insurances");
  const jobs = await publicFetch<CommonResponse[]>("/common/jobs");
  const kbAcceptors = await publicFetch<CommonResponse[]>(
    "/common/kb-acceptors",
  );
  const recordStatus = await publicFetch<CommonResponse[]>(
    "/common/record-status",
  );
  const religions = await publicFetch<CommonResponse[]>("/common/religions");

  return {
    birthPlaces: birthPlaces?.data ?? [],
    birthTypes: birthTypes?.data ?? [],
    bloodGroups: bloodGroups?.data ?? [],
    canReads: canReads?.data ?? [],
    chronicPains: chronicPains?.data ?? [],
    currentEducations: currentEducations?.data ?? [],
    disabilities: disabilities?.data ?? [],
    educationKK: educationKK?.data ?? [],
    electronicIdentities: electronicIdentities?.data ?? [],
    ethnics: ethnics?.data ?? [],
    familyRoles: familyRoles?.data ?? [],
    healthcareWorkers: healthcareWorkers?.data ?? [],
    insurances: insurances?.data ?? [],
    jobs: jobs?.data ?? [],
    kbAcceptors: kbAcceptors?.data ?? [],
    recordStatus: recordStatus?.data ?? [],
    religions: religions?.data ?? [],
  };
}

export default async function AddResidentPage() {
  unstable_noStore();
  const selectOptions = await getAllSelectOptions();
  return (
    <LayoutDashboard>
      <AddResident selectOptions={selectOptions} />
    </LayoutDashboard>
  );
}
