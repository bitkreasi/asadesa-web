"use client";

import styles from "./add-resident.module.scss";
import Header from "@/app/ui/components/header/header";
import { useRouter } from "next/navigation";
import {
  ResidentForm,
  ResidentSchema,
} from "../../_components/form/resident-form";
import { ResidentFormSelectOptions } from "../page";
import { useToastMutate } from "@/hooks/use-toast-mutate";
import { addResidentAction } from "../../actions";

export function AddResident(props: {
  selectOptions: ResidentFormSelectOptions;
}) {
  const router = useRouter();
  const mutateAddResidentToast = useToastMutate({
    success: "Anda berhasil menambahkan data penduduk",
  });
  function handleSubmit(data: ResidentSchema) {
    mutateAddResidentToast.mutate(addResidentAction(data));
  }
  return (
    <section>
      <div className={styles["header"]}>
        <Header
          items={[
            {
              id: "dashboard",
              name: "Beranda",
              link: "/dashboard",
            },
            {
              id: "resident",
              name: "Data Penduduk",
              link: "/dashboard/resident",
            },
            {
              id: "add-resident",
              name: "Tambah Penduduk",
              link: "/dashboard/resident/add",
            },
          ]}
          isBack={true}
          title="Tambah Penduduk"
          subTitle=""
        />
      </div>
      <ResidentForm
        variant="add"
        selectOptions={props.selectOptions}
        onSubmit={handleSubmit}
        onCancel={() => router.replace("/dashboard/resident")}
      />
    </section>
  );
}

export default AddResident;
