"use server";

import { publicFetch } from "@/utils/fetch";
import { revalidatePath } from "next/cache";
import { redirect } from "next/navigation";
import { ResidentSchema } from "./_components/form/resident-form";

export async function addResidentAction(data: ResidentSchema) {
  try {
    const res = await publicFetch("/residents", { method: "POST", body: data });
    if (res?.error) {
      return { error: res?.error };
    }
  } catch (error) {
    console.error(error);
    return { error: "Terjadi kesalahan" };
  }
  revalidatePath("/dashboard/resident");
  redirect("/dashboard/resident");
}
