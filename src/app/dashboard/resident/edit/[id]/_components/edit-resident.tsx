"use client";

import styles from "./edit-resident.module.scss";
import Header from "@/app/ui/components/header/header";
import { useRouter } from "next/navigation";
import { ResidentForm } from "../../../_components/form/resident-form";

export function EditResident({ residentId }: { residentId: string }) {
  const router = useRouter();
  return (
    <section>
      <div className={styles["header"]}>
        <Header
          items={[
            {
              id: "dashboard",
              name: "Beranda",
              link: "/dashboard",
            },
            {
              id: "resident",
              name: "Data Penduduk",
              link: "/dashboard/resident",
            },
            {
              id: "detail-resident",
              name: "Detail Penduduk",
              link: `/dashboard/resident/detail/${residentId}`,
            },
            {
              id: "edit-resident",
              name: "Edit Penduduk",
              link: `/dashboard/resident/edit/${residentId}`,
            },
          ]}
          isBack={true}
          title="Edit Penduduk"
          subTitle=""
        />
      </div>
      {/* <ResidentForm */}
      {/*   selectOptions={[]} */}
      {/*   variant="edit" */}
      {/*   onSubmit={() => console.log("hello")} */}
      {/*   onCancel={() => router.replace("/dashboard/resident")} */}
      {/* /> */}
    </section>
  );
}

export default EditResident;
