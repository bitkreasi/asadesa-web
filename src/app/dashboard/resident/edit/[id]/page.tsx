import { commonDescription, sufixTitle } from "@/app/shared";
import LayoutDashboard from "@/app/ui/layouts/dashboard/layout-dashboard";
import { Metadata } from "next";
import EditResident from "./_components/edit-resident";

export const metadata: Metadata = {
  title: `Edit Penduduk ${sufixTitle}`,
  description: `${commonDescription}`,
};
interface IdProps {
  params: {
    id: string;
  };
}

const DashboardEditResident = ({ params }: IdProps) => {
  return (
    <LayoutDashboard>
      <EditResident residentId={params.id} />
    </LayoutDashboard>
  );
};

export default DashboardEditResident;
