"use client";

import React from "react";
import styles from "./prints.module.scss";
import { type Metadata } from "next";
import LayoutDashboard from "@/app/ui/layouts/dashboard/layout-dashboard";
import Header from "@/app/ui/components/header/header";
import Button from "@/app/ui/components/button/button";
import IconPrinter from "@/app/ui/icons/icon-printer";
import Input from "@/app/ui/components/input/input";
import { dummyResident } from "@/app/shared";
import Checkbox from "@/app/ui/components/checkbox/checkbox";
import Link from "next/link";
import Avatar from "@/app/ui/components/avatar/avatar";
import Pagination from "@/app/ui/components/pagination/pagination";
import IconTrash from "@/app/ui/icons/icon-trash";
import { Heading2 } from "@/app/ui/components/typography/typography";

const PrintsLetter = () => {
  const [search, setSearch] = React.useState("");
  const [items, setItems] = React.useState("");
  return (
    <LayoutDashboard>
      <section>
        <div className={styles["header"]}>
          <Header
            items={[
              {
                id: "dashboard",
                name: "Beranda",
                link: "/dashboard",
              },
              {
                id: "resident",
                name: "Data Penduduk",
                link: "/dashboard/resident",
              },
              {
                id: "add-resident",
                name: "Cetak Surat",
                link: "/dashboard/resident/print-person",
              },
            ]}
            isBack={true}
            title="Cetak Surat"
            subTitle=""
          />

          <div className={styles["header__button"]}>
            <div>
              <Button
                text="Cetak Massal"
                variant="solid"
                color="primary"
                size="md"
                prefix={
                  <IconPrinter
                    width={22}
                    height={22}
                    color="#fff"
                    backgroundColor="#fff"
                  />
                }
              />
            </div>
          </div>
        </div>
        {/* Table*/}

        <section className={styles["resident--table__section"]}>
          <Heading2 variant="display" size="sm" fontStyle="medium">
            Daftar penduduk yang dipilih
          </Heading2>
          <div className={styles["content__resident"]}>
            <div className={styles["resident__table--container"]}>
              <table className={styles["resident__table"]}>
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>NIK</th>
                    <th>Dusun</th>
                    <th>RT</th>
                    <th>RW</th>
                    <th>Umur</th>
                  </tr>
                </thead>
                <tbody>
                  {dummyResident?.map((resident, index) => (
                    <tr key={resident.id}>
                      <td>{index + 1}</td>
                      <td>
                        {resident.imageUrl ? (
                          <Link
                            href={`/dashboard/resident/detail/${resident.id}`}
                            className={styles["resident__table--user__profile"]}
                          >
                            <Avatar
                              variant="photo"
                              source={resident.imageUrl}
                              size="sm"
                            />
                            <span
                              className={styles["resident__table--user__name"]}
                            >
                              {resident.nama}
                            </span>
                          </Link>
                        ) : (
                          <Link
                            href={`/dashboard/resident/detail/${resident.id}`}
                            className={styles["resident__table--user__profile"]}
                          >
                            <Avatar variant="icon" size="sm" />
                            <span>{resident.nama}</span>
                          </Link>
                        )}
                      </td>
                      <td>
                        <Link
                          href={`/dashboard/resident/detail/${resident.id}`}
                        >
                          {resident.nik}
                        </Link>
                      </td>
                      <td>
                        <Link
                          href={`/dashboard/resident/detail/${resident.id}`}
                        >
                          {resident.dusun}
                        </Link>
                      </td>
                      <td>
                        <Link
                          href={`/dashboard/resident/detail/${resident.id}`}
                        >
                          {resident.rt}
                        </Link>
                      </td>
                      <td>
                        <Link
                          href={`/dashboard/resident/detail/${resident.id}`}
                        >
                          {resident.rw}
                        </Link>
                      </td>
                      <td>
                        <Link
                          href={`/dashboard/resident/detail/${resident.id}`}
                        >
                          {resident.umur}
                        </Link>
                      </td>
                      <td>
                        <div>
                          {
                            <IconTrash
                              color="#F49C56"
                              backgroundColor="#F49C56"
                            />
                          }
                        </div>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </section>
        {/* Table*/}
        <div className={styles["content"]}>
          <div className={styles["main-content"]}>
            {/* TODO: add print letter here */}
          </div>

          <div className={styles["right-content"]}>
            <Input
              variant="dropdown-select"
              placeholder="masukkan nama"
              size="md"
              label="Penduduk"
              helperText=""
              withSearch={true}
              value={search}
              setValue={setSearch}
              items={dummyResident.map((resident) => resident.nama)}
            />

            <Input
              variant="dropdown-select"
              placeholder="Jenis Surat"
              size="md"
              label="Jenis Surat"
              value={items}
              helperText=""
              setValue={setItems}
              withSearch={false}
              items={["Biodata Penduduk", "asdf"]}
            />
          </div>
        </div>
      </section>
    </LayoutDashboard>
  );
};

export default PrintsLetter;
