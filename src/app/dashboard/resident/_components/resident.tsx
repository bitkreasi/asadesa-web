"use client";

import React from "react";
import { HeaderSection } from "./header";
import { DataTable } from "./resident-table/datatable";
import { columns } from "./resident-table/columns";
import {
  getCoreRowModel,
  getFilteredRowModel,
  getPaginationRowModel,
  useReactTable,
} from "@tanstack/react-table";
import { fuzzyFilter } from "@/app/ui/components/table/table";
import { ResidentResponse, ResidentSearchParams } from "../page";

export interface ResidentFilter {
  gender: string[];
  residentStatus: string[];
  residentVillages: { name: string }[];
}

export default function Resident({
  data,
  filter,
  filterDefaults,
}: {
  data: ResidentResponse[];
  filter: ResidentFilter;
  filterDefaults: ResidentSearchParams;
}) {
  const [globalFilter, setGlobalFilter] = React.useState<string>("");
  const table = useReactTable({
    data,
    columns,
    filterFns: {
      fuzzy: fuzzyFilter,
    },
    state: {
      globalFilter,
    },
    enableRowSelection: true,
    onGlobalFilterChange: setGlobalFilter,
    globalFilterFn: fuzzyFilter,
    getCoreRowModel: getCoreRowModel(),
    getFilteredRowModel: getFilteredRowModel(),
    getPaginationRowModel: getPaginationRowModel(),
  });
  const selectedRowIds: string[] = table
    .getSelectedRowModel()
    .rows.map((row) => row.getValue("nik"));
  const isDataEmpty = data.length === 0;
  return (
    <>
      <HeaderSection selectedRowIds={selectedRowIds} isEmpty={isDataEmpty} />
      <DataTable
        data={data}
        table={table}
        columns={columns}
        filter={{ globalFilter, setGlobalFilter }}
        filterValues={filter}
        filterDefaults={filterDefaults}
      />
    </>
  );
}
