"use client";

import React from "react";
import IconPrinter from "@/app/ui/icons/icon-printer";
import { ColumnDef } from "@tanstack/react-table";
import Image from "next/image";
import Link from "next/link";
import { IndeterminateCheckbox } from "@/app/ui/components/checkbox/v2/checkbox";
import { ResidentResponse } from "../../page";

export const columns: ColumnDef<ResidentResponse>[] = [
  {
    id: "select",
    header: ({ table }) => {
      return (
        <IndeterminateCheckbox
          {...{
            checked: table.getIsAllRowsSelected(),
            indeterminate: table.getIsSomeRowsSelected(),
            onChange: table.getToggleAllRowsSelectedHandler(),
          }}
        />
      );
    },
    cell: ({ row }) => {
      return (
        <div>
          <IndeterminateCheckbox
            {...{
              checked: row.getIsSelected(),
              disabled: !row.getCanSelect(),
              indeterminate: row.getIsSomeSelected(),
              onChange: row.getToggleSelectedHandler(),
            }}
          />
        </div>
      );
    },
  },
  {
    header: "No",
    cell: ({ row }) => {
      const idx = row.index;
      return <div>{idx + 1}</div>;
    },
  },
  {
    id: "nama",
    filterFn: "fuzzy",
    accessorKey: "fullname",
    header: "Nama",
    cell: ({ row }) => {
      return (
        <div style={{ display: "flex", gap: 8, alignItems: "center" }}>
          <Image
            style={{ borderRadius: "9999px" }}
            loading="eager"
            width={32}
            height={32}
            alt={row.getValue("nama")}
            src={"https://source.unsplash.com/random"}
          />
          <p>{row.original.fullname}</p>
        </div>
      );
    },
  },
  {
    accessorKey: "nik",
    header: "NIK",
  },
  {
    accessorKey: "village",
    header: "Dusun",
  },
  {
    accessorKey: "rt",
    header: "RT",
  },
  {
    accessorKey: "rw",
    header: "RW",
  },
  {
    accessorKey: "age",
    header: "Umur",
  },
  {
    accessorKey: "job",
    header: "Pekerjaan",
  },
  {
    accessorKey: "marriage_status",
    header: "Status",
  },
  {
    id: "action",
    cell: () => {
      return (
        <Link
          href="/dashboard/resident/print-person"
          style={{ display: "flex", gap: 8 }}
        >
          <IconPrinter color="#F49C56" backgroundColor="#F49C56" />
        </Link>
      );
    },
  },
];
