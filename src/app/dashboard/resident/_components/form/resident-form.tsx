"use client";
import Button from "@/app/ui/components/button/button";
import {
  Card,
  CardContent,
  CardHeader,
  CardTitle,
} from "@/app/ui/components/card/card";
import {
  Form,
  FormControl,
  FormDescription,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/app/ui/components/form/form";
import IconImport from "@/app/ui/icons/icon-import";
import { yupResolver } from "@hookform/resolvers/yup";
import React from "react";
import { useForm } from "react-hook-form";
import * as yup from "yup";
import styles from "./resident-form.module.scss";
import Image from "next/image";
import { Text } from "@/app/ui/components/typography/typography";
import { Input } from "@/app/ui/components/input/v2/input";
import { Select, SelectItem } from "@/app/ui/components/select/select";
import { Textarea } from "@/app/ui/components/textarea/textarea";
import {
  Modal,
  ModalContent,
  ModalFooter,
  useModal,
} from "@/app/ui/components/modal/v2/modal";
import { ResidentFormSelectOptions } from "../../add/page";

const residentSchema = yup.object({
  resident_type: yup.string().required("Jenis penduduk wajib diisi"),
  entry_date: yup.string(), // only for anggota keluarga masuk
  report_date: yup.string().required("Tanggal lapor wajib diisi"),
  fullname: yup.string().required("Nama lengkap wajib diisi"),
  nik: yup.string().required("Nomor nik wajib diisi"),
  family_role: yup.string().required("Peran dalam keluarga wajib diisi"),
  mandatory_identity: yup.string(),
  electronic_identity: yup.string(),
  record_status: yup.string(),
  tag_card: yup.string(),
  prev_nomor_kk: yup.string(),
  gender: yup.string().required("Jenis kelamin wajib diisi"),
  religion: yup.string().required("Agama wajib diisi"),
  resident_status: yup.string().required("Status penduduk wajib diisi"),
  birth_certificate: yup.string().required("Nomor akta kelahiran wajib diisi"),
  birth_city: yup.string(),
  birth_date: yup.string().required("Tanggal lahir wajib diisi"),
  birth_time: yup.string().required("Waktu kelahiran wajib diisi"),
  birth_place: yup.string().required("Tempat kelahiran wajib diisi"),
  birth_type: yup.string().required("Jenis kelahiran wajib diisi"),
  child_of: yup.string().required("Anak ke wajib diisi"),
  healthcare_worker: yup.string().required("Tenaga kesehatan wajib diisi"),
  birth_weight: yup.string(),
  birth_length: yup.string(),
  pendidikan_dalam_kk: yup.string(),
  pendidikan_sedang_ditempuh: yup.string(),
  pekerjaan: yup.string(),
  suku_etnis: yup.string(),
  status_warga_negara: yup.string(),
  nomor_paspor: yup.string(),
  tanggal_berakhir_paspor: yup.string(),
  nama_ayah: yup.string(),
  nik_ayah: yup.string(),
  nama_ibu: yup.string(),
  nik_ibu: yup.string(),
  dusun: yup.string(),
  rt: yup.string(),
  rw: yup.string(),
  nomor_telepon: yup.string(),
  email: yup.string(),
  status_nikah: yup.string(),
  no_akta_nikah: yup.string(),
  tanggal_pernikahan: yup.string(),
  akta_perceraian: yup.string(),
  tanggal_perceraian: yup.string(),
  gol_darah: yup.string(),
  cacat: yup.string(),
  sakit_menahun: yup.string(),
  akseptor_kb: yup.string(),
  asuransi: yup.string(),
  nomor_bpjs_ketenagakerjaan: yup.string(),
  dapat_membaca_huruf: yup.string(),
  catatan: yup.string(),
});

export type ResidentSchema = yup.InferType<typeof residentSchema>;

interface ResidentFormProps {
  variant?: "add" | "edit";
  selectOptions: ResidentFormSelectOptions;
  onSubmit: (data: ResidentSchema) => void;
  onCancel: () => void;
}

const ResidentForm = (props: ResidentFormProps) => {
  const form = useForm({
    resolver: yupResolver(residentSchema),
  });
  const watchJenisPenduduk = form.watch("resident_type");
  const watchStatusNikah = form.watch("status_nikah");
  const isMarried = watchStatusNikah === "Kawin";
  const isDivorced =
    watchStatusNikah === "Cerai Mati" || watchStatusNikah === "Cerai Hidup";
  const confirModal = useModal();

  return (
    <Form {...form}>
      <form
        className={styles["resident__form__container"]}
        onSubmit={form.handleSubmit(() => confirModal.setOpenModal(true))}
      >
        <Card>
          <CardContent className={styles["resident__form__profile"]}>
            <div className={styles["resident__form__profile__info"]}>
              <Image
                className={styles["resident__form__profile__info__image"]}
                alt="foto profil"
                src="https://source.unsplash.com/random"
                width={56}
                height={56}
              />
              <div>
                <Text variant="text" size="md" fontStyle="medium">
                  Foto profil
                </Text>
                <Text variant="text" size="sm" fontStyle="regular" muted>
                  PNG, JPEG dibawah 2MB
                </Text>
              </div>
            </div>
            <Button
              prefix={<IconImport color="#fff" backgroundColor="#fff" />}
              variant="solid"
              text="Unggah Foto"
            />
          </CardContent>
        </Card>

        <Card>
          <CardHeader>
            <CardTitle>Jenis Penduduk</CardTitle>
          </CardHeader>
          <CardContent>
            <section className={styles["resident__form__input__three"]}>
              <FormField
                control={form.control}
                name="report_date"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Tanggal Lapor</FormLabel>
                    <FormControl>
                      <Input
                        placeholder="Pilih tanggal lapor"
                        type="date"
                        {...field}
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="resident_type"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Jenis penduduk</FormLabel>
                    <FormControl>
                      <Select value={field.value} onChange={field.onChange}>
                        {["Penduduk Masuk", "Penduduk Lahir"].map((item) => (
                          <SelectItem value={item} key={item}>
                            {item}
                          </SelectItem>
                        ))}
                      </Select>
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="entry_date"
                render={({ field }) => (
                  <FormItem>
                    {watchJenisPenduduk === "Penduduk Masuk" ? (
                      <FormLabel>Tanggal Masuk</FormLabel>
                    ) : (
                      <FormLabel aria-disabled>Tanggal Masuk</FormLabel>
                    )}
                    <FormControl>
                      <Input
                        placeholder="Pilih tanggal masuk"
                        disabled={watchJenisPenduduk !== "Penduduk Masuk"}
                        type="date"
                        {...field}
                      />
                    </FormControl>
                    <FormDescription>
                      Diisi jika jenis penduduk adalah penduduk masuk
                    </FormDescription>
                    <FormMessage />
                  </FormItem>
                )}
              />
            </section>{" "}
          </CardContent>
        </Card>

        <Card>
          <CardHeader>
            <CardTitle>Data Diri</CardTitle>
          </CardHeader>
          <CardContent>
            <section className={styles["resident__form__input__three"]}>
              <FormField
                control={form.control}
                name="fullname"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Nama Lengkap</FormLabel>
                    <FormControl>
                      <Input
                        type="text"
                        placeholder="Masukkan nama lengkap"
                        {...field}
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="nik"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Nomor NIK</FormLabel>
                    <FormControl>
                      <Input placeholder="0" type="text" {...field} />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="family_role"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Peran dalam keluarga</FormLabel>
                    <FormControl>
                      <Select value={field.value} onChange={field.onChange}>
                        {props.selectOptions.familyRoles.map((item) => (
                          <SelectItem value={item.id} key={item.id}>
                            {item.name}
                          </SelectItem>
                        ))}
                      </Select>
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
            </section>

            <Card>
              <CardHeader>
                <CardTitle>Status Kepemilikan Identitas</CardTitle>
              </CardHeader>
              <CardContent>
                <section className={styles["resident__form__input__four"]}>
                  <FormField
                    control={form.control}
                    name="mandatory_identity"
                    render={({ field }) => (
                      <FormItem>
                        <FormLabel aria-disabled>Wajib Identitas</FormLabel>
                        <FormControl>
                          <Select
                            disabled
                            value={field.value}
                            onChange={field.onChange}
                          >
                            {["Wajib", "Belum Wajib"].map((item) => (
                              <SelectItem value={item} key={item}>
                                {item}
                              </SelectItem>
                            ))}
                          </Select>
                        </FormControl>
                        <FormDescription aria-disabled>
                          Tidak bisa diubah
                        </FormDescription>
                        <FormMessage />
                      </FormItem>
                    )}
                  />
                  <FormField
                    control={form.control}
                    name="electronic_identity"
                    render={({ field }) => (
                      <FormItem>
                        <FormLabel>Identitas Elektronik</FormLabel>
                        <FormControl>
                          <Select value={field.value} onChange={field.onChange}>
                            {props.selectOptions.electronicIdentities.map(
                              (item) => (
                                <SelectItem value={item.id} key={item.id}>
                                  {item.name}
                                </SelectItem>
                              ),
                            )}
                          </Select>
                        </FormControl>
                        <FormMessage />
                      </FormItem>
                    )}
                  />
                  <FormField
                    control={form.control}
                    name="record_status"
                    render={({ field }) => (
                      <FormItem>
                        <FormLabel>Status Rekam</FormLabel>
                        <FormControl>
                          <Select value={field.value} onChange={field.onChange}>
                            {props.selectOptions.recordStatus.map((item) => (
                              <SelectItem value={item.id} key={item.id}>
                                {item.name}
                              </SelectItem>
                            ))}
                          </Select>
                        </FormControl>
                        <FormMessage />
                      </FormItem>
                    )}
                  />
                  <FormField
                    control={form.control}
                    name="tag_card"
                    render={({ field }) => (
                      <FormItem>
                        <FormLabel>Tag ID Card</FormLabel>
                        <FormControl>
                          <Input
                            type="text"
                            placeholder="Masukkan ID Card"
                            {...field}
                          />
                        </FormControl>
                        <FormMessage />
                      </FormItem>
                    )}
                  />
                </section>
              </CardContent>
            </Card>
            <section className={styles["resident__form__input__four"]}>
              <FormField
                control={form.control}
                name="prev_nomor_kk"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Nomor KK Sebelumnya</FormLabel>
                    <FormControl>
                      <Input
                        placeholder="Masukkan nomor"
                        type="text"
                        {...field}
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="gender"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Jenis Kelamin</FormLabel>
                    <FormControl>
                      <Select value={field.value} onChange={field.onChange}>
                        <SelectItem value="L">Laki-laki</SelectItem>
                        <SelectItem value="P">Perempuan</SelectItem>
                      </Select>
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="religion"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Agama</FormLabel>
                    <FormControl>
                      <Select value={field.value} onChange={field.onChange}>
                        {props.selectOptions.religions.map((item) => (
                          <SelectItem value={item.id} key={item.id}>
                            {item.name}
                          </SelectItem>
                        ))}
                      </Select>
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="resident_status"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Status Penduduk</FormLabel>
                    <FormControl>
                      <Select value={field.value} onChange={field.onChange}>
                        {["Penduduk Tetap", "Penduduk Tidak Tetap"].map(
                          (item) => (
                            <SelectItem value={item} key={item}>
                              {item}
                            </SelectItem>
                          ),
                        )}
                      </Select>
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
            </section>
          </CardContent>
        </Card>

        <Card>
          <CardHeader>
            <CardTitle>Data Kelahiran</CardTitle>
          </CardHeader>
          <CardContent>
            <section className={styles["resident__form__input__two"]}>
              <FormField
                control={form.control}
                name="birth_certificate"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Nomor akta kelahiran</FormLabel>
                    <FormControl>
                      <Input
                        placeholder="Masukkan nomor"
                        type="text"
                        {...field}
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="birth_city"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Kota kelahiran</FormLabel>
                    <FormControl>
                      <Input
                        type="text"
                        {...field}
                        placeholder="Masukkan nama kota"
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
            </section>
            <section className={styles["resident__form__input__three"]}>
              <FormField
                control={form.control}
                name="birth_date"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Tanggal lahir</FormLabel>
                    <FormControl>
                      <Input type="date" {...field} />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="birth_time"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Waktu kelahiran</FormLabel>
                    <FormControl>
                      <Input type="time" {...field} />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="birth_place"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Tempat dilahirkan</FormLabel>
                    <FormControl>
                      <Select value={field.value} onChange={field.onChange}>
                        {props.selectOptions.birthPlaces.map((item) => (
                          <SelectItem value={item.id} key={item.id}>
                            {item.name}
                          </SelectItem>
                        ))}
                      </Select>
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
            </section>
            <section className={styles["resident__form__input__three"]}>
              <FormField
                control={form.control}
                name="birth_type"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Jenis kelahiran</FormLabel>
                    <FormControl>
                      <Select value={field.value} onChange={field.onChange}>
                        {props.selectOptions.birthTypes.map((item) => (
                          <SelectItem value={item.id} key={item.id}>
                            {item.name}
                          </SelectItem>
                        ))}
                      </Select>
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="child_of"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Anak ke</FormLabel>
                    <FormControl>
                      <Input placeholder="Anak ke-" type="number" {...field} />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="healthcare_worker"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Tenaga kesehatan</FormLabel>
                    <FormControl>
                      <Select value={field.value} onChange={field.onChange}>
                        {props.selectOptions.healthcareWorkers.map((item) => (
                          <SelectItem value={item.id} key={item.id}>
                            {item.name}
                          </SelectItem>
                        ))}
                      </Select>
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
            </section>
            <section className={styles["resident__form__input__two"]}>
              <FormField
                control={form.control}
                name="birth_weight"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Berat lahir (gram)</FormLabel>
                    <FormControl>
                      <Input
                        type="number"
                        {...field}
                        placeholder="Berat lahir"
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="birth_length"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Panjang lahir (cm)</FormLabel>
                    <FormControl>
                      <Input
                        type="number"
                        placeholder="Panjang lahir"
                        {...field}
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
            </section>
          </CardContent>
        </Card>

        <Card>
          <CardHeader>
            <CardTitle>Pendidikan dan Pekerjaan</CardTitle>
          </CardHeader>
          <CardContent>
            <section className={styles["resident__form__input__three"]}>
              <FormField
                control={form.control}
                name="pendidikan_dalam_kk"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Pendidikan dalam KK</FormLabel>
                    <FormControl>
                      <Select value={field.value} onChange={field.onChange}>
                        {props.selectOptions.educationKK.map((item) => (
                          <SelectItem value={item.id} key={item.id}>
                            {item.name}
                          </SelectItem>
                        ))}
                      </Select>
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="pendidikan_sedang_ditempuh"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Pendidikan yang sedang ditempuh</FormLabel>
                    <FormControl>
                      <Select value={field.value} onChange={field.onChange}>
                        {props.selectOptions.currentEducations.map((item) => (
                          <SelectItem value={item.id} key={item.id}>
                            {item.name}
                          </SelectItem>
                        ))}
                      </Select>
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="pekerjaan"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Pekerjaan</FormLabel>
                    <FormControl>
                      <Select value={field.value} onChange={field.onChange}>
                        {props.selectOptions.jobs.map((item) => (
                          <SelectItem value={item.id} key={item.id}>
                            {item.name}
                          </SelectItem>
                        ))}
                      </Select>
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
            </section>
          </CardContent>
        </Card>

        <Card>
          <CardHeader>
            <CardTitle>Data Kewarganegaraan</CardTitle>
          </CardHeader>
          <CardContent>
            <section className={styles["resident__form__input__four"]}>
              <FormField
                control={form.control}
                name="suku_etnis"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Suku/Etnis</FormLabel>
                    <FormControl>
                      <Select value={field.value} onChange={field.onChange}>
                        {props.selectOptions.ethnics.map((item) => (
                          <SelectItem value={item.id} key={item.id}>
                            {item.name}
                          </SelectItem>
                        ))}
                      </Select>
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="status_warga_negara"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Status warga negara</FormLabel>
                    <FormControl>
                      <Select value={field.value} onChange={field.onChange}>
                        {["WNI", "WNA", "Dua Kewarganegaraan"].map((item) => (
                          <SelectItem value={item} key={item}>
                            {item}
                          </SelectItem>
                        ))}
                      </Select>
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="nomor_paspor"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Nomor Paspor</FormLabel>
                    <FormControl>
                      <Input
                        placeholder="Masukkan nomor"
                        type="text"
                        {...field}
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="tanggal_berakhir_paspor"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Tanggal berakhir paspor</FormLabel>
                    <FormControl>
                      <Input type="date" {...field} />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
            </section>
          </CardContent>
        </Card>

        <Card>
          <CardHeader>
            <CardTitle>Data Orang Tua</CardTitle>
          </CardHeader>
          <CardContent>
            <section className={styles["resident__form__input__two"]}>
              <FormField
                control={form.control}
                name="nama_ayah"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Nama Ayah</FormLabel>
                    <FormControl>
                      <Input
                        placeholder="Masukkan nama ayah"
                        type="text"
                        {...field}
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="nik_ayah"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>NIK Ayah</FormLabel>
                    <FormControl>
                      <Input
                        placeholder="Masukkna nomor nik ayah"
                        type="text"
                        {...field}
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
            </section>
            <section className={styles["resident__form__input__two"]}>
              <FormField
                control={form.control}
                name="nama_ibu"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Nama Ibu</FormLabel>
                    <FormControl>
                      <Input
                        placeholder="Masukkan nama ibu"
                        type="text"
                        {...field}
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="nik_ibu"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>NIK Ibu</FormLabel>
                    <FormControl>
                      <Input
                        placeholder="Masukkan nomor nik ibu"
                        type="text"
                        {...field}
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
            </section>
          </CardContent>
        </Card>

        <Card>
          <CardHeader>
            <CardTitle>Alamat</CardTitle>
          </CardHeader>
          <CardContent>
            <section className={styles["resident__form__input__three"]}>
              <FormField
                control={form.control}
                name="dusun"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Alamat</FormLabel>
                    <FormControl>
                      <Select value={field.value} onChange={field.onChange}>
                        {["Menjangan", "Mangsit", "Welahan"].map((item) => (
                          <SelectItem value={item} key={item}>
                            {item}
                          </SelectItem>
                        ))}
                      </Select>
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="rt"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>RT</FormLabel>
                    <FormControl>
                      <Input placeholder="Masukkan RT" type="text" {...field} />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="rw"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>RW</FormLabel>
                    <FormControl>
                      <Input type="text" placeholder="Masukkan RW" {...field} />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
            </section>
            <section className={styles["resident__form__input__two"]}>
              <FormField
                control={form.control}
                name="nomor_telepon"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Nomor Telepon</FormLabel>
                    <FormControl>
                      <Input
                        type="text"
                        placeholder="Masukkan nomor telepon"
                        {...field}
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="email"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Email</FormLabel>
                    <FormControl>
                      <Input
                        type="email"
                        placeholder="Masukkan email"
                        {...field}
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
            </section>
          </CardContent>
        </Card>

        <Card>
          <CardHeader>
            <CardTitle>Status Pernikahan</CardTitle>
          </CardHeader>
          <CardContent>
            <section className={styles["resident__form__input__three"]}>
              <FormField
                control={form.control}
                name="status_nikah"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Status nikah</FormLabel>
                    <FormControl>
                      <Select value={field.value} onChange={field.onChange}>
                        {[
                          "Belum Kawin",
                          "Kawin",
                          "Cerai Hidup",
                          "Cerai Mati",
                        ].map((item) => (
                          <SelectItem value={item} key={item}>
                            {item}
                          </SelectItem>
                        ))}
                      </Select>
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="no_akta_nikah"
                render={({ field }) => (
                  <FormItem>
                    {!isMarried ? (
                      <FormLabel aria-disabled>
                        No Akta Nikah (Buku nikah)
                      </FormLabel>
                    ) : (
                      <FormLabel>No Akta Nikah (Buku nikah)</FormLabel>
                    )}
                    <FormControl>
                      <Input
                        disabled={!isMarried}
                        placeholder="Masukkan no akta nikah"
                        type="text"
                        {...field}
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="tanggal_pernikahan"
                render={({ field }) => (
                  <FormItem>
                    {!isMarried ? (
                      <FormLabel aria-disabled>Tanggal pernikahan</FormLabel>
                    ) : (
                      <FormLabel>Tanggal pernikahan</FormLabel>
                    )}
                    <FormControl>
                      <Input type="date" disabled={!isMarried} {...field} />
                    </FormControl>
                    <FormDescription>
                      Diisi jika status sudah menikah
                    </FormDescription>
                    <FormMessage />
                  </FormItem>
                )}
              />
            </section>
            <section className={styles["resident__form__input__two"]}>
              <FormField
                control={form.control}
                name="akta_perceraian"
                render={({ field }) => (
                  <FormItem>
                    {!isDivorced ? (
                      <FormLabel aria-disabled>Akta perceraian</FormLabel>
                    ) : (
                      <FormLabel>Akta perceraian</FormLabel>
                    )}
                    <FormControl>
                      <Select
                        value={field.value}
                        disabled={!isDivorced}
                        onChange={field.onChange}
                      >
                        {["Belum cerai", "Cerai"].map((item) => (
                          <SelectItem value={item} key={item}>
                            {item}
                          </SelectItem>
                        ))}
                      </Select>
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="tanggal_perceraian"
                render={({ field }) => (
                  <FormItem>
                    {!isDivorced ? (
                      <FormLabel aria-disabled>Tanggal perceraian</FormLabel>
                    ) : (
                      <FormLabel>Tanggal perceraian</FormLabel>
                    )}
                    <FormControl>
                      <Input
                        type="date"
                        {...field}
                        disabled={!isDivorced}
                        placeholder="Pilih tanggal"
                      />
                    </FormControl>
                    <FormDescription>Diisi jika status cerai</FormDescription>
                    <FormMessage />
                  </FormItem>
                )}
              />
            </section>
          </CardContent>
        </Card>

        <Card>
          <CardHeader>
            <CardTitle>Data Kesehatan</CardTitle>
          </CardHeader>
          <CardContent>
            <section className={styles["resident__form__input__three"]}>
              <FormField
                control={form.control}
                name="gol_darah"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Golongan darah</FormLabel>
                    <FormControl>
                      <Select value={field.value} onChange={field.onChange}>
                        {props.selectOptions.bloodGroups.map((item) => (
                          <SelectItem value={item.id} key={item.id}>
                            {item.name}
                          </SelectItem>
                        ))}
                      </Select>
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="cacat"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Cacat</FormLabel>
                    <FormControl>
                      <Select value={field.value} onChange={field.onChange}>
                        {props.selectOptions.disabilities.map((item) => (
                          <SelectItem value={item.id} key={item.id}>
                            {item.name}
                          </SelectItem>
                        ))}
                      </Select>
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="sakit_menahun"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Sakit menahun</FormLabel>
                    <FormControl>
                      <Select value={field.value} onChange={field.onChange}>
                        {props.selectOptions.chronicPains.map((item) => (
                          <SelectItem value={item.id} key={item.id}>
                            {item.name}
                          </SelectItem>
                        ))}
                      </Select>
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
            </section>
            <section className={styles["resident__form__input__three"]}>
              <FormField
                control={form.control}
                name="akseptor_kb"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Akseptor KB</FormLabel>
                    <FormControl>
                      <Select value={field.value} onChange={field.onChange}>
                        {props.selectOptions.kbAcceptors.map((item) => (
                          <SelectItem value={item.id} key={item.id}>
                            {item.name}
                          </SelectItem>
                        ))}
                      </Select>
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="asuransi"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Pilih asuransi</FormLabel>
                    <FormControl>
                      <Select value={field.value} onChange={field.onChange}>
                        {props.selectOptions.insurances.map((item) => (
                          <SelectItem value={item.id} key={item.id}>
                            {item.name}
                          </SelectItem>
                        ))}
                      </Select>
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="nomor_bpjs_ketenagakerjaan"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Nomor BPJS Ketenagakerjaan</FormLabel>
                    <FormControl>
                      <Input
                        type="text"
                        placeholder="Masukkan nomor"
                        {...field}
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
            </section>
          </CardContent>
        </Card>

        <Card>
          <CardHeader>
            <CardTitle>Lainnya</CardTitle>
          </CardHeader>
          <CardContent>
            <section className={styles["resident__form__input__two"]}>
              <FormField
                control={form.control}
                name="dapat_membaca_huruf"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Dapat membaca huruf</FormLabel>
                    <FormControl>
                      <Select value={field.value} onChange={field.onChange}>
                        {props.selectOptions.canReads.map((item) => (
                          <SelectItem value={item.id} key={item.id}>
                            {item.name}
                          </SelectItem>
                        ))}
                      </Select>
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="catatan"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Catatan</FormLabel>
                    <FormControl>
                      <Textarea rows={5} {...field} />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
            </section>
          </CardContent>
        </Card>

        <section className={styles["resident__form__action"]}>
          <Button
            size="lg"
            text="Batal"
            onHandleClick={props.onCancel}
            variant="outline"
          />
          <Button
            size="lg"
            text={props.variant === "add" ? "Tambahkan" : "Simpan"}
            variant="solid"
            type="submit"
          />
        </section>

        <Modal
          openModal={confirModal.openModal}
          setOpenModal={confirModal.setOpenModal}
        >
          <ModalContent style={{ width: 400 }}>
            <div className={styles["resident__form__dialog__content"]}>
              <Text variant="text" size="lg" fontStyle="semibold">
                Anda yakin ingin menyimpan data ini?
              </Text>
              <Text variant="text" size="sm" fontStyle="regular" muted>
                Setelah anda menyimpan data ini, anda dapat kembali mengedit
                data ini dengan pergi ke detail dan edit data.
              </Text>
            </div>
          </ModalContent>
          <ModalFooter className={styles["resident__form__dialog__footer"]}>
            <Button
              text="Cek Ulang"
              variant="outline"
              color="neutral"
              width="max"
              onHandleClick={() => confirModal.setOpenModal(false)}
            />
            <Button
              text="Simpan"
              variant="solid"
              color="primary"
              width="max"
              onHandleClick={() => {
                const data = form.getValues();
                props.onSubmit(data);
              }}
            />
          </ModalFooter>
        </Modal>
      </form>
    </Form>
  );
};

export { ResidentForm };
