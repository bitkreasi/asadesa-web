import Button from "@/app/ui/components/button/button";
import styles from "./delete-resident-dialog.module.scss";

import {
  Dialog,
  DialogClose,
  DialogContent,
  DialogFooter,
  DialogTrigger,
} from "@/app/ui/components/dialog/dialog";
import IconTrash from "@/app/ui/icons/icon-trash";
import React from "react";
import { Text } from "@/app/ui/components/typography/typography";
import IconMessageQuestion from "@/app/ui/icons/icon-message-question";
import { toast } from "@/app/ui/components/toast/toast";

const DeleteResidentDialog = () => {
  return (
    <Dialog>
      <DialogTrigger>
        <button
          className={styles["content__more--item__button"]}
          style={{ width: "100%" }}
        >
          <IconTrash
            color="#272E38"
            backgroundColor="#272E38"
            width={20}
            height={20}
          />
          Hapus
        </button>
      </DialogTrigger>
      <DialogContent>
        <div className={styles["delete--content__dialog"]}>
          <div className={styles["delete--icon"]}>
            <IconMessageQuestion width={32} height={32} />
          </div>
          <Text variant="text" size="lg" fontStyle="semibold">
            Anda yakin ingin menghapus data penduduk ini?
          </Text>
          <Text variant="text" size="sm" fontStyle="regular" muted>
            Data penduduk yang dihapus akan terhapus secara permanen.
          </Text>
        </div>
        <DialogFooter className={styles["delete--footer__dialog"]}>
          <DialogClose style={{ width: "100%" }}>
            <Button
              text="Batal"
              variant="outline"
              color="neutral"
              width="max"
            />
          </DialogClose>
          <DialogClose style={{ width: "100%" }}>
            <Button
              onHandleClick={() =>
                toast.success("Data penduduk berhasil dihapus")
              }
              text="Hapus"
              variant="solid"
              color="error"
              width="max"
            />
          </DialogClose>
          <DialogClose style={{ width: "100%" }}>
            <Button
              onHandleClick={() =>
                toast.success("Data penduduk berhasil dihapus")
              }
              text="Hapus"
              variant="solid"
              color="error"
              width="max"
            />
          </DialogClose>
        </DialogFooter>
      </DialogContent>
    </Dialog>
  );
};

export default DeleteResidentDialog;
