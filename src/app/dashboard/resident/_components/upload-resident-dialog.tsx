import styles from "./upload-resident-dialog.module.scss";
import { Text } from "@/app/ui/components/typography/typography";
import Button from "@/app/ui/components/button/button";
import {
  Dialog,
  DialogClose,
  DialogContent,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/app/ui/components/dialog/dialog";
import IconExport from "@/app/ui/icons/icon-export";
import IconImport from "@/app/ui/icons/icon-import";
import React from "react";
import { Select, SelectItem } from "@/app/ui/components/select/select";

const ImportDialog = () => {
  return (
    <>
      <DialogHeader>
        <DialogTitle>Import Data Penduduk</DialogTitle>
      </DialogHeader>

      <div className={styles["upload__content"]}>
        <div className={styles["upload__icon"]}>
          <IconExport />
        </div>
        <Text variant="text" size="md" fontStyle="semibold">
          Drag & drop file atau <span style={{ color: "blue" }}>cari file</span>
        </Text>
        <Text variant="text" size="sm" fontStyle="regular" muted>
          XLSX (maksimal 5MB)
        </Text>
      </div>
    </>
  );
};

const DownloadDialog = () => {
  return (
    <>
      <div className={styles["file--type__dialog"]}>
        <Text variant="text" size="md" fontStyle="semibold">
          Tipe file
        </Text>
        <Select defaultValue="xlsx">
          <SelectItem value="xlsx">xlsx</SelectItem>
          <SelectItem value="csv">csv</SelectItem>
        </Select>
      </div>
    </>
  );
};

const UploadResidentDialog = () => {
  const [dialogType, setDialogType] = React.useState<"import" | "download">(
    "import",
  );
  return (
    <Dialog onClose={() => setDialogType("import")}>
      <DialogTrigger>
        <div className={styles["header__dropdown__item"]}>
          <IconImport
            color="#243576"
            backgroundColor="#272E38"
            width={20}
            height={20}
          />
          <Text variant="text" size="sm" fontStyle="medium">
            Import Data Penduduk
          </Text>
        </div>
      </DialogTrigger>
      <DialogContent>
        {dialogType === "import" && <ImportDialog />}
        {dialogType === "download" && <DownloadDialog />}
        <DialogFooter className={styles["upload__footer"]}>
          {dialogType === "import" && (
            <Button
              text="Domload Template Data"
              variant="outline"
              color="primary"
              width="max"
              onHandleClick={() => {
                if (dialogType === "import") {
                  setDialogType("download");
                }
              }}
            />
          )}
          {dialogType === "download" && (
            <DialogClose style={{ width: "100%" }}>
              <Button
                text="Batal"
                variant="outline"
                color="neutral"
                width="max"
              />
            </DialogClose>
          )}
          <Button
            variant="solid"
            text={dialogType === "import" ? "Import" : "Download"}
            width="max"
          />
        </DialogFooter>
      </DialogContent>
    </Dialog>
  );
};

export default UploadResidentDialog;
