"use client";

import React from "react";
import styles from "./header.module.scss";
import Header from "@/app/ui/components/header/header";
import Link from "next/link";
import Button from "@/app/ui/components/button/button";
import IconAddCircle from "@/app/ui/icons/icon-add-circle";
import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuTrigger,
} from "@/app/ui/components/dropdown-menu/dropdown-menu";
import IconMoreCircle from "@/app/ui/icons/icon-more-circle";
import IconPrinter from "@/app/ui/icons/icon-printer";
import { Text } from "@/app/ui/components/typography/typography";
import {
  Dialog,
  DialogClose,
  DialogContent,
  DialogFooter,
  DialogTrigger,
} from "@/app/ui/components/dialog/dialog";
import IconDownload from "@/app/ui/icons/icon-download";
import { Label } from "@/app/ui/components/label/label";
import { Select, SelectItem } from "@/app/ui/components/select/select";
import IconTrash from "@/app/ui/icons/icon-trash";
import IconMessageQuestion from "@/app/ui/icons/icon-message-question";
import { toast } from "@/app/ui/components/toast/toast";
import UploadResidentDialog from "./upload-resident-dialog";

const HeaderSection = ({
  selectedRowIds,
  isEmpty,
}: {
  selectedRowIds: string[];
  isEmpty: boolean;
}) => {
  const isRowSelectedExists = selectedRowIds.length !== 0;
  return (
    <div className={styles["content__header"]}>
      <Header
        items={[
          {
            id: "dashboard",
            name: "Beranda",
            link: "/dashboard",
          },
          {
            id: "family",
            name: "Data Penduduk",
            link: "/dashboard/resident",
          },
        ]}
        size="xl"
        title="Data Penduduk"
        subTitle=""
      />
      {!isEmpty && (
        <div className={styles["content__header__action"]}>
          <Link href={"/dashboard/resident/add"}>
            <Button
              variant="outline"
              text="Tambah Penduduk"
              color="primary"
              size="md"
              prefix={
                <IconAddCircle color="#4163E7" backgroundColor="#4163E7" />
              }
            />
          </Link>
          <DropdownMenu>
            <DropdownMenuTrigger
              className={styles["header__dropdown__trigger"]}
            >
              <IconMoreCircle
                color="#4163E7"
                width={25}
                height={25}
                backgroundColor="#4163E7"
              />
            </DropdownMenuTrigger>

            <DropdownMenuContent
              className={styles["header__dropdown__content"]}
            >
              {isRowSelectedExists ? (
                <Link
                  href="/dashboard/resident/prints"
                  className={styles["header__dropdown__item"]}
                >
                  <IconPrinter color="#243576" backgroundColor="#243576" />
                  <Text variant="text" size="sm" fontStyle="medium">
                    Cetak massal
                  </Text>
                </Link>
              ) : (
                <div className={styles["header__dropdown__item__disabled"]}>
                  <IconPrinter color="#243576" backgroundColor="#243576" />
                  <Text variant="text" size="sm" fontStyle="medium">
                    Cetak massal
                  </Text>
                </div>
              )}
              <UploadResidentDialog />
              <Dialog>
                <DialogTrigger>
                  <div className={styles["header__dropdown__item"]}>
                    <IconDownload
                      color="#243576"
                      backgroundColor="#272E38"
                      width={20}
                      height={20}
                    />
                    <Text variant="text" size="sm" fontStyle="medium">
                      Download
                    </Text>
                  </div>
                </DialogTrigger>
                <DialogContent>
                  <div className={styles["download__dialog__content"]}>
                    <Label>Tipe file</Label>
                    <Select defaultValue="xlsx">
                      <SelectItem>xlsx</SelectItem>
                      <SelectItem>csv</SelectItem>
                    </Select>
                  </div>
                  <DialogFooter className={styles["download__dialog__footer"]}>
                    <DialogClose style={{ width: "100%" }}>
                      <Button
                        text="Batal"
                        color="neutral"
                        variant="outline"
                        width="max"
                      />
                    </DialogClose>
                    <Button
                      text="Download"
                      variant="solid"
                      color="primary"
                      width="max"
                    />
                  </DialogFooter>
                </DialogContent>
              </Dialog>
              <Dialog>
                <DialogTrigger>
                  <div className={styles["header__dropdown__item"]}>
                    <IconTrash
                      color="#243576"
                      backgroundColor="#243576"
                      width={20}
                      height={20}
                    />
                    <Text variant="text" size="sm" fontStyle="medium">
                      Hapus
                    </Text>
                  </div>
                </DialogTrigger>
                <DialogContent>
                  <div className={styles["delete__dialog__content"]}>
                    <div className={styles["delete__dialog__icon"]}>
                      <IconMessageQuestion width={32} height={32} />
                    </div>
                    <Text variant="text" size="lg" fontStyle="semibold">
                      Anda yakin ingin menghapus data penduduk ini?
                    </Text>
                    <Text muted variant="text" size="sm" fontStyle="regular">
                      Data penduduk yang dihapus akan terhapus secara permanen.
                    </Text>
                  </div>
                  <DialogFooter className={styles["delete__dialog__footer"]}>
                    <DialogClose style={{ width: "100%" }}>
                      <Button
                        color="neutral"
                        text="Batal"
                        variant="outline"
                        width="max"
                      />
                    </DialogClose>
                    <DialogClose style={{ width: "100%" }}>
                      <Button
                        text="Hapus"
                        variant="solid"
                        color="error"
                        width="max"
                        onHandleClick={() =>
                          toast.success("Data penduduk berhasil dihapus")
                        }
                      />
                    </DialogClose>
                  </DialogFooter>
                </DialogContent>
              </Dialog>
            </DropdownMenuContent>
          </DropdownMenu>
        </div>
      )}
    </div>
  );
};

export { HeaderSection };
