import { commonDescription, sufixTitle } from "@/app/shared";
import LayoutDashboard from "@/app/ui/layouts/dashboard/layout-dashboard";
import { Metadata } from "next";
import HeaderSection from "./_components/header";
import styles from "./resident-detail.module.scss";
import ResidentDetail from "./_components/resident-detail";

export const metadata: Metadata = {
  title: `Detail Penduduk ${sufixTitle}`,
  description: `${commonDescription}`,
};

interface IdProps {
  params: {
    id: string;
  };
}

const DashboardDetailResident = ({ params }: IdProps) => {
  return (
    <LayoutDashboard>
      <div className={styles["content__wrapper"]}>
        <HeaderSection residentId={params.id} />
        <ResidentDetail residentId={params.id} />
      </div>
    </LayoutDashboard>
  );
};

export default DashboardDetailResident;
