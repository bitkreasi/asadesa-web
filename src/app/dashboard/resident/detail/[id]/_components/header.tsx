"use client";

import React from "react";
import styles from "./header.module.scss";
import Header from "@/app/ui/components/header/header";
import Link from "next/link";
import Button from "@/app/ui/components/button/button";
import IconEdit from "@/app/ui/icons/icon-edit";
import IconPrinter from "@/app/ui/icons/icon-printer";
import IconTrash from "@/app/ui/icons/icon-trash";
import {
  Dialog,
  DialogClose,
  DialogContent,
  DialogFooter,
  DialogTrigger,
} from "@/app/ui/components/dialog/dialog";
import IconMessageQuestion from "@/app/ui/icons/icon-message-question";
import { Text } from "@/app/ui/components/typography/typography";
import { toast } from "@/app/ui/components/toast/toast";

interface HeaderSectionProps {
  residentId: string;
}

export default function HeaderSection(props: HeaderSectionProps) {
  return (
    <div className={styles["header"]}>
      <Header
        items={[
          {
            id: "dashboard",
            name: "Beranda",
            link: "/dashboard",
          },
          {
            id: "resident",
            name: "Data Penduduk",
            link: "/dashboard/resident",
          },
          {
            id: "detail-resident",
            name: "Detail Penduduk",
            link: `/dashboard/resident/detail/${props.residentId}`,
          },
        ]}
        isBack={true}
        title="Detail Penduduk"
        subTitle=""
      />

      <div className={styles["header__action"]}>
        <Link href={`/dashboard/resident/edit/${props.residentId}`}>
          <Button
            text="Edit"
            variant="outline"
            color="primary"
            size="md"
            prefix={
              <IconEdit
                width={22}
                height={22}
                color="#4163E7"
                backgroundColor="#4163E7"
              />
            }
          />
        </Link>
        <div>
          <Link href={`/dashboard/resident/print-person`}>
            <Button
              text="Cetak Surat"
              variant="outline"
              color="primary"
              size="md"
              prefix={
                <IconPrinter
                  width={22}
                  height={22}
                  color="#4163E7"
                  backgroundColor="#4163E7"
                />
              }
            />
          </Link>
        </div>
        <div>
          <Dialog>
            <DialogTrigger>
              <Button
                text="Hapus"
                variant="outline"
                color="error"
                size="md"
                prefix={
                  <IconTrash
                    width={22}
                    height={22}
                    color="#F42E2E"
                    backgroundColor="#F42E2E"
                  />
                }
              />
            </DialogTrigger>
            <DialogContent>
              <div className={styles["delete__dialog__content"]}>
                <div className={styles["delete__dialog__icon"]}>
                  <IconMessageQuestion width={32} height={32} />
                </div>
                <Text variant="text" size="lg" fontStyle="semibold">
                  Anda yakin ingin menghapus data penduduk ini?
                </Text>
                <Text muted variant="text" size="sm" fontStyle="regular">
                  Data penduduk yang dihapus akan terhapus secara permanen.
                </Text>
              </div>
              <DialogFooter className={styles["delete__dialog__footer"]}>
                <DialogClose style={{ width: "100%" }}>
                  <Button
                    color="neutral"
                    text="Batal"
                    variant="outline"
                    width="max"
                  />
                </DialogClose>
                <DialogClose style={{ width: "100%" }}>
                  <Button
                    text="Hapus"
                    variant="solid"
                    color="error"
                    width="max"
                    onHandleClick={() =>
                      toast.success("Data penduduk berhasil dihapus")
                    }
                  />
                </DialogClose>
              </DialogFooter>
            </DialogContent>
          </Dialog>
        </div>
      </div>
    </div>
  );
}
