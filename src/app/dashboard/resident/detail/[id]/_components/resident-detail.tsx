import {
  Card,
  CardContent,
  CardHeader,
  CardTitle,
} from "@/app/ui/components/card/card";
import Image from "next/image";
import React from "react";
import { Text } from "@/app/ui/components/typography/typography";
import styles from "./resident-detail.module.scss";

interface ResidentDetailProps {
  residentId: string;
}

const DetailItem = ({
  label,
  value,
}: {
  label: string;
  value: string | number;
}) => {
  return (
    <div className={styles["detail__item"]}>
      <Text muted variant="text" size="sm" fontStyle="medium">
        {label}
      </Text>
      <Text variant="text" size="sm" fontStyle="regular">
        {value}
      </Text>
    </div>
  );
};

export default function ResidentDetail(props: ResidentDetailProps) {
  return (
    <section className={styles["container"]}>
      <Card>
        <CardContent className={styles["profile"]}>
          <Image
            style={{
              borderRadius: 99999,
            }}
            width={60}
            height={60}
            src="https://source.unsplash.com/random"
            alt="Image profile"
          />
          <Text variant="text" size="md" fontStyle="medium">
            Foto Profile
          </Text>
        </CardContent>
      </Card>

      <Card>
        <CardHeader>
          <CardTitle>Jenis Penduduk</CardTitle>
        </CardHeader>
        <CardContent className={styles["info__card__content"]}>
          <section className={styles["info__card__section"]}>
            <DetailItem label="Tanggal lapor" value="20-01-2022" />
            <DetailItem label="Jenis penduduk" value="Penduduk lahir" />
            <DetailItem label="Tanggal masuk" value="-" />
          </section>
        </CardContent>
      </Card>

      <Card>
        <CardHeader>
          <CardTitle>Data Diri</CardTitle>
        </CardHeader>
        <CardContent className={styles["info__card__content"]}>
          <section className={styles["info__card__section"]}>
            <DetailItem label="Nama lengkap" value="Asep Uyee" />
            <DetailItem label="Nomor NIk" value="3302042001040001" />
          </section>
          <Card>
            <CardHeader>
              <CardTitle>Status Kepemilikan Identitas</CardTitle>
            </CardHeader>
            <CardContent className={styles["info__card__section"]}>
              <DetailItem label="Wajib identitas" value="Belum Wajib" />
              <DetailItem label="Identtias Elektronik" value="KTP" />
              <DetailItem label="Status rekam" value="Belum rekam" />
              <DetailItem label="Tag ID Card" value="LK12345" />
            </CardContent>
          </Card>
          <section className={styles["info__card__section"]}>
            <DetailItem label="Nomor KK sebelumnya" value="3302042001040001" />
            <DetailItem label="Jenis kelamin" value="Laki-laki" />
            <DetailItem label="Agama" value="Islam" />
            <DetailItem label="Status penduduk" value="Tetap" />
          </section>
        </CardContent>
      </Card>

      <Card>
        <CardHeader>
          <CardTitle>Data Kelahiran</CardTitle>
        </CardHeader>
        <CardContent className={styles["info__card__content"]}>
          <section className={styles["info__card__section"]}>
            <DetailItem label="Nomor akta kelahiran" value="1345789012342790" />
            <DetailItem label="Kota kelahiran" value="Kebumen" />
            <DetailItem label="Tanggal lahir" value="20-01-1975" />
            <DetailItem label="Waktu Kelahiran" value="13:51" />
            <DetailItem label="Tempat dilahirkan" value="PUSKEMSAS" />
          </section>
          <section className={styles["info__card__section"]}>
            <DetailItem label="Jenis Kelahiran" value="Tunggal" />
            <DetailItem label="Anak ke" value="1" />
            <DetailItem label="Tenaga Kesehatan" value="Bidan/Perawat" />
            <DetailItem label="Berat Lahir" value="3500 Gram" />
            <DetailItem label="Panjang lahir" value="50 cm" />
          </section>
        </CardContent>
      </Card>

      <Card>
        <CardHeader>
          <CardTitle>Pendidikan dan Pekerjaan</CardTitle>
        </CardHeader>
        <CardContent className={styles["info__card__content"]}>
          <section className={styles["info__card__section"]}>
            <DetailItem label="Pendidikan dalam KK" value="Strata II" />
            <DetailItem
              label="Pendidikan yang sedang ditempuh"
              value="Sedang S-3/Sederajat"
            />
            <DetailItem label="Pekerjaan" value="Programmer" />
          </section>
        </CardContent>
      </Card>

      <Card>
        <CardHeader>
          <CardTitle>Data Kewarganegaraan</CardTitle>
        </CardHeader>
        <CardContent className={styles["info__card__content"]}>
          <section className={styles["info__card__section"]}>
            <DetailItem label="Suku/Etnis" value="Jawa" />
            <DetailItem label="Status warga negara" value="WNI" />
            <DetailItem label="Nomor paspor" value="1234567890" />
            <DetailItem label="Tanggal berakhir paspor" value="12/19/2023" />
          </section>
        </CardContent>
      </Card>

      <Card>
        <CardHeader>
          <CardTitle>Data Orang Tua</CardTitle>
        </CardHeader>
        <CardContent className={styles["info__card__content"]}>
          <section className={styles["info__card__section"]}>
            <DetailItem label="Nama Ayah" value="Budi" />
            <DetailItem label="NIK Ayah" value="3302042001040001" />
            <DetailItem label="Nama Ibu" value="Siti" />
            <DetailItem label="NIK Ibu" value="3302042001040001" />
          </section>
        </CardContent>
      </Card>

      <Card>
        <CardHeader>
          <CardTitle>Alamat</CardTitle>
        </CardHeader>
        <CardContent className={styles["info__card__content"]}>
          <section className={styles["info__card__section"]}>
            <DetailItem label="Dusun" value="Mangsit" />
            <DetailItem label="RT" value="01" />
            <DetailItem label="RW" value="05" />
            <DetailItem label="Nomor Telepon" value="081234567890" />
            <DetailItem label="Email" value="asep@gmail.com" />
          </section>
        </CardContent>
      </Card>

      <Card>
        <CardHeader>
          <CardTitle>Status Pernikahan</CardTitle>
        </CardHeader>
        <CardContent className={styles["info__card__content"]}>
          <section className={styles["info__card__section"]}>
            <DetailItem label="Status nikah" value="Kawin" />
            <DetailItem label="No Akta Nikah (Buku Nikah)" value="1234567890" />
            <DetailItem label="Tanggal Pernikahan" value="12/19/2023" />
            <DetailItem label="Akta Perceraian" value="-" />
            <DetailItem label="Tanggal Perceraian" value="-" />
          </section>
        </CardContent>
      </Card>

      <Card>
        <CardHeader>
          <CardTitle>Data Kesehatan</CardTitle>
        </CardHeader>
        <CardContent className={styles["info__card__content"]}>
          <section className={styles["info__card__section"]}>
            <DetailItem label="Golongan Darah" value="AB" />
            <DetailItem label="Cacat" value="Tidak Cacat" />
            <DetailItem label="Sakit Menahun" value="Tidak Ada/Tidak Sakit" />
          </section>
          <section className={styles["info__card__section"]}>
            <DetailItem label="Akseptor KB" value="Pil" />
            <DetailItem label="Pilih Asuransi" value="Tidak/Belum Punya" />
            <DetailItem label="Nomor BPJS Ketenagakerjaan" value="1234567890" />
          </section>
        </CardContent>
      </Card>

      <Card>
        <CardHeader>
          <CardTitle>Lainnya</CardTitle>
        </CardHeader>
        <CardContent className={styles["info__card__content"]}>
          <section className={styles["info__card__section"]}>
            <DetailItem label="Lainnya" value="Arab, Latin, dan Daerah" />
            <DetailItem label="Catatan" value="-" />
          </section>
        </CardContent>
      </Card>
    </section>
  );
}
