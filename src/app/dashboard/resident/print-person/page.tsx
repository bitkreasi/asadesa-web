import LayoutDashboard from "@/app/ui/layouts/dashboard/layout-dashboard";
import PrintPerson from "../print-person";

const DashboardPrintPerson = () => {
  return (
    <LayoutDashboard>
      <PrintPerson />
    </LayoutDashboard>
  );
};

export default DashboardPrintPerson;
