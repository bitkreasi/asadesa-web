"use client";

import Header from "@/app/ui/components/header/header";
import styles from "./print-person.module.scss";
import { useState } from "react";
import Button from "@/app/ui/components/button/button";
import IconPrinter from "@/app/ui/icons/icon-printer";
import Input from "@/app/ui/components/input/input";
import { dummyResident } from "@/app/shared";

export function PrintPerson() {
  const [search, setSearch] = useState("");
  const [items, setItems] = useState("");
  return (
    <section>
      <div className={styles["header"]}>
        <Header
          items={[
            {
              id: "dashboard",
              name: "Beranda",
              link: "/dashboard",
            },
            {
              id: "resident",
              name: "Data Penduduk",
              link: "/dashboard/resident",
            },
            {
              id: "add-resident",
              name: "Cetak Surat",
              link: "/dashboard/resident/print-person",
            },
          ]}
          isBack={true}
          title="Cetak Surat"
          subTitle=""
        />

        <div className={styles["header__button"]}>
          <div>
            <Button
              text="Cetak"
              variant="solid"
              color="primary"
              size="md"
              prefix={
                <IconPrinter
                  width={22}
                  height={22}
                  color="#fff"
                  backgroundColor="#fff"
                />
              }
            />
          </div>
        </div>
      </div>
      <div className={styles["content"]}>
        <div className={styles["main-content"]}>
          {/* TODO: add print letter here */}
        </div>

        <div className={styles["right-content"]}>
          <Input
            variant="dropdown-select"
            placeholder="masukkan nama"
            size="md"
            label="Penduduk"
            helperText=""
            withSearch={true}
            value={search}
            setValue={setSearch}
            items={dummyResident.map((resident) => resident.nama)}
          />

          <Input
            variant="dropdown-select"
            placeholder="Jenis Surat"
            size="md"
            label="Jenis Surat"
            value={items}
            helperText=""
            setValue={setItems}
            withSearch={false}
            items={["Biodata Penduduk", "asdf"]}
          />
        </div>
      </div>
    </section>
  );
}
export default PrintPerson;
