"use client";

import { useState } from "react";
import styles from "./edit-resident.module.scss";
import { ResidentProps } from "@/app/shared";
import Header from "@/app/ui/components/header/header";
import Avatar from "@/app/ui/components/avatar/avatar";
import Button from "@/app/ui/components/button/button";
import IconImport from "@/app/ui/icons/icon-import";
import IconTrash from "@/app/ui/icons/icon-trash";
import Input from "@/app/ui/components/input/input";

// TODO: get resident data dummy

interface CurrentResidentProps {
  data: ResidentProps;
}

export function EditResident({ data }: CurrentResidentProps) {
  const [search, setSearch] = useState("");
  const [reportDate, setReportDate] = useState<Date>(
    new Date(data.tanggalLapor)
  );
  const [selectTypeResident, setSelectTypeResident] = useState(
    data.jenisPenduduk
  );
  // ! ERROR: the date type is should be Date type
  const [dateIn, setDateIn] = useState<Date>(new Date(data.tanggalMasuk || ""));
  const [fullname, setFullname] = useState(data.nama);
  const [idNumber, setIdNumber] = useState(data.nik);
  const [familyRole, setFamilyRole] = useState(data.peranDalamKeluarga);
  const [elektronicIdentity, setElektronicIdentity] = useState(
    data.identitasElektronik
  );
  const [recordStatus, setRecordStatus] = useState(data.statusRekam);
  const [idCardTag, setIdCardTag] = useState(data.tagIdCard);
  const [familyCardNumber, setFamilyCardNumber] = useState(
    data.nomorKKSebelumnya
  );
  const [gender, setGender] = useState(data.JenisKelamin);
  const [religion, setReligion] = useState(data.agama);
  const [residentStatus, setResidentStatus] = useState(data.statusPenduduk);
  const [birthNumber, setBirthNumber] = useState(data.nomorAktaKelahiran);
  const [hometown, setHometown] = useState(data.KotaKelahiran);
  const [birthOfDate, setBirthOfDate] = useState<Date>(
    new Date(data.tanggalKelahiran)
  );
  const [timeOfBirth, setTimeOfBirth] = useState(data.waktuKelahiran);
  const [placeOfBirth, setPlaceOfBirth] = useState(data.tempatDilahirkan);
  const [typeOfBirth, setTypeOfBirth] = useState(data.jenisKelahiran);
  const [childOrderFamily, setChildOrderFamily] = useState(data.kelahiranKe);
  const [healtWorker, setHealtWorker] = useState(data.tenagaKesehatan);
  const [birthWeight, setBirthWeight] = useState(data.beratLahir);
  const [birthLength, setBirthLength] = useState(data.panjangLahir);
  const [educationInFamilyCard, setEducationInFamilyCard] = useState(
    data.pendidikanDalamKK
  );
  const [currentEducation, setCurrentEducation] = useState(
    data.pendidikanDitempuh
  );
  const [workStatus, setWorkStatus] = useState(data.pekerjaan);
  const [ethnicGroup, setEthnicGroup] = useState(data.sukuAtauEtnis);
  const [citizenStatus, setCitizenStatus] = useState(data.kewarganegaraan);
  const [passportNumber, setPassportNumber] = useState(data.nomorPaspor);
  const [passportExp, setPassportExp] = useState<Date>(
    new Date(data.tanggalBerakhirPaspor)
  );
  const [fatherName, setFatherName] = useState(data.namaAyah);
  const [motherName, setMotherName] = useState(data.namaIbu);
  const [credentialFather, setCredentialFather] = useState(data.nikAyah);
  const [credentialMother, setCredentialMother] = useState(data.nikIbu);
  const [village, setVillage] = useState(data.dusun);
  const [rt, setRt] = useState(data.rt);
  const [rw, setRw] = useState(data.rw);
  const [phoneNumber, setPhoneNumber] = useState(data.nomorTelepon);
  const [email, setEmail] = useState(data.email);
  const [maritalStatus, setMaritalStatus] = useState(data.statusNikah);
  const [maritalNumber, setMaritalNumber] = useState(data.nomorAktaNikah);
  const [maritalDate, setMaritalDate] = useState<Date>(
    new Date(data.tanggalPernikahan || "")
  );
  const [divorceDate, setDivorceDate] = useState<Date>(
    new Date(data.tanggalCerai || Date.now())
  );
  const [divorceNumber, setDivorceNumber] = useState(data.aktaCerai);
  const [bloodType, setBloodType] = useState(data.golonganDarah);
  const [disabillity, setDisabillity] = useState(data.cacat);
  const [chronicIllness, setChronicIllness] = useState(data.sakitMenahun);
  const [acceptorKB, setAcceptorKB] = useState(data.akseptorKB);
  const [insurance, setInsurance] = useState(data.asuransi);
  const [insuranceNumber, setInsuranceNumber] = useState(data.nomorBPJS);
  const [readLetters, setReadLetters] = useState(data.dapatMembacaHuruf);
  const [notes, setNotes] = useState(data.catatan);

  return (
    <section>
      {/* header */}
      <div className={styles["header"]}>
        <Header
          items={[
            {
              id: "dashboard",
              name: "Beranda",
              link: "/dashboard",
            },
            {
              id: "resident",
              name: "Data Penduduk",
              link: "/dashboard/resident",
            },
            {
              id: "detail-resident",
              name: "Detail Penduduk",
              link: `/dashboard/resident/detail/${data.id}`,
            },
            {
              id: "edit-resident",
              name: "Edit",
              link: `/dashboard/resident/edit/${data.id}`,
            },
          ]}
          isBack={true}
          title="Edit Penduduk"
          subTitle=""
        />
      </div>

      {/* content */}
      <div className={styles["container"]}>
        {/* profile */}
        <div className={styles["container__base"]}>
          <div className={styles["container__base--profile"]}>
            <div className={styles["container__base--profile__info"]}>
              {data.imageUrl ? (
                <Avatar variant="photo" source={data.imageUrl} size="xxl" />
              ) : (
                <Avatar variant="icon" size="xxl" />
              )}
              <div className={styles["container__base--profile__info--text"]}>
                <span
                  className={
                    styles["container__base--profile__info--text__title"]
                  }
                >
                  Foto profile
                </span>
                <span
                  className={
                    styles["container__base--profile__info--text__sub-title"]
                  }
                >
                  PNG, JPEG dibawah 2MB
                </span>
              </div>
            </div>
            <div className={styles["container__base--profile__button"]}>
              <div>
                <Button
                  text="Unggah Foto"
                  color="primary"
                  variant="solid"
                  prefix={
                    <IconImport
                      width={22}
                      height={22}
                      backgroundColor="white"
                      color="white"
                    />
                  }
                />
              </div>
              <div>
                <Button
                  text="Hapus Foto"
                  color="error"
                  variant="outline"
                  prefix={
                    <IconTrash
                      width={22}
                      height={22}
                      backgroundColor="#F42E2E"
                      color="#F42E2E"
                    />
                  }
                />
              </div>
            </div>
          </div>
        </div>

        {/* type of population */}
        <div className={styles["container__base"]}>
          <div className={styles["container__base--type-population"]}>
            <Header title="Jenis Penduduk" subTitle="" size="lg" />
            <div className={styles["container__base--type-population__input"]}>
              <Input
                label="Tanggal lapor"
                variant="datetime"
                helperText=""
                placeholder="Pilih tanggal"
                size="sm"
                date={new Date(reportDate)}
                setDate={setReportDate}
              />{" "}
              <Input
                variant="dropdown-select"
                label="Jenis penduduk"
                helperText=""
                size="sm"
                items={["Penduduk Masuk", "Penduduk Lahir"]}
                value={selectTypeResident}
                setValue={setSelectTypeResident}
                withSearch={false}
              />
              <Input
                label="Tanggal masuk"
                variant="datetime"
                helperText="Diisi jika jenis penduduk adalah penduduk masuk."
                placeholder="Pilih tanggal"
                size="sm"
                isDisable={
                  selectTypeResident === "Penduduk Masuk" ? false : true
                }
                date={dateIn}
                setDate={setDateIn}
              />
            </div>
          </div>
        </div>

        {/* personal info */}
        <div className={styles["container__base"]}>
          <div className={styles["container__base--personal-info"]}>
            <Header title="Data Diri" size="lg" subTitle="" />
            <div className={styles["container__base--personal-info__content"]}>
              {/* first */}
              <div
                className={
                  styles["container__base--personal-info__content--first"]
                }
              >
                <Input
                  label="Nama lengkap"
                  variant="text-field"
                  helperText=""
                  size="sm"
                  placeholder="Masukkan nama lengkap"
                  value={fullname}
                  setValue={setFullname}
                />
                <Input
                  label="Nomor NIK"
                  variant="text-field"
                  helperText=""
                  size="sm"
                  placeholder="0"
                  textFieldType="number"
                  value={idNumber}
                  setValue={setIdNumber}
                />
                <Input
                  label="Peran dalam keluarga"
                  variant="dropdown-select"
                  helperText=""
                  size="sm"
                  placeholder="Pilih peran"
                  items={[
                    "Kepala Keluarga",
                    "Suami",
                    "Istri",
                    "Anak",
                    "Menantu",
                    "Cucu",
                    "Orang Tua",
                    "Mertua",
                    "Famili Lain",
                    "Pembantu",
                    "Lainnya",
                  ]}
                  value={familyRole}
                  setValue={setFamilyRole}
                  isDropdownFiltered={true}
                />
              </div>

              {/* second */}
              <div
                className={
                  styles["container__base--personal-info__content--second"]
                }
              >
                <Header
                  size="md"
                  title="Status Kepemilikan Identitas"
                  subTitle=""
                />
                <div
                  className={
                    styles[
                      "container__base--personal-info__content--second__base"
                    ]
                  }
                >
                  <Input
                    variant="text-field"
                    label="Wajib identitas"
                    isDisable={true}
                    helperTextCol="Tidak bisa diubah"
                    helperText=""
                    size="sm"
                    placeholder="Belum wajib"
                  />
                  <Input
                    variant="dropdown-select"
                    label="Identitas elektronik"
                    helperText=""
                    size="sm"
                    placeholder="Pilih identitas"
                    items={["Belum", "KTP-EL", "KIA"]}
                    value={elektronicIdentity}
                    setValue={setElektronicIdentity}
                    withSearch={false}
                  />
                  <Input
                    variant="dropdown-select"
                    label="Status rekam"
                    helperText=""
                    size="sm"
                    placeholder="Pilih status rekam"
                    items={[
                      "Belum Rekam",
                      "Sudah Rekam",
                      "Card Printed",
                      "Print Ready Record",
                      "Card Shipped",
                      "Sent For Card Printing",
                      "Card Issued",
                      "Belum Wajib",
                    ]}
                    value={recordStatus}
                    setValue={setRecordStatus}
                    withSearch={false}
                  />
                  <Input
                    label="Tag ID card"
                    variant="text-field"
                    helperText=""
                    size="sm"
                    placeholder="Masukkan ID card"
                    value={idCardTag}
                    setValue={setIdCardTag}
                  />
                </div>
              </div>

              {/* third */}
              <div
                className={
                  styles["container__base--personal-info__content--third"]
                }
              >
                <Input
                  label="Nomor KK sebelumnya"
                  variant="text-field"
                  helperText=""
                  size="sm"
                  placeholder="Masukkan nomor"
                  textFieldType="number"
                  value={familyCardNumber}
                  setValue={setFamilyCardNumber}
                />
                <Input
                  variant="dropdown-select"
                  label="Jenis kelamin"
                  helperText=""
                  size="sm"
                  placeholder="Pilih jenis kelamin"
                  items={["Laki laki", "Perempuan"]}
                  value={gender}
                  setValue={setGender}
                  withSearch={false}
                />
                <Input
                  variant="dropdown-select"
                  label="Agama"
                  helperText=""
                  size="sm"
                  placeholder="Pilih agama"
                  items={[
                    "Islam",
                    "Kristen",
                    "Katholik",
                    "Hindu",
                    "Budha",
                    "Konghucu",
                    "Kepercayaan Kepada Tuhan YME/Lainnya",
                  ]}
                  value={religion}
                  setValue={setReligion}
                  withSearch={false}
                />
                <Input
                  variant="dropdown-select"
                  label="Status penduduk"
                  helperText=""
                  size="sm"
                  placeholder="Pilih peran"
                  items={["Tetap", "Tidak Tetap"]}
                  value={residentStatus}
                  setValue={setResidentStatus}
                  withSearch={false}
                />
              </div>
            </div>
          </div>
        </div>

        {/* birth date */}
        <div className={styles["container__base"]}>
          <div className={styles["container__base--birth-date"]}>
            <Header title="Data Kelahiran" subTitle="" size="lg" />
            <div className={styles["container__base--birth-date__content"]}>
              {/* first */}
              <div
                className={
                  styles["container__base--birth-date__content--first"]
                }
              >
                <Input
                  label="Nomor akta kelahiran"
                  variant="text-field"
                  helperText=""
                  placeholder="Masukkan nomor"
                  size="sm"
                  textFieldType="number"
                  value={birthNumber}
                  setValue={setBirthNumber}
                />
                <Input
                  label="Kota kelahiran"
                  variant="text-field"
                  helperText=""
                  placeholder="Masukkan nama kota"
                  size="sm"
                  value={placeOfBirth}
                  setValue={setPlaceOfBirth}
                />
              </div>

              {/* second */}
              <div
                className={
                  styles["container__base--birth-date__content--second"]
                }
              >
                <Input
                  label="Tanggal lahir"
                  variant="datetime"
                  helperText=""
                  placeholder="Pilih tanggal"
                  size="sm"
                  date={birthOfDate}
                  setDate={setBirthOfDate}
                />
                <Input
                  label="Waktu kelahiran"
                  variant="time"
                  helperText=""
                  placeholder="Pilih waktu"
                  size="sm"
                  value={timeOfBirth}
                  setValue={setTimeOfBirth}
                />
                <Input
                  label="Tempat dilahirkan"
                  variant="text-field"
                  helperText=""
                  placeholder="Pilih tempat"
                  size="sm"
                  value={hometown}
                  setValue={setHometown}
                />
              </div>

              {/* third */}
              <div
                className={
                  styles["container__base--birth-date__content--third"]
                }
              >
                <Input
                  variant="dropdown-select"
                  label="Jenis kelahiran"
                  helperText=""
                  size="sm"
                  placeholder="Pilih jenis kelahiran"
                  items={["Tunggal", "Kembar 2", "Kembar 3", "Kembar 4"]}
                  value={typeOfBirth}
                  setValue={setTypeOfBirth}
                  withSearch={false}
                />

                <Input
                  variant="text-field"
                  label="Anak ke"
                  helperText=""
                  size="sm"
                  placeholder="Anak ke-"
                  value={childOrderFamily}
                  setValue={setChildOrderFamily}
                  textFieldType="number"
                />

                <Input
                  variant="dropdown-select"
                  label="Tenaga Kesehatan"
                  helperText=""
                  size="sm"
                  placeholder="Pilih tenaga kesehatan"
                  items={["Dokter", "Bidan/Perawat", "Dukun", "Lainnya"]}
                  value={healtWorker}
                  setValue={setHealtWorker}
                  withSearch={false}
                />
              </div>

              {/* fourth */}
              <div
                className={
                  styles["container__base--birth-date__content--fourth"]
                }
              >
                <Input
                  variant="text-field"
                  label="Berat lahir"
                  helperText=""
                  size="sm"
                  placeholder="Berat lahir"
                  value={birthWeight}
                  setValue={setBirthWeight}
                  textFieldType="number"
                  suffix="Gram"
                />
                <Input
                  variant="text-field"
                  label="Panjang lahir"
                  helperText=""
                  size="sm"
                  placeholder="Panjang lahir"
                  value={birthLength}
                  setValue={setBirthLength}
                  textFieldType="number"
                  suffix="cm"
                />
              </div>
            </div>
          </div>
        </div>

        {/* education and work */}
        <div className={styles["container__base"]}>
          <div className={styles["container__base--education-work"]}>
            <Header title="Pendidikan dan Pekerjaan" subTitle="" size="lg" />
            <div className={styles["container__base--education-work__content"]}>
              <Input
                variant="dropdown-select"
                label="Pendidikan dalam KK"
                helperText=""
                size="sm"
                placeholder="Pilih pendidikan (Dalam KK)"
                items={[
                  "Tidak/Belum Sekolah",
                  "Belum Tamat SD atau Sederajat",
                  "Tamat SD atau Sederajat",
                  "SLTP atau Sederajat",
                  "SLTA atau Sederajat",
                  "Diploma I / II",
                  "Akademi / Diploma III / S.Muda",
                  "Diploma IV / Strata I",
                  "Strata II",
                  "Strata III",
                ]}
                value={educationInFamilyCard}
                setValue={setEducationInFamilyCard}
                withSearch={false}
              />
              <Input
                variant="dropdown-select"
                label="Pendidikan yang sedang ditempuh"
                helperText=""
                size="sm"
                placeholder="Pilih pendidikan"
                items={[
                  "Belum Masuk TK/Kelompok Bermain",
                  "Sedang TK/Kelompok Bermain",
                  "Sedang SD/Sederajat",
                  "Tidak Tamat SD/Sederajat",
                  "Sedang SLTP/Sederajat",
                  "Sedang SLTA/Sederajat",
                  "Sedang D-1/Sederajat",
                  "Sedang D-2/Sederajat",
                  "Sedang D-3/Sederajat",
                  "Sedang S-1/Sederajat",
                  "Sedang S-2/Sederajat",
                  "Sedang S-3/Sederajat",
                  "Sedang SLB A/Sederajat",
                  "Sedang SLB B/Sederajat",
                  "Sedang SLB C/Sederajat",
                  "Tidak Dapat Membaca dan Menulis Huruf Latin / Arab",
                  "Tidak Sedang Sekolah",
                ]}
                value={currentEducation}
                setValue={setCurrentEducation}
                withSearch={false}
              />

              <Input
                variant="dropdown-select"
                label="Pekerjaan"
                helperText=""
                size="sm"
                placeholder="Pilih pekerjaan"
                items={[
                  "Belum/Tidak Bekerja",
                  "Mengurus Rumah Tangga",
                  "Pelajar/Mahasiswa",
                  "Pensiunan",
                  "Pegawai Negeri Sipil (PNS)",
                  "Tentara Nasional Indonesia (TNI)",
                  "Kepolisian RI (POLRI)",
                  "Perdagangan",
                  "Petani/Pekebun",
                  "Peternak",
                  "Nelayan/Perikanan",
                  "Industri",
                  "Konstruksi",
                  "Transportasi",
                  "Karyawan Swasta",
                  "Karyawan BUMN",
                  "Karyawan BUMD",
                  "Karyawan Honorer",
                  "Buruh Harian Lepas",
                  "Buruh Tani/Perkebunan",
                  "Buruh Nelayan/Perikanan",
                  "Buruh Peternakan",
                  "Pembantu Rumah Tangga",
                  "Tukang Cukur",
                  "Tukang Listrik",
                  "Tukang Batu",
                  "Tukang Kayu",
                  "Tukang Sol Sepatu",
                  "Tukang Las/Pandai Besi",
                  "Tukang Jahit",
                  "Penata Rias",
                  "Penata Busana",
                  "Penata Rambut",
                  "Mekanik",
                  "Tukang Gigi",
                  "Seniman",
                  "Tabib",
                  "Paraji",
                  "Perancang Busana",
                  "Penterjemah",
                  "Imam Masjid",
                  "Pendeta",
                  "Pastor",
                  "Wartawan",
                  "Ustadz/Mubaligh",
                  "Juru Masak",
                  "Promotor Acara",
                  "Anggota DPR-RI",
                  "Anggota DPD",
                  "Anggota BPK",
                  "Presiden",
                  "Wakil Presiden",
                  "Anggota Mahkamah Konstitusi",
                  "Anggota Kabinet/Kementerian",
                  "Duta Besar",
                  "Gubernur",
                  "Wakil Gubernur",
                  "Bupati",
                  "Wakil Bupati",
                  "Walikota",
                  "Wakil Walikota",
                  "Anggota DPRD Propinsi",
                  "Anggota DPRD Kabupaten/Kota",
                  "Dosen",
                  "Guru",
                  "Pilot",
                  "Pengacara",
                  "Notaris",
                  "Arsitek",
                  "Akuntan",
                  "Konsultan",
                  "Dokter",
                  "Bidan",
                  "Perawat",
                  "Apoteker",
                  "Psikiater/Psikolog",
                  "Penyiar Televisi",
                  "Penyiar Radio",
                  "Pelaut",
                  "Peneliti",
                  "Sopir",
                  "Pialang",
                  "Paranormal",
                  "Pedagang",
                  "Perangkat Desa",
                  "Kepala Desa",
                  "Biarawati",
                  "Lainnya",
                ]}
                value={workStatus}
                setValue={setWorkStatus}
                withSearch={false}
              />
            </div>
          </div>
        </div>

        {/* citizenship data */}
        <div className={styles["container__base"]}>
          <div className={styles["container__base--citizenship-data"]}>
            <Header title="Data Kewarganegaraan" subTitle="" size="lg" />
            <div
              className={styles["container__base--citizenship-data__content"]}
            >
              <Input
                variant="dropdown-select"
                label="Suku/Etnis"
                helperText=""
                size="sm"
                placeholder="Pilih suku/etnis"
                items={[
                  "Aceh",
                  "Alas",
                  "Alor",
                  "Ambon",
                  "Ampana",
                  "Anak Dalam",
                  "Aneuk Jamee",
                  "Angkola",
                  "Arab: Orang Hanharami",
                  "Aru",
                  "Asmat",
                  "Baduy",
                  "Bajau",
                  "Balantak",
                  "Bali",
                  "Banggai",
                  "Banjar",
                  "Banten",
                  "Bare'e",
                  "Batak",
                  "Batak Karo",
                  "Batik",
                  "Bawean",
                  "Bentong",
                  "Berau",
                  "Basemah",
                  "Betawi",
                  "Bima",
                  "Bolang Monggondow",
                  "Bonai",
                  "Boti",
                  "Bugis",
                  "Bulungan",
                  "Bungku",
                  "Buol",
                  "Buru",
                  "Buton",
                  "Cham",
                  "Cirebon",
                  "Dairi",
                  "Damal",
                  "Dampeles",
                  "Dani",
                  "Dayak",
                  "Dompu",
                  "Dondo",
                  "Dongga",
                  "Donggo",
                  "Duri",
                  "Eropa",
                  "Flores",
                  "Gayo",
                  "Gorontalo",
                  "Gumai",
                  "India",
                  "Jambi",
                  "Jawa",
                  "Jepang",
                  "Kaili",
                  "Kampar",
                  "Kaur",
                  "Kayu Agung",
                  "Kei",
                  "Kerinci",
                  "Kluet",
                  "Komering",
                  "Konjo Pegunungan",
                  "Konjo Pesisir",
                  "Korea",
                  "Koto",
                  "Krui",
                  "Kubu",
                  "Kulawi",
                  "Kutai",
                  "Lamaholot",
                  "Lampung",
                  "Laut",
                  "Lematang",
                  "Lembak",
                  "Lintang",
                  "Lom",
                  "Lore",
                  "Lubu",
                  "Madura",
                  "Makassar",
                  "Mamasa",
                  "Manda",
                  "Mandailing",
                  "Mekongga",
                  "Melayu",
                  "Mentawai",
                  "Minahasa",
                  "Minangkabau",
                  "Mongondow",
                  "Mori",
                  "Moro",
                  "Moronene",
                  "Muko-Muko",
                  "Muna",
                  "Muyu",
                  "Ngada",
                  "Nias",
                  "Ocu",
                  "Ogan",
                  "Osing",
                  "Padoe",
                  "Pakistani",
                  "Pakpak",
                  "Palembang",
                  "Pamona",
                  "Papua",
                  "Pasir",
                  "Pattae",
                  "Peranakan",
                  "Pesisi",
                  "Penosokan",
                  "Pubian",
                  "Rawa",
                  "Rejang",
                  "Rohingya",
                  "Rongga",
                  "Rote",
                  "Sabu",
                  "Saluan",
                  "Sambas",
                  "Samin",
                  "Sangi",
                  "Sasak",
                  "Sekak Bangka",
                  "Sekayu",
                  "Semendo",
                  "Serawai",
                  "Sigulai",
                  "Simalungun",
                  "Simeulue",
                  "Suluk",
                  "Sumba",
                  "Sumbawa",
                  "Sunda",
                  "Sungkai",
                  "Talang Mamak",
                  "Talau",
                  "Tamiang",
                  "Tengger",
                  "Ternate",
                  "Tidore",
                  "Tidung",
                  "Timor",
                  "Tionghoa",
                  "Toba",
                  "Tojo",
                  "Tolaki",
                  "Toli Toli",
                  "Tomini",
                  "Toraja",
                  "Ulu",
                  "Una-una",
                  "Wolio",
                ]}
                value={ethnicGroup}
                setValue={setEthnicGroup}
                withSearch={true}
                isDropdownFiltered={true}
              />
              <Input
                variant="dropdown-select"
                label="Status warga negara"
                helperText=""
                size="sm"
                placeholder="Pilih warga negara"
                items={["WNI", "WNA", "Dua Kewarganegaraan"]}
                value={citizenStatus}
                setValue={setCitizenStatus}
                withSearch={false}
              />
              <Input
                label="Nomor paspor"
                variant="text-field"
                helperText=""
                size="sm"
                placeholder="Masukkan nomor "
                value={passportNumber}
                setValue={setPassportNumber}
                textFieldType="number"
              />
              <Input
                label="Tanggal berakhir paspor"
                variant="datetime"
                helperText=""
                placeholder="Pilih tanggal"
                size="sm"
                date={passportExp}
                setDate={setPassportExp}
              />
            </div>
          </div>
        </div>

        {/* parental data */}
        <div className={styles["container__base"]}>
          <div className={styles["container__base--parental-data"]}>
            <Header title="Data Orang Tua" subTitle="" size="lg" />
            <div className={styles["container__base--parental-data__content"]}>
              {/* first */}
              <div
                className={
                  styles["container__base--parental-data__content--first"]
                }
              >
                <Input
                  label="Nama ayah"
                  variant="text-field"
                  helperText=""
                  size="sm"
                  placeholder="Masukkan nama ayah"
                  value={fatherName}
                  setValue={setFatherName}
                />
                <Input
                  label="NIK Ayah"
                  variant="text-field"
                  size="sm"
                  placeholder="Masukkan nomor NIK"
                  helperText=""
                  textFieldType="number"
                  value={credentialFather}
                  setValue={setCredentialFather}
                />
              </div>

              {/* second */}
              <div
                className={
                  styles["container__base--parental-data__content--second"]
                }
              >
                <Input
                  label="Nama ibu"
                  variant="text-field"
                  helperText=""
                  size="sm"
                  placeholder="Masukkan nama ibu"
                  value={motherName}
                  setValue={setMotherName}
                />
                <Input
                  label="NIK Ibu"
                  variant="text-field"
                  size="sm"
                  placeholder="Masukkan nomor NIK"
                  helperText=""
                  textFieldType="number"
                  value={credentialMother}
                  setValue={setCredentialMother}
                />
              </div>
            </div>
          </div>
        </div>

        {/* address */}
        <div className={styles["container__base"]}>
          <div className={styles["container__base--address"]}>
            <Header title="Alamat" subTitle="" size="lg" />
            <div className={styles["container__base--address__content"]}>
              {/* first */}
              <div
                className={styles["container__base--address__content--first"]}
              >
                <Input
                  label="Dusun"
                  variant="dropdown-select"
                  helperText=""
                  size="sm"
                  placeholder="Pilih Dusun"
                  value={village}
                  setValue={setVillage}
                  items={["Mangsit", "Senggigi", "Kerandang", "Loco"]}
                  withSearch={false}
                />
                <Input
                  label="RT"
                  variant="text-field"
                  helperText=""
                  size="sm"
                  placeholder="Masukkan RT"
                  value={rt}
                  setValue={setRt}
                  textFieldType="number"
                />
                <Input
                  label="RW"
                  variant="text-field"
                  helperText=""
                  size="sm"
                  placeholder="Masukkan RW"
                  value={rw}
                  setValue={setRw}
                  textFieldType="number"
                />
              </div>

              {/* second */}
              <div
                className={styles["container__base--address__content--second"]}
              >
                <Input
                  label="Nomor telepon"
                  variant="text-field"
                  helperText=""
                  size="sm"
                  placeholder="Masukkan nomor telepon"
                  value={phoneNumber}
                  setValue={setPhoneNumber}
                  textFieldType="number"
                />
                <Input
                  label="Email"
                  variant="text-field"
                  helperText=""
                  size="sm"
                  placeholder="Masukkan email"
                  value={email}
                  setValue={setEmail}
                />
              </div>
            </div>
          </div>
        </div>

        {/* marriage status*/}
        <div className={styles["container__base"]}>
          <div className={styles["container__base--marriage-status"]}>
            <Header title="Status Pernikahan" subTitle="" size="lg" />
            <div
              className={styles["container__base--marriage-status__content"]}
            >
              {/* first */}
              <div
                className={
                  styles["container__base--marriage-status__content--first"]
                }
              >
                <Input
                  variant="dropdown-select"
                  label="Status nikah"
                  helperText=""
                  size="sm"
                  placeholder="Pilih status"
                  items={["Belum Kawin", "Kawin", "Cerai Hidup", "Cerai Mati"]}
                  value={maritalStatus}
                  setValue={setMaritalStatus}
                  withSearch={false}
                />
                <Input
                  label="No. Akta Nikah (Buku Nikah)"
                  variant="text-field"
                  helperText=""
                  size="sm"
                  placeholder="Masukkan No Akta Nikah"
                  value={maritalNumber}
                  setValue={setMaritalNumber}
                  isDisable={maritalStatus === "Kawin" ? false : true}
                />
                <Input
                  label="Tanggal Pernikahan"
                  variant="datetime"
                  helperText=""
                  placeholder="Pilih tanggal"
                  size="sm"
                  date={maritalDate}
                  setDate={setMaritalDate}
                  isDisable={maritalStatus === "Kawin" ? false : true}
                />
              </div>

              {/* second */}
              <div
                className={
                  styles["container__base--marriage-status__content--second"]
                }
              >
                <Input
                  label="Akta Perceraian"
                  variant="text-field"
                  helperText=""
                  placeholder="Akta Perceraian"
                  size="sm"
                  value={divorceNumber}
                  setValue={setDivorceNumber}
                  isDisable={maritalStatus === "Cerai Hidup" ? false : true}
                />
                <Input
                  label="Tanggal Perceraian"
                  variant="datetime"
                  helperText=""
                  size="sm"
                  placeholder="Pilih tanggal"
                  date={divorceDate}
                  setDate={setDivorceDate}
                  textFieldType="number"
                  isDisable={maritalStatus === "Cerai Hidup" ? false : true}
                />
              </div>
            </div>
          </div>
        </div>

        {/* health data */}
        <div className={styles["container__base"]}>
          <div className={styles["container__base--health-data"]}>
            <Header title="Data Kesehatan" subTitle="" size="lg" />
            <div className={styles["container__base--health-data__content"]}>
              {/* first */}
              <div
                className={
                  styles["container__base--health-data__content--first"]
                }
              >
                <Input
                  variant="dropdown-select"
                  label="Golongan darah"
                  helperText=""
                  size="sm"
                  placeholder="Pilih golongan darah"
                  items={[
                    "A",
                    "B",
                    "AB",
                    "O",
                    "A+",
                    "A-",
                    "B+",
                    "B-",
                    "AB+",
                    "AB-",
                    "O+",
                    "O-",
                  ]}
                  value={bloodType}
                  setValue={setBloodType}
                  withSearch={false}
                />
                <Input
                  variant="dropdown-select"
                  label="Cacat"
                  helperText=""
                  size="sm"
                  placeholder="Pilih jenis cacat"
                  items={[
                    "Cacat Fisik",
                    "Cacat Netra/Buta",
                    "Cacat Rungu",
                    "Cacat Mental/Jiwa",
                    "Cacat Fisik dan Mental",
                    "Cacat Lainnya",
                    "Tidak Cacat",
                  ]}
                  value={disabillity}
                  setValue={setDisabillity}
                  withSearch={false}
                />
                <Input
                  variant="dropdown-select"
                  label="Sakit menahun"
                  helperText=""
                  size="sm"
                  placeholder="Pilih sakit menahun"
                  items={[
                    "Jantung",
                    "Lever",
                    "Paru-paru",
                    "Kanker",
                    "Stroke",
                    "Diabetes Melitus",
                    "Ginjal",
                    "Malaria",
                    "Lepra/Kusta",
                    "HIV/AIDS",
                    "TBC",
                    "Asma",
                    "Tidak ada/Tidak Sakit",
                  ]}
                  value={chronicIllness}
                  setValue={setChronicIllness}
                  withSearch={false}
                />
              </div>

              {/* second */}
              <div
                className={
                  styles["container__base--health-data__content--second"]
                }
              >
                <Input
                  variant="dropdown-select"
                  label="Akseptor KB"
                  helperText=""
                  size="sm"
                  placeholder="Pilih cara KB saat ini"
                  items={[
                    "Pil",
                    "IUD",
                    "Suntik",
                    "Kondom",
                    "Susuk KB",
                    "Sterilisasi Wanita",
                    "Sterilisasi Pria",
                    "Lainnya",
                    "Tidak Menggunakan",
                  ]}
                  value={acceptorKB}
                  setValue={setAcceptorKB}
                  withSearch={false}
                />
                <Input
                  variant="dropdown-select"
                  label="Pilih asuransi"
                  helperText=""
                  size="sm"
                  placeholder="Masukkan Asuransi"
                  items={[
                    "Tidak/Belum Punya",
                    "BPJS Penerima Bantuan Iuran",
                    "BPJS Non Penerima Bantuan Iuran",
                    "Asuransi Lainnya",
                  ]}
                  value={insurance}
                  setValue={setInsurance}
                  withSearch={false}
                />
                <Input
                  label="Nomor BPJS Ketenagakerjaan"
                  variant="text-field"
                  helperText=""
                  size="sm"
                  placeholder="Masukkan nomor"
                  value={insuranceNumber}
                  setValue={setInsuranceNumber}
                />
              </div>
            </div>
          </div>
        </div>

        {/* others */}
        <div className={styles["container__base"]}>
          <div className={styles["container__base--others"]}>
            <Header title="Lainnya" subTitle="" size="lg" />
            <div className={styles["container__base--others__content"]}>
              <Input
                label="Dapat membaca huruf"
                variant="dropdown-select"
                items={[
                  "Latin",
                  "Daerah",
                  "Arab",
                  "Arab dan Latin",
                  "Arab dan Daerah",
                  "Arab, Latin dan Daerah",
                ]}
                helperText=""
                size="sm"
                placeholder="Pilih "
                value={readLetters}
                setValue={setReadLetters}
                withSearch={false}
              />
              <Input
                label="Catatan"
                variant="text-area"
                helperText=""
                size="sm"
                placeholder="Masukkan catatan"
                value={notes}
                setValue={setNotes}
              />
            </div>
          </div>
        </div>

        {/* button */}
        <div className={styles["container__button"]}>
          <div>
            <Button
              text="Batal"
              color="neutral"
              variant="outline"
              size="lg"
              // TODO: implement cancle button
              onHandleClick={() => console.log("cancle")}
            />
          </div>
          <div>
            <Button
              text="Simpan"
              color="primary"
              variant="solid"
              size="lg"
              // TODO: implement save button
              onHandleClick={() => console.log("save")}
            />
          </div>
        </div>
      </div>
    </section>
  );
}

export default EditResident;
