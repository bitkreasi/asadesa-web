import { commonDescription, sufixTitle } from "@/app/shared";
import LayoutDashboard from "@/app/ui/layouts/dashboard/layout-dashboard";
import { Metadata } from "next";
import styles from "./resident.module.scss";
import Resident from "./_components/resident";
import { publicFetch } from "@/utils/fetch";
import { buildResidentSearchParams } from "./helper";

export const metadata: Metadata = {
  title: `Data Penduduk ${sufixTitle}`,
  description: `${commonDescription}`,
};

export type ResidentResponse = {
  age: number;
  id: string;
  fullname: string;
  nik: string;
  village: string;
  rt: string;
  rw: string;
  job: string;
  birth_date: string;
  marriage_status: string;
};

export interface ResidentSearchParams {
  genders?: string[] | string;
  villages?: string[] | string;
  status?: string[] | string;
}

export default async function ResidentPage({
  searchParams,
}: {
  searchParams: ResidentSearchParams;
}) {
  const residents = await publicFetch<ResidentResponse[]>(
    "/residents?" + buildResidentSearchParams(searchParams),
    { cache: "no-store" },
  );
  const genders = await publicFetch<string[]>("/common/gender");
  const residentStatus = await publicFetch<string[]>("/common/resident-status");
  const residentVillages = await publicFetch<{ name: string }[]>(
    "/common/resident-villages",
  );
  return (
    <LayoutDashboard>
      <section>
        <div className={styles["container"]}>
          <div className={`${styles["main__content"]}`}>
            <Resident
              data={residents?.data ?? []}
              filter={{
                gender: genders.data ?? [],
                residentStatus: residentStatus.data ?? [],
                residentVillages: residentVillages.data ?? [],
              }}
              filterDefaults={searchParams}
            />
          </div>
        </div>
      </section>
    </LayoutDashboard>
  );
}
