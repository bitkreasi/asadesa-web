"use client";

import React from "react";
import styles from "./empty.module.scss";
import {
  EmptyState,
  EmptyStateAction,
  EmptyStateDescription,
  EmptyStateTitle,
} from "../../components/empty-state/empty-state";
import {
  Dialog,
  DialogClose,
  DialogContent,
  DialogFooter,
  DialogTrigger,
} from "@/app/ui/components/dialog/dialog";
import Button from "@/app/ui/components/button/button";
import IconImport from "@/app/ui/icons/icon-import";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import {
  AddHouseholdConfirmDialog,
  AddHouseholdDialog,
  DownloadDialog,
  ImportDialog,
  addHouseholdSchema,
} from "./header";
import IconAddCircle from "@/app/ui/icons/icon-add-circle";

export default function EmptyDataTable() {
  const form = useForm({
    resolver: yupResolver(addHouseholdSchema),
    defaultValues: {
      kepala_rumah_tangga: "",
      bdt: "",
      terdaftar_dtks: false,
    },
  });
  const [dialogType, setDialogType] = React.useState<"import" | "download">(
    "import",
  );
  const [addHouseholdDialogType, setAddHouseholdDialogType] = React.useState<
    "form" | "confirm"
  >("form");
  return (
    <div className={styles["content__empty"]}>
      <EmptyState>
        <EmptyStateTitle>Data rumah tangga belum tersedia.</EmptyStateTitle>
        <EmptyStateDescription>
          Tidak ada hasil atau data untuk ditampilkan di sini. Silakan pilih
          tindakan di bawah untuk melanjutkan.
        </EmptyStateDescription>
        <EmptyStateAction>
          <Dialog onClose={() => setDialogType("import")}>
            <DialogTrigger>
              <Button
                text="Import Data Rumah Tangga"
                variant="outline"
                prefix={
                  <IconImport color="#4163E7" backgroundColor="#4163E7" />
                }
              />
            </DialogTrigger>
            <DialogContent>
              {dialogType === "import" && <ImportDialog />}
              {dialogType === "download" && <DownloadDialog />}
              <DialogFooter className={styles["download__dialog__footer"]}>
                {dialogType === "import" && (
                  <DialogClose style={{ width: "100%" }}>
                    <Button
                      text="Batal"
                      variant="outline"
                      color="primary"
                      width="max"
                    />
                  </DialogClose>
                )}
                {dialogType === "download" && (
                  <Button
                    text="Download Template Data"
                    variant="outline"
                    color="primary"
                    width="max"
                  />
                )}
                <Button
                  onHandleClick={() => {
                    if (dialogType === "import") {
                      setDialogType("download");
                    }
                  }}
                  variant="solid"
                  text={dialogType === "import" ? "Import" : "Download"}
                  width="max"
                />
              </DialogFooter>
            </DialogContent>
          </Dialog>
          <Dialog onClose={() => setAddHouseholdDialogType("form")}>
            <DialogTrigger>
              <Button
                text="Tambah Data Rumah Tangga"
                variant="solid"
                prefix={<IconAddCircle color="#fff" backgroundColor="#fff" />}
              />
            </DialogTrigger>
            {addHouseholdDialogType === "form" ? (
              <AddHouseholdDialog
                form={form}
                setDialogType={setAddHouseholdDialogType}
              />
            ) : (
              <AddHouseholdConfirmDialog form={form} />
            )}
          </Dialog>
        </EmptyStateAction>
      </EmptyState>
    </div>
  );
}
