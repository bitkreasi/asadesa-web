"use client";

import { IndeterminateCheckbox } from "@/app/ui/components/checkbox/v2/checkbox";
import { ColumnDef } from "@tanstack/react-table";
import Image from "next/image";

type dtksStatus = "Terdaftar" | "Tidak Terdaftar";

export interface Household {
  kepala_rumah_tangga: string;
  nomor_rumah_tangga: string;
  nik: string;
  dtks: dtksStatus;
  jumlah_anggota: number;
  dusun: string;
  rt: string;
  rw: string;
}

export const columns: ColumnDef<Household>[] = [
  {
    id: "select",
    header: ({ table }) => {
      return (
        <IndeterminateCheckbox
          {...{
            checked: table.getIsAllRowsSelected(),
            indeterminate: table.getIsSomeRowsSelected(),
            onChange: table.getToggleAllRowsSelectedHandler(),
          }}
        />
      );
    },
    cell: ({ row }) => {
      return (
        <div>
          <IndeterminateCheckbox
            {...{
              checked: row.getIsSelected(),
              disabled: !row.getCanSelect(),
              indeterminate: row.getIsSomeSelected(),
              onChange: row.getToggleSelectedHandler(),
            }}
          />
        </div>
      );
    },
  },
  {
    header: "No",
    cell: ({ row }) => {
      const idx = row.index;
      return <div>{idx + 1}</div>;
    },
  },
  {
    id: "kepala_rumah_tangga",
    filterFn: "fuzzy",
    accessorKey: "kepala_rumah_tangga",
    header: "Kepala Rumah Tangga",
    cell: ({ row }) => {
      return (
        <div style={{ display: "flex", gap: 8, alignItems: "center" }}>
          <Image
            style={{ borderRadius: "9999px" }}
            loading="eager"
            width={32}
            height={32}
            alt={row.getValue("kepala_rumah_tangga")}
            src={"https://source.unsplash.com/random"}
          />
          <p>{row.getValue("kepala_rumah_tangga")}</p>
        </div>
      );
    },
  },
  {
    id: "nomor_rumah_tangga",
    accessorKey: "nomor_rumah_tangga",
    header: "Nomor Rumah Tangga",
  },
  {
    id: "nik",
    accessorKey: "nik",
    header: "NIK",
  },
  {
    id: "dtks",
    accessorKey: "dtks",
    header: "DTKS",
  },
  {
    id: "jumlah_anggota",
    accessorKey: "jumlah_anggota",
    header: "Jumlah Anggota",
  },
  {
    id: "dusun",
    accessorKey: "dusun",
    header: "Dusun",
  },
  { id: "rt", accessorKey: "rt", header: "RT" },
  { id: "rw", accessorKey: "rw", header: "RW" },
];
