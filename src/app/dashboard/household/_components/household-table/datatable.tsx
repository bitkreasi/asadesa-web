"use client";

import React from "react";
import styles from "./datatable.module.scss";
import {
  DataTableProps,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeader,
  TableRow,
  fuzzyFilter,
} from "@/app/ui/components/table/table";
import {
  flexRender,
  getCoreRowModel,
  getFilteredRowModel,
  getPaginationRowModel,
  useReactTable,
} from "@tanstack/react-table";
import { Input } from "@/app/ui/components/input/v2/input";
import IconSearch from "@/app/ui/icons/icon-search";
import {
  DropdownMenu,
  DropdownMenuClose,
  DropdownMenuContent,
  DropdownMenuTrigger,
} from "@/app/ui/components/dropdown-menu/dropdown-menu";
import IconCloseCircle from "@/app/ui/icons/icon-close-circle";
import { Checkbox } from "@/app/ui/components/checkbox/v2/checkbox";
import { Text } from "@/app/ui/components/typography/typography";
import IconArrowLeft from "@/app/ui/icons/icon-arrow-left";
import IconArrowRight from "@/app/ui/icons/icon-arrow-right";
import { Select, SelectItem } from "@/app/ui/components/select/select";
import Link from "next/link";

export function DataTable<TData, TValue>({
  columns,
  data,
}: DataTableProps<TData, TValue>) {
  const [globalFilter, setGlobalFilter] = React.useState("");
  const table = useReactTable({
    data,
    columns,
    filterFns: {
      fuzzy: fuzzyFilter,
    },
    state: {
      globalFilter,
    },
    enableRowSelection: true,
    onGlobalFilterChange: setGlobalFilter,
    globalFilterFn: fuzzyFilter,
    getCoreRowModel: getCoreRowModel(),
    getFilteredRowModel: getFilteredRowModel(),
    getPaginationRowModel: getPaginationRowModel(),
  });

  return (
    <div className={styles["datatable__wrapper"]}>
      {/* start -- search and filter */}
      <div className={styles["search__filter__container"]}>
        <div className={styles["search"]}>
          <Input
            onChange={(e) => setGlobalFilter(String(e.target.value))}
            type="text"
            placeholder="Cari data rumah tangga"
            prefixIcon={<IconSearch />}
          />
        </div>

        <DropdownMenu>
          <DropdownMenuTrigger
            withArrow
            className={styles["filter__dropdown__trigger"]}
          >
            <Text variant="text" size="sm" fontStyle="regular">
              Filter
            </Text>
          </DropdownMenuTrigger>

          <DropdownMenuContent
            title="Filter"
            className={styles["filter__dropdown"]}
          >
            <section className={styles["filter__dropdown__head"]}>
              <Text variant="text" size="md" fontStyle="medium">
                Filter
              </Text>
              <DropdownMenuClose>
                <IconCloseCircle width={24} height={24} />
              </DropdownMenuClose>
            </section>
            <section className={styles["filter__dropdown__content"]}>
              <div className={styles["filter__category"]}>
                <Text variant="text" muted size="sm" fontStyle="medium">
                  Status DTKS
                </Text>
                <div className={styles["filter__sub"]}>
                  <Checkbox />{" "}
                  <Text variant="text" size="sm" fontStyle="regular">
                    Terdaftar
                  </Text>
                </div>
                <div className={styles["filter__sub"]}>
                  <Checkbox />{" "}
                  <Text variant="text" size="sm" fontStyle="regular">
                    Tidak Terdaftar
                  </Text>
                </div>
              </div>
              <div className={styles["filter__category"]}>
                <Text variant="text" muted size="sm" fontStyle="medium">
                  Jenis Kelamin
                </Text>
                <div className={styles["filter__sub"]}>
                  <Checkbox />{" "}
                  <Text variant="text" size="sm" fontStyle="regular">
                    Laki - Laki
                  </Text>
                </div>
                <div className={styles["filter__sub"]}>
                  <Checkbox />{" "}
                  <Text variant="text" size="sm" fontStyle="regular">
                    Perempuan
                  </Text>
                </div>
              </div>
              <div className={styles["filter__category"]}>
                <Text variant="text" muted size="sm" fontStyle="medium">
                  Dusun
                </Text>
                <div className={styles["filter__sub"]}>
                  <Checkbox />{" "}
                  <Text variant="text" size="sm" fontStyle="regular">
                    Majenang
                  </Text>
                </div>
                <div className={styles["filter__sub"]}>
                  <Checkbox />{" "}
                  <Text variant="text" size="sm" fontStyle="regular">
                    Banyumas
                  </Text>
                </div>
                <div className={styles["filter__sub"]}>
                  <Checkbox />{" "}
                  <Text variant="text" size="sm" fontStyle="regular">
                    Gombong
                  </Text>
                </div>
                <div className={styles["filter__sub"]}>
                  <Checkbox />{" "}
                  <Text variant="text" size="sm" fontStyle="regular">
                    Slawi
                  </Text>
                </div>
              </div>
              <div className={styles["filter__category"]}>
                <Text variant="text" muted size="sm" fontStyle="medium">
                  RW
                </Text>
                <div className={styles["filter__sub"]}>
                  <Checkbox />{" "}
                  <Text variant="text" size="sm" fontStyle="regular">
                    01
                  </Text>
                </div>
                <div className={styles["filter__sub"]}>
                  <Checkbox />{" "}
                  <Text variant="text" size="sm" fontStyle="regular">
                    02
                  </Text>
                </div>
                <div className={styles["filter__sub"]}>
                  <Checkbox />{" "}
                  <Text variant="text" size="sm" fontStyle="regular">
                    03
                  </Text>
                </div>
                <div className={styles["filter__sub"]}>
                  <Checkbox />{" "}
                  <Text variant="text" size="sm" fontStyle="regular">
                    04
                  </Text>
                </div>
                <div className={styles["filter__sub"]}>
                  <Checkbox />{" "}
                  <Text variant="text" size="sm" fontStyle="regular">
                    05
                  </Text>
                </div>
                <div className={styles["filter__sub"]}>
                  <Checkbox />{" "}
                  <Text variant="text" size="sm" fontStyle="regular">
                    06
                  </Text>
                </div>
              </div>
            </section>
          </DropdownMenuContent>
        </DropdownMenu>
      </div>

      {/* end -- search and filter */}

      {/* start -- table */}
      <div>
        <Table>
          <TableHeader>
            {table.getHeaderGroups().map((headerGroup) => (
              <TableRow key={headerGroup.id}>
                {headerGroup.headers.map((header) => {
                  return (
                    <TableHead key={header.id}>
                      {header.isPlaceholder
                        ? null
                        : flexRender(
                            header.column.columnDef.header,
                            header.getContext(),
                          )}
                    </TableHead>
                  );
                })}
              </TableRow>
            ))}
          </TableHeader>

          <TableBody>
            {table.getRowModel().rows?.length ? (
              table.getRowModel().rows.map((row) => (
                <TableRow key={row.id}>
                  {row.getVisibleCells().map((cell) => (
                    <TableCell key={cell.id}>
                      {cell.column.id !== "select" ? (
                        <Link
                          href={`/dashboard/household/detail/${row.getValue("nik")}`}
                        >
                          {flexRender(
                            cell.column.columnDef.cell,
                            cell.getContext(),
                          )}
                        </Link>
                      ) : (
                        flexRender(
                          cell.column.columnDef.cell,
                          cell.getContext(),
                        )
                      )}
                    </TableCell>
                  ))}
                </TableRow>
              ))
            ) : (
              <TableRow>
                <TableCell colSpan={columns.length} className="">
                  No results.
                </TableCell>
              </TableRow>
            )}
          </TableBody>
        </Table>
      </div>

      {/* end -- table */}

      {/* start -- pagination */}
      <div className={styles["datatable__footer"]}>
        <div className={styles["pagination__page"]}>
          <Text variant="text" size="sm" fontStyle="regular">
            Tampilkan
          </Text>
          <Select
            name="paginationPage"
            id="paginationPage"
            value={table.getState().pagination.pageSize}
            onChange={(e) => {
              table.setPageSize(Number(e.target.value));
            }}
          >
            {[10, 20, 30, 40, 50].map((pageSize) => (
              <SelectItem key={pageSize} value={pageSize}>
                {pageSize}
              </SelectItem>
            ))}
          </Select>
          <Text variant="text" size="sm" fontStyle="regular">
            data
          </Text>
        </div>
        <div className={styles["datatable__pagination"]}>
          <button
            className={styles["btn__navigation"]}
            onClick={() => table.previousPage()}
            disabled={!table.getCanPreviousPage()}
          >
            <IconArrowLeft />
          </button>
          <div className={styles["pagination__number"]}>
            <Text variant="text" size="sm" fontStyle="regular">
              Halaman
            </Text>
            <Input
              className={styles["pagination__input"]}
              value={table.getState().pagination.pageIndex + 1}
              onChange={(e) => table.setPageIndex(Number(e.target.value) - 1)}
            />
            <Text variant="text" size="sm" fontStyle="regular">
              dari {table.getPageCount()}
            </Text>
          </div>
          <button
            className={styles["btn__navigation"]}
            onClick={() => table.nextPage()}
            disabled={!table.getCanNextPage()}
          >
            <IconArrowRight />
          </button>
        </div>
      </div>
      {/* end -- pagination */}
    </div>
  );
}
