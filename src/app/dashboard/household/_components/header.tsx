"use client";

import Header from "@/app/ui/components/header/header";
import styles from "./header.module.scss";
import React from "react";
import Link from "next/link";
import Button from "@/app/ui/components/button/button";
import IconAddCircle from "@/app/ui/icons/icon-add-circle";
import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuTrigger,
} from "@/app/ui/components/dropdown-menu/dropdown-menu";
import IconMoreCircle from "@/app/ui/icons/icon-more-circle";
import IconPrinter from "@/app/ui/icons/icon-printer";
import { Text } from "@/app/ui/components/typography/typography";
import {
  Dialog,
  DialogClose,
  DialogContent,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/app/ui/components/dialog/dialog";
import IconDownload from "@/app/ui/icons/icon-download";
import { Label } from "@/app/ui/components/label/label";
import { Select, SelectItem } from "@/app/ui/components/select/select";
import IconImport from "@/app/ui/icons/icon-import";
import IconTrash from "@/app/ui/icons/icon-trash";
import IconExport from "@/app/ui/icons/icon-export";
import {
  Form,
  FormControl,
  FormDescription,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/app/ui/components/form/form";
import { SubmitHandler, UseFormReturn, useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { Input } from "@/app/ui/components/input/v2/input";
import { Checkbox } from "@/app/ui/components/checkbox/v2/checkbox";
import { toast } from "@/app/ui/components/toast/toast";

export const ImportDialog = () => {
  return (
    <>
      <DialogHeader>
        <DialogTitle>Import Data Penduduk</DialogTitle>
      </DialogHeader>

      <div className={styles["dialog__upload__content"]}>
        <div className={styles["dialog__upload__icon"]}>
          <IconExport />
        </div>
        <Text variant="text" size="md" fontStyle="semibold">
          Drag & drop file atau <span>cari file</span>
        </Text>
        <Text variant="text" size="sm" fontStyle="regular" muted>
          XLSX (maksimal 5MB)
        </Text>
      </div>
    </>
  );
};

export const DownloadDialog = () => {
  return (
    <>
      <div className={styles["dialog__file_type__content"]}>
        <Text variant="text" size="md" fontStyle="semibold">
          Tipe file
        </Text>
        <div>
          <Select value="xlsx">
            <SelectItem value="xlsx">xlsx</SelectItem>
            <SelectItem value="csv">csv</SelectItem>
          </Select>
        </div>
      </div>
    </>
  );
};

export const addHouseholdSchema = yup.object({
  kepala_rumah_tangga: yup.string().required("Kepala rumah tangga wajib diisi"),
  bdt: yup.string().required("BDT wajib diisi"),
  terdaftar_dtks: yup.boolean().default(false).required(),
});

export type FormReturn = UseFormReturn<AddHousehold, any, AddHousehold>;
export type AddHousehold = yup.InferType<typeof addHouseholdSchema>;

export const AddHouseholdConfirmDialog = ({ form }: { form: FormReturn }) => {
  return (
    <DialogContent className={styles["confirm__dialog__content"]}>
      <div className={styles["confirm__dialog__text"]}>
        <Text variant="text" size="lg" fontStyle="semibold">
          Anda yakin ingin menyimpan data ini?
        </Text>
        <Text variant="text" size="sm" fontStyle="regular" muted>
          Setelah anda menyimpan data ini, anda dapat kembali mengedit data ini
          kedalam detail dan edit data.
        </Text>
      </div>

      <DialogFooter className={styles["confirm__footer"]}>
        <Button width="max" text="Cek Ulang" variant="outline" />
        <Button
          width="max"
          text="Simpan"
          type="submit"
          onHandleClick={() => console.log(form.getValues())}
        />
      </DialogFooter>
    </DialogContent>
  );
};

export const AddHouseholdDialog = function ({
  setDialogType,
  form,
}: {
  form: FormReturn;
  setDialogType: React.Dispatch<React.SetStateAction<"form" | "confirm">>;
}) {
  const onSubmit: SubmitHandler<AddHousehold> = (data) => {
    setDialogType("confirm");
    console.log(data);
  };
  return (
    <DialogContent>
      <DialogHeader>
        <DialogTitle>Tambah Data Rumah Tangga Per Penduduk</DialogTitle>
      </DialogHeader>
      <Form {...form}>
        <form
          className={styles["form__add__household"]}
          onSubmit={form.handleSubmit(onSubmit)}
        >
          <FormField
            control={form.control}
            name="kepala_rumah_tangga"
            render={({ field }) => (
              <FormItem>
                <FormLabel>Kepala Rumah Tangga</FormLabel>
                <FormControl>
                  <Select value={field.value} onChange={field.onChange}>
                    <SelectItem value="NIK :5201140301916995 - AHMAD HABIB - ANAK">
                      NIK :5201140301916995 - AHMAD HABIB - ANAK
                    </SelectItem>
                    <SelectItem value="NIK :52011407069966997 - AHMAD ALLIF RIZKI - ANAK">
                      NIK :52011407069966997 - AHMAD ALLIF RIZKI - ANAK
                    </SelectItem>
                  </Select>
                </FormControl>
                <FormMessage />
                <FormDescription>
                  Silahkan cari nama / NIK dari data penduduk yang sudah
                  terinput. Penduduk yang dipilih otomatis berstatus sebagai
                  Kepala Rumah Tangga baru tersebut.
                </FormDescription>
              </FormItem>
            )}
          />
          <FormField
            control={form.control}
            name="bdt"
            render={({ field }) => (
              <FormItem>
                <FormLabel>BDT</FormLabel>
                <FormControl>
                  <Input {...field} placeholder="BDT" />
                </FormControl>
                <FormMessage />
              </FormItem>
            )}
          />
          <FormField
            control={form.control}
            name="terdaftar_dtks"
            render={({ field }) => (
              <FormItem>
                <FormControl className={styles["form__checkbox"]}>
                  <Checkbox
                    value={"terdaftar"}
                    checked={field.value}
                    onChange={field.onChange}
                  />
                  <Text variant="text" size="sm" fontStyle="regular" muted>
                    Terdaftar di DTKS
                  </Text>
                </FormControl>
              </FormItem>
            )}
          />

          <div className={styles["form__footer"]}>
            <Button width="max" text="Cek Ulang" variant="outline" />
            <Button width="max" text="Simpan" type="submit" />
          </div>
        </form>
      </Form>
    </DialogContent>
  );
};

const HeaderSection = () => {
  const [dialogType, setDialogType] = React.useState<"import" | "download">(
    "import",
  );
  const [addHouseholdDialogType, setAddHouseholdDialogType] = React.useState<
    "form" | "confirm"
  >("form");
  const form = useForm({
    resolver: yupResolver(addHouseholdSchema),
    defaultValues: {
      kepala_rumah_tangga: "",
      bdt: "",
      terdaftar_dtks: false,
    },
  });
  return (
    <div className={styles["content__header"]}>
      <Header
        items={[
          {
            id: "dashboard",
            name: "Beranda",
            link: "/dashboard",
          },
          {
            id: "family",
            name: "Data Rumah Tangga",
            link: "/dashboard/household",
          },
        ]}
        size="xl"
        title="Pengelompokan Rumah Tangga"
        subTitle=""
      />
      <div className={styles["content__header--button"]}>
        <Dialog onClose={() => setAddHouseholdDialogType("form")}>
          <DialogTrigger>
            <Button
              variant="outline"
              text="Tambah Rumah Tangga"
              color="primary"
              size="md"
              prefix={
                <IconAddCircle color="#4163E7" backgroundColor="#4163E7" />
              }
            />
          </DialogTrigger>
          {addHouseholdDialogType === "form" ? (
            <AddHouseholdDialog
              form={form}
              setDialogType={setAddHouseholdDialogType}
            />
          ) : (
            <AddHouseholdConfirmDialog form={form} />
          )}
        </Dialog>
        <DropdownMenu>
          <DropdownMenuTrigger className={styles["header__dropdown__trigger"]}>
            <IconMoreCircle
              color="#4163E7"
              width={25}
              height={25}
              backgroundColor="#4163E7"
            />
          </DropdownMenuTrigger>
          <DropdownMenuContent className={styles["header__dropdown__content"]}>
            <Link
              href="/dashboard/household/prints"
              className={styles["header__dropdown__item"]}
            >
              <IconPrinter color="#243576" backgroundColor="#272E38" />
              <Text variant="text" size="sm" fontStyle="medium">
                Cetak massal
              </Text>
            </Link>
            <Dialog onClose={() => setDialogType("import")}>
              <DialogTrigger>
                <div className={styles["header__dropdown__item"]}>
                  <IconImport
                    color="#243576"
                    backgroundColor="#272E38"
                    width={20}
                    height={20}
                  />
                  <Text variant="text" size="sm" fontStyle="medium">
                    Import Data Rumah Tangga
                  </Text>
                </div>
              </DialogTrigger>
              <DialogContent>
                {dialogType === "import" && <ImportDialog />}
                {dialogType === "download" && <DownloadDialog />}
                <DialogFooter className={styles["download__dialog__footer"]}>
                  {dialogType === "import" && (
                    <DialogClose style={{ width: "100%" }}>
                      <Button
                        text="Batal"
                        variant="outline"
                        color="primary"
                        width="max"
                      />
                    </DialogClose>
                  )}
                  {dialogType === "download" && (
                    <Button
                      text="Download Template Data"
                      variant="outline"
                      color="primary"
                      width="max"
                    />
                  )}
                  <Button
                    onHandleClick={() => {
                      if (dialogType === "import") {
                        setDialogType("download");
                      }
                    }}
                    variant="solid"
                    text={dialogType === "import" ? "Import" : "Download"}
                    width="max"
                  />
                </DialogFooter>
              </DialogContent>
            </Dialog>
            <Dialog>
              <DialogTrigger>
                <div className={styles["header__dropdown__item"]}>
                  <IconDownload
                    color="#243576"
                    backgroundColor="#272E38"
                    width={20}
                    height={20}
                  />
                  <Text variant="text" size="sm" fontStyle="medium">
                    Download
                  </Text>
                </div>
              </DialogTrigger>
              <DialogContent>
                <div className={styles["download__dialog__content"]}>
                  <Label>Tipe file</Label>
                  <Select defaultValue="xlsx">
                    <SelectItem>xlsx</SelectItem>
                    <SelectItem>csv</SelectItem>
                  </Select>
                </div>
                <DialogFooter className={styles["download__dialog__footer"]}>
                  <DialogClose style={{ width: "100%" }}>
                    <Button text="Batal" variant="outline" width="max" />
                  </DialogClose>
                  <Button
                    text="Download"
                    variant="solid"
                    color="primary"
                    width="max"
                  />
                </DialogFooter>
              </DialogContent>
            </Dialog>
            <Dialog>
              <DialogTrigger>
                <div className={styles["header__dropdown__item"]}>
                  <IconTrash
                    color="#243576"
                    backgroundColor="#243576"
                    width={20}
                    height={20}
                  />
                  <Text variant="text" size="sm" fontStyle="medium">
                    Hapus
                  </Text>
                </div>
              </DialogTrigger>
              <DialogContent>
                <div className={styles["delete__dialog__content"]}>
                  <Text variant="text" size="lg" fontStyle="semibold">
                    Anda yakin ingin menghapus data rumah tangga ini?
                  </Text>
                  <Text muted variant="text" size="sm" fontStyle="regular">
                    Data rumah tangga yang dihapus akan terhapus secara
                    permanen.
                  </Text>
                </div>
                <DialogFooter className={styles["delete__dialog__footer"]}>
                  <DialogClose style={{ width: "100%" }}>
                    <Button text="Batal" variant="outline" width="max" />
                  </DialogClose>
                  <DialogClose style={{ width: "100%" }}>
                    <Button
                      text="Hapus"
                      variant="solid"
                      color="error"
                      width="max"
                      onHandleClick={() =>
                        toast.success("Data Rumah tangga berhasil dihapus")
                      }
                    />
                  </DialogClose>
                </DialogFooter>
              </DialogContent>
            </Dialog>
          </DropdownMenuContent>
        </DropdownMenu>
      </div>
    </div>
  );
};

export default HeaderSection;
