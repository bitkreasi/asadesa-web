import React from "react";
import styles from "./household.module.scss";
import { Metadata } from "next";
import LayoutDashboard from "@/app/ui/layouts/dashboard/layout-dashboard";
import { DataTable } from "./_components/household-table/datatable";
import { Household, columns } from "./_components/household-table/columns";
import EmptyDataTable from "./_components/empty";
import HeaderSection from "./_components/header";
import { houseHoldData } from "./household-data";

export const metadata: Metadata = {
  title: "Rumah Tangga",
  description: "Rumah tanggal",
};

export default function HouseholdPage() {
  const data: Household[] = houseHoldData;
  const isDataEmpty = data.length === 0;
  return (
    <LayoutDashboard>
      <section>
        <div className={styles["container"]}>
          <div className={`${styles["main__content"]}`}>
            <div>
              {isDataEmpty ? (
                <EmptyDataTable />
              ) : (
                <>
                  <HeaderSection />
                  <DataTable columns={columns} data={data} />
                </>
              )}
            </div>
          </div>
        </div>
      </section>
    </LayoutDashboard>
  );
}
