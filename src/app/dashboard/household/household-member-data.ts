export interface HouseholdMember {
  nama_lengkap: string;
  nik: string;
  dusun: string;
  rt: string;
  rw: string;
  jenis_kelamin: string;
  hubungan: string;
}

export const householdMember: HouseholdMember[] = [
  {
    nama_lengkap: "Dahri",
    nik: "5201143112797117",
    dusun: "LOCO",
    rt: "03",
    rw: "08",
    jenis_kelamin: "LAKI-LAKI",
    hubungan: "Kepala Keluarga",
  },
  {
    nama_lengkap: "Hilmiait",
    nik: "5201140110137011",
    dusun: "LOCO",
    rt: "03",
    rw: "08",
    jenis_kelamin: "PEREMPUAN",
    hubungan: "Anggota",
  },
  {
    nama_lengkap: "M. Fa’iz Azami",
    nik: "5201143112897123",
    dusun: "LOCO",
    rt: "03",
    rw: "08",
    jenis_kelamin: "LAKI-LAKI",
    hubungan: "Anggota",
  },
  {
    nama_lengkap: "HJ. Paridah",
    nik: "5201144912146994",
    dusun: "LOCO",
    rt: "03",
    rw: "08",
    jenis_kelamin: "PEREMPUAN",
    hubungan: "Anggota",
  },
];
