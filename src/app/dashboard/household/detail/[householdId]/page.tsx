import React from "react";
import styles from "./household-detail.module.scss";
import { Metadata } from "next";
import LayoutDashboard from "@/app/ui/layouts/dashboard/layout-dashboard";
import HeaderSection from "./_components/header";
import ProfileSection from "@/app/dashboard/components/profile-section";
import HouseholdSection from "./_components/household";
import { householdMember } from "../../household-member-data";
import HouseholdMember from "./_components/household-member";

export const metadata: Metadata = {
  title: "Pengelompokan Rumah Tangga",
  description: "Pengelompokan Rumah Tangga",
};

export default function HouseholdDetailPage({
  params,
}: {
  params: { householdId: string };
}) {
  return (
    <LayoutDashboard>
      <section>
        <div className={styles["container"]}>
          <div className={`${styles["main__content"]}`}>
            <HeaderSection householdId={params.householdId} />
            <ProfileSection
              data={{ imageUrl: "https://source.unsplash.com/random" }}
            />
            <HouseholdSection
              data={{
                nama_lengkap: "Dahri",
                nomor_nik: "5201143112797117",
                bdt: "2176564657854753",
                jenis_kelamin: "LAKI-LAKI",
                agama: "Kristen",
                program_bantuan: "-",
              }}
            />
            <HouseholdMember data={householdMember} />
          </div>
        </div>
      </section>
    </LayoutDashboard>
  );
}
