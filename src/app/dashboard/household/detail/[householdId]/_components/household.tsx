import React from "react";
import styles from "./household.module.scss";
import {
  Card,
  CardContent,
  CardHeader,
  CardTitle,
} from "@/app/ui/components/card/card";
import { Text } from "@/app/ui/components/typography/typography";

interface HouseholdSectionProps {
  data: {
    nama_lengkap: string;
    nomor_nik: string;
    bdt: string;
    jenis_kelamin: string;
    agama: string;
    program_bantuan: string;
  };
}

const HouseholdSection = (props: HouseholdSectionProps) => {
  return (
    <Card>
      <CardHeader>
        <CardTitle>Detail Rumah Tangga</CardTitle>
      </CardHeader>
      <CardContent>
        <div className={styles["household__info"]}>
          <div className={styles["two__column"]}>
            <div className={styles["household__info--item"]}>
              <Text variant="text" size="sm" fontStyle="medium" muted>
                Nama Lengkap
              </Text>
              <Text variant="text" size="sm" fontStyle="regular">
                {props.data.nama_lengkap}
              </Text>
            </div>
            <div className={styles["household__info--item"]}>
              <Text variant="text" size="sm" fontStyle="medium" muted>
                Nomor NIK
              </Text>
              <Text variant="text" size="sm" fontStyle="regular">
                {props.data.nomor_nik}
              </Text>
            </div>
          </div>
          <div className={styles["four__column"]}>
            <div className={styles["household__info--item"]}>
              <Text variant="text" size="sm" fontStyle="medium" muted>
                BDT
              </Text>
              <Text variant="text" size="sm" fontStyle="regular">
                {props.data.bdt}
              </Text>
            </div>
            <div className={styles["household__info--item"]}>
              <Text variant="text" size="sm" fontStyle="medium" muted>
                Jenis Kelamin
              </Text>
              <Text variant="text" size="sm" fontStyle="regular">
                {props.data.jenis_kelamin}
              </Text>
            </div>
            <div className={styles["household__info--item"]}>
              <Text variant="text" size="sm" fontStyle="medium" muted>
                Agama
              </Text>
              <Text variant="text" size="sm" fontStyle="regular">
                {props.data.agama}
              </Text>
            </div>
            <div className={styles["household__info--item"]}>
              <Text variant="text" size="sm" fontStyle="medium" muted>
                Program Bantuan
              </Text>
              <Text variant="text" size="sm" fontStyle="regular">
                {props.data.program_bantuan}
              </Text>
            </div>
          </div>
        </div>
      </CardContent>
    </Card>
  );
};

export default HouseholdSection;
