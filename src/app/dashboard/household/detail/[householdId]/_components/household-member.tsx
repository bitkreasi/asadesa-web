import React from "react";
import styles from "./household-member.module.scss";
import { type HouseholdMember } from "../../../household-member-data";
import { Text } from "@/app/ui/components/typography/typography";
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeader,
  TableRow,
} from "@/app/ui/components/table/table";
import IconEdit2 from "@/app/ui/icons/icon-edit-2";
import IconTrash from "@/app/ui/icons/icon-trash";

interface HouseholdMemberProps {
  data: HouseholdMember[];
}
const HouseholdMember = (props: HouseholdMemberProps) => {
  return (
    <section>
      <Text variant="display" size="xs" fontStyle="medium">
        Daftar Anggota
      </Text>
      <div>
        <Table>
          <TableHeader>
            <TableRow>
              {/* <TableHead> */}
              {/*   <Checkbox /> */}
              {/* </TableHead> */}
              <TableHead>No</TableHead>
              <TableHead>Nama</TableHead>
              <TableHead>NIK</TableHead>
              <TableHead>Dusun</TableHead>
              <TableHead>RT</TableHead>
              <TableHead>RW</TableHead>
              <TableHead>Jenis Kelamin</TableHead>
              <TableHead>Hubungan</TableHead>
              <TableHead>Aksi</TableHead>
            </TableRow>
          </TableHeader>

          <TableBody>
            {props.data.map((member, idx) => (
              <TableRow key={idx}>
                {/* <TableCell> */}
                {/*   <Checkbox /> */}
                {/* </TableCell> */}
                <TableCell>{idx + 1}</TableCell>
                <TableCell>{member.nama_lengkap}</TableCell>
                <TableCell>{member.nik}</TableCell>
                <TableCell>{member.dusun}</TableCell>
                <TableCell>{member.rw}</TableCell>
                <TableCell>{member.rt}</TableCell>
                <TableCell>{member.jenis_kelamin}</TableCell>
                <TableCell>{member.hubungan}</TableCell>
                <TableCell>
                  <div className={styles["member__table__action"]}>
                    <IconEdit2 color="#F49C56" backgroundColor="#F49C56" />
                    <IconTrash color="#F49C56" backgroundColor="#F49C56" />
                    <Text
                      variant="text"
                      size="sm"
                      fontStyle="medium"
                      className={styles["text__detail"]}
                    >
                      Detail
                    </Text>
                  </div>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </div>
    </section>
  );
};

export default HouseholdMember;
