import React from "react";
import styles from "./header.module.scss";
import Header from "@/app/ui/components/header/header";
import Link from "next/link";
import Button from "@/app/ui/components/button/button";
import IconEdit from "@/app/ui/icons/icon-edit";

const HeaderSection = ({ householdId }: { householdId: string }) => {
  return (
    <div className={styles["content__header"]}>
      <Header
        items={[
          {
            id: "dashboard",
            name: "Beranda",
            link: "/dashboard",
          },
          {
            id: "household",
            name: "Data Rumah Tangga",
            link: "/dashboard/household",
          },
          {
            id: "detail-household",
            name: "Detail Rumah Tangga",
            link: `/dashboard/household/detail/${householdId}`,
          },
        ]}
        isBack={true}
        title="Pengelompokan Rumah Tangga"
        subTitle=""
      />
      <div className={styles["content__header--button"]}>
        <Link href={`/dashboard/household/${householdId}/edit`}>
          <Button
            variant="outline"
            text="Edit"
            color="primary"
            size="md"
            prefix={<IconEdit color="#4163E7" backgroundColor="#4163E7" />}
          />
        </Link>
      </div>
    </div>
  );
};

export default HeaderSection;
