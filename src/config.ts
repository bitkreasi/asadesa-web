import { createEnv } from "@t3-oss/env-nextjs";
import { z } from "zod";

const APP_ENV = ["production", "staging", "demo", "development"] as const;

/**
 * Serverside Environment variables, not available on the client.
 * Will throw if you access these variables on the client.
 */
const server = {
  DATABASE_URL: z.string().url(),
  APP_ENV: z.enum(APP_ENV),
};

/**
 * Environment variables available on the client (and server).
 * You'll get type errors if these are not prefixed with NEXT_PUBLIC_.
 */
const client = {
  NEXT_PUBLIC_SITE_URL: z.string().url().default("http://localhost:3000"),
};

const union = { ...server, ...client };

// @ref: https://env.t3.gg/docs/nextjs
export const env: {
  [key in keyof typeof union]: z.infer<(typeof union)[key]>;
} = createEnv({
  server,
  client,
  /**
   * Due to how Next.js bundles environment variables on Edge and Client,
   * we need to manually destructure them to make sure all are included in bundle.
   *
   * You'll get type errors if not all variables from `server` & `client` are included here.
   */
  runtimeEnv: {
    NEXT_PUBLIC_SITE_URL: process.env.NEXT_PUBLIC_SITE_URL,
    DATABASE_URL: process.env.DATABASE_URL,
    DATABASE_NAME: process.env.DATABASE_NAME,
    APP_ENV:
      APP_ENV.find((env) => env === process.env.APP_ENV) ?? "development",
  },
});

export const siteMeta = {
  title: "Global Capacity Building Coalition",
  description:
    "Increasing availability, accessibility, and effectiveness of capacity building, technical assistance, and learning resources for climate and finance to scale the green transition.",
  baseUrl: env.NEXT_PUBLIC_SITE_URL,
};
