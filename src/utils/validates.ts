export function validatePassword(
  value: string,
  callback: (...arg: any) => void
) {
  const errors: any = {}

  if (!/(?=.*\d)/.test(value)) {
    errors.number = false
  } else {
    errors.number = true
  }

  if (!/(?=.*[a-z])(?=.*[A-Z])/.test(value)) {
    errors.case = false
  } else {
    errors.case = true
  }

  if (!/(?=.*[@#$%^&+=!])/.test(value)) {
    errors.symbol = false
  } else {
    errors.symbol = true
  }

  if (value.length < 8) {
    errors.length = false
  } else {
    errors.length = true
  }

  callback(errors)
}
