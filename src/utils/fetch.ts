export interface APIResponse<TData> {
  error?: string;
  data?: TData;
}

type FetchOption = {
  method?: "GET" | "POST" | "PATCH" | "DELETE";
  body?: unknown;
  headers?: Record<string, string>;
  cache?: RequestCache;
};

/**
 * Fetches data from a public endpoint.
 * Make sure to only call this function inside Server Component
 *
 * @template T The expected return type from the API endpoint.
 * @param {string} endpoint - The API endpoint to fetch data from.
 * @returns {Promise<APIResponse<T>>} The data returned from the API endpoint.
 * @throws Will throw an error if the fetch request fails.
 */
export async function publicFetch<T>(
  endpoint: string,
  option?: FetchOption,
): Promise<APIResponse<T>> {
  const url = `${process.env.API_URL}/${process.env.API_VERSION}${endpoint}`;
  const method = option?.method ?? "GET";
  const defaultHeaders = {
    "Content-Type": "application/json",
  };
  const headers = { ...defaultHeaders, ...option?.headers };
  const res = await fetch(url, {
    method: method,
    headers: headers,
    cache: option?.cache,
    body: option?.body ? JSON.stringify(option?.body) : undefined,
  });
  const jsonRes = (await res.json()) as APIResponse<T>;
  return jsonRes;
}
