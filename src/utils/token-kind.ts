import { z } from 'zod'

export const TokenKind = z.enum([
  'access_token',
  'verify_email_token',
  'reset_password_token',
  'otp',
])
export type TokenKindEnum = z.infer<typeof TokenKind>
