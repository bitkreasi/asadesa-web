import {
  OperandValueExpressionOrList,
  RawBuilder,
  SelectExpression,
  SelectQueryBuilder,
  sql,
  TableExpression,
} from "kysely";
import { Pagination, Search } from "./read-many";

export function search<DB, TB extends keyof DB, O>(
  query: SelectQueryBuilder<DB, TB, O>,
  opts: Search
): SelectQueryBuilder<DB, TB, O> {
  if (!!opts.search_by && !!opts.search_query) {
    return query.where(
      toSearchExpr(opts.search_by),
      "ilike",
      toPatternExpr(opts.search_query)
    );
  }
  return query;
}

export function searchIn<DB, TB extends keyof DB, O>(
  query: SelectQueryBuilder<DB, TB, O>,
  values: string[],
  column: string
): SelectQueryBuilder<DB, TB, O> {
  if (values.includes("")) return query;
  return query.where(
    toSearchExpr(column),
    "in",
    values.map((v) => v.toLowerCase()) as OperandValueExpressionOrList<
      DB,
      TB,
      RawBuilder<string>
    >
  );
}

export function searchMultiple<DB, TB extends keyof DB, O>(
  query: SelectQueryBuilder<DB, TB, O>,
  value: string,
  columns: string[]
): SelectQueryBuilder<DB, TB, O> {
  return query.where((eb) =>
    eb.or(
      columns.map((col) => eb(toSearchExpr(col), "ilike", toPatternExpr(value)))
    )
  );
}

export function searchFilter<DB, TB extends keyof DB, O>(
  query: SelectQueryBuilder<DB, TB, O>,
  value: string,
  columns: string[],
  opts: Search
): SelectQueryBuilder<DB, TB, O> {
  if (opts.search_by) {
    return search(query, opts);
  }

  return searchMultiple(query, value, columns);
}

export function paginate<DB, TB extends keyof DB, O>(
  query: SelectQueryBuilder<DB, TB, O>,
  opts: Pagination
): SelectQueryBuilder<DB, TB, O> {
  return query.offset((opts.page - 1) * opts.size).limit(opts.size);
}

export async function getPaginationInfo<DB, TB extends keyof DB, O>(
  baseQuery: SelectQueryBuilder<DB, TB, O>,
  opts: Pagination,
  current_size: number,
  options?: {
    distinctOn?: SelectExpression<DB, TB>;
  }
) {
  const { rows } = (await baseQuery
    .select(
      sql
        .raw<number>(
          `count(${
            options?.distinctOn ? "DISTINCT " + options.distinctOn : "*"
          })`
        )
        .as("rows")
    )
    .executeTakeFirst()) as { rows: number };

  return {
    current_size: current_size,
    current_page: opts.page,
    total_size: Number(rows) ?? 0,
    total_pages: Math.ceil((rows ?? 0) / opts.size),
  };
}

export function toSearchExpr(searchBy: string): RawBuilder<string> {
  const searchExpr = `LOWER(COALESCE(${searchBy}::text, ''))`;
  return sql<string>`${sql.raw(searchExpr)}`;
}

export function toPatternExpr(searchQuery: string): string {
  return `%${searchQuery}%`.toLowerCase();
}

export function seederUpsert(data: { [key: string]: any }[]) {
  return () => {
    return data.reduce((init, data) => {
      Object.keys(data).forEach((k) => (init[k] = sql.raw(`excluded.${k}`)));
      return init;
    }, {} as { [k: string]: RawBuilder<unknown> });
  };
}
