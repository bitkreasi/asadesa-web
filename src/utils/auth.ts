import { cache } from "react";
import { cookies } from "next/headers";
import { NodePostgresAdapter } from "@lucia-auth/adapter-postgresql";
import { Lucia, TimeSpan } from "lucia";

import db, { pool } from "@/database/client";
import { Session } from "@/database/tables/auth.table";
import { User } from "@/database/tables/users.table";

import { AllowedRoleEnum } from "./enums/role";
import { throwResponse } from "./response";

const adapter = new NodePostgresAdapter(pool, {
  session: "sessions",
  user: "users",
});

export const lucia = new Lucia(adapter, {
  sessionExpiresIn: new TimeSpan(1, "d"),
  getUserAttributes: (attributes) => {
    return attributes;
  },
  getSessionAttributes: (attributes) => {
    return attributes;
  },
});

export const validateRequest = cache(
  async (): Promise<
    { user: User; session: Session } | { user: null; session: null }
  > => {
    const sessionId = cookies().get(lucia.sessionCookieName)?.value ?? null;
    if (!sessionId) {
      return {
        user: null,
        session: null,
      };
    }

    const result = await lucia.validateSession(sessionId);
    if (result.session && result.session.fresh) {
      const sessionCookie = lucia.createSessionCookie(result.session.id);
      cookies().set(
        sessionCookie.name,
        sessionCookie.value,
        sessionCookie.attributes
      );
    }
    if (!result.session) {
      const sessionCookie = lucia.createBlankSessionCookie();
      cookies().set(
        sessionCookie.name,
        sessionCookie.value,
        sessionCookie.attributes
      );
    }

    return result as
      | { user: User; session: Session }
      | { user: null; session: null };
  }
);

// TODO: Perform validation check for admin in here
export async function validateAuth(params?: {
  allowedRoles: AllowedRoleEnum;
  forOtp?: boolean;
}) {
  const allowedRoles = params?.allowedRoles ?? "all";
  const forOTP = params?.forOtp ?? false;

  const { session, user } = await validateRequest();

  if (!session) {
    throw throwResponse(401, "Unauthorized");
  }
  let userOrganization = null;

  return {
    ...user,
    metadata: {
      ...(user?.metadata || {}),
    },
    organization: userOrganization,
    token: session.id,
  };
}

export async function publicAuth() {
  await validateRequest();
}

export async function isAuthenticated() {
  try {
    const sessionId = cookies().get(lucia.sessionCookieName)?.value ?? null;
    if (!sessionId) {
      return false;
    }
    return true;
  } catch (error) {
    return false;
  }
}

// IMPORTANT!
declare module "lucia" {
  interface Register {
    Lucia: typeof lucia;
  }
}

export type Auth = typeof lucia;
