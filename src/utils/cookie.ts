import { setCookie, getCookie, hasCookie, deleteCookie } from 'cookies-next'

const COOKIE_KEYS = {
  AUTH_TOKEN: 'auth_token',
  REFRESH_TOKEN: 'refresh_token'
}

type CookieKey = keyof typeof COOKIE_KEYS

export const cookieStorage = {
  set: (key: CookieKey, data: string, option?: any) => {
    return setCookie(key, data, { ...option })
  },
  get: (key: CookieKey, option?: any) => {
    return getCookie(key, { ...option })
  },
  delete: (key: CookieKey, option?: any) => {
    return deleteCookie(key, { ...option })
  },
  has: (key: CookieKey) => {
    return hasCookie(key)
  },
}
