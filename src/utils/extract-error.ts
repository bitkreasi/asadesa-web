import * as yup from 'yup'

export function extractErrors(error: unknown, callback: (...arg: any) => void) {
  const errorMessages: any = {}
  error instanceof yup.ValidationError &&
    error?.inner?.forEach((error: any) => {
      errorMessages[error.path] = error.message
    })
  callback(errorMessages)
}
