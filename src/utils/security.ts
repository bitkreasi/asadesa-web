import { pbkdf2Sync, randomBytes } from 'crypto'
import { bcrypt, bcryptVerify } from 'hash-wasm'
import { getRandomValues } from 'uncrypto'

export async function hashPassword(password: string): Promise<string> {
  const salt = new Uint8Array(16)

  getRandomValues(salt)

  return await bcrypt({
    password,
    salt, // salt is a buffer containing 16 random bytes
    costFactor: 12,
    outputType: 'encoded', // return standard encoded string
  })
}

export async function verifyPassword(password: string, hash: string): Promise<boolean> {
  return await bcryptVerify({ password, hash })
}

// Computes the SHA-256 digest of a string with Web Crypto
// Source: https://developer.mozilla.org/en-US/docs/Web/API/SubtleCrypto/digest
export function sha256(str: string): string {
  const salt = randomBytes(16).toString('hex')

  // Hash the salt and password with 1000 iterations, 32 length and sha256 digest
  const hash = pbkdf2Sync(str, salt, 1000, 32, 'sha256').toString('hex')
  return hash
}

function hex(buffer: ArrayBuffer) {
  let digest = ''
  const view = new DataView(buffer, 0)
  for (let i = 0; i < view.byteLength; i += 4) {
    // We use getUint32 to reduce the number of iterations (notice the `i += 4`)
    const value = view.getUint32(i)
    // toString(16) will transform the integer into the corresponding hex string
    // but will remove any initial "0"
    const stringValue = value.toString(16)
    // One Uint32 element is 4 bytes or 8 hex chars (it would also work with 4
    // chars for Uint16 and 2 chars for Uint8)
    const padding = '00000000'
    const paddedValue = (padding + stringValue).slice(-padding.length)
    digest += paddedValue
  }
  return digest
}
