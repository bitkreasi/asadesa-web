import { GenericResponse, throwResponse } from './response'

export function handleApiError(error: Error): GenericResponse<undefined> {
  if (error instanceof Response) {
    return error
  }
  console.error(error)
  return throwResponse(500, error?.message, (error as any)?.cause || error?.name)
}
