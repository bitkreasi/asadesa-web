import { z } from "zod";

export const SearchSchema = z.object({
  search_query: z.string().optional(),
  search_by: z.string().optional(),
});
export type Search = z.infer<typeof SearchSchema>;

export const SortSchema = z.object({
  sort_by: z.string().optional(),
  sort_direction: z.enum(["asc", "desc"]).optional(),
});

export type Order = z.infer<typeof SortSchema>;

export const PaginationSchema = z.object({
  page: z.number().min(1).default(1),
  size: z.number().min(1).default(10),
});
export type Pagination = z.infer<typeof PaginationSchema>;

export const PaginationInfoSchema = z.object({
  current_size: z.number().min(1),
  current_page: z.number().min(1),
  total_size: z.number().min(1),
  total_pages: z.number().min(1),
});
export type PaginationInfo = z.infer<typeof PaginationInfoSchema>;
export const ReadManySchema = z.union([
  SearchSchema,
  SortSchema,
  PaginationSchema,
]);
export type ReadMany = z.infer<typeof ReadManySchema>;
