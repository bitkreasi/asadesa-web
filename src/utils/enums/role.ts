import { z } from "zod";

export const Role = z.enum(["super_admin", "admin", "owner"]);
export type RoleEnum = z.infer<typeof Role>;

export const AllowedRole = z.union([z.array(Role), z.literal("all")]);
export type AllowedRoleEnum = z.infer<typeof AllowedRole>;
