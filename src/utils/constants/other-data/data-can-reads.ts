export const dataCanReads = [
  { name: 'Latin' },
  { name: 'Daerah' },
  { name: 'Arab' },
  { name: 'Arab dan Latin' },
  { name: 'Arab dan Daerah' },
  { name: 'Arab, Latin, dan Daerah' },
];
