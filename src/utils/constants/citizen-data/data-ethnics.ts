export const dataEthnics = [
  { name: 'Aceh', description: 'Aceh' },
  { name: 'Alas', description: 'Aceh' },
  { name: 'Alor', description: 'NTT' },
  { name: 'Ambon', description: 'Ambon' },
  { name: 'Ampana', description: 'Sulawesi Tengah' },
  { name: 'Anak Dalam', description: 'Jambi' },
  { name: 'Aneuk Jamee', description: 'Aceh' },
  { name: 'Arab: Orang Hadhrami', description: 'Arab: Orang Hadhrami' },
  { name: 'Aru', description: 'Maluku' },
  { name: 'Asmat', description: 'Papua' },
  {
    name: 'Bare’e',
    description: 'Bare’e di Kabupaten Tojo Una-Una Tojo dan Tojo Barat',
  },
  { name: 'Banten', description: 'Banten di Banten' },
  { name: 'Besemah', description: 'Besemah di Sumatera Selatan' },
  {
    name: 'Bali',
    description:
      'Bali di Bali terdiri dari: Suku Bali Majapahit di sebagian besar Pulau Bali; Suku Bali Aga di Karangasem dan Kintamani',
  },
  { name: 'Balantak', description: 'Balantak di Sulawesi Tengah' },
  {
    name: 'Banggai',
    description: 'Banggai di Sulawesi Tengah (Kabupaten Banggai Kepulauan)',
  },
  { name: 'Baduy', description: 'Baduy di Banten' },
  { name: 'Bajau', description: 'Bajau di Kalimantan Timur' },
  { name: 'Banjar', description: 'Banjar di Kalimantan Selatan' },
  { name: 'Batak', description: 'Sumatera Utara' },
  { name: 'Batak Karo', description: 'Sumatera Utara' },
  { name: 'Mandailing', description: 'Sumatera Utara' },
  { name: 'Angkola', description: 'Sumatera Utara' },
  { name: 'Toba', description: 'Sumatera Utara' },
  { name: 'Pakpak', description: 'Sumatera Utara' },
  { name: 'Simalungun', description: 'Sumatera Utara' },
  { name: 'Batin', description: 'Batin di Jambi' },
  { name: 'Bawean', description: 'Bawean di Jawa Timur (Gresik)' },
  { name: 'Bentong', description: 'Bentong di Sulawesi Selatan' },
  {
    name: 'Berau',
    description: 'Berau di Kalimantan Timur (kabupaten Berau)',
  },
  { name: 'Betawi', description: 'Betawi di Jakarta' },
  { name: 'Bima', description: 'Bima NTB (kota Bima)' },
  {
    name: 'Boti',
    description: 'Boti di kabupaten Timor Tengah Selatan',
  },
  {
    name: 'Bolang Mongondow',
    description:
      'Bolang Mongondow di Sulawesi Utara (Kabupaten Bolaang Mongondow)',
  },
  {
    name: 'Bugis',
    description:
      'Bugis di Sulawesi Selatan: Orang Bugis Pagatan di Kalimantan Selatan, Kusan Hilir, Tanah Bumbu',
  },
  {
    name: 'Bungku',
    description: 'Bungku di Sulawesi Tengah (Kabupaten Morowali)',
  },
  { name: 'Buru', description: 'Buru di Maluku (Kabupaten Buru)' },
  {
    name: 'Buol',
    description: 'Buol di Sulawesi Tengah (Kabupaten Buol)',
  },
  {
    name: 'Bulungan ',
    description: 'Bulungan di Kalimantan Timur (Kabupaten Bulungan)',
  },
  {
    name: 'Buton',
    description:
      'Buton di Sulawesi Tenggara (Kabupaten Buton dan Kota Bau-Bau)',
  },
  {
    name: 'Bonai',
    description: 'Bonai di Riau (Kabupaten Rokan Hilir)',
  },
  { name: 'Cham ', description: 'Cham di Aceh' },
  {
    name: 'Cirebon ',
    description: 'Cirebon di Jawa Barat (Kota Cirebon)',
  },
  { name: 'Damal', description: 'Damal di Mimika' },
  { name: 'Dampeles', description: 'Dampeles di Sulawesi Tengah' },
  { name: 'Dani ', description: 'Dani di Papua (Lembah Baliem)' },
  { name: 'Dairi', description: 'Dairi di Sumatera Utara' },
  { name: 'Daya ', description: 'Daya di Sumatera Selatan' },
  {
    name: 'Dayak',
    description:
      'Dayak terdiri dari: Suku Dayak Ahe di Kalimantan Barat; Suku Dayak Bajare di Kalimantan Barat; Suku Dayak Damea di Kalimantan Barat; Suku Dayak Banyadu di Kalimantan Barat; Suku Bakati di Kalimantan Barat; Suku Punan di Kalimantan Tengah; Suku Kanayatn di Kalimantan Barat; Suku Dayak Krio di Kalimantan Barat (Ketapang); Suku Dayak Sungai Laur di Kalimantan Barat (Ketapang); Suku Dayak Simpangh di Kalimantan Barat (Ketapang); Suku Iban di Kalimantan Barat; Suku Mualang di Kalimantan Barat (Sekada',
  },
  { name: 'Dompu', description: 'Dompu NTB (Kabupaten Dompu)' },
  { name: 'Donggo', description: 'Donggo, Bima' },
  { name: 'Dongga', description: 'Donggala di Sulawesi Tengah' },
  {
    name: 'Dondo ',
    description: 'Dondo di Sulawesi Tengah (Kabupaten Toli-Toli)',
  },
  {
    name: 'Duri',
    description:
      'Duri Terletak di bagian utara Kabupaten Enrekang berbatasan dengan Kabupaten Tana Toraja, meliputi tiga kecamatan induk Anggeraja, Baraka, dan Alla di Sulawesi Selatan',
  },
  {
    name: 'Eropa ',
    description:
      'Eropa (orang Indo, peranakan Eropa-Indonesia, atau etnik Mestizo)',
  },
  { name: 'Flores', description: 'Flores di NTT (Flores Timur)' },
  {
    name: 'Lamaholot',
    description:
      'Lamaholot, Flores Timur, terdiri dari: Suku Wandan, di Solor Timur, Flores Timur; Suku Kaliha, di Solor Timur, Flores Timur; Suku Serang Gorang, di Solor Timur, Flores Timur; Suku Lamarobak, di Solor Timur, Flores Timur; Suku Atanuhan, di Solor Timur, Flores Timur; Suku Wotan, di Solor Timur, Flores Timur; Suku Kapitan Belen, di Solor Timur, Flores Timur',
  },
  {
    name: 'Gayo',
    description:
      'Gayo di Aceh (Gayo Lues Aceh Tengah Bener Meriah Aceh Tenggara Aceh Timur Aceh Tamiang)',
  },
  {
    name: 'Gorontalo',
    description: 'Gorontalo di Gorontalo (Kota Gorontalo)',
  },
  { name: 'Gumai ', description: 'Gumai di Sumatera Selatan (Lahat)' },
  {
    name: 'India',
    description:
      'India, terdiri dari: Suku Tamil di Aceh, Sumatera Utara, Sumatera Barat, dan DKI Jakarta; Suku Punjab di Sumatera Utara, DKI Jakarta, dan Jawa Timur; Suku Bengali di DKI Jakarta; Suku Gujarati di DKI Jakarta dan Jawa Tengah; Orang Sindhi di DKI Jakarta dan Jawa Timur; Orang Sikh di Sumatera Utara, DKI Jakarta, dan Jawa Timur',
  },
  {
    name: 'Jawa',
    description: 'Jawa di Jawa Tengah, Jawa Timur, DI Yogyakarta',
  },
  {
    name: 'Tengger',
    description: 'Tengger di Jawa Timur (Probolinggo, Pasuruan, dan Malang)',
  },
  { name: 'Osing ', description: 'Osing di Jawa Timur (Banyuwangi)' },
  { name: 'Samin ', description: 'Samin di Jawa Tengah (Purwodadi)' },
  {
    name: 'Bawean',
    description: 'Bawean di Jawa Timur (Pulau Bawean)',
  },
  { name: 'Jambi ', description: 'Jambi di Jambi (Kota Jambi)' },
  {
    name: 'Jepang',
    description: 'Jepang di DKI Jakarta, Jawa Timur, dan Bali',
  },
  {
    name: 'Kei',
    description:
      'Kei di Maluku Tenggara (Kabupaten Maluku Tenggara dan Kota Tual)',
  },
  {
    name: 'Kaili ',
    description: 'Kaili di Sulawesi Tengah (Kota Palu)',
  },
  { name: 'Kampar', description: 'Kampar' },
  { name: 'Kaur ', description: 'Kaur di Bengkulu (Kabupaten Kaur)' },
  { name: 'Kayu Agung', description: 'Kayu Agung di Sumatera Selatan' },
  {
    name: 'Kerinci',
    description: 'Kerinci di Jambi (Kabupaten Kerinci)',
  },
  {
    name: 'Komering ',
    description:
      'Komering di Sumatera Selatan (Kabupaten Ogan Komering Ilir, Baturaja)',
  },
  {
    name: 'Konjo Pegunungan',
    description: 'Konjo Pegunungan, Kabupaten Gowa, Sulawesi Selatan',
  },
  {
    name: 'Konjo Pesisir',
    description: 'Konjo Pesisir, Kabupaten Bulukumba, Sulawesi Selatan',
  },
  { name: 'Koto', description: 'Koto di Sumatera Barat' },
  { name: 'Kubu', description: 'Kubu di Jambi dan Sumatera Selatan' },
  { name: 'Kulawi', description: 'Kulawi di Sulawesi Tengah' },
  {
    name: 'Kutai ',
    description: 'Kutai di Kalimantan Timur (Kutai Kartanegara)',
  },
  { name: 'Kluet ', description: 'Kluet di Aceh (Aceh Selatan)' },
  { name: 'Korea ', description: 'Korea di DKI Jakarta' },
  { name: 'Krui', description: 'Krui di Lampung' },
  { name: 'Laut,', description: 'Laut, Kepulauan Riau' },
  {
    name: 'Lampung',
    description:
      'Lampung, terdiri dari: Suku Sungkai di Lampung; Suku Abung di Lampung; Suku Way Kanan di Lampung, Sumatera Selatan dan Bengkulu; Suku Pubian di Lampung; Suku Tulang Bawang di Lampung; Suku Melinting di Lampung; Suku Peminggir Teluk di Lampung; Suku Ranau di Lampung, Sumatera Selatan dan Sumatera Utara; Suku Komering di Sumatera Selatan; Suku Cikoneng di Banten; Suku Merpas di Bengkulu; Suku Belalau di Lampung; Suku Smoung di Lampung; Suku Semaka di Lampung',
  },
  { name: 'Lematang ', description: 'Lematang di Sumatera Selatan' },
  {
    name: 'Lembak',
    description: 'Lembak, Kabupaten Rejang Lebong, Bengkulu',
  },
  { name: 'Lintang', description: 'Lintang, Sumatera Selatan' },
  { name: 'Lom', description: 'Lom, Bangka Belitung' },
  { name: 'Lore', description: 'Lore, Sulawesi Tengah' },
  {
    name: 'Lubu',
    description:
      'Lubu, daerah perbatasan antara Provinsi Sumatera Utara dan Provinsi Sumatera Barat',
  },
  { name: 'Moronene', description: 'Moronene di Sulawesi Tenggara.' },
  {
    name: 'Madura',
    description:
      'Madura di Jawa Timur (Pulau Madura, Kangean, wilayah Tapal Kuda)',
  },
  {
    name: 'Makassar',
    description:
      'Makassar di Sulawesi Selatan: Kabupaten Gowa, Kabupaten Takalar, Kabupaten Jeneponto, Kabupaten Bantaeng, Kabupaten Bulukumba (sebagian), Kabupaten Sinjai (bagian perbatasan Kab Gowa), Kabupaten Maros (sebagian), Kabupaten Pangkep (sebagian), Kota Makassar',
  },
  {
    name: 'Mamasa',
    description: 'Mamasa (Toraja Barat) di Sulawesi Barat: Kabupaten Mamasa',
  },
  {
    name: 'Manda',
    description: 'Mandar Sulawesi Barat: Polewali Mandar',
  },
  {
    name: 'Melayu',
    description:
      'Melayu, terdiri dari Suku Melayu Tamiang di Aceh (Aceh Tamiang); Suku Melayu Riau di Riau dan Kepulauan Riau; Suku Melayu Deli di Sumatera Utara; Suku Melayu Jambi di Jambi; Suku Melayu Bangka di Pulau Bangka; Suku Melayu Belitung di Pulau Belitung; Suku Melayu Sambas di Kalimantan Barat',
  },
  {
    name: 'Mentawai',
    description: 'Mentawai di Sumatera Barat (Kabupaten Kepulauan Mentawai)',
  },
  {
    name: 'Minahasa',
    description:
      'Minahasa di Sulawesi Utara (Kabupaten Minahasa), terdiri 9 subetnik : Suku Babontehu; Suku Bantik; Suku Pasan Ratahan',
  },
  {
    name: 'Ponosakan',
    description:
      'Ponosakan; Suku Tonsea; Suku Tontemboan; Suku Toulour; Suku Tonsawang; Suku Tombulu',
  },
  { name: 'Minangkabau', description: 'Minangkabau, Sumatera Barat' },
  { name: 'Mongondow', description: 'Mongondow, Sulawesi Utara' },
  {
    name: 'Mori',
    description: 'Mori, Kabupaten Morowali, Sulawesi Tengah',
  },
  {
    name: 'Muko-Muko',
    description: 'Muko-Muko di Bengkulu (Kabupaten Mukomuko)',
  },
  {
    name: 'Muna',
    description: 'Muna di Sulawesi Tenggara (Kabupaten Muna)',
  },
  {
    name: 'Muyu',
    description: 'Muyu di Kabupaten Boven Digoel, Papua',
  },
  {
    name: 'Mekongga',
    description:
      'Mekongga di Sulawesi Tenggara (Kabupaten Kolaka dan Kabupaten Kolaka Utara)',
  },
  {
    name: 'Moro',
    description: 'Moro di Kalimantan Barat dan Kalimantan Utara',
  },
  {
    name: 'Nias',
    description:
      'Nias di Sumatera Utara (Kabupaten Nias, Nias Selatan dan Nias Utara dari dua keturunan Jepang dan Vietnam)',
  },
  { name: 'Ngada ', description: 'Ngada di NTT: Kabupaten Ngada' },
  { name: 'Osing', description: 'Osing di Banyuwangi Jawa Timur' },
  { name: 'Ogan', description: 'Ogan di Sumatera Selatan' },
  { name: 'Ocu', description: 'Ocu di Kabupaten Kampar, Riau' },
  {
    name: 'Padoe',
    description: 'Padoe di Sulawesi Tengah dan Sulawesi Selatan',
  },
  {
    name: 'Papua',
    description:
      'Papua / Irian, terdiri dari: Suku Asmat di Kabupaten Asmat; Suku Biak di Kabupaten Biak Numfor; Suku Dani, Lembah Baliem, Papua; Suku Ekagi, daerah Paniai, Abepura, Papua; Suku Amungme di Mimika; Suku Bauzi, Mamberamo hilir, Papua utara; Suku Arfak di Manokwari; Suku Kamoro di Mimika',
  },
  {
    name: 'Palembang',
    description: 'Palembang di Sumatera Selatan (Kota Palembang)',
  },
  {
    name: 'Pamona',
    description:
      'Pamona di Sulawesi Tengah (Kabupaten Poso) dan di Sulawesi Selatan',
  },
  {
    name: 'Pesisi',
    description: 'Pesisi di Sumatera Utara (Tapanuli Tengah)',
  },
  {
    name: 'Pasir',
    description: 'Pasir di Kalimantan Timur (Kabupaten Pasir)',
  },
  { name: 'Pubian', description: 'Pubian di Lampung' },
  { name: 'Pattae', description: 'Pattae di Polewali Mandar' },
  {
    name: 'Pakistani',
    description: 'Pakistani di Sumatera Utara, DKI Jakarta, dan Jawa Tengah',
  },
  {
    name: 'Peranakan',
    description: 'Peranakan (Tionghoa-Peranakan atau Baba Nyonya)',
  },
  { name: 'Rawa', description: 'Rawa, Rokan Hilir, Riau' },
  {
    name: 'Rejang',
    description:
      'Rejang di Bengkulu (Kabupaten Bengkulu Tengah, Kabupaten Bengkulu Utara, Kabupaten Kepahiang, Kabupaten Lebong, dan Kabupaten Rejang Lebong)',
  },
  { name: 'Rote', description: 'Rote di NTT (Kabupaten Rote Ndao)' },
  {
    name: 'Rongga',
    description: 'Rongga di NTT Kabupaten Manggarai Timur',
  },
  { name: 'Rohingya', description: 'Rohingya' },
  { name: 'Sabu', description: 'Sabu di Pulau Sabu, NTT' },
  { name: 'Saluan', description: 'Saluan di Sulawesi Tengah' },
  {
    name: 'Sambas',
    description: 'Sambas (Melayu Sambas) di Kalimantan Barat: Kabupaten Sambas',
  },
  {
    name: 'Samin',
    description: 'Samin di Jawa Tengah (Blora) dan Jawa Timur (Bojonegoro)',
  },
  {
    name: 'Sangi',
    description: 'Sangir di Sulawesi Utara (Kepulauan Sangihe)',
  },
  { name: 'Sasak', description: 'Sasak di NTB, Lombok' },
  { name: 'Sekak Bangka', description: 'Sekak Bangka' },
  { name: 'Sekayu', description: 'Sekayu di Sumatera Selatan' },
  {
    name: 'Semendo ',
    description: 'Semendo di Bengkulu, Sumatera Selatan (Muara Enim)',
  },
  {
    name: 'Serawai ',
    description:
      'Serawai di Bengkulu (Kabupaten Bengkulu Selatan dan Kabupaten Seluma)',
  },
  {
    name: 'Simeulue',
    description: 'Simeulue di Aceh (Kabupaten Simeulue)',
  },
  {
    name: 'Sigulai ',
    description: 'Sigulai di Aceh (Kabupaten Simeulue bagian utara',
  },
  { name: 'Suluk', description: 'Suluk di Kalimantan Utara)' },
  {
    name: 'Sumbawa ',
    description: 'Sumbawa Di NTB (Kabupaten Sumbawa)',
  },
  {
    name: 'Sumba',
    description: 'Sumba di NTT (Sumba Barat, Sumba Timur)',
  },
  {
    name: 'Sunda',
    description:
      'Sunda di Jawa Barat, Banten, DKI Jakarta, Lampung, Sumatra Selatan dan Jawa Tengah',
  },
  {
    name: 'Sungkai ',
    description: 'Sungkai di Lampung Lampung Utara',
  },
  {
    name: 'Talau',
    description: 'Talaud di Sulawesi Utara (Kepulauan Talaud)',
  },
  {
    name: 'Talang Mamak',
    description: 'Talang Mamak di Riau (Indragiri Hulu)',
  },
  {
    name: 'Tamiang ',
    description: 'Tamiang di Aceh (Kabupaten Aceh Tamiang)',
  },
  {
    name: 'Tengger ',
    description:
      'Tengger di Jawa Timur (Kabupaten Pasuruan) dan Probolinggo (lereng G. Bromo)',
  },
  {
    name: 'Ternate ',
    description: 'Ternate di Maluku Utara (Kota Ternate)',
  },
  {
    name: 'Tidore',
    description: 'Tidore di Maluku Utara (Kota Tidore)',
  },
  {
    name: 'Tidung',
    description: 'Tidung di Kalimantan Timur (Kabupaten Tanah Tidung)',
  },
  { name: 'Timor', description: 'Timor di NTT, Kota Kupang' },
  {
    name: 'Tionghoa',
    description:
      'Tionghoa, terdiri dari: Orang Cina Parit di Pelaihari, Tanah Laut, Kalsel; Orang Cina Benteng di Tangerang, Provinsi Banten; Orang Tionghoa Hokkien di Jawa dan Sumatera Utara; Orang Tionghoa Hakka di Belitung dan Kalimantan Barat; Orang Tionghoa Hubei; Orang Tionghoa Hainan; Orang Tionghoa Kanton; Orang Tionghoa Hokchia; Orang Tionghoa Tiochiu',
  },
  {
    name: 'Tojo',
    description: 'Tojo di Sulawesi Tengah (Kabupaten Tojo Una-Una)',
  },
  {
    name: 'Toraja',
    description: 'Toraja di Sulawesi Selatan (Tana Toraja)',
  },
  {
    name: 'Tolaki',
    description: 'Tolaki di Sulawesi Tenggara (Kendari)',
  },
  {
    name: 'Toli Toli',
    description: 'Toli Toli di Sulawesi Tengah (Kabupaten Toli-Toli)',
  },
  {
    name: 'Tomini',
    description: 'Tomini di Sulawesi Tengah (Kabupaten Parigi Mouton',
  },
  {
    name: 'Una-una ',
    description: 'Una-una di Sulawesi Tengah (Kabupaten Tojo Una-Una)',
  },
  {
    name: 'Ulu',
    description: 'Ulu di Sumatera Utara (Mandailing natal)',
  },
  { name: 'Wolio', description: 'Wolio di Sulawesi Tenggara (Buton)' },
];
