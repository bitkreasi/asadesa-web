export const dataInsurances = [
  { name: 'Tidak/Belum Punya' },
  { name: 'BPJS Penerima Bantuan Iuran' },
  { name: 'BPJS Non Penerima Bantuan Iuran' },
  { name: 'Asuransi Lainnya' },
];
