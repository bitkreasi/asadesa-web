export const dataDisabilities = [
  { name: 'Cacat Fisik' },
  { name: 'Cacat Netra/Buta' },
  { name: 'Cacat Rungu' },
  { name: 'Cacat Mental/Jiwa' },
  { name: 'Cacat Fisik dan Mental' },
  { name: 'Cacat Lainnya' },
  { name: 'Tidak Cacat' },
];
