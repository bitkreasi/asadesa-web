export const dataKBAcceptors = [
  { name: 'Pil' },
  { name: 'IUD' },
  { name: 'Suntik' },
  { name: 'Kondom' },
  { name: 'Susuk KB' },
  { name: 'Sterilisasi Wanita' },
  { name: 'Sterilisasi Pria' },
  { name: 'Lainnya' },
  { name: 'Tidak Menggunakan' },
];
