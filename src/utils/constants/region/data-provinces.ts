export const dataProvinces = [
  {
    CODE: 11,
    NAME: 'ACEH',
  },
  {
    CODE: 12,
    NAME: 'SUMATERA UTARA',
  },
  {
    CODE: 13,
    NAME: 'SUMATERA BARAT',
  },
  {
    CODE: 14,
    NAME: 'RIAU',
  },
  {
    CODE: 15,
    NAME: 'JAMBI',
  },
  {
    CODE: 16,
    NAME: 'SUMATERA SELATAN',
  },
  {
    CODE: 17,
    NAME: 'BENGKULU',
  },
  {
    CODE: 18,
    NAME: 'LAMPUNG',
  },
  {
    CODE: 19,
    NAME: 'KEPULAUAN BANGKA BELITUNG',
  },
  {
    CODE: 21,
    NAME: 'KEPULAUAN RIAU',
  },
  {
    CODE: 31,
    NAME: 'DKI JAKARTA',
  },
  {
    CODE: 32,
    NAME: 'JAWA BARAT',
  },
  {
    CODE: 33,
    NAME: 'JAWA TENGAH',
  },
  {
    CODE: 34,
    NAME: 'DI YOGYAKARTA',
  },
  {
    CODE: 35,
    NAME: 'JAWA TIMUR',
  },
  {
    CODE: 36,
    NAME: 'BANTEN',
  },
  {
    CODE: 51,
    NAME: 'BALI',
  },
  {
    CODE: 52,
    NAME: 'NUSA TENGGARA BARAT',
  },
  {
    CODE: 53,
    NAME: 'NUSA TENGGARA TIMUR',
  },
  {
    CODE: 61,
    NAME: 'KALIMANTAN BARAT',
  },
  {
    CODE: 62,
    NAME: 'KALIMANTAN TENGAH',
  },
  {
    CODE: 63,
    NAME: 'KALIMANTAN SELATAN',
  },
  {
    CODE: 64,
    NAME: 'KALIMANTAN TIMUR',
  },
  {
    CODE: 65,
    NAME: 'KALIMANTAN UTARA',
  },
  {
    CODE: 71,
    NAME: 'SULAWESI UTARA',
  },
  {
    CODE: 72,
    NAME: 'SULAWESI TENGAH',
  },
  {
    CODE: 73,
    NAME: 'SULAWESI SELATAN',
  },
  {
    CODE: 74,
    NAME: 'SULAWESI TENGGARA',
  },
  {
    CODE: 75,
    NAME: 'GORONTALO',
  },
  {
    CODE: 76,
    NAME: 'SULAWESI BARAT',
  },
  {
    CODE: 81,
    NAME: 'MALUKU',
  },
  {
    CODE: 82,
    NAME: 'MALUKU UTARA',
  },
  {
    CODE: 91,
    NAME: 'PAPUA BARAT',
  },
  {
    CODE: 94,
    NAME: 'PAPUA',
  },
];
