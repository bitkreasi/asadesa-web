export const dataFamilyRoles = [
  { name: 'Kepala Keluarga' },
  { name: 'Suami' },
  { name: 'Istri' },
  { name: 'Anak' },
  { name: 'Menantu' },
  { name: 'Cucu' },
  { name: 'Orang Tua' },
  { name: 'Mertua' },
  { name: 'Famili Lain' },
  { name: 'Pembantu' },
  { name: 'Lainnya' },
];
