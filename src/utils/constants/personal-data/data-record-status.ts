export const dataRecordStatus = [
  { name: 'Belum Rekam' },
  { name: 'Sudah Rekam' },
  { name: 'Card Printed' },
  { name: 'Print Ready Record' },
  { name: 'Card Shipped ' },
  { name: 'Sent For Card Printing' },
  { name: 'Card Issued' },
  { name: 'Belum Wajib' },
];
