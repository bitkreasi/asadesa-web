export const sufixTitle = ' - Asadesa: Aplikasi Sistem Informasi Desa';
export const commonDescription = 'Aplikasi Sistem Informasi Desa yang membantu desa dalam manajemen pemerintahan desa. Kelola data penduduk, data kependudukan, data keuangan, data inventaris, data administrasi, dan data lainnya dengan mudah dalam satu aplikasi terpadu.';
