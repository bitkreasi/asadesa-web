export const dataEducationKK = [
  { name: 'Tidak atau Belum Sekolah' },
  { name: 'Belum Tamat SD atau Sederajat' },
  { name: 'Tamat SD atau Sederajat' },
  { name: 'SLTP atau Sederajat' },
  { name: 'SLTA atau Sederajat' },
  { name: 'Diploma I / II' },
  { name: 'Akademi / Diploma III / S. Muda' },
  { name: 'Diploma IV / Strata I' },
  { name: 'Strata II' },
  { name: 'Strata III' },
];
