import { AuthService } from "./auth";
import { VillageService } from "./village";

export const BASE_URL = process.env.NEXT_PUBLIC_SITE_URL;
export const VERSION = "/api";

export const authService = new AuthService(BASE_URL + VERSION ?? "");
export const villageService = new VillageService(BASE_URL + VERSION ?? "");
