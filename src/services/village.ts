import axios, { AxiosError, AxiosInstance } from 'axios'

export interface ErrorResponse {
  errors?: any[]
  message?: string
}

export class VillageService {
  protected readonly instance: AxiosInstance
  public constructor(url: string) {
    this.instance = axios.create({
      baseURL: url,
      timeout: 30000,
      timeoutErrorMessage: 'Time out!',
    })
  }

  getInstitutions = async (id: string) => {
    try {
      const res = await this.instance.get('/institutions/' + id)
      return res
    } catch (error) {
      if (axios.isAxiosError(error)) {
        const axiosError = error as AxiosError
        if (axiosError.response) {
          return { errors: axiosError.response }
        } else if (axiosError.request) {
          return {
            data: { message: 'No response received from the server' },
            status: 500,
            statusText: 'Internal Server Error',
          }
        } else {
          return {
            data: { message: 'Error' },
            status: 500,
            statusText: 'Internal Server Error',
          }
        }
      } else {
        throw error
      }
    }
  }
  updateInstitutions = async (id: string, payload: any) => {
    try {
      const res = await this.instance.patch('/institutions/' + id, payload)
      return res
    } catch (error) {
      if (axios.isAxiosError(error)) {
        const axiosError = error as AxiosError
        if (axiosError.response) {
          return { errors: axiosError.response }
        } else if (axiosError.request) {
          return {
            data: { message: 'No response received from the server' },
            status: 500,
            statusText: 'Internal Server Error',
          }
        } else {
          return {
            data: { message: 'Error' },
            status: 500,
            statusText: 'Internal Server Error',
          }
        }
      } else {
        throw error
      }
    }
  }
}
