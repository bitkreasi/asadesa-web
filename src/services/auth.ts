import axios, {
  AxiosError,
  AxiosInstance,
  AxiosResponse,
  isAxiosError,
} from "axios";

export interface LoginResponse {
  token: string;
}
export interface RegisterResponse {
  message: string;
}

export interface ErrorResponse {
  errors?: any[];
  message?: string;
}

export type RegisterPayload = {
  full_name: string;
  email: string;
  phone_number: string;
  role_id: string;
  province_id: string;
  district_id: string;
  regency_id: string;
  village_id: string;
  password: string;
  password_confirmation: string;
};

export class AuthService {
  protected readonly instance: AxiosInstance;
  public constructor(url: string) {
    this.instance = axios.create({
      baseURL: url,
      timeout: 30000,
      timeoutErrorMessage: "Time out!",
    });
  }

  login = async (email: string, password: string) => {
    try {
      const res = await this.instance.post("/auth/signin", {
        email,
        password,
      });
      return res;
    } catch (error) {
      if (axios.isAxiosError(error)) {
        const axiosError = error as AxiosError;
        if (axiosError.response) {
          return { errors: axiosError.response };
        } else if (axiosError.request) {
          return {
            data: { message: "No response received from the server" },
            status: 500,
            statusText: "Internal Server Error",
          };
        } else {
          return {
            data: { message: "Error" },
            status: 500,
            statusText: "Internal Server Error",
          };
        }
      } else {
        throw error;
      }
    }
  };
  register = async (payload: RegisterPayload) => {
    try {
      const res = await this.instance.post("/auth/register", {
        ...payload,
      });
      return res;
    } catch (error) {
      if (axios.isAxiosError(error)) {
        const axiosError = error as AxiosError;
        if (axiosError.response) {
          return { errors: axiosError.response };
        } else if (axiosError.request) {
          return {
            data: { message: "No response received from the server" },
            status: 500,
            statusText: "Internal Server Error",
          };
        } else {
          return {
            data: { message: "Error" },
            status: 500,
            statusText: "Internal Server Error",
          };
        }
      } else {
        throw error;
      }
    }
  };
}
